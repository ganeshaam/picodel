package app.apneareamein.shopping.interfaces;

import app.apneareamein.shopping.BuildConfig;

public interface ApplicationUrlAndConstants {

    String versionName = BuildConfig.VERSION_NAME;

    int REQUEST_TIMEOUT_MS = 600000;
    int SPLASH_SHOW_TIME = 3000;
    int SELECT_PICTURE = 1;

    String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

    String MobileregEx = "(^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[56789]\\d{9}$)";

    // broadcast receiver intent filters
    String REGISTRATION_COMPLETE = "registrationComplete";
    String PUSH_NOTIFICATION = "pushNotification";

    String SHARED_PREF = "aamFireBase";

    //String IMAGE_URL = "https://simg.apneareamein.com/";
    String IMAGE_URL = "https://simg.picodel.com/";
    //String SHOPPING_INITIAL_URL = "https://www.apneareamein.com/And/shopping/AppAPI/";
    //String SHOPPING_INITIAL_URL1 = "https://www.apneareamein.com/And/shopping/RESTAPI/";
    //String SHOPPING_INITIAL_URL2 = "https://www.apneareamein.com/And/shopping/";
    String ADMIN_INITIAL_URL = "https://apneareamein.com/And/app_admin_panel/";

    String SHOPPING_INITIAL_URL = "https://www.picodel.com/And/shopping/AppAPI/";
    String SHOPPING_INITIAL_URL1 = "https://www.picodel.com/And/shopping/RESTAPI/";
    String SHOPPING_INITIAL_URL2 = "https://www.picodel.com/And/shopping/";

    //start----- commonly used urls
    String urlGetuserdata = SHOPPING_INITIAL_URL + "fetchUserDataWithAddress.php";
    String urlSimilarResult = SHOPPING_INITIAL_URL + "similarProductResult.php";
    String urlSimilarResult11 = SHOPPING_INITIAL_URL + "similarProductResult11.php";
    String urlReviewResult = SHOPPING_INITIAL_URL + "rating_review.php";
    //String urlReviewAll = SHOPPING_INITIAL_URL + "picodel_review.php";
    String urlReviewAll = SHOPPING_INITIAL_URL + "picodel_review11.php";

    String urlUserRedeemOffer = SHOPPING_INITIAL_URL + "userRedeemOffer.php";
    String urlUserTracking = SHOPPING_INITIAL_URL + "user_tracking.php";
    String urlAddUserNotifications = SHOPPING_INITIAL_URL + "add_user_notifications.php";
    String urlAddCartItemToWishlist = SHOPPING_INITIAL_URL + "addCarttoWishlist.php";//sync with local
    String urlGetCartCount = SHOPPING_INITIAL_URL + "GetCartCount.php";//sync with local
    String GetAndroidKey = SHOPPING_INITIAL_URL + "GetAndroidKey.php";//sync with local
    String urlStopOrder = SHOPPING_INITIAL_URL + "stopOrder.php";
    String urlOneTimeCallMethod = SHOPPING_INITIAL_URL + "oneTimeCallMethod.php";
    //end----- commonly used urls

    //start----- HomePage
    //String urlArea = SHOPPING_INITIAL_URL + "getArea.php";
    String urlCity = SHOPPING_INITIAL_URL + "getCity.php";
    String urlHomePage = SHOPPING_INITIAL_URL1 + "HomePage.php";
    //String urlHomePage11 = SHOPPING_INITIAL_URL1 + "HomePage11.php"; //Live AREA WISE
    //String urlHomePage11 = SHOPPING_INITIAL_URL1 + "HomePageLive.php"; //Live Product Visibility
    //String urlHomePage11 = "https://www.picodel.com/seller/homepage/homepageandroid"; // For Speed change Yii Live
    //String urlHomePage11 = "https://www.picodel.com/seller/homepage/homepageandroidtest"; // City wise product dynamic 2.0.46
    String urlHomePage11 = "https://www.picodel.com/seller/homepagetest/homepageandroidtest"; // City wise product dynamic
    //String urlHomePage11 = SHOPPING_INITIAL_URL1 + "HomePageLive1.php"; // Testing value <br error
    //String urlHomePage11 = SHOPPING_INITIAL_URL1 + "HomePageTest.php";
    //String urlHomePage = SHOPPING_INITIAL_URL + "HomePage_v1.7.php";
    String urlServicePage = SHOPPING_INITIAL_URL + "Services.php";
    //String urlCityConfiguration = SHOPPING_INITIAL_URL + "activeCityConfiguration.php"; // Live
    String urlCityConfiguration = SHOPPING_INITIAL_URL + "activeCityandState.php";
    String urlStateConfiguration = SHOPPING_INITIAL_URL + "activeStateConfiguration.php";
    String urlAreaConfiguration = SHOPPING_INITIAL_URL + "ActiveAreaConfiguration.php";

    String urlgetAreaList = SHOPPING_INITIAL_URL + "getAreaList.php";
    String urlUpdateUserCity = SHOPPING_INITIAL_URL + "UpdateUserCity.php";
    String urlCashBackTracking = SHOPPING_INITIAL_URL + "cashback_user_tracking.php";
    String urlGetComboOffers = SHOPPING_INITIAL_URL + "getComboOffer_Homepage.php";
    String urlUpdateStatusOfCartDialog = SHOPPING_INITIAL_URL + "updateStatusCartDialog.php";
    //end----- HomePage

    //start----- SearchProducts
    //String urlNewGetProducts12 = SHOPPING_INITIAL_URL + "searchProducts12.php"; //Live for Area wise List
    //String urlNewGetProducts12 = SHOPPING_INITIAL_URL + "searchProductsLive.php"; //for Product Visibility List
    String urlNewGetProducts12 = SHOPPING_INITIAL_URL + "searchProductsLive2.php"; //Live for city wise product table
    String urloffers_list_special = SHOPPING_INITIAL_URL + "offers_list_special.php"; // User for special offfers
    //String urlNewGetProducts11 = SHOPPING_INITIAL_URL + "searchProducts11.php"; // use in shopkeeper
    String urlGetAllFilterData = SHOPPING_INITIAL_URL + "get_all_filter.php";
    String urlGetSubCategories = SHOPPING_INITIAL_URL + "new_getSubCategories.php";
    String urlGetCategories = SHOPPING_INITIAL_URL + "getCategories.php";
    String urlCheckCouponCode = SHOPPING_INITIAL_URL + "checkCouponCode.php";
    //end----- SearchProducts

    //start----- AddToCart
    String urlAdd_UpdateCartDetails_50 = SHOPPING_INITIAL_URL + "newAddUpdateCartDetails.php";// Using for 50 % Offer List
    //String urlAdd_UpdateCartDetails = SHOPPING_INITIAL_URL + "newAddUpdateCartDetails11.php";//For Normal Cart Live is working for Both Category
    //String urlAdd_UpdateCartDetails = SHOPPING_INITIAL_URL + "newAddUpdateCartDetailsLive.php";//For Product Visibility Live
    String urlAdd_UpdateCartDetails = SHOPPING_INITIAL_URL + "newAddUpdateCartDetailsLive2.php";// Live product table city wise
    //String urlAdd_UpdateCartDetails = SHOPPING_INITIAL_URL + "newAddUpdateCartDetails.php";//

    String urlComboCartResult = SHOPPING_INITIAL_URL + "updateComboCartDetails.php";//For Combo Cart
    String urlGetCartItem = SHOPPING_INITIAL_URL + "fetchCartID.php";
    //String urlGetMyCartItems = SHOPPING_INITIAL_URL1 + "cartProducts.php";//For Normal & Combo Cart
    //String urlGetMyCartItems = SHOPPING_INITIAL_URL1 + "androidcartProductsLive.php";// Live API 2.0.44
    String urlGetMyCartItems = SHOPPING_INITIAL_URL1 + "androidcartProductsLive2.php";// City wise Product table dynamic
    String urlGetMyCartOutOfStockItems = SHOPPING_INITIAL_URL + "GetMyCartOutOfStockItems.php";//For Normal & Combo Cart
    String urlCheckQtyProductWise = SHOPPING_INITIAL_URL + "checkQtyProductWise.php"; // Live working in userApp

    //String urlDeleteCartResult = SHOPPING_INITIAL_URL + "deleteCartItems.php";// Live API in 2.0.45
    String urlDeleteCartResult = SHOPPING_INITIAL_URL + "deleteCartItemsLive.php";//For Normal & Combo Cartl
    //end----- AddToCart

    //start------ Wishlist
    String urlGetMyWishListItems = SHOPPING_INITIAL_URL1 + "wishListProducts.php";
    String urlAddMyWishListItems = SHOPPING_INITIAL_URL + "AddWishListDetails.php";
    String urlDeleteMyWishListItems = SHOPPING_INITIAL_URL + "deleteWishListItems.php";
    String urlGetWishListItem = SHOPPING_INITIAL_URL + "fetchWishlistID.php";//sync with local
    String urlAddWishListItemToCart = SHOPPING_INITIAL_URL + "addWishlisttoCart.php";//sync with local
    //end----- Wishlist

    //start----- ProductInformationFragment
    String urlGetProductReviewResult = SHOPPING_INITIAL_URL + "get_Product_Review.php";
    String urlGetSingleProductInformation = SHOPPING_INITIAL_URL + "single_product_information.php";
    //String urlGetSingleProductInformation11 = SHOPPING_INITIAL_URL + "single_product_information11.php";  //Live
    //String urlGetSingleProductInformation11 = SHOPPING_INITIAL_URL + "single_product_informationLive.php";  // product visibility
    String urlGetSingleProductInformation11 = SHOPPING_INITIAL_URL + "single_product_informationLive2.php";  // product city wise price
    //end----- ProductInformationFragment

    //start----- MasterSearch
    //String urlMasterSearch = SHOPPING_INITIAL_URL1 + "masterSearch.php"; //Live 2.0.44
    //String urlMasterSearch = SHOPPING_INITIAL_URL1 + "masterSearchLive.php"; //Area wise List
    String urlMasterSearch = SHOPPING_INITIAL_URL1 + "masterSearchLive2.php"; // product city wise table dynamic
    String urlAddAppSearchHistory = SHOPPING_INITIAL_URL1 + "addAppSearchHistory.php";
    //end----- MasterSearch

    //start----- MobileVerification
    String urlCheckUserDetails = SHOPPING_INITIAL_URL + "checkUserStatus.php";

    String notification_update = SHOPPING_INITIAL_URL + "notification_update.php";
    String urlRegistrationVerificationCode = SHOPPING_INITIAL_URL + "registrationVerificationCode.php";
    String urlRegistration = SHOPPING_INITIAL_URL + "user_registration.php";
    String urlRegistration2 = SHOPPING_INITIAL_URL + "user_registration2.php";
    String urlSavePassword = SHOPPING_INITIAL_URL + "savePassword.php";
    String urlForgetPassword = SHOPPING_INITIAL_URL + "new_checkUserDetails.php";
    String urlCheckPassword = SHOPPING_INITIAL_URL + "checkPassword.php";
    String urlReferInsert = SHOPPING_INITIAL_URL + "refer_insert.php";
    String urlrefgetPoints = SHOPPING_INITIAL_URL + "getrefer_points.php";
    String urlget_Known_From = SHOPPING_INITIAL_URL + "get_Known_From.php";
    String urlinsert_Known_From = SHOPPING_INITIAL_URL + "insert_know_from.php";

    //end----- MobileVerification

    //start----- CategoryWiseProducts
    String urlGetSubCat = SHOPPING_INITIAL_URL + "getBannerProducts.php";
    //end----- CategoryWiseProducts

    //start----- EasyOrderDetails
    String urlQuickOrder = SHOPPING_INITIAL_URL + "newQuickOrder.php";
    String urlEasyOrderDeliveryOptions = SHOPPING_INITIAL_URL + "easy_order_deliveryType.php";
    //end----- EasyOrderDetails

    //start----- OrderDetails
    //String urlGetPriceDetails = SHOPPING_INITIAL_URL + "newAddToCartCalculationLive2.php"; // Live with dynamic products 2.0.46
    String urlGetPriceDetails = SHOPPING_INITIAL_URL + "newAddToCartCalculation_v1.7.php"; // Test free delivery
    //String urlGetPriceDetails = SHOPPING_INITIAL_URL + "newAddToCartCalculation_v1.8.php"; // Live 2.0.44
    //String urlCashOnDelivery = SHOPPING_INITIAL_URL + "new_place_order_v1.7.php";
    //String urlCashOnDelivery = SHOPPING_INITIAL_URL + "new_place_order_v1.8.php";
    //String urlCashOnDelivery = SHOPPING_INITIAL_URL + "new_place_order_v1.9.php"; //Live
    //String urlCashOnDelivery = SHOPPING_INITIAL_URL + "new_place_order_v1.10.php"; // Live 2.0.44
    String urlCashOnDelivery = SHOPPING_INITIAL_URL + "new_place_order_Live2.php"; //Nearby shop test
    String urlProblematicDeleteCondition = SHOPPING_INITIAL_URL + "problematicDeleteCartItems.php";
    String urlOrderConfirmationNotification = SHOPPING_INITIAL_URL + "orderConfirmationNotification.php";
    String urlCouponsDetails = SHOPPING_INITIAL_URL + "couponDetails.php";
    String urlCouponsDetails11 = SHOPPING_INITIAL_URL + "couponDetails11.php";
    String urlNewScheduleDates = SHOPPING_INITIAL_URL + "newScheduleDatesSlots_v1.7.php";
    //String urlNewScheduleDates = SHOPPING_INITIAL_URL + "newScheduleTwoHoursSlots.php";
    //end----- OrderDetails

    //start----- ComboOffer
    String urlComboOffer = SHOPPING_INITIAL_URL + "getComboOffer.php";
    String urlAddCartComboOffer = SHOPPING_INITIAL_URL + "addComboCartDetails.php";
    //end----- ComboOffer

    //start----- FilterLayout
    String urlGetDependentBrand = SHOPPING_INITIAL_URL + "getDependentBrandData.php";
    String urlGetDependentSize = SHOPPING_INITIAL_URL + "getDependentSizeData.php";
    //end----- FilterLayout

    //start----- GetCoupons
    String urlUserRedeemCoupons = SHOPPING_INITIAL_URL + "getRedeemCouponsForUser.php";
    String urlApplyCouponCode = SHOPPING_INITIAL_URL + "applyCouponCode.php";
    //end----- GetCoupons

    //start----- MyOrder
    String urlMyOrderForUser = SHOPPING_INITIAL_URL + "userMyOrdersHome.php";
    String urlOrderRemark = SHOPPING_INITIAL_URL + "userOrderRemark.php";
    String urlCancelOrderSpinnerData = SHOPPING_INITIAL_URL + "cancelOrderSpinner.php";
    String urlCancelOrder = SHOPPING_INITIAL_URL + "userCancelOrder.php";
    //String urlOrderDetails = SHOPPING_INITIAL_URL + "DeliveryType_v1.7.php";
    String urlOrderDetails = SHOPPING_INITIAL_URL + "DeliveryType_v1.8.php";
    //end----- MyOrder

    //start----- MyOrderDetails
    String urlProductDetails = SHOPPING_INITIAL_URL + "userMyOrderDetails.php";
    String urlmyLastOrder = SHOPPING_INITIAL_URL + "userMyLastOrder.php";
    String urlUserRepeatOrder = SHOPPING_INITIAL_URL + "userRepeatOrder.php";
    //end----- MyOrderDetails

    //start----- CashBackOffer
    String urlUserCashBackCategory = SHOPPING_INITIAL_URL + "getRedeemCategory.php";
    //end----- CashBackOffer

    //start----- DataSegregationFourTabLayout
    String urlGetSubCategory = SHOPPING_INITIAL_URL + "subCategoriesName.php";
    //end----- DataSegregationFourTabLayout

    //start----- SubCatProductFragment
    //String urlGetSubCategoryProductsShopWise = SHOPPING_INITIAL_URL + "getSubCategoryProductsShopWise.php"; // OLD
    //String urlGetSubCategoryProductsShopWise11 = SHOPPING_INITIAL_URL + "getSubCategoryProductsShopWise11.php";
    //String urlGetSubCategoryProductsShopWise = SHOPPING_INITIAL_URL + "getSubCategoryProductsShopWiseLive.php";  // Live product visibility
    String urlGetSubCategoryProductsShopWise = SHOPPING_INITIAL_URL + "getSubCategoryProductsShopWiseLive2.php";  // City wise product table
    //String urlGetSubCategoryProductsShopWise11 = SHOPPING_INITIAL_URL + "getSubCategoryProductsShopWise.php";
    //end----- SubCatProductFragment

    //start----- DataSegregationLevelThree
    String urlShopSubCatResult = SHOPPING_INITIAL_URL + "DataSegregationLevelThree.php";
    String urlEssentialProducts = SHOPPING_INITIAL_URL + "essentialProducts.php";
    //end----- DataSegregationLevelThree

    //start----- NotificationsHome
    String urlGetNotification = SHOPPING_INITIAL_URL + "getNotifications.php";
    String urlDeactiveNotification = SHOPPING_INITIAL_URL + "deactiveNotification.php";
    //start----- NotificationsHome

    //start----- DiscountHome
    String urlDiscount = SHOPPING_INITIAL_URL + "getDiscountData.php";
    //end----- DiscountHome

    //start-----AddNewAddress
    String urlAddNewAddress = SHOPPING_INITIAL_URL + "addNewAddress.php";
    String urlCheckPincode = SHOPPING_INITIAL_URL + "checkPinCodeIsValid.php";

    //start----- BrowseByCategory
    //String urlBrowseByCategory = SHOPPING_INITIAL_URL + "browseByCategory.php";
    String urlBrowseByCategory =   SHOPPING_INITIAL_URL + "browseByCategory.php";
    String urlBrowseByMainCategory =   SHOPPING_INITIAL_URL + "browseByMainCategory.php";
    String urlbrowseByMainCat_PopUp =   SHOPPING_INITIAL_URL + "browseByMainCat_PopUp.php";
    String urlevent_activity =   SHOPPING_INITIAL_URL + "event_activity.php";
    String urlevent_answer =   SHOPPING_INITIAL_URL + "event_answer.php";
    String urldrawer_options =   SHOPPING_INITIAL_URL + "drawer_options.php";
    //end----- BrowseByCategory

    //start----- QuickLinks
    String about_us = SHOPPING_INITIAL_URL2 + "aboutUs.html";
    String terms = SHOPPING_INITIAL_URL2 + "termsConditions.html";
    String cash_back = SHOPPING_INITIAL_URL2 + "cashback.html";
    String help = SHOPPING_INITIAL_URL2 + "help.html";
    String blank_page = SHOPPING_INITIAL_URL2 + "blank_page.html";
    //end----- QuickLinks

    //start----- Order_Details
    String urlSetDefaultAddress = SHOPPING_INITIAL_URL + "setDefaultAddress.php";
    //end----- Order_Details

    //start----- BaseActivity
    String urlStoredDeviceInfo = SHOPPING_INITIAL_URL + "storedDeviceInfo.php";
    //end----- BaseActivity

    //start----- AddShop
    String urlCategory = SHOPPING_INITIAL_URL + "category.php";
    String urlAddShop = SHOPPING_INITIAL_URL + "add_shop_details.php";
    //end----- AddShop

    String urlTest = SHOPPING_INITIAL_URL1 + "products.php";

    //start----- EasyOrder
    String urlUploadAudioFile = SHOPPING_INITIAL_URL + "UploadAudioToServer.php";
    String urlUploadImageFile = SHOPPING_INITIAL_URL + "UploadImageToServer.php";
    //end----- EasyOrder

    //start----- PromotionalPartner
    String urlGetShopkeeperData = SHOPPING_INITIAL_URL + "getPromotionalUserData.php";
    String urlGetCashBackType = SHOPPING_INITIAL_URL1 + "cashback_type.php";
    String urlUpdateShoppingDetails = SHOPPING_INITIAL_URL + "updateShoppingDetails.php";
    String urlGetShopCategories = SHOPPING_INITIAL_URL + "getShopCategories.php";
    //end----- PromotionalPartner

    //start----- GetAuto
    String urlInsertAtServer = SHOPPING_INITIAL_URL + "insertAutoData.php";
    String urlGetAutoDataFromServer = SHOPPING_INITIAL_URL + "getAutoOrdersUserWise.php";
    //end----- GetAuto

    //start----- AutoDashboard
    String urlLoadMenus = ADMIN_INITIAL_URL + "getHomeMenus.php";
    //end----- AutoDashboard

    //start----- History
    String urlAutoHistory = ADMIN_INITIAL_URL + "autoWalaHistory.php";
    //end----- History

    //start----- PendingOrders
    String urlManageOrders = ADMIN_INITIAL_URL + "listOfOnlineOrder.php";
    //end----- PendingOrders

    //start----- CheckCartDetails
    String urlCheckCartDetails = ADMIN_INITIAL_URL + "userMyOrderDetails.php";
    String urlGetTimeSlots = SHOPPING_INITIAL_URL + "slip_delivery_time.php";
    //end----- CheckCartDetails



    //start----applayCouponCode
    String urlsendcouponCode = SHOPPING_INITIAL_URL + "sendcouponCode.php";
    String urlgetNewScheduledDatesForCoupon = SHOPPING_INITIAL_URL + "getNewScheduledDatesForCoupon.php";
    String urlverifyTRN = SHOPPING_INITIAL_URL + "verifyTRN.php";
    //end-----

    //start---- scratch card
    String user_scratch_insert = SHOPPING_INITIAL_URL + "user_scrach_insert.php";
    //String get_scratch_amount = SHOPPING_INITIAL_URL + "get_scratch_amount.php";
    String get_scratch_amount = SHOPPING_INITIAL_URL + "get_continueshopping.php";
    String urldyanamic_activity = SHOPPING_INITIAL_URL + "dyanamic_activity.php";

    //Check order place limit and dialog message for home and place order dialog
    String urlPlaceOrderLimit = SHOPPING_INITIAL_URL + "PlaceOrderLimit.php";
    //String urlPlaceOrderLimit = SHOPPING_INITIAL_URL + "PlaceOrderLimitwithCity.php"; //Live in version 2.0.41
    // get Dyanamic checkout Conditons
    String get_terms_conditions = SHOPPING_INITIAL_URL + "get_terms_conditions.php";

    //String paymentmode = "https://www.picodel.com/seller/shoptestapi/paymentmode";
    String paymentmode = "https://www.picodel.com/seller/shoptestapi/paymentmodelist";
    String urlOfferScreen = SHOPPING_INITIAL_URL+"offerscreenapi.php";
    String urlCheckRefferalCode = SHOPPING_INITIAL_URL + "CheckRefferalCode.php";
    String urlgetActivityBanners = SHOPPING_INITIAL_URL + "getActivityBanners.php";
    //String urlgetFreechargePayment =  "https://checkout-sandbox.freecharge.in/api/v1/co/pay/init";
    String urlgetFreechargePayment =  "https://checkout.freecharge.in/api/v1/co/pay/init";
    String url_getCartCount = "https://www.picodel.com/seller/homepagetest/countcartitems";
    String url_getdrawerOption = "https://www.picodel.com/seller/homepagetest/getdraweroption";
    String url_getbrowoseCategory = "https://www.picodel.com/seller/homepagetest/browsebycategory";
    String url_Setdeviceinfo = "https://www.picodel.com/seller/homepagetest/setdeviceinfo";
    // for the update cart old prices
    String urlUpdate_User_Cart_Pirces =   SHOPPING_INITIAL_URL + "Update_User_Cart_Pirces.php";

    String urlGetBannerShopCategory = SHOPPING_INITIAL_URL + "getBannerShopCategory.php";
    String urlGetBannerShopList = SHOPPING_INITIAL_URL + "getBannerShopList.php";
    String urlBannerShopPointsMinus = SHOPPING_INITIAL_URL + "bannerShopPointsMinus.php?shopid=";

}
