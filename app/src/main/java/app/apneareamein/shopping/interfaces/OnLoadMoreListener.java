package app.apneareamein.shopping.interfaces;

public interface OnLoadMoreListener {
    void onLoadMore();
}