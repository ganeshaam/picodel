package app.apneareamein.shopping.stepper.interfaces;

public interface OnPreStepAction {

    void onSkipStep();

}
