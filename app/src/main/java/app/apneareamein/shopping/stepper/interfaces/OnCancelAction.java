

package app.apneareamein.shopping.stepper.interfaces;

public interface OnCancelAction {

    void onCancel();

}
