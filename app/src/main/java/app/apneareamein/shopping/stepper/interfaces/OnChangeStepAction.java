package app.apneareamein.shopping.stepper.interfaces;


import app.apneareamein.shopping.stepper.SteppersItem;

public interface OnChangeStepAction {

    void onChangeStep(int position, SteppersItem activeStep);

}
