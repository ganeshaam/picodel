
package app.apneareamein.shopping.stepper.interfaces;

public interface OnFinishAction {

    void onFinish();

}
