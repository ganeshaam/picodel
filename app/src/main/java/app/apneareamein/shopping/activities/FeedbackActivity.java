package app.apneareamein.shopping.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.KnownFromAdaptor;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.ReferPoints;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import dmax.dialog.SpotsDialog;

public class FeedbackActivity extends AppCompatActivity {

    RatingBar ratingBar, rb_shopkeeper, rb_deliveryboy, rb_rate_us;
    EditText editWriteReview1, editWriteReview2, editWriteReview, et_shop_review, et_deliveryboy_review;
    Button btn_rwsubmit, btn_rwcancel,btn_ok,btn_later,btn_delboy_review,btn_shop_review;

    TextView tv_playstore,tv_orderid, tv_ordergiven, tv_orderdelivered, tv_from_shopName, tv_deliverdby, txt_shop_ch_heading, txt_delboy_ch_head, txt_fb_note;
    CheckBox shop_ch_qty, shop_ch_qlty, shop_ch_packing, shop_ch_price, ch_punctuality, ch_behaviour, ch_appearance, ch_communication, ch_handling, ch_tools;

    double Rating_value = 4.0, shop_rating, delboy_rating;
    Context mcontext;

    KnownFromAdaptor knownFromAdaptor;
    ArrayList<ReferPoints> arrayList;
    android.app.AlertDialog progressDialog2;
    GateWay gateWay;
    String user_id="0";
    LinearLayoutManager layoutManager;


    String delboy_id = "0", shop_id = "0", order_id = "0", total_amount = "0", fbnote, value_known_from="0";
    LinearLayout layout_shop_ch, layout_delboy_ck, layout_rate_us,ll_playstore;
    ProgressBar simpleProgressBar;
    public static final String MY_PREFS_NAME = "PICoDEL";
    RecyclerView rv_known_from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Feedback Activity");


        mcontext = FeedbackActivity.this;
        progressDialog2 = new SpotsDialog.Builder().setContext(this).build();
        initViews();

        rv_known_from = findViewById(R.id.rv_known_from);
        rv_known_from.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this,1);
        //layoutManager = new GridLayoutManager(this,2);
        rv_known_from.setLayoutManager(layoutManager);

        arrayList = new ArrayList<>();
        knownFromAdaptor = new KnownFromAdaptor(arrayList,this);
        rv_known_from.setAdapter(knownFromAdaptor);

        getknownFrom();

        rv_known_from.addOnItemTouchListener(new RecyclerTouchListener(mcontext, rv_known_from,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        value_known_from = arrayList.get(position).getAmount();
                        Log.e("KnownFrom:",""+value_known_from);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));


        btn_rwsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    ratingBar.getNumStars();
                    Rating_value =   ratingBar.getRating();

                    if(Rating_value<1){
                        Toast.makeText(mcontext,"Please give Company Ratings",Toast.LENGTH_LONG).show();
                    }else if(editWriteReview.getText().toString().isEmpty()){
                        editWriteReview.setError("Please write comment");
                    }else {
                    /*if(Rating_value>=4){
                        Uri uri = Uri.parse("market://details?id=" + mcontext.getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        // To count with Play market backstack, After pressing back button,
                        // to taken back to our application, we need to add following flags to intent.
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + mcontext.getPackageName())));
                        }
                        FeedbackReviewDataToServer();
                    }else {*/

                        Log.e("delboy_review:", "" + del_checkOperations());
                        Log.e("shop_review:", "" + shop_checkOperations());

                        FeedbackReviewDataToServer();
                    }
                    //}
                //}

            }
        });

        rb_shopkeeper.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if(rating==5){
                    layout_shop_ch.setVisibility(View.VISIBLE);
                    shop_rating=rating;
                    txt_shop_ch_heading.setText("What did you like?");
                }else {
                    shop_rating=rating;
                    //layout_shop_ch.setVisibility(View.GONE);
                    txt_shop_ch_heading.setText("What all will you like us to improve?");
                }

            }
        });
        rb_deliveryboy.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if(rating==5){
                    layout_delboy_ck.setVisibility(View.VISIBLE);
                    delboy_rating=rating;
                    txt_delboy_ch_head.setText("What did you like?");
                }else {
                    delboy_rating=rating;
                    txt_delboy_ch_head.setText("What all will you like us to improve?");
                }

            }
        });

        btn_rwcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //onBackPressed();
            }
        });

        layout_rate_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gotoPlayStore();

                /*Intent scratintent = new Intent(FeedbackActivity.this,Scratched_Card_Activity.class);
                startActivity(scratintent);*/

            }
        });



        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                /*if(rating>=4){
                    Uri uri = Uri.parse("market://details?id=" + mcontext.getPackageName());
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our application, we need to add following flags to intent.
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    try {
                        startActivity(goToMarket);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=" + mcontext.getPackageName())));
                    }

                }*/
            }
        });

        getOrderDetails();


         try {

             txt_fb_note.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Uri uri = Uri.parse(fbnote); // missing 'http://' will cause crashed
                     Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                     startActivity(intent);
                 }
             });
                /* Uri uri = Uri.parse(txt_fb_note.getText().toString()); // missing 'http://' will cause crashed
                 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                 startActivity(intent);

             intent = new Intent();
             intent.setAction(Intent.ACTION_SEND);
             intent.putExtra(Intent.EXTRA_TEXT, fbnote);
             intent.setType("text/plain");
             startActivity(intent);*/


         }catch (Exception e){

         }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoPlayStore();

            }
        });

         btn_later.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 finish();
             }
         });

        btn_delboy_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(delboy_rating < 1){
                    Toast.makeText(mcontext,"Please Give Delivery Ratings",Toast.LENGTH_LONG).show();
                }else if(editWriteReview2.getText().toString().isEmpty()){
                    editWriteReview2.setError("Please write comment");
                }else {

                    Log.e("delboy_review:", "" + del_checkOperations());

                    FeedbackReviewDataToServer();
                }
            }
        });

        btn_shop_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 if(shop_rating < 1){
                    Toast.makeText(mcontext,"Please Give Shop Ratings",Toast.LENGTH_LONG).show();
                 }else if(editWriteReview1.getText().toString().isEmpty()){
                    editWriteReview1.setError("Please write comment");
                 }else {

                     Log.e("shop_review:", "" + shop_checkOperations());

                     FeedbackReviewDataToServer();
                 }
            }
        });

    }

    public void  initViews(){

        ratingBar = findViewById(R.id.ratingBar);
        rb_shopkeeper = findViewById(R.id.rb_shopkeeper);
        rb_deliveryboy = findViewById(R.id.rb_deliveryboy);

        editWriteReview1 = findViewById(R.id.editWriteReview1);
        editWriteReview2 = findViewById(R.id.editWriteReview2);
        editWriteReview = findViewById(R.id.editWriteReview);

        btn_rwsubmit = findViewById(R.id.btn_rwsubmit);
        btn_rwcancel = findViewById(R.id.btn_rwcancel);

        tv_orderid = findViewById(R.id.tv_orderid);
        tv_ordergiven = findViewById(R.id.tv_ordergiven);
        tv_orderdelivered = findViewById(R.id.tv_orderdelivered);
        tv_from_shopName = findViewById(R.id.tv_from_shopName);
        tv_deliverdby = findViewById(R.id.tv_deliverdby);

        et_shop_review = findViewById(R.id.et_shop_review);
        et_deliveryboy_review = findViewById(R.id.et_deliveryboy_review);

        shop_ch_qty = findViewById(R.id.shop_ch_qty);
        shop_ch_qlty = findViewById(R.id.shop_ch_qlty);
        shop_ch_packing = findViewById(R.id.shop_ch_packing);
        shop_ch_price= findViewById(R.id.shop_ch_price);

        ch_punctuality = findViewById(R.id.ch_punctuality);
        ch_behaviour = findViewById(R.id.ch_behaviour);
        ch_appearance = findViewById(R.id.ch_appearance);
        ch_communication = findViewById(R.id.ch_communication);
        ch_handling = findViewById(R.id.ch_handling);
        ch_tools = findViewById(R.id.ch_tools);

        layout_shop_ch = findViewById(R.id.layout_shop_ch);
        layout_delboy_ck = findViewById(R.id.layout_delboy_ck);
        layout_rate_us = findViewById(R.id.layout_rate_us);
        rb_rate_us = findViewById(R.id.rb_rate_us);
        //,txt_delboy_ch_head;
        txt_shop_ch_heading = findViewById(R.id.txt_shop_ch_heading);
        txt_delboy_ch_head = findViewById(R.id.txt_delboy_ch_head);
        txt_fb_note = findViewById(R.id.txt_fb_note);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);
        tv_playstore = findViewById(R.id.tv_playstore);
        ll_playstore = findViewById(R.id.ll_playstore);

        btn_later = findViewById(R.id.btn_later);
        btn_ok = findViewById(R.id.btn_ok);

        btn_shop_review = findViewById(R.id.btn_shop_review);
        btn_delboy_review = findViewById(R.id.btn_delboy_review);

    }

    private void gotoPlayStore() {
        if (Connectivity.isConnected(FeedbackActivity.this)) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=app.apneareamein.shopping&hl=en"));
            startActivity(intent);
        } else {
            Toast.makeText(FeedbackActivity.this,"Please check internet Connection",Toast.LENGTH_LONG).show();
        }
    }

    public ArrayList<String> shop_checkOperations(){
        ArrayList<String> shopReviews = new ArrayList<>();
        String qty = shop_ch_qty.getText().toString();
        String qlty = shop_ch_qlty.getText().toString();
        String placking = shop_ch_packing.getText().toString();
        String price = shop_ch_price.getText().toString();


        if (shop_ch_qty.isChecked()) {
            shopReviews.add(qty);
        }
        if (shop_ch_qlty.isChecked()) {
            shopReviews.add(qlty);
        }
        if (shop_ch_packing.isChecked()) {
            shopReviews.add(placking);
        }
        if (shop_ch_price.isChecked()) {
            shopReviews.add(price);
        }
        shopReviews.remove(Collections.singleton(""));
        return  shopReviews;

    }

    public ArrayList<String> del_checkOperations(){
        ArrayList<String> delboy_Reviews = new ArrayList<>();
        String punctuality = ch_punctuality.getText().toString();
        String behaviour = ch_behaviour.getText().toString();
        String appearance = ch_appearance.getText().toString();
        String communication = ch_communication.getText().toString();
        String handling = ch_handling.getText().toString();
        String tools = ch_tools.getText().toString();


        if (ch_punctuality.isChecked()) {
            delboy_Reviews.add(punctuality);
        }
        if (ch_behaviour.isChecked()) {
            delboy_Reviews.add(behaviour);
        }
        if (ch_appearance.isChecked()) {
            delboy_Reviews.add(appearance);
        }
        if (ch_communication.isChecked()) {
            delboy_Reviews.add(communication);
        }
        if (ch_handling.isChecked()) {
            delboy_Reviews.add(handling);
        }
        if (ch_tools.isChecked()) {
            delboy_Reviews.add(tools);
        }
        delboy_Reviews.remove(Collections.singleton(""));
        return  delboy_Reviews;

    }

    // Get Order Details
    private void getOrderDetails() { //TODO Server method here
        if (Connectivity.isConnected(FeedbackActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(FeedbackActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);
            String strContact = gateWay.getContact();
            JSONObject params = new JSONObject();
            try {
                params.put("contact_no", strContact);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlmyLastOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.e("LastOrderRes:",""+response);
                        if (response.isNull("posts")) {

                            Toast.makeText(mcontext,"Not Found",Toast.LENGTH_LONG).show();

                        } else {
                            JSONArray mainOrderArray = response.getJSONArray("posts");
                            //Toast.makeText(mcontext,"Review Successfully",Toast.LENGTH_LONG).show();
                            for (int i = 0; i < mainOrderArray.length(); i++) {
                                JSONObject jSonMyOrderData = mainOrderArray.getJSONObject(i);

                                order_id = jSonMyOrderData.getString("order_id");
                                shop_id = jSonMyOrderData.getString("assign_shop_id");
                                delboy_id = jSonMyOrderData.getString("assign_delboy_id");
                                total_amount = jSonMyOrderData.getString("total_amount");

                                tv_orderid.setText(jSonMyOrderData.getString("order_id"));
                                tv_ordergiven.setText(jSonMyOrderData.getString("order_date"));
                                tv_orderdelivered.setText(jSonMyOrderData.getString("updated_at"));
                                tv_deliverdby.setText(jSonMyOrderData.getString("boy_name"));
                                tv_from_shopName.setText(jSonMyOrderData.getString("shop_name"));
                                //txt_fb_note.setText("Note: "+jSonMyOrderData.getString("fb_note"));
                                fbnote = jSonMyOrderData.getString("fb_note");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.GONE);

                    GateWay gateWay = new GateWay(FeedbackActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }


    // put the Review and Ratings to server
    private void FeedbackReviewDataToServer() { //TODO Server method here
        String pId = null;
        if (Connectivity.isConnected(FeedbackActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(FeedbackActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {

                //JSONArray del_boy_chks = new JSONArray(del_checkOperations());
             String   del_boy_chks = del_checkOperations().toString();
                //JSONArray shop_chks = new JSONArray(shop_checkOperations());
             String   shop_chks = shop_checkOperations().toString();

                params.put("delboy_id", delboy_id);
                params.put("order_id", order_id);
                params.put("shop_id", shop_id);
                params.put("shop_review", shop_chks);
                params.put("shop_rating", shop_rating);
                params.put("delboy_review", del_boy_chks);
                params.put("delboy_rating", delboy_rating);
                params.put("picodel_review", editWriteReview.getText().toString());
                params.put("picodel_rating", Rating_value);
                params.put("known_from", value_known_from);

                Log.e("review_params:",params.toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlReviewAll, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.e("feedback_Response:",""+response);
                        String result = response.getString("posts");
                        Log.e("success_res:",""+result);
                        if (result.equals("success")) {
                            Toast.makeText(FeedbackActivity.this, "Thanks for giving rating & review.", Toast.LENGTH_LONG).show();
                            //finish();
                            getScratch_amount();

                        }else if(result.equalsIgnoreCase("Already given the Feedback")){
                            Toast.makeText(FeedbackActivity.this, "Already given the Feedback.", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                   // gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.GONE);

                    GateWay gateWay = new GateWay(FeedbackActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(FeedbackActivity.this,"Please check internet connection",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Already got the Benifit

    private void getScratch_amount() { //TODO Server method here
        if (Connectivity.isConnected(FeedbackActivity.this)) {
            GateWay gateWay = new GateWay(FeedbackActivity.this);

            String email = gateWay.getUserEmail();
            String user_contact = gateWay.getContact();


            JSONObject params = new JSONObject();
            try {

                params.put("email", email);
                params.put("user_contact", user_contact);
                params.put("order_id", order_id);
                params.put("order_amount", total_amount);
                Log.d("scratch_get", ";params: " + params.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.get_scratch_amount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("GET_SCRATCH_STATUS", "onResponse: " + response.toString());
                        String result = response.getString("scratch_amount");
                        Log.e("scratch_amount",""+result);

                        if (result.equals("Already got the Benifit")) {
                            Toast.makeText(FeedbackActivity.this, "Already got the Benefit for:"+order_id +"Order ID", Toast.LENGTH_LONG).show();
                            finish();

                        }else{
                           /* Intent scratintent = new Intent(FeedbackActivity.this,Scratched_Card_Activity.class);
                            scratintent.putExtra("order_id",order_id);
                            scratintent.putExtra("order_amount",total_amount);
                            startActivity(scratintent);
                            finish();*/
                           finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    Log.d("SCRATCHED", "onErr: " + error.toString());
                    GateWay gateWay = new GateWay(FeedbackActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(FeedbackActivity.this, "Please check Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void getknownFrom() { //TODO Server method here
        if (Connectivity.isConnected(mcontext)) {
            // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(mcontext);
            progressDialog2.show();

            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String user_id = prefs.getString("user_id", "");

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                //params.put("user_id", user_id);
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlget_Known_From, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("ReferInsertRes", "" + response.toString());
                    progressDialog2.dismiss();
                    if (response.isNull("posts")) {

                        // progressDialog2.dismiss();
                    }else{


                        try {
                            JSONArray postsJsonArray = response.getJSONArray("posts");

                            Log.e("points_RS:",""+postsJsonArray.toString());

                            for (int i = 0; i < postsJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = postsJsonArray.getJSONObject(i);

                                ReferPoints points = new ReferPoints();
                                points.setAmount(jSonClassificationData.getString("message"));
                                points.setOrder_id(jSonClassificationData.getString("id"));//order_id

                                arrayList.add(points);
                            }

                            knownFromAdaptor.notifyDataSetChanged();
                        }catch (Exception e){

                        }

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    progressDialog2.dismiss();
                    GateWay gateWay = new GateWay(mcontext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(this,"Please check the Internet Connection",Toast.LENGTH_LONG).show();
        }
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }


}
