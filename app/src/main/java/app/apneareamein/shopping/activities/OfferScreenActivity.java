package app.apneareamein.shopping.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.ReferPoints;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class OfferScreenActivity extends AppCompatActivity {

    TextView tv_title,tv_description;
    ImageView iv_offer;
    GateWay gateWay;
    //android.app.AlertDialog progressDialog2;
    ProgressDialog progressDialog2;
    public static final String MY_PREFS_NAME = "PICoDEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_screen);

       /* Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Check out Offer");*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Check out Offer");

        progressDialog2 = new ProgressDialog(this);

        tv_title = findViewById(R.id.tv_offer);
        tv_description = findViewById(R.id.tv_offerdecription);
        iv_offer = findViewById(R.id.iv_offer);

        getOfferDetails();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getOfferDetails() { //TODO Server method here
        if (Connectivity.isConnected(OfferScreenActivity.this)) {
            // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(OfferScreenActivity.this);
            progressDialog2.show();

            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String user_id = prefs.getString("user_id", "");

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("user_id", user_id);
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlOfferScreen, params, new Response.Listener<JSONObject>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("ReferInsertRes", "" + response.toString());
                    progressDialog2.dismiss();
                    if (response.isNull("posts")) {

                        // progressDialog2.dismiss();
                    }else{
                        try {
                            JSONArray postsJsonArray = response.getJSONArray("posts");
                            JSONObject jSonClassificationData = postsJsonArray.getJSONObject(0);
                            tv_title.setText(jSonClassificationData.getString("title"));
                            tv_description.setText(jSonClassificationData.getString("description"));
                            String imageurl = jSonClassificationData.getString("imageurl");
                            Picasso.with(OfferScreenActivity.this).load(imageurl)
                                    .placeholder(R.drawable.loading)
                                    .error(R.drawable.ic_app_transparent)
                                    .into(iv_offer);

                        }catch (Exception e){

                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    progressDialog2.dismiss();
                    GateWay gateWay = new GateWay(OfferScreenActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(this,"Please check the Internet Connection",Toast.LENGTH_LONG).show();
        }
    }
}
