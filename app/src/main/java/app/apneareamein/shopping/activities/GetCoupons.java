package app.apneareamein.shopping.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.Movie;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class GetCoupons extends AppCompatActivity {

    private Handler handler;
    private DialogPlus dialog;
    private String redeemCategory;
    private TextView tvMessage, tvServicesMessage;
    private CoordinatorLayout getCouponsMainLayout;
    private RecyclerView redeemOfferRecyclerView, service_recycler_view;
    private RedeemCoupons redeemCoupons;
    private RedeemServices redeemServices;
    private LinearLayoutManager mLayoutManagerForServices;
    private ArrayList<String> redeem_id;
    private LinearLayout Main_Layout_NoInternet;
    private Network_Change_Receiver myReceiver;
    private FloatingActionButton floatingActionButton;
    private TextView txtNoConnection;

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                tvMessage.setVisibility(View.GONE);
                redeemOfferRecyclerView.setVisibility(View.GONE);
                floatingActionButton.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);

            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                getCouponsWithRespectiveCoupons(redeemCategory);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_coupons);

        myReceiver = new Network_Change_Receiver();

        Toolbar toolbar = findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_get_coupons);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            redeemCategory = bundle.getString("cashBackCategory");
        }
        txtNoConnection = findViewById(R.id.txtNoConnection);
        floatingActionButton = findViewById(R.id.listServices);
        getCouponsMainLayout = findViewById(R.id.getCouponsMainLayout);
        tvMessage = findViewById(R.id.txtMessage);
        redeemOfferRecyclerView = findViewById(R.id.redeemOfferRecyclerView);
        redeemOfferRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManagerForRedeemCoupons = new LinearLayoutManager(this);
        redeemOfferRecyclerView.setLayoutManager(mLayoutManagerForRedeemCoupons);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = DialogPlus.newDialog(GetCoupons.this)
                        .setContentHolder(new ViewHolder(R.layout.dialog_redeem_serices))
                        .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                        .setGravity(Gravity.BOTTOM)
                        .create();

                tvServicesMessage = (TextView) dialog.findViewById(R.id.txtServicesMessage);
                ImageView closedDialog = (ImageView) dialog.findViewById(R.id.imgClosedDialog);
                closedDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                service_recycler_view = (RecyclerView) dialog.findViewById(R.id.service_recycler_view);
                service_recycler_view.setHasFixedSize(true);
                mLayoutManagerForServices = new GridLayoutManager(GetCoupons.this, 2);
                service_recycler_view.setLayoutManager(mLayoutManagerForServices);

                getCouponsWithRespectiveServices(redeemCategory);
            }
        });
    }

    private void getCouponsWithRespectiveCoupons(String redeemCategory) { //TODO Server method here
        if (Connectivity.isConnected(GetCoupons.this)) {
            final GateWay gateWay = new GateWay(GetCoupons.this);
            gateWay.progressDialogStart();

            redeem_id = new ArrayList<>();
            redeemCoupons = new RedeemCoupons(GetCoupons.this);

            JSONObject params = new JSONObject();
            try {
                params.put("category", redeemCategory);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUserRedeemCoupons, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.isNull("redeemService")) {
                            tvMessage.setVisibility(View.VISIBLE);
                            redeemOfferRecyclerView.setVisibility(View.GONE);
                            floatingActionButton.setVisibility(View.GONE);
                        } else {
                            tvMessage.setVisibility(View.GONE);
                            redeemOfferRecyclerView.setVisibility(View.VISIBLE);
                            floatingActionButton.setVisibility(View.VISIBLE);
                            JSONArray redeemCategoryJsonArray = response.getJSONArray("redeemService");

                            for (int i = 0; i < redeemCategoryJsonArray.length(); i++) {
                                JSONObject jSonRedeemCategory = redeemCategoryJsonArray.getJSONObject(i);

                                redeem_id.add(jSonRedeemCategory.getString("redeem_id"));

                                Movie movie = new Movie(jSonRedeemCategory.getString("redeem_id"), jSonRedeemCategory.getString("shop_name"),
                                        jSonRedeemCategory.getString("redeem_offer"), jSonRedeemCategory.getString("min_amount"),
                                        jSonRedeemCategory.getString("offer_amount"), jSonRedeemCategory.getString("offer_unit"),
                                        jSonRedeemCategory.getString("redeem_startdate"), jSonRedeemCategory.getString("redeem_enddate")
                                        , jSonRedeemCategory.getString("shop_address"));
                                redeemCoupons.add(movie);
                            }
                            redeemOfferRecyclerView.setAdapter(redeemCoupons);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(GetCoupons.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    private void getCouponsWithRespectiveServices(String redeemCategory) { //TODO Server method here
        if (Connectivity.isConnected(GetCoupons.this)) {
            final GateWay gateWay = new GateWay(GetCoupons.this);
            gateWay.progressDialogStart();

            redeemServices = new RedeemServices();

            JSONObject params = new JSONObject();
            try {
                params.put("category", redeemCategory);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUserRedeemCoupons, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("service")) {
                            tvServicesMessage.setVisibility(View.VISIBLE);
                        } else {
                            tvServicesMessage.setVisibility(View.GONE);
                            JSONArray serviceJsonArray = response.getJSONArray("service");

                            for (int i = 0; i < serviceJsonArray.length(); i++) {
                                JSONObject jSonServiceArray = serviceJsonArray.getJSONObject(i);

                                Movie movie1 = new Movie(jSonServiceArray.getString("service_image"), jSonServiceArray.getString("service"),
                                        jSonServiceArray.getString("service_cost"));
                                redeemServices.add(movie1);
                            }
                            service_recycler_view.setAdapter(redeemServices);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    gateWay.progressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(GetCoupons.this);
            gateWay.displaySnackBar(getCouponsMainLayout);
        }
    }

    private class RedeemCoupons extends RecyclerView.Adapter<RedeemCoupons.ViewHolder> {

        private final List<Movie> mItems = new ArrayList<>();
        final Context context;

        RedeemCoupons(GetCoupons getCoupons) {
            super();
            this.context = getCoupons;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_redeem_coupons, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
            final int mainPosition = viewHolder.getAdapterPosition();
            Movie movie = mItems.get(mainPosition);

            viewHolder.tvExpireDate.setText(movie.getRedeem_end_date());
            viewHolder.tvRedeemOfferName.setText(movie.getRedeem_offer_name());
            viewHolder.tvShopName.setText("By: " + movie.getRedeem_shop_name());
            if (!movie.getRedeem_shop_address().equals("")) {
                viewHolder.tvShopAddress.setText("Address: " + movie.getRedeem_shop_address());
            } else {
                viewHolder.tvShopAddress.setVisibility(View.GONE);
            }
            viewHolder.tvMinAmt.setText("₹" + movie.getRedeem_min_amount());
            viewHolder.tvOfferAmt.setText(movie.getRedeem_offer_amount() + movie.getRedeem_offer_unit() + " off");
            viewHolder.tvGetCouponCode.setText("Get Coupon Code");

            final String endDate = movie.getRedeem_end_date();
            handler = new Handler();
            Runnable runnable = new Runnable() {
                public void run() {
                    handler.postDelayed(this, 1000);
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        // Here Set your Event Date
                        Date futureDate = dateFormat.parse(endDate);
                        Date currentDate = new Date();
                        if (currentDate.before(futureDate)) {
                        } else {
                            tvMessage.setVisibility(View.VISIBLE);
                            mItems.remove(mainPosition);
                            notifyItemRemoved(mainPosition);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.postDelayed(runnable, 0);

            viewHolder.tvGetCouponCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(GetCoupons.this)) {
                        GateWay gateWay = new GateWay(GetCoupons.this);

                        ViewHolder holder = (ViewHolder) (v.getTag());
                        int pos = holder.getAdapterPosition();
                        String str_redeem_id = redeem_id.get(pos);
                        applyCouponCode(str_redeem_id, gateWay.getUserEmail(), gateWay.getContact()); //this is for apply coupons code
                    } else {
                        GateWay gateWay = new GateWay(GetCoupons.this);
                        gateWay.displaySnackBar(getCouponsMainLayout);
                    }
                }

                private void applyCouponCode(String str_redeem_id, String email, String contact) { //TODO Server method here
                    if (Connectivity.isConnected(GetCoupons.this)) {
                        JSONObject params = new JSONObject();
                        try {
                            params.put("redeem_id", str_redeem_id);
                            params.put("email", email);
                            params.put("contact", contact);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlApplyCouponCode, params, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    String strResult = response.getString("result");
                                    String text = response.getString("text");
                                    if (strResult.equals("true")) {
                                        infoAlertDialog(); //this is for if user apply code then show the dialog
                                        viewHolder.tvGetCouponCode.setText(text);
                                    } else {
                                        viewHolder.tvGetCouponCode.setText(text);
                                        alertDialogMessageIfAlreadyUsed();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(GetCoupons.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                        GateWay gateWay = new GateWay(GetCoupons.this);
                        gateWay.displaySnackBar(getCouponsMainLayout);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(Movie movie) {
            mItems.add(movie);
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView tvExpireDate;
            private TextView tvRedeemOfferName;
            private TextView tvShopName;
            private TextView tvShopAddress;
            private TextView tvMinAmt;
            private TextView tvOfferAmt;
            private TextView tvGetCouponCode;

            public ViewHolder(View itemView) {
                super(itemView);
                tvExpireDate = itemView.findViewById(R.id.txtExpireDate);
                tvRedeemOfferName = itemView.findViewById(R.id.txtRedeemOfferName);
                tvShopName = itemView.findViewById(R.id.txtShopName);
                tvShopAddress = itemView.findViewById(R.id.txtShopAddress);
                tvMinAmt = itemView.findViewById(R.id.txtMinAmt);
                tvOfferAmt = itemView.findViewById(R.id.txtOfferAmt);
                tvGetCouponCode = itemView.findViewById(R.id.txtGetCouponCode);
                tvGetCouponCode.setTag(this);
            }
        }
    }

    private void infoAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Coupon Code is sent successfully, please check SMS.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void alertDialogMessageIfAlreadyUsed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your already used this coupon.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class RedeemServices extends RecyclerView.Adapter<RedeemServices.ViewHolder> {

        private final List<Movie> mItems = new ArrayList<>();

        RedeemServices() {
            super();
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_redeem_coupons_services, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
            Movie movie = mItems.get(position);

            //TODO here coupons images setup to glide
            Glide.with(GetCoupons.this).load("http://s.apneareamein.com/backend/web/assets/" + movie.getRedeem_service_image())
                    .thumbnail(Glide.with(GetCoupons.this).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.servicesImg);

            viewHolder.tvServicesName.setText(movie.getRedeem_service_name());
            viewHolder.tvServicesAmt.setText(movie.getRedeem_service_cost());
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(Movie movie) {
            mItems.add(movie);
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            final TextView tvServicesName;
            final TextView tvServicesAmt;
            final ImageView servicesImg;

            public ViewHolder(View itemView) {
                super(itemView);
                tvServicesName = itemView.findViewById(R.id.txtServicesName);
                tvServicesAmt = itemView.findViewById(R.id.txtServicesAmt);
                servicesImg = itemView.findViewById(R.id.servicesImg);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(GetCoupons.this)) {
            String class_name = this.getClass().getSimpleName();

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, GetCoupons.this);*/
        }
    }
}
