package app.apneareamein.shopping.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.SliderPagerAdapter;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.SliderModel;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.MySingleton;

public class ActivityPulicSlider extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener{

    SliderPagerAdapter sliderPagerAdapter;
    ArrayList<SliderModel> modelArrayList;
    ViewPager vp_publicSlider;
    Button btn_topskip,btn_bottom_skip;
    Context mContext;
    ProgressDialog progressDialog;
    private int currentPage = 0;
    Timer timer;
    public static final String MY_PREFS_NAME = "PICoDEL";
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    String  latitude,longitude;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager locationManager,mLocationManager;
    private LocationRequest mLocationRequest;
    public static final String TAG = "ActivityPulicSlider";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_public_slider);
        mContext = ActivityPulicSlider.this;
        progressDialog = new ProgressDialog(this);

        try{
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("public_slider", "true");
            editor.apply();
            editor.commit();
        }catch (Exception e){

        }

        vp_publicSlider = findViewById(R.id.vp_publicSlider);
        btn_topskip = findViewById(R.id.btn_topskip);
        btn_bottom_skip = findViewById(R.id.btn_bottom_skip);

        modelArrayList = new ArrayList<>();
        sliderPagerAdapter = new SliderPagerAdapter(mContext,modelArrayList);
        vp_publicSlider.setAdapter(sliderPagerAdapter);

        getSlider();

        btn_topskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(ActivityPulicSlider.this, MobileVerification.class);
                intent.putExtra("tag", "registration");
                startActivity(intent);*/

                Intent intent = new Intent(ActivityPulicSlider.this, MobileVerification.class);
                //intent = new Intent(Splash.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                ActivityPulicSlider.this.finish();

            }
        });

        btn_bottom_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ActivityPulicSlider.this, MobileVerification.class);
                //intent = new Intent(Splash.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                ActivityPulicSlider.this.finish();


            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }

        /*Location Initialization*/

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        checkLocation(); //check whether location service is enable or not in your  phone

    }


    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    protected void startLocationUpdates() {

        mLocationRequest = LocationRequest.create();//.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }



    //check for the location

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    //get slider images
    public void getSlider(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();
            String urlSLiderGet = "https://www.picodel.com/seller/shoptestapi/getslideruser/";
            //String urlSLiderGet = "https://www.picodel.com/seller/shopapi/getslideruser/" +"?tblName="+"Seller";

            Log.d("urlSLiderGet",urlSLiderGet);

            StringRequest stringRequestSlider = new StringRequest(Request.Method.GET,
                    urlSLiderGet,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss){
                                    //slider
                                    JSONArray jsonArraySlider = jsonObject.getJSONArray("slider");
                                    for(int i=0; i<jsonArraySlider.length(); i++) {
                                        JSONObject jsonObjectSlider = jsonArraySlider.getJSONObject(i);
                                        SliderModel model = new SliderModel();
                                        model.setImgUrl(jsonObjectSlider.getString("front_search_banner"));
                                        modelArrayList.add(model);
                                    }
                                    sliderPagerAdapter.notifyDataSetChanged();
                                    setupAutoPager(modelArrayList.size());

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            //stringRequestOtp.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestSlider);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //slider scroll handle
    private void setupAutoPager(final int listSize) {
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run()
            {
               vp_publicSlider.setCurrentItem(currentPage, true);
                if(currentPage == listSize)
                {
                    currentPage = 0;
                }
                else
                {
                    ++currentPage ;
                }
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 2500, 2500);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            Log.d("Location",mLocation.getLatitude()+" "+mLocation.getLongitude());
            latitude = String.valueOf(mLocation.getLatitude());
            longitude = String.valueOf(mLocation.getLongitude());
            Toast.makeText(this, "Location :"+latitude +""+longitude, Toast.LENGTH_SHORT).show();
            //mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            //Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            Log.e("Location:",""+location.getLatitude()+""+location.getLongitude());
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());

            Toast.makeText(ActivityPulicSlider.this,"Location"+""+latitude+""+longitude,Toast.LENGTH_LONG).show();

            try {
                Geocoder gcd = new Geocoder(ActivityPulicSlider.this, Locale.getDefault());
                List<Address> addresses = null;
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    Log.e("AddressDetails:",""+addresses.toString());
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                    String area = addresses.get(0).getAdminArea(); // Only if available else return NULL
                    String subAdminArea = addresses.get(0).getSubAdminArea(); // Only if available else return NULL

                    Log.d(TAG, "getAddress:  address" + address);
                    Log.d(TAG, "getAddress:  city" + city);
                    Log.d(TAG, "getAddress:  state" + state);
                    Log.d(TAG, "getAddress:  postalCode" + postalCode);
                    Log.d(TAG, "getAddress:  knownName" + knownName);
                    Log.d(TAG, "getAddress:  area" + area);
                    Log.d(TAG, "subAdminArea:  area" + addresses.get(0).getSubLocality());

                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    if(city!=null) {
                        editor.putString("v_city", city);
                        editor.putString("v_area", addresses.get(0).getSubLocality());
                        String myArea = addresses.get(0).getSubLocality();
                        Toast.makeText(ActivityPulicSlider.this,""+city+" "+myArea,Toast.LENGTH_LONG).show();

                    }
                    editor.apply();
                    editor.commit();
                }
                else {
                    // do your stuff
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


           /* val MY_READ_EXTERNAL_REQUEST : Int = 1
            if (checkSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_READ_EXTERNAL_REQUEST)
            }*/

        }

    }
}
