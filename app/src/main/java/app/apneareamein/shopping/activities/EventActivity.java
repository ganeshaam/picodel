package app.apneareamein.shopping.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.EventAdaptor;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.EventModel;
import app.apneareamein.shopping.model.PaymentModeModel;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.MySingleton;

public class EventActivity extends AppCompatActivity {

    CardView cv_header_event;
    ImageView event_banner;
    TextView txt_eventDescription;
    RecyclerView rv_questionaries;
    Button btn_submit;
    ArrayList<EventModel> ArraylistEvent;
    EventAdaptor eventAdaptor;
    LinearLayoutManager layoutManager;
    private ProgressBar simpleProgressBar;

    String banner_image;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Event Activity");

        btn_submit = findViewById(R.id.btn_submit);
        rv_questionaries = findViewById(R.id.rv_questionaries);
        event_banner = findViewById(R.id.event_banner);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);
        txt_eventDescription = findViewById(R.id.txt_eventDescription);

        rv_questionaries.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(EventActivity.this);
        ArraylistEvent = new ArrayList<>();

        rv_questionaries.setLayoutManager(layoutManager);
        rv_questionaries.setItemAnimator(new DefaultItemAnimator());
        eventAdaptor = new EventAdaptor(EventActivity.this,ArraylistEvent);
        rv_questionaries.setAdapter(eventAdaptor);

        getQutionariesData();

        rv_questionaries.addOnItemTouchListener(new RecyclerTouchListener(EventActivity.this, rv_questionaries, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {




            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    //Get Event Details Api method
    public void EventDtails_List(){

        if(Connectivity.isConnected(this)){
            String urlEventDetails = ApplicationUrlAndConstants.urlCouponsDetails;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, urlEventDetails, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("");

                        for(int i=0; i<=jsonArray.length();i++){

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }else {
            Toast.makeText(this,"Check Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void getQutionariesData() { //TODO Server method here
        if (Connectivity.isConnected(EventActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(EventActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);


            JSONObject params = new JSONObject();
            try {
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlevent_activity, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("EventQustion_Response:",""+response.toString());

                    if (response.isNull("posts")) {
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        //Log.e("EventQustion_Response:",""+response.toString());

                        try {
                            JSONArray eventArray = response.getJSONArray("posts");
                            banner_image = response.getString("banner_image");
                            txt_eventDescription.setText(response.getString("Event_Description"));
                            for (int i = 0; i < eventArray.length(); i++) {
                                JSONObject jsonObject = (JSONObject) eventArray.get(i);
                                EventModel model = new EventModel();
                                model.setQuestion(jsonObject.getString("question"));
                                model.setOption1(jsonObject.getString("option1"));
                                model.setOption2(jsonObject.getString("option2"));
                                model.setOption3(jsonObject.getString("option3"));
                                model.setOption4(jsonObject.getString("option4"));
                                model.setStage(jsonObject.getString("stage"));
                                model.setStatus(jsonObject.getString("status"));

                                ArraylistEvent.add(model);
                            }
                            Log.e("QUESTION_Size:",""+ArraylistEvent.size());
                            eventAdaptor.notifyDataSetChanged();
                            Picasso.with(EventActivity.this)
                                    .load(banner_image)
                                    .placeholder(R.drawable.ic_app_transparent)
                                    .error(R.drawable.ic_app_transparent)
                                    //.resize(355,220)
                                    //.fit()
                                    .into(event_banner);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(EventActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    private void submit_answer(String q_id, String user_answer, String picodel_answer, String flag_stage) { //TODO Server method here
        if (Connectivity.isConnected(EventActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(EventActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                //params.put("city", gateWay.getCity());
                //params.put("area", gateWay.getArea());
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("q_id", q_id);
                params.put("user_answer", user_answer);
                params.put("picodel_answer", picodel_answer);
                params.put("flag_stage", flag_stage);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlevent_answer, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("EventAnswer_Response:",""+response.toString());

                    if (response.isNull("posts")) {
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        //Log.e("EventQustion_Response:",""+response.toString());

                        try {
                            JSONArray eventArray = response.getJSONArray("posts");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(EventActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);

        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
}
