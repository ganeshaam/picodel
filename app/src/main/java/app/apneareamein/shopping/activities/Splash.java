package app.apneareamein.shopping.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.RemoteException;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;
import static java.lang.Thread.sleep;

public class Splash extends AppCompatActivity {

    private Intent intent;
    private BroadcastReceiver myReceiver;
    private TextView txtNoConnection;
    public static final String MY_PREFS_NAME = "PICoDEL";
    public String notificationId="abc";
    boolean notification_flag = false;
    Handler handler=new Handler();
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        myReceiver = new Network_Change_Receiver();
        txtNoConnection = findViewById(R.id.txtNoConnection);
        printHashKey(Splash.this);

        try{
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("visitCount", "false");
            editor.apply();
            editor.commit();
        }catch (Exception e){

        }

        try {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                             //   Log.e("FirebaseToken2", "getInstanceId failed", task.getException());
                                return;
                            }
                            // Get new Instance ID token
                            String token = task.getResult().getToken();
                            notificationId = token;
                            //Toast.makeText(Splash.this, ""+token, Toast.LENGTH_SHORT).show();
                           // Log.e("Splash_FToken2", ""+token, task.getException());
                        }
                    });
        }catch (Exception e){
            e.getMessage();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }

    }

    private void gotoNextMethodUsingThread() { //this method is for thread purpose if time is over then goto next activity.
        final GateWay gateWay = new GateWay(Splash.this);
        /*Thread timer = new Thread() {
            public void run() {*/
        handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

        try {
                    if (gateWay.getContact() == null) {
                        sleep(ApplicationUrlAndConstants.SPLASH_SHOW_TIME);   // Thread will sleep for 3 seconds
                        /*intent = new Intent(Splash.this, MobileVerification.class);
                        //intent = new Intent(Splash.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Splash.this.finish();*/

                        intent = new Intent(Splash.this, ActivityPulicSlider.class);
                        //intent = new Intent(Splash.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Splash.this.finish();

                    } else {
                        if (gateWay.getCity() == null && gateWay.getArea() == null) {
                            Intent intent = new Intent(Splash.this, LocationActivity.class);
                            intent.putExtra("tag", "registration");
                            startActivity(intent);
                        } else {
                            //sleep(ApplicationUrlAndConstants.SPLASH_SHOW_TIME);   // Thread will sleep for 3 seconds
                            //notification_update();
                            //notification_flag= true;
                            intent = new Intent(Splash.this, BaseActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            Splash.this.finish();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                }
            }, 1000); //1000
           // }
       /* };
        timer.start();*/
    }

    //check for the location

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
                gotoNextMethodUsingThread();


            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }
    private void notification_update() { //TODO Server method here
        if (Connectivity.isConnected(Splash.this)) { // Internet connection is not present, Ask user to connect to Internet
            GateWay  gateWay = new GateWay(Splash.this);

            JSONObject params = new JSONObject();
            try {
                params.put("mob_number", gateWay.getContact());
                params.put("notification_id", notificationId);
                params.put("user_version_name", ApplicationUrlAndConstants.versionName);
                Log.e("NotificationIdParm:",""+notificationId);
                Log.e("splashuser_Parm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.notification_update, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("MobileVerificationRes",""+response.toString());

                    if (response.isNull("posts")) {
                       // Toast.makeText(Splash.this,"Notification not updated",Toast.LENGTH_LONG).show();
                    } else {
                        Log.e("responseNoti:",""+response);

                        try {
                            String user_id = response.getString("user_id");
                            Log.e("updated_user_id:",""+user_id);
                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putString("user_id", user_id);
                            editor.apply();
                            editor.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        intent = new Intent(Splash.this, BaseActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Splash.this.finish();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(Splash.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(Splash.this,"Please check internet connection",Toast.LENGTH_LONG).show();
        }
    }

    /*For the Key Has Code*/
    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("Splash_screen", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("Splash_screen", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("Splash_screen", "printHashKey()", e);
        }
    }

}
