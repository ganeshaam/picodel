package app.apneareamein.shopping.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import app.apneareamein.shopping.R;
import app.apneareamein.shopping.androidscratchcard.ScratchCard;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
//import in.myinnos.androidscratchcard.ScratchCard;
import  app.apneareamein.shopping.androidscratchcard.ScratchCard;



public class Scratched_Card_Activity extends AppCompatActivity {

    private TextView textView, txReload;
    private ScratchCard mScratchCard;
    String order_id ="0",order_amount="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratched__card_);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Scratch Card Activity");


        textView = (TextView) findViewById(R.id.textView);
        txReload = (TextView) findViewById(R.id.txReload);
        //loadRandomNumber();


        mScratchCard = (ScratchCard) findViewById(R.id.scratchCard);
        mScratchCard.setOnScratchListener(new  ScratchCard.OnScratchListener() {
            @Override
            public void onScratch(ScratchCard scratchCard, float visiblePercent) {
                if (visiblePercent > 0.3) {
                    mScratchCard.setVisibility(View.GONE);
                    //Toast.makeText(Scratched_Card_Activity.this, "Content Visible", Toast.LENGTH_SHORT).show();
                    user_scrath_send();
                }
            }
        });


        try{
            Intent intent = getIntent();
             if(intent!=null){

                 order_id = intent.getStringExtra("order_id");
                 Log.e("ORDER_ID",""+order_id);
                 order_amount = intent.getStringExtra("order_amount");
             }

        }catch (Exception e){

        }
        getScratch_amount();


        txReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadRandomNumber(){
        Random rand = new Random();
        String randomNum = String.valueOf(5 + rand.nextInt((100 - 20) + 1));
        textView.setText(randomNum);
    }

    private void user_scrath_send() { //TODO Server method here
        if (Connectivity.isConnected(Scratched_Card_Activity.this)) {
           GateWay gateWay = new GateWay(Scratched_Card_Activity.this);

           String email = gateWay.getUserEmail();
           //String user_id = gateWay.
           String user_contact = gateWay.getContact();
           String amount = "";

            JSONObject params = new JSONObject();
            try {

                params.put("user_id", "4063");
                params.put("email", email);
                params.put("user_contact", user_contact);
                params.put("amount", textView.getText().toString());
                params.put("order_id", order_id);
                params.put("order_amount", order_amount);

                Log.d("REG", ";params: " + params.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.user_scratch_insert, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("REG", "onResponse: " + response.toString());
                        String result = response.getString("posts");
                        if (result.equals("fail")) {
                            Toast.makeText(Scratched_Card_Activity.this, "Scratch is Not Applied", Toast.LENGTH_LONG).show();

                        }else if(result.equals("success")){
                            Toast.makeText(Scratched_Card_Activity.this, "Scratch is Applied Successfully", Toast.LENGTH_LONG).show();
                        }else if(result.equals("Already got the Benifit")) {
                            Toast.makeText(Scratched_Card_Activity.this, "Already got the Benefit for this Order", Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    Log.d("SCRATCHED", "onErr: " + error.toString());
                    GateWay gateWay = new GateWay(Scratched_Card_Activity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(Scratched_Card_Activity.this, "Please check Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void getScratch_amount() { //TODO Server method here
        if (Connectivity.isConnected(Scratched_Card_Activity.this)) {
            GateWay gateWay = new GateWay(Scratched_Card_Activity.this);

            String email = gateWay.getUserEmail();
            String user_contact = gateWay.getContact();


            JSONObject params = new JSONObject();
            try {

                params.put("email", email);
                params.put("user_contact", user_contact);
                params.put("order_id", order_id);
                params.put("order_amount", order_amount);
                Log.d("scratch_get", ";params: " + params.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.get_scratch_amount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("REG", "onResponse: " + response.toString());
                        String result = response.getString("scratch_amount");

                        textView.setText(result);

                        /*if (result.equals("success")) {
                            Toast.makeText(Scratched_Card_Activity.this, "Scratch is Not Applied", Toast.LENGTH_LONG).show();

                        }*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    Log.d("SCRATCHED", "onErr: " + error.toString());
                    GateWay gateWay = new GateWay(Scratched_Card_Activity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(Scratched_Card_Activity.this, "Please check Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
