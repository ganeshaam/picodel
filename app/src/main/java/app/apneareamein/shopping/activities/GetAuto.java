package app.apneareamein.shopping.activities;

import android.app.TimePickerDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.GateWay;

public class GetAuto extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private DialogPlus dialogPlus;
    private String timeType;
    private TextInputLayout txtInputTime;
    private EditText editFrom, editTo, editTime;
    CardAdapter cardAdapter;
    TextView txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_auto);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_get_auto);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtMessage = findViewById(R.id.txtMessage);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        FloatingActionButton fab = findViewById(R.id.getAuto);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPlus = DialogPlus.newDialog(GetAuto.this)
                        .setContentHolder(new ViewHolder(R.layout.dialog_layout_for_auto))
                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                        .setGravity(Gravity.CENTER)
                        .create();

                Button btnSubmit = (Button) dialogPlus.findViewById(R.id.btnSubmit);
                txtInputTime = (TextInputLayout) dialogPlus.findViewById(R.id.txtInputTime);
                editFrom = (EditText) dialogPlus.findViewById(R.id.editFrom);
                editTo = (EditText) dialogPlus.findViewById(R.id.editTo);
                editTime = (EditText) dialogPlus.findViewById(R.id.editTime);

                RadioGroup timeRG = (RadioGroup) dialogPlus.findViewById(R.id.timeRG);
                timeRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        // checkedId is the RadioButton selected
                        RadioButton radioButtonModelName = (RadioButton) dialogPlus.findViewById(checkedId);
                        timeType = radioButtonModelName.getText().toString();

                        if (timeType.equals("When")) {
                            // Get Current Time
                            final Calendar c = Calendar.getInstance();
                            int mHour = c.get(Calendar.HOUR_OF_DAY);
                            int mMinute = c.get(Calendar.MINUTE);

                            // Launch Time Picker Dialog
                            TimePickerDialog timePickerDialog = new TimePickerDialog(GetAuto.this,
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay,
                                                              int minute) {
                                            if (hourOfDay > 0 && minute > 0) {
                                                txtInputTime.setVisibility(View.VISIBLE);
                                                editTime.setText(hourOfDay + ":" + minute);
                                            }
                                        }
                                    }, mHour, mMinute, false);
                            timePickerDialog.show();
                        } else {
                            editTime.setText("");
                            txtInputTime.setVisibility(View.GONE);
                        }
                    }
                });

                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (editFrom.getText().toString().trim().equals("")) {
                            Toast.makeText(GetAuto.this, "Please enter From point", Toast.LENGTH_SHORT).show();
                        } else if (editTo.getText().toString().trim().equals("")) {
                            Toast.makeText(GetAuto.this, "Please enter To point", Toast.LENGTH_SHORT).show();
                        } else {
                            if (timeType == null) {
                                timeType = "Right Now";
                                insertAtServer();
                            } else if (timeType.equals("When") && editTime.getText().toString().trim().equals("")) {
                                Toast.makeText(GetAuto.this, "Please select time", Toast.LENGTH_SHORT).show();
                            } else {
                                insertAtServer();
                            }
                        }
                    }
                });

                dialogPlus.show();
            }
        });

        getAutoDataFromServer();
    }

    private void getAutoDataFromServer() {
        cardAdapter = new CardAdapter();

        final GateWay gateWay = new GateWay(GetAuto.this);
        gateWay.progressDialogStart();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("city", gateWay.getCity());
            jsonObject.put("area", gateWay.getArea());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetAutoDataFromServer, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                gateWay.progressDialogStop();
                try {
                    if (!response.isNull("posts")) {
                        txtMessage.setVisibility(View.GONE);
                        JSONArray jsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            POJOClass pojoClass = new POJOClass(jsonObject1.getString("user_id"),
                                    jsonObject1.getString("user_name"),
                                    jsonObject1.getString("from_point"),
                                    jsonObject1.getString("to_point"),
                                    jsonObject1.getString("type"),
                                    jsonObject1.getString("time"));

                            cardAdapter.add(pojoClass);
                        }
                        mRecyclerView.setAdapter(cardAdapter);
                    } else {
                        txtMessage.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                gateWay.progressDialogStop();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.IMMEDIATE);
        AppController.getInstance().addToRequestQueue(request);
    }

    private void insertAtServer() {
        final GateWay gateWay = new GateWay(GetAuto.this);
        gateWay.progressDialogStart();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("from_point", editFrom.getText().toString());
            jsonObject.put("to_point", editTo.getText().toString());
            jsonObject.put("type", timeType);
            jsonObject.put("time", editTime.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlInsertAtServer, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("posts").equals("true")) {
                        gateWay.progressDialogStop();
                        dialogPlus.dismiss();
                        Toast.makeText(GetAuto.this, "Successfully Applied", Toast.LENGTH_SHORT).show();
                        getAutoDataFromServer();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                gateWay.progressDialogStop();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class POJOClass {

        String user_id;
        String user_name;
        String from_point;
        String to_point;
        String type;
        String time;

        POJOClass(String user_id, String user_name, String from_point, String to_point, String type, String time) {
            this.user_id = user_id;
            this.user_name = user_name;
            this.from_point = from_point;
            this.to_point = to_point;
            this.type = type;
            this.time = time;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public String getFrom_point() {
            return from_point;
        }

        public String getTo_point() {
            return to_point;
        }

        public String getType() {
            return type;
        }

        public String getTime() {
            return time;
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<AutoViewHolder> {
        private List<POJOClass> pojoClasses = new ArrayList<>();

        public void add(POJOClass pojoClass) {
            pojoClasses.add(pojoClass);
        }

        @NonNull
        @Override
        public AutoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_auto_list, viewGroup, false);
            return new AutoViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull AutoViewHolder holder, int position) {
            POJOClass pojoClass = pojoClasses.get(position);

            holder.txtUserName.setText("User Name - " + pojoClass.getUser_name());
            holder.txtFromPoint.setText("From Point - " + pojoClass.getFrom_point());
            holder.txtToPoint.setText("To Point - " + pojoClass.getTo_point());
            holder.txtType.setText("Type - " + pojoClass.getType());
            holder.txtTime.setText("& Time is - " + pojoClass.getTime());
        }

        @Override
        public int getItemCount() {
            return pojoClasses.size();
        }
    }

    private class AutoViewHolder extends RecyclerView.ViewHolder {

        TextView txtUserName, txtFromPoint, txtToPoint, txtType, txtTime;

        AutoViewHolder(View itemView) {
            super(itemView);

            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtFromPoint = itemView.findViewById(R.id.txtFromPoint);
            txtToPoint = itemView.findViewById(R.id.txtToPoint);
            txtType = itemView.findViewById(R.id.txtType);
            txtTime = itemView.findViewById(R.id.txtTime);
        }
    }
}
