package app.apneareamein.shopping.activities;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.VolleySingleton;

public class CheckCartDetails extends AppCompatActivity {

    private CoordinatorLayout checkCartItemsLayout;
    private RecyclerView mRecyclerView;
    private CartDetailsCardAdapter cardAdapter;
    private List<CartDetailsItems> cartDetails;
    private LinearLayout footer;
    private TextView txtProductCount, txtTotalPrice, txtDeliveryDate, txtDeliveryTime;
    String order_id, user_id, delivery_date, delivery_time;
    RelativeLayout rlTime;
    TextView txtTimeSelect;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_cart_details);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_cart_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtTimeSelect = findViewById(R.id.txtTimeSelect);
        rlTime = findViewById(R.id.rlTime);
        rlTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                geTimeResult();
            }
        });

        footer = findViewById(R.id.footer);
        checkCartItemsLayout = findViewById(R.id.checkCartItemsLayout);

        txtProductCount = findViewById(R.id.txtProductCount);
        txtTotalPrice = findViewById(R.id.txtTotalPrice);
        txtDeliveryDate = findViewById(R.id.txtDeliveryDate);
        txtDeliveryTime = findViewById(R.id.txtDeliveryTime);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            order_id = bundle.getString("order_id");
            user_id = bundle.getString("user_id");
            delivery_date = bundle.getString("delivery_date");
            delivery_time = bundle.getString("delivery_time");
        }
        txtDeliveryDate.setText("Delivery Date - " + delivery_date);
        txtDeliveryTime.setText("Delivery Time - " + delivery_time);

        getCartItems(order_id, user_id);
    }

    private void geTimeResult() {
        final Dialog dialog = new Dialog(CheckCartDetails.this);
        dialog.setContentView(R.layout.time_item);
        dialog.setTitle("Select Time Slot");

        lv = dialog.findViewById(R.id.listView1);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String strSelect_type = (String) adapterView.getItemAtPosition(i);
                txtTimeSelect.setText(strSelect_type);
                dialog.dismiss();
            }
        });
        dialog.show();

        getTimeSlots();
    }

    private void getTimeSlots() {
        if (Connectivity.isConnected(CheckCartDetails.this)) {
            final GateWay gateWay = new GateWay(CheckCartDetails.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("order_time_slot", delivery_time);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetTimeSlots, params,new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        /*JSONArray array = new JSONArray(response);
                        JSONObject jsonObject = array.getJSONObject(0);*/
                        List<String> timeResult = Collections.singletonList(response.getString("posts"));
                        ArrayAdapter adapter = new ArrayAdapter<>(CheckCartDetails.this, android.R.layout.simple_list_item_1, timeResult);
                        lv.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    gateWay.progressDialogStop();
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(CheckCartDetails.this).addToRequestQueue(request);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getCartItems(String order_id, String user_id) { //TODO Server method here
        if (Connectivity.isConnected(CheckCartDetails.this)) {
            final GateWay gateWay = new GateWay(CheckCartDetails.this);
            gateWay.progressDialogStart();

            cartDetails = new ArrayList<>();

            JSONObject params = new JSONObject();
            try {
                params.put("order_id", order_id);
                params.put("user_id", user_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckCartDetails, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (!response.isNull("posts")) {
                            String productCount = response.getString("product_count");
                            String total = response.getString("total");

                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = mainClassificationJsonArray.getJSONObject(i);

                                cartDetails.add(new CartDetailsItems(jSonClassificationData.getString("shop_id"), jSonClassificationData.getString("shop_name"),
                                        jSonClassificationData.getString("product_id"), jSonClassificationData.getString("product_name"),
                                        jSonClassificationData.getString("product_image"), jSonClassificationData.getString("product_price"),
                                        jSonClassificationData.getString("product_quantity"), jSonClassificationData.getString("p_name")
                                        , jSonClassificationData.getString("product_total")));
                                cardAdapter = new CartDetailsCardAdapter(cartDetails);
                                mRecyclerView.setAdapter(cardAdapter);
                            }
                            footer.setVisibility(View.VISIBLE);

                            txtProductCount.setText("ITEMS" + " " + "(" + productCount + ")");
                            txtTotalPrice.setText("Total: ₹ " + total + "/-");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(CheckCartDetails.this);
            gateWay.displaySnackBar(checkCartItemsLayout); //TODO SnackBar method here
        }
    }

    private class CartDetailsCardAdapter extends RecyclerView.Adapter<CartDetailsViewHolder> {

        List<CartDetailsItems> mItems;
        double final_price;

        CartDetailsCardAdapter(List<CartDetailsItems> innerMovies) {
            this.mItems = innerMovies;
        }

        @NonNull
        @Override
        public CartDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_cart_details, parent, false);
            return new CartDetailsViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull CartDetailsViewHolder holder, int position) {
            CartDetailsItems innerMovie = mItems.get(position);

            Glide.with(CheckCartDetails.this).load("https://simg.picodel.com/" + innerMovie.getProduct_image())
                    .into(holder.tvProductImage);
            holder.tvProductName.setText(innerMovie.getProduct_name());
            holder.tvSellerName.setText("By: " + innerMovie.getShop_name());
            holder.tvProductPrice.setText("₹ " + innerMovie.getProduct_price() + "/-");
            holder.tvProductQty.setText("Quantity: " + innerMovie.getProduct_quantity());
            holder.txtProductTotal.setText("₹ " + innerMovie.getProduct_total() + "/-");
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

    private class CartDetailsViewHolder extends RecyclerView.ViewHolder {

        TextView tvProductName;
        TextView tvSellerName;
        TextView tvProductPrice;
        TextView txtProductTotal;
        TextView tvProductQty;
        ImageView tvProductImage;

        CartDetailsViewHolder(View itemView) {
            super(itemView);

            tvProductName = itemView.findViewById(R.id.txtProductName);
            tvProductPrice = itemView.findViewById(R.id.txtProductPrice);
            txtProductTotal = itemView.findViewById(R.id.txtProductTotal);
            tvSellerName = itemView.findViewById(R.id.txtSellerName);
            tvProductQty = itemView.findViewById(R.id.txtProductQuantity);
            tvProductImage = itemView.findViewById(R.id.productImage);
        }
    }

    private class CartDetailsItems {

        String shop_id;
        String shop_name;
        String product_id;
        String product_name;
        String product_image;
        String product_price;
        String product_quantity;
        String p_name;
        String product_total;

        CartDetailsItems(String shop_id, String shop_name, String product_id, String product_name, String product_image, String product_price, String product_quantity, String p_name, String product_total) {
            this.shop_id = shop_id;
            this.shop_name = shop_name;
            this.product_id = product_id;
            this.product_name = product_name;
            this.product_image = product_image;
            this.product_price = product_price;
            this.product_quantity = product_quantity;
            this.p_name = p_name;
            this.product_total = product_total;
        }

        public String getShop_id() {
            return shop_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getProduct_image() {
            return product_image;
        }

        public String getProduct_price() {
            return product_price;
        }

        public String getProduct_quantity() {
            return product_quantity;
        }

        public String getP_name() {
            return p_name;
        }

        public String getProduct_total() {
            return product_total;
        }
    }
}
