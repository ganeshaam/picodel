package app.apneareamein.shopping.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;/*
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;*/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class LoginActivity extends AppCompatActivity {


    EditText login_mobile,verification_code,usepass;
    Button btn_login;
    LinearLayout layout_registration,layout_resendOTP,layout_facebook;
    private final ArrayList<String> vCode = new ArrayList<>();
    // CallbackManager callbackManager;
    ProgressDialog progressDialog;
    String firebaseTokan;
    private static final String EMAIL = "email";
   // com.facebook.login.widget.LoginButton loginButton;

    //AccessTokenTracker accessTokenTracker;
    //AccessToken accessToken;
    ImageView imageView;
    TextView txtUsername,txtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_mobile = findViewById(R.id.login_mobile);
        verification_code = findViewById(R.id.verification_code);
        layout_resendOTP = findViewById(R.id.layout_resendOTP);
        usepass = findViewById(R.id.usepass);

        btn_login = findViewById(R.id.btn_login);
        layout_registration = findViewById(R.id.layout_registration);
        layout_facebook = findViewById(R.id.layout_facebook);

      /*  callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));
*/
        imageView = findViewById(R.id.imageView);
        txtUsername = findViewById(R.id.txtUsername);
        txtEmail = findViewById(R.id.txtEmail);
        progressDialog = new ProgressDialog(this);

        //printHashKey(this);

        login_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==10){
                    getVerificationCode(s.toString());
                    layout_resendOTP.setVisibility(View.VISIBLE);
                    Toast.makeText(LoginActivity.this,"inLoop",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        layout_resendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(login_mobile.getText().toString().length()==10) {
                    getVerificationCode(login_mobile.getText().toString());
                }else {
                    Toast.makeText(LoginActivity.this,"Please Enter Mobile No.",Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateOtp()) {
                    popupVerifyCode();
                }else {
                    Intent homeIntent = new Intent(LoginActivity.this,LocationActivity.class);
                    startActivity(homeIntent);
                    finish();
                }
            }
        });

        layout_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this,MobileVerification.class);
                startActivity(registerIntent);
            }
        });



      /*  loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Log.e("LoginResult:",""+loginResult);

                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                Log.d("API123", loggedIn + " ??");

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
*/



    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    AccessTokenTracker tokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            if(currentAccessToken==null){
                layout_facebook.setVisibility(View.GONE);
            }else {
                loadUserProfile(currentAccessToken);
            }

        }
    };

    private  void loadUserProfile(AccessToken token){
        GraphRequest graphRequest = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                Log.e("FaceBookResponseNew", object.toString());
                try {
                    layout_facebook.setVisibility(View.VISIBLE);
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String email = object.getString("email");
                    String id = object.getString("id");
                    String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

                    txtUsername.setText("First Name: " + first_name + "\nLast Name: " + last_name);
                    txtEmail.setText(email);
                    Picasso.with(LoginActivity.this).load(image_url)
                            .placeholder(R.drawable.loading)
                            .into(imageView);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

*/



    private void getVerificationCode(String mobNumber) { //TODO Server method here
        if (Connectivity.isConnected(LoginActivity.this)) {
            JSONObject params = new JSONObject();
            try {
                progressDialog.setMessage("Please wait ..!!");
                progressDialog.setCancelable(false);
                progressDialog.show();
                params.put("mob_number", mobNumber);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlRegistrationVerificationCode, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        try {
                            progressDialog.dismiss();
                            JSONArray verificationCodeJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < verificationCodeJsonArray.length(); i++) {
                                JSONObject jSonVCodeData = verificationCodeJsonArray.getJSONObject(i);
                                vCode.add(jSonVCodeData.getString("vCode"));
                                Log.e("Vcode",""+vCode.toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                }
            }, new Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    progressDialog.dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            /*gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);*/
            progressDialog.dismiss();
            Toast.makeText(LoginActivity.this,"Please check Internet Connectivity",Toast.LENGTH_LONG).show();
        }
    }

    private boolean validateOtp() {
        String strVCode = verification_code.getText().toString();
        if (verification_code.getText().toString().trim().isEmpty()) {
            verification_code.requestFocus();
            verification_code.setError("Please enter verification code");

            return false;
        } else if (!vCode.contains(strVCode)) {
            verification_code.requestFocus();
            verification_code.setError("Please enter Valid verification code");
            return false;
        }
        return true;
    }

    private void popupVerifyCode(){
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
        builder1.setMessage("\n \t Please enter valid code, \n Your verification code is in valid");
        builder1.setCancelable(true);
        builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

   public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("HashKey", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("HashKey", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("HashKey", "printHashKey()", e);
        }
    }

    private void loginValidation() { //TODO Server method here
        if (Connectivity.isConnected(LoginActivity.this)) {
            String mobNumber = login_mobile.getText().toString();
            String strPassword = usepass.getText().toString();

            JSONObject params = new JSONObject();
            try {
                params.put("mob_number", mobNumber);
                params.put("password", strPassword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckPassword, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String result = response.getString("posts");
                        if (result.equals("true")) {
                            DBHelper db = new DBHelper(LoginActivity.this);
                          //  db.updateUserInfoAgain(emailId.get(0), fName.get(0), address.get(0), contactNo.get(0));

                            GateWay gateWay = new GateWay(LoginActivity.this);
                            String City = gateWay.getCity();
                            String Area = gateWay.getArea();

                            if (City == null && Area == null) {
                                Intent intent = new Intent(LoginActivity.this, LocationActivity.class);
                                intent.putExtra("tag", "registration");
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(LoginActivity.this, BaseActivity.class);
                                intent.putExtra("tag", "");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, "Please enter valid password", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(LoginActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            /*gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);*/
            Toast.makeText(LoginActivity.this,"Please Check Internet Connectivity",Toast.LENGTH_LONG).show();
        }
    }
}
