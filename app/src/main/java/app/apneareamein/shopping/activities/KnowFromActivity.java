package app.apneareamein.shopping.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.KnownFromAdaptor;
import app.apneareamein.shopping.adapters.ReferralPointsAdaptor;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.KnownFrom;
import app.apneareamein.shopping.model.ReferPoints;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import dmax.dialog.SpotsDialog;

public class KnowFromActivity extends AppCompatActivity {

    RecyclerView rv_known_from;
    LinearLayoutManager layoutManager;
    Context mContext;
    KnownFromAdaptor knownFromAdaptor;
    ArrayList<ReferPoints> arrayList;
    public static final String MY_PREFS_NAME = "PICoDEL";
    android.app.AlertDialog progressDialog2;
    GateWay gateWay;
    String user_id="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_know_from);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Known From Activity");
        progressDialog2 = new SpotsDialog.Builder().setContext(this).build();
        mContext = KnowFromActivity.this;
        arrayList = new ArrayList<>();
        rv_known_from = findViewById(R.id.rv_known_from);



        rv_known_from.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this,1);
        //layoutManager = new GridLayoutManager(this,2);
        rv_known_from.setLayoutManager(layoutManager);

        arrayList = new ArrayList<>();
        knownFromAdaptor = new KnownFromAdaptor(arrayList,this);
        rv_known_from.setAdapter(knownFromAdaptor);


        getReferralPoints();
    }


    private void getReferralPoints() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) {
            // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(mContext);
            progressDialog2.show();

            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String user_id = prefs.getString("user_id", "");

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                //params.put("user_id", user_id);
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlget_Known_From, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("ReferInsertRes", "" + response.toString());
                    progressDialog2.dismiss();
                    if (response.isNull("posts")) {

                        // progressDialog2.dismiss();
                    }else{


                        try {
                            JSONArray postsJsonArray = response.getJSONArray("posts");

                            Log.e("points_RS:",""+postsJsonArray.toString());

                            for (int i = 0; i < postsJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = postsJsonArray.getJSONObject(i);

                                ReferPoints points = new ReferPoints();
                                points.setAmount(jSonClassificationData.getString("message"));
                                points.setOrder_id(jSonClassificationData.getString("id"));//order_id

                                arrayList.add(points);
                            }

                            knownFromAdaptor.notifyDataSetChanged();
                        }catch (Exception e){

                        }

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    progressDialog2.dismiss();
                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(this,"Please check the Internet Connection",Toast.LENGTH_LONG).show();
        }
    }

    private void KnownInsert() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(mContext);
            //gateWay.progressDialogStart();
            progressDialog2.show();
            try {
                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                user_id = prefs.getString("user_id", "");

            }catch (Exception e){
                e.getMessage();
            }

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("user_id", user_id);
                params.put("message", "");


                Log.e("user_ref_Param:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlinsert_Known_From, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("ReferInsertRes", "" + response.toString());

                    if (response.isNull("posts")) {

                        progressDialog2.dismiss();
                    }else{

                        try {
                            String res = response.getString("posts");
                            Log.e("insert_res:",""+res);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog2.dismiss();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    progressDialog2.dismiss();
                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
           Toast.makeText(mContext,"Please Check Internet Connection",Toast.LENGTH_LONG).show();
        }
    }


}
