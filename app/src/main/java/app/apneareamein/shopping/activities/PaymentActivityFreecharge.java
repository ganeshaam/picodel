package app.apneareamein.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.utils.GateWay;

public class PaymentActivityFreecharge extends AppCompatActivity {


    Button btn_home;
    TextView tvTitle;
    WebView wb_payment_freecharge;
    public static final String MY_PREFS_NAME = "PICoDEL";
    Context mContext;
    String order_id="",strFinalAmount="",strName="",strEmail="",strContact="",
            merchantTxnId ="",Freecharge_checksum="";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_freecharge);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Payment Activity");

        mContext = PaymentActivityFreecharge.this;

        btn_home = findViewById(R.id.btn_home);
        wb_payment_freecharge = findViewById(R.id.wb_payment_freecharge);
        WebSettings webSettings = wb_payment_freecharge.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wb_payment_freecharge.getSettings().setJavaScriptEnabled(true);
        wb_payment_freecharge.getSettings().setLoadsImagesAutomatically(true);
        wb_payment_freecharge.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        //webSettings.setJavaScriptEnabled(true);
        wb_payment_freecharge.getSettings().setLoadWithOverviewMode(true);
        wb_payment_freecharge.getSettings().setUseWideViewPort(true);
        wb_payment_freecharge.getSettings().setBuiltInZoomControls(true);
        wb_payment_freecharge.getSettings().setDomStorageEnabled(true);
        wb_payment_freecharge.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wb_payment_freecharge.getSettings().setAllowFileAccess(true);
        wb_payment_freecharge.getSettings().setAllowContentAccess(true);
        wb_payment_freecharge.getSettings().setAllowFileAccessFromFileURLs(true);
        wb_payment_freecharge.getSettings().setAllowUniversalAccessFromFileURLs(true);

        if (18 < Build.VERSION.SDK_INT ){
            //18 = JellyBean MR2, KITKAT=19
            wb_payment_freecharge.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        }

        GateWay gateWay = new GateWay(mContext);
        strEmail = gateWay.getUserEmail();
        strContact = gateWay.getContact();
        strName = gateWay.getUserName();

        Intent paymnt_intents= getIntent();
        Bundle bundle = paymnt_intents.getExtras();

        if(bundle!=null)
        {
            strFinalAmount = (String) bundle.get("strFinalAmount");
            strFinalAmount = paymnt_intents.getStringExtra("strFinalAmount");
            order_id = paymnt_intents.getStringExtra("order_id");
            Log.e("strFinalAmount",""+strFinalAmount);

        }
        merchantTxnId = "picodel_android_"+order_id;
        try {
            Freecharge_checksum = generateChecksum();
            Log.e("Freecharge_checksum",""+Freecharge_checksum);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //wb_payment_freecharge.loadUrl("https://www.picodel.com/And/shopping/AppAPI/get_freecharge_payment_sucess.php?order_id="+order_id+"&merchantTxnId="+merchantTxnId+"&status=failed");
        getWebviewFreeCharge();
        //testWebview();
        //testWebview2();

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PaymentActivityFreecharge.this, BaseActivity.class);
                intent.putExtra("tag", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


    }

    // Generate Valid Checksum working fine
    public String generateChecksum()
            throws Exception {
       // String merchantKey = "95205790-466c-42b9-8daa-b83c19bdef79"; // sandbox key
       String merchantKey = "23c9668b-6ea4-4622-8c0b-957291f0b6ce"; // production key
        String jsonString2 ="";
        JSONObject  jsonObject = new JSONObject();
        try {
            //jsonObject.put("amount", "211.65");
            jsonObject.put("amount", strFinalAmount);//strFinalAmount //1.21
            jsonObject.put("channel", "ANDROID");
            jsonObject.put("currency", "INR");
            jsonObject.put("customNote", "hello_freecharge");
            jsonObject.put("customerName", strName);
            //jsonObject.put("customerName",strName);
            //jsonObject.put("email", "panderinath.aam@gmail.com");
            jsonObject.put("email", strEmail);
            //jsonObject.put("furl", "https://www.google.com/");
            jsonObject.put("furl", "https://www.picodel.com/And/shopping/AppAPI/get_freecharge_payment_sucess.php?order_id="+order_id+"&merchantTxnId="+merchantTxnId+"&status=failed");
            jsonObject.put("merchantId", "5YkbjlyQBKjHq1");//
            jsonObject.put("merchantTxnId", "picodel_android_"+order_id);
            //jsonObject.put("mobile", "7276201058");
            jsonObject.put("mobile", strContact);
            jsonObject.put("os", "ubuntu-14.04");
            jsonObject.put("productInfo", "auth");
            //jsonObject.put("surl", "https://picodel.com/");
            jsonObject.put("surl", "https://www.picodel.com/And/shopping/AppAPI/get_freecharge_payment_sucess.php?order_id="+order_id+"&merchantTxnId="+merchantTxnId+"&status=success");
            jsonString2 = jsonObject.toString();
            jsonString2 = jsonString2.replace("\\","");
            Log.e("JsonStringparam:",""+jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        MessageDigest md;
        String plainText = jsonString2.concat(merchantKey);
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(); //
        }
        md.update(plainText.getBytes(Charset.defaultCharset()));
        byte[] mdbytes = md.digest();
// convert the byte to hex format method 1
        StringBuffer checksum = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
            checksum.append(Integer.toString((mdbytes[i] & 0xff) +
                    0x100, 16).substring(1));
        }
        return checksum.toString();
    }


    private void getWebviewFreeCharge(){
       // String url = "https://checkout-sandbox.freecharge.in/api/v1/co/pay/init";  // sandbox key url
        String url = "https://checkout.freecharge.in/api/v1/co/pay/init"; // Production Key url
        try {  //strFinalAmount
            String postData = "amount=" + strFinalAmount + "&channel=ANDROID" + "&currency=INR"
                    + "&customNote=" + URLEncoder.encode("hello_freecharge", "UTF-8")
                    +"&customerName="+ URLEncoder.encode(strName, "UTF-8")
                    +"&email="+strEmail     //+ URLEncoder.encode(strEmail, "UTF-8")
                    //+"&furl="+ URLEncoder.encode("https://www.google.com/", "UTF-8")
                    +"&furl="+ URLEncoder.encode("https://www.picodel.com/And/shopping/AppAPI/get_freecharge_payment_sucess.php?order_id="+order_id+"&merchantTxnId="+merchantTxnId+"&status=failed", "UTF-8")
                    +"&merchantId="+ URLEncoder.encode("5YkbjlyQBKjHq1", "UTF-8")
                    +"&merchantTxnId="+ URLEncoder.encode("picodel_android_"+order_id, "UTF-8")
                    +"&mobile="+ URLEncoder.encode(strContact, "UTF-8")
                    +"&os="+ URLEncoder.encode("ubuntu-14.04", "UTF-8")
                    +"&productInfo="+ URLEncoder.encode("auth", "UTF-8")
                    +"&surl="+ URLEncoder.encode("https://www.picodel.com/And/shopping/AppAPI/get_freecharge_payment_sucess.php?order_id="+order_id+"&merchantTxnId="+merchantTxnId+"&status=success", "UTF-8")
                    //+"&surl=https://picodel.com/" //URLEncoder.encode("https://picodel.com/", "UTF-8")
                    +"&checksum="+Freecharge_checksum;
            wb_payment_freecharge.postUrl(url, postData.getBytes());
            Log.e("WebviewUrl:",""+url+postData);

        }catch (Exception e){
            e.printStackTrace();
            Log.e("WebviewExp",""+e.getMessage());
            Log.e("WebviewExp",""+e);
        }


    }

    public String generateTxnChecksum(String jsonString, String merchantKey)
            throws Exception {
        //merchantKey = "95205790-466c-42b9-8daa-b83c19bdef79"; // sandbox key
        merchantKey = "23c9668b-6ea4-4622-8c0b-957291f0b6ce"; // production key
        String jsonString2 ="";
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("merchantId", "5YkbjlyQBKjHq1");//
            jsonObject.put("merchantTxnId", "picodel_orderid_12621");
            jsonObject.put("txnType", "CUSTOMER_PAYMENT");

            Log.e("JsonStringparam:",""+jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        MessageDigest md;
        String plainText = jsonString2.concat(merchantKey);
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(); //
        }
        md.update(plainText.getBytes(Charset.defaultCharset()));
        byte[] mdbytes = md.digest();
// convert the byte to hex format method 1
        StringBuffer checksum = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
            checksum.append(Integer.toString((mdbytes[i] & 0xff) +
                    0x100, 16).substring(1));
        }
        return checksum.toString();
    }

   public void testWebview(){
        String url = "https://checkout-sandbox.freecharge.in/api/v1/co/pay/init";
        String UrlData ="amount=211.65&channel=WEB&currency=INR&customNote=please+make+it+fast...%3B+%21&customerName=pramod&email=panderinath.aam%40gmail.com&furl=https%3A%2F%2Fpicodel.com%2F&merchantId=5YkbjlyQBKjHq1&merchantTxnId=picodel2020&mobile=7276201058&os=ubuntu-14.04&productInfo=auth&surl=https%3A%2F%2Fwww.picodel.com%2Fform%2Ftermsandcondition&checksum=c469a388fb23f55972266eb3f19615a40c7ac8012efde187944b2382a289d0dd";
       Log.e("URLbYTES:",""+UrlData.getBytes());
       wb_payment_freecharge.postUrl(url, UrlData.getBytes());

   }
   public void testWebview2(){
        String url = "https://checkout-sandbox.freecharge.in/api/v1/co/pay/init";
        String UrlData ="amount=1.00&channel=ANDROID&currency=INR&customNote=hello_freecharge&customerName=Pramod+Test&email=Ganeshb.aam@gmail.com&furl=https%3A%2F%2Fwww.google.com%2F&merchantId=5YkbjlyQBKjHq1&merchantTxnId=picodel_android_12678&mobile=7276201058&os=ubuntu-14.04&productInfo=auth&surl=https://picodel.com/&checksum=aedcbb69ab552355f2f2e43cdf0b6002effdda8453fc776ff42e6aa8a102a50e";
       Log.e("URLbYTES:",""+UrlData.getBytes());
       wb_payment_freecharge.postUrl(url, UrlData.getBytes());

   }
}
