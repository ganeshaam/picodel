package app.apneareamein.shopping.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.GateWay;

public class RedeemPoints extends AppCompatActivity {

    TextView tvTitle, txtCashBackPoints;
    String shop_id, shop_name, contact_no, cash_back_points, adjustment_in_percentage;
    EditText editMobileNo, editPurchaseAmt;
    Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_points);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            shop_id = bundle.getString("shop_id");
            shop_name = bundle.getString("shop_name");
            contact_no = bundle.getString("contact_no");
            cash_back_points = bundle.getString("cash_back_points");
            adjustment_in_percentage = bundle.getString("adjustment_in_percentage");
        }
        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_redeem_points);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtCashBackPoints = findViewById(R.id.txtCashBackPoints);
        txtCashBackPoints.setText("You have ( " + cash_back_points + " ) Cash Back Points");

        editMobileNo = findViewById(R.id.editMobileNo);
        editMobileNo.setText(contact_no);

        editPurchaseAmt = findViewById(R.id.editPurchaseAmt);

        btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editPurchaseAmt.getText().toString().trim().equals("")) {
                    Toast.makeText(RedeemPoints.this, "Please enter purchase amount", Toast.LENGTH_SHORT).show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RedeemPoints.this);
                    builder.setTitle("Please Confirm Purchase Amount INR - " + editPurchaseAmt.getText().toString() + "/-");
                    builder.setMessage("If you want proceed, please click on Yes")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    updateShoppingDetails();
                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
    }

    private void updateShoppingDetails() {
        final GateWay gateWay = new GateWay(RedeemPoints.this);
        gateWay.progressDialogStart();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shop_contact", editMobileNo.getText().toString());
            jsonObject.put("purchase_or_fare", editPurchaseAmt.getText().toString());
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("type", "use");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdateShoppingDetails, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                gateWay.progressDialogStop();
                try {
                    switch (response.getString("key")) {
                        case "promotional": {
                            String cash_back_points = response.getString("message");

                            AlertDialog.Builder builder = new AlertDialog.Builder(RedeemPoints.this);
                            builder.setTitle("Successfully Applied");
                            builder.setMessage(cash_back_points)
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            onBackPressed();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                            break;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                gateWay.progressDialogStop();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
