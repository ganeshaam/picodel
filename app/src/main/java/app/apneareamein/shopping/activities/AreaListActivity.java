package app.apneareamein.shopping.activities;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.ColorGenerator;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.TextDrawable;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class AreaListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerViewAreaList;
    private AreaListAdapter areaListAdapter;
    private List<InnerMovie> innerMovies;
    private String strCity;
    private final String class_name = this.getClass().getSimpleName();
    private LinearLayout LayNoResult;
    private EditText et;
    RelativeLayout addToCartMainLayout;
    private LinearLayout Main_Layout_NoInternet;
    private Network_Change_Receiver myReceiver;
    private TextView txtNoConnection;

    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_list);

        myReceiver = new Network_Change_Receiver();

        txtNoConnection = findViewById(R.id.txtNoConnection);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        addToCartMainLayout = findViewById(R.id.addToCartMainLayout);
        LayNoResult = findViewById(R.id.LayNoResult);

        recyclerViewAreaList = findViewById(R.id.areaListRecyclerview);
        recyclerViewAreaList.setNestedScrollingEnabled(false);
        recyclerViewAreaList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AreaListActivity.this);
        recyclerViewAreaList.setLayoutManager(linearLayoutManager);

        if (getIntent().getExtras() != null) {
            strCity = getIntent().getStringExtra("city");
        }

        getArea();
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                hideKeyBoard();
                addToCartMainLayout.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                hideKeyBoard();
                Main_Layout_NoInternet.setVisibility(View.GONE);
                addToCartMainLayout.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void hideKeyBoard() {
        View view = getCurrentFocus();  // Check if no view has focus:
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyBoard();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            hideKeyBoard();
            String query = intent.getStringExtra(SearchManager.QUERY);
            et.setText(query);
            et.setSelection(et.getText().length());
            doSearch(query);
        }
    }

    private void doSearch(String query) {
        UserTracking UT = new UserTracking(UserTracking.context);
        UT.user_tracking(class_name + ": searched for keyword: " + query, AreaListActivity.this);
        Toast.makeText(this, "Searching for: " + query, Toast.LENGTH_LONG).show();

        areaListAdapter.filter(query);
    }

    private void getArea() { //TODO Server method here
        if (Connectivity.isConnected(AreaListActivity.this)) {
            final GateWay gateWay = new GateWay(AreaListActivity.this);
            gateWay.progressDialogStart();

            innerMovies = new ArrayList<>();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("tag", "area");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCity, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        JSONArray jsonArray = response.getJSONArray("searchResult");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jSonSearchData = jsonArray.getJSONObject(i);
                            innerMovies.add(new InnerMovie(jSonSearchData.getString("searchResult")));
                            areaListAdapter = new AreaListAdapter(innerMovies);
                            recyclerViewAreaList.setAdapter(areaListAdapter);
                        }
                        SearchView editsearch = findViewById(R.id.search);
                        editsearch.setIconifiedByDefault(false);
                        et = editsearch.findViewById(R.id.search_src_text); //Find EditText view
                        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
                        editsearch.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                        editsearch.setOnQueryTextListener(AreaListActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(AreaListActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        areaListAdapter.filter(newText);
        return false;
    }

    private class AreaListAdapter extends RecyclerView.Adapter<MainViewHolder> {

        List<InnerMovie> areaArrayList;
        private final ArrayList<InnerMovie> arraylist;
        String searchString = "";

        AreaListAdapter(List<InnerMovie> innerMovies) {
            this.areaArrayList = innerMovies;
            this.arraylist = new ArrayList<>();
            this.arraylist.addAll(areaArrayList);
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_area_list, parent, false);
            return new MainViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
            int pos = holder.getAdapterPosition();
            final InnerMovie innerMovie = areaArrayList.get(pos);
            holder.tvArea.setText(innerMovie.getArea());
            String name = innerMovie.getArea().toLowerCase(Locale.getDefault());

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connectivity.isConnected(AreaListActivity.this)) {
                        UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": selected area: " + innerMovie.getArea(), AreaListActivity.this);

                        hideKeyBoard();

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", innerMovie.getArea());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        GateWay gateWay = new GateWay(AreaListActivity.this);
                        gateWay.displaySnackBar(addToCartMainLayout);

                    }
                }
            });

            if (name.contains(searchString)) {
                int startPos = name.indexOf(searchString);
                int endPos = startPos + searchString.length();

                Spannable spanString = Spannable.Factory.getInstance().newSpannable(holder.tvArea.getText());
                spanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.ColorAccentDeepOrange)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvArea.setText(spanString);
            }

            String firstLetter = String.valueOf(innerMovie.getArea().charAt(0));
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            int color = generator.getColor(innerMovie.getArea());
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect(firstLetter, color); // radius in px
            holder.imageView.setImageDrawable(drawable);
        }

        @Override
        public int getItemCount() {
            return areaArrayList.size();
        }

        public void add(InnerMovie innerMovie) {
            areaArrayList.add(innerMovie);
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase();
            this.searchString = charText;

            areaArrayList.clear();
            if (charText.length() == 0) {
                areaArrayList.addAll(arraylist);
            } else {
                for (InnerMovie s : arraylist) {
                    if (s.getArea().toLowerCase().contains(charText)) {
                        areaArrayList.add(s);
                    }
                }
            }
            notifyDataSetChanged();

            if (areaArrayList.size() == 0) {
                recyclerViewAreaList.setVisibility(View.GONE);
                LayNoResult.setVisibility(View.VISIBLE);
            } else {
                recyclerViewAreaList.setVisibility(View.VISIBLE);
                LayNoResult.setVisibility(View.GONE);
            }
        }
    }

    private class MainViewHolder extends RecyclerView.ViewHolder {

        private final View mView;
        private final TextView tvArea;
        private final ImageView imageView;

        private MainViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            tvArea = itemView.findViewById(R.id.txtArea);
            imageView = itemView.findViewById(R.id.image_view);
        }
    }

    private class InnerMovie {

        final String area;

        InnerMovie(String area) {
            this.area = area;
        }

        public String getArea() {
            return area;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(AreaListActivity.this)) {
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, AreaListActivity.this);
        }
    }
}
