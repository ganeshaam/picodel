package app.apneareamein.shopping.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.provider.Settings;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.ShopListAdapter;
import app.apneareamein.shopping.adapters.TermsCondAdaptor;
import app.apneareamein.shopping.fragments.Order_Details;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.model.ShopModel;
import app.apneareamein.shopping.model.TermCondModel;
import app.apneareamein.shopping.sync.SyncWishListItems;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.MySingleton;
import app.apneareamein.shopping.utils.ShowcaseViewBuilder;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class AddToCart extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener{

    public static int size = 0;
    private final String class_name = this.getClass().getSimpleName();
    private RelativeLayout addToCartMainLayout;
    private RelativeLayout addToCartRelativeLayout;
    private int cnt, presentQty, holder2iCount;
    private String shopId,nearbyShopID="NA";
    private DialogPlus dialog, dialogPlus;
    private String pId;
    private String offerCode;
    private String strContact;
    private String strEmail;
    private String final_price, strMsg;
    private String strType;
    private TextView tvPrice, tvCount, tvTitle, tvMsg;
    private Button btnShopNow;
    private RecyclerView mRecyclerView;
    private CardAdapter cardAdapter;
    private CardAdapter_OutOfStock outofstock_cardAdapter;
    private RelativeLayout relativeLayoutEmptyCart, msgLayout;
    private LinearLayout relativeLayoutPrice;
    private MenuItem item;
    private LinearLayout Main_Layout_NoInternet;
    private List<Cart_Item> innerMovies1;
    private List<Normal_Cart_Item> innerMovies2;
    private Button tvPlaceOrder;
    private EditText editAddress;
    private LinearLayout totalLay;
    private ShowcaseViewBuilder showcaseViewBuilder;
    private String product_total;
    private String Saving_Amount = null;
    private TextView tvTotalSaving,tv_add_more_items;
    private BroadcastReceiver myReceiver;
    private TextView txtNoConnection;
    String product_maincat="",checkout_status="";
    private ProgressBar simpleProgressBar;
    private ArrayList<TermCondModel> termCondModelArrayList;
    TermsCondAdaptor termsCondAdaptor;
    ShopListAdapter shopListAdapter;
    ArrayList <String> condition_types;
    ArrayList<ShopModel> orderArrayList;
    ArrayList<String> arrayshoplist;
    public static final String MY_PREFS_NAME = "PICoDEL";

    private String v_city,v_state,Zone_Area,sessionMainCat="NA";
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final String TAG = "AddToCart";

    String  latitude,longitude;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager locationManager,mLocationManager;
    private LocationRequest mLocationRequest;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart);



        GateWay gateWay = new GateWay(AddToCart.this);
        strContact = gateWay.getContact();
        strEmail = gateWay.getUserEmail();

        termCondModelArrayList = new ArrayList<>();
        condition_types = new ArrayList<>();
        orderArrayList = new ArrayList<>();
        arrayshoplist = new ArrayList<>();
        //getShopList();
        get_terms_cond();
        init();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");
        v_city = prefs.getString("v_city", "");
        v_state = prefs.getString("v_state", "");
        sessionMainCat = prefs.getString("sessionMainCat", "NA");

        if(v_city.isEmpty()||v_city == null){

            Intent bundle = new Intent(AddToCart.this, LocationActivity.class);
            bundle.putExtra("tag", "homepage");
            startActivity(bundle);
        }
        btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                    Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    GateWay gateWay = new GateWay(AddToCart.this);
                    gateWay.displaySnackBar(addToCartMainLayout);
                }
            }
        });

        tv_add_more_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                    Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    GateWay gateWay = new GateWay(AddToCart.this);
                    gateWay.displaySnackBar(addToCartMainLayout);
                }
            }
        });

        fetchUserAddress();

        /*Location Initialization*/

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        checkLocation(); //check whether location service is enable or not in your  phone

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }


    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private void init() {
        myReceiver = new Network_Change_Receiver();

        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_add_to_cart);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtNoConnection = findViewById(R.id.txtNoConnection);
        addToCartRelativeLayout = findViewById(R.id.addToCartRelativeLayout);
        addToCartMainLayout = findViewById(R.id.addToCartMainLayout);
        totalLay = findViewById(R.id.totalLay);
        tvPlaceOrder = findViewById(R.id.btnPlaceOrder);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

        relativeLayoutEmptyCart = findViewById(R.id.relativeLayoutEmptyCart);
        relativeLayoutPrice = findViewById(R.id.relativeLayoutPrice);
        msgLayout = findViewById(R.id.msgLayout);
        tvPrice = findViewById(R.id.txtPrice);
        tvTotalSaving = findViewById(R.id.txtTotalSaving);
        tvCount = findViewById(R.id.txtCount);
        tvMsg = findViewById(R.id.txtMsg);
        btnShopNow = findViewById(R.id.btnShopNow);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        tv_add_more_items = findViewById(R.id.tv_add_more_items);
        editAddress = findViewById(R.id.editAddress);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        tvPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(AddToCart.this)) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + " clicked on CHECKOUT button", AddToCart.this);*/
                    final GateWay gateWay = new GateWay(AddToCart.this);

                    String urlPlaceOrderLimit = null;
                    try {
                        if(sessionMainCat.equalsIgnoreCase("Both")){

                        }else {
                            sessionMainCat = product_maincat;
                        }
                        urlPlaceOrderLimit = ApplicationUrlAndConstants.urlPlaceOrderLimit
                                +"?flag=checkout"

                                +"&product_maincat="+ URLEncoder.encode(sessionMainCat,"utf-8")
                                +"&final_price="+final_price
                                +"&contact="+gateWay.getContact()
                                +"&city="+v_city
                                +"&state="+URLEncoder.encode(v_state,"utf-8");

                        Log.e("urlPlaceOrderLimit",""+urlPlaceOrderLimit);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    StringRequest requestPlaceOrderLimit = new StringRequest(Request.Method.GET,
                            urlPlaceOrderLimit,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String status = jsonObject.getString("status");
                                        Log.e("PlaceOrderLimitRS",response);
                                        if(status.equals("true")){
                                            JSONArray jsonArray = jsonObject.getJSONArray("homemessage");
                                            JSONObject objectMessage = jsonArray.getJSONObject(0);

                                            String messageG = objectMessage.getString("messageG");
                                            String messageFre = objectMessage.getString("messageFre");
                                            String expectedDel = objectMessage.getString("expected");
                                            String oklimit = objectMessage.getString("oklimit");
                                            popupLimitbyCat2(messageG, messageFre,expectedDel,oklimit);

                                        }else if(status.equals("false")){

                                            /*if(checkout_status.equalsIgnoreCase("active")){
                                                termsConditonDialog(); // checkout_status
                                            }else {*/
                                            String localFinalPrice = tvPrice.getText().toString();
                                            Intent intent = new Intent(AddToCart.this, Order_Details_StepperActivity.class);
                                            intent.putExtra("finalPrice", localFinalPrice);
                                            startActivity(intent);
                                            //}

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                    MySingleton.getInstance(AddToCart.this).addToRequestQueue(requestPlaceOrderLimit);

                 /*   if (product_maincat.equalsIgnoreCase("Grocery") && Double.parseDouble(final_price) < 500) {
                        Double chkPrice = 500 - Double.parseDouble(final_price);
                        //popupLimitbyCat(product_maincat, final_price,chkPrice);
                        popupLimitbyCat2(product_maincat, final_price,chkPrice);
                    } else if (product_maincat.equalsIgnoreCase("Fruits and Veggies") && Double.parseDouble(final_price) < 300) {
                        Double chkPrice =  300 - Double.parseDouble(final_price);
                        //popupLimitbyCat(product_maincat, final_price,chkPrice);
                        popupLimitbyCat2(product_maincat, final_price,chkPrice);
                    } else
                    {       // StopOrder(); chk order limit
                        try {
                            String localFinalPrice = tvPrice.getText().toString();
                            Intent intent = new Intent(AddToCart.this, Order_Details_StepperActivity.class);
                            intent.putExtra("finalPrice", localFinalPrice);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                       }*/
                } else {
                    GateWay gateWay = new GateWay(AddToCart.this);
                    gateWay.displaySnackBar(addToCartMainLayout);
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            Log.d("Location",mLocation.getLatitude()+" "+mLocation.getLongitude());
            latitude = String.valueOf(mLocation.getLatitude());
            longitude = String.valueOf(mLocation.getLongitude());
            Toast.makeText(this, "Location :"+latitude +""+longitude, Toast.LENGTH_SHORT).show();
            //mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
              //Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            Log.e("Location:",""+location.getLatitude()+""+location.getLongitude());
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());


            try {
                Geocoder gcd = new Geocoder(AddToCart.this, Locale.getDefault());
                List<Address> addresses = null;
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    Log.e("AddressDetails:",""+addresses.toString());
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                    String area = addresses.get(0).getAdminArea(); // Only if available else return NULL
                    String subAdminArea = addresses.get(0).getSubAdminArea(); // Only if available else return NULL

                    Log.d(TAG, "getAddress:  address" + address);
                    Log.d(TAG, "getAddress:  city" + city);
                    Log.d(TAG, "getAddress:  state" + state);
                    Log.d(TAG, "getAddress:  postalCode" + postalCode);
                    Log.d(TAG, "getAddress:  knownName" + knownName);
                    Log.d(TAG, "getAddress:  area" + area);
                    Log.d(TAG, "subAdminArea:  area" + addresses.get(0).getSubLocality());
                }
                else {
                    // do your stuff
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


           /* val MY_READ_EXTERNAL_REQUEST : Int = 1
            if (checkSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_READ_EXTERNAL_REQUEST)
            }*/

        }
    }

    protected void startLocationUpdates() {

        mLocationRequest = LocationRequest.create();//.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                // Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() +", "+place.getLatLng());
                if (place != null) {
                    LatLng latLng = place.getLatLng();
                    latitude  = String.valueOf(latLng.latitude);
                    longitude =  String.valueOf(latLng.longitude);
                    // Log.d("latlong",latitude+" "+mStringLongitude);
                    //etAddress.setText(place.getName());
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                if (item != null) {
                    item.setVisible(false);
                }
                relativeLayoutEmptyCart.setVisibility(View.GONE);
                addToCartMainLayout.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                addToCartMainLayout.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);

                //check internet connectivity available or not
                //if internet connection is available then we check there is
                //any product is in out of stock from this method
                fetch_outofstock_item();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void StopOrder() { //TODO Server method here
        final GateWay gateWay = new GateWay(AddToCart.this);
        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);
            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlStopOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            if (Connectivity.isConnected(AddToCart.this)) {
                                try {
                                    String localFinalPrice = tvPrice.getText().toString();
                                    Intent intent = new Intent(AddToCart.this, Order_Details_StepperActivity.class);
                                    intent.putExtra("finalPrice", localFinalPrice);
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        } else {
                            JSONArray array = response.getJSONArray("posts");
                            JSONObject jsonObject = array.getJSONObject(0);

                            String reason = jsonObject.getString("reason");
                            String image = jsonObject.getString("image");

                            dialog = DialogPlus.newDialog(AddToCart.this)
                                    .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_stop_order))
                                    .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                    .setGravity(Gravity.CENTER)
                                    .setCancelable(false)
                                    .setPadding(20, 20, 20, 20)
                                    .create();
                            dialog.show();
                            ImageView imgStopOrder = (ImageView) dialog.findViewById(R.id.imgCashBack);
                            RelativeLayout stop_orderLayout = (RelativeLayout) dialog.findViewById(R.id.stop_orderLayout);
                            TextView tvMsg = (TextView) dialog.findViewById(R.id.txtMessage);
                            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                            if (reason.equals("")) {
                                tvMsg.setVisibility(View.GONE);
                            } else {
                                tvMsg.setVisibility(View.VISIBLE);
                                tvMsg.setText(reason);
                            }
                            if (image.equals("")) {
                                stop_orderLayout.setVisibility(View.GONE);
                            } else {
                                stop_orderLayout.setVisibility(View.VISIBLE);

                                //TODO here show app update image setup to glide
                                /*Glide.with(AddToCart.this)
                                        .load(image)
                                        .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(imgStopOrder);*/
                                Picasso.with(AddToCart.this)
                                        .load(image)
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(imgStopOrder);
                            }

                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(AddToCart.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(addToCartMainLayout);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_cart, menu);
        item = menu.findItem(R.id.action_empty_cart);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_empty_cart:
                EmptyCartAlertDialog();
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddToCart.this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(AddToCart.this)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteAllProductFromCartItem() { //TODO Server method here
        if (Connectivity.isConnected(AddToCart.this)) {
            final GateWay gateWay = new GateWay(AddToCart.this);
            // gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
                params.put("tag", "delete_all_items");
                Log.e("delete_all_items",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (response.isNull("posts")) {
                    } else {
                        item.setVisible(false);

                        DBHelper db = new DBHelper(AddToCart.this);
                        db.deleteOnlyCartTable();

                        tvPrice.setText("0");
                        addToCartMainLayout.setVisibility(View.GONE);
                        relativeLayoutEmptyCart.setVisibility(View.VISIBLE);

                     /*   btnShopNow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                    Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                    intent.putExtra("tag", "");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    gateWay.displaySnackBar(addToCartMainLayout);
                                }
                            }
                        });*/
                    }
                   // gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    error.printStackTrace();

                    //GateWay gateWay = new GateWay(AddToCart.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    private void fetch_cart_item() { //TODO Server method here
        if (Connectivity.isConnected(AddToCart.this)) {
            final RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params1.addRule(RelativeLayout.ABOVE, R.id.relativeLayoutPrice);
            mRecyclerView.setLayoutParams(params1);
            innerMovies2 = new ArrayList<>();

            final GateWay gateWay = new GateWay(AddToCart.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String Zone_Area = prefs.getString("Zone_Area", "");
            String v_city = prefs.getString("v_city", "");
            String v_state = prefs.getString("v_state", "");


            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("areaname", Zone_Area);
                params.put("v_city", v_city);
                params.put("v_state", v_state);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetMyCartItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("CartListItems:",""+response.toString());

                        if (response.isNull("posts")) {
                            tvTitle.setText(R.string.title_activity_add_to_cart);
                            item.setVisible(false);
                            addToCartMainLayout.setVisibility(View.GONE);
                            relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                        } else {
                            tvTitle.setText(R.string.title_activity_add_to_cart);
                            item.setVisible(true);
                            msgLayout.setVisibility(View.GONE);
                            addToCartMainLayout.setVisibility(View.VISIBLE);

                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");

                            final_price = response.getString("Final_Price");
                            Log.e("FinalAmount:",""+final_price);
                            Saving_Amount = response.getString("productSavingOnMrp");
                            //String product_maincat="";

                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jsonCart = mainClassificationJsonArray.getJSONObject(i);

                                innerMovies2.add(new Normal_Cart_Item(jsonCart.getString("normal_cart_shop_id"),
                                        jsonCart.getString("product_id"), jsonCart.getString("product_name"),
                                        jsonCart.getString("normal_shop_name"), Double.parseDouble(jsonCart.getString("product_price")),
                                        jsonCart.getString("product_qty"), jsonCart.getString("product_image"),
                                        jsonCart.getString("type"), jsonCart.getString("available_product_quantity"),
                                        jsonCart.getString("allowed_qty"), jsonCart.getString("hindi_name"),
                                        jsonCart.getString("product_size"), jsonCart.getString("p_name"),
                                        jsonCart.getString("is_supersaver"), jsonCart.getString("supersaver_price"),
                                        jsonCart.getString("supersaver_qty"), jsonCart.getString("proPrice"),
                                        Double.parseDouble(jsonCart.getString("product_mrp")), jsonCart.getString("product_discount"),jsonCart.getString("product_maincat"),
                                        Double.parseDouble (jsonCart.getString("product_total"))));
                                        product_maincat = jsonCart.getString("product_maincat");
                                cardAdapter = new CardAdapter(innerMovies2);
                                mRecyclerView.setAdapter(cardAdapter);

                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("product_maincat", product_maincat);
                                editor.apply();
                                editor.commit();

                            }
                           /* if(product_maincat.equalsIgnoreCase("Grocery")&& Double.parseDouble(final_price)<500) {
                                popupLimitbyCat(product_maincat, final_price);
                            }else if(product_maincat.equalsIgnoreCase("Fruits and Veggies")&& Double.parseDouble(final_price)<300){
                                popupLimitbyCat(product_maincat, final_price);
                            }*/
                            tvCount.setText("ITEMS" + " " + "(" + innerMovies2.size() + ")");
                            double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                          //  tvPrice.setText("Total: ₹ " + Double.toString(finalValue));
                            tvPrice.setText( Double.toString(finalValue));
                            if (!Saving_Amount.equals("0")) {
                                tvTotalSaving.setVisibility(View.VISIBLE);

                                SpannableStringBuilder builder = new SpannableStringBuilder();
                                SpannableString str1 = new SpannableString("Your total savings:");
                                str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.textColor)), 0, str1.length(), 0);
                                builder.append(str1);
                                SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                str2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 0, str2.length(), 0);
                                builder.append(str2);

                                tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                            }
                            displayTuto("1");
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(AddToCart.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().setPriority(Request.Priority.LOW);
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    private void displayTuto(String Tag) {
        if (Tag.equals("1")) {
            showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                    .setTargetView(totalLay)
                    .setBackgroundOverlayColor()
                    .singleUse("1")
                    .setRingColor()
                    .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                    .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                    .addCustomView(Gravity.CENTER_HORIZONTAL, "Total Amount & No. of Items !", "Here you can see your ITEM COUNT & TOTAL AMOUNT !", "Next")
                    .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
            showcaseViewBuilder.show();

            showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcaseViewBuilder.hide();
                    displayTuto("2");
                }
            });
        } else {
            showcaseViewBuilder = ShowcaseViewBuilder.init(this)
                    .setTargetView(tvPlaceOrder)
                    .setBackgroundOverlayColor()
                    .singleUse("2")
                    .setRingColor()
                    .setRingWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()))
                    .setMarkerDrawable(getResources().getDrawable(R.drawable.ic_top_arrow2), Gravity.TOP)
                    .addCustomView(Gravity.CENTER_HORIZONTAL, "Checkout !", "Tap on CHECKOUT to PLACE the ORDER !", "Got it")
                    .setCustomViewMargin((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));
            showcaseViewBuilder.show();

            showcaseViewBuilder.setClickListenerOnView(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcaseViewBuilder.hide();
                }
            });
        }
    }

    private void fetch_outofstock_item() { //TODO Server method here
        if (Connectivity.isConnected(AddToCart.this)) {
            RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params1.addRule(RelativeLayout.BELOW, R.id.msgLayout);
            addToCartMainLayout.setLayoutParams(params1);

            innerMovies1 = new ArrayList<>();

            final GateWay gateWay = new GateWay(AddToCart.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetMyCartOutOfStockItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            //check first in cart there is any product is out of stock or not
                            //if there is no product in out of stock then this method call
                            fetch_cart_item();
                        } else {
                            tvTitle.setText("Items unavailable");

                            item.setVisible(false);

                            msgLayout.setVisibility(View.VISIBLE);
                            tv_add_more_items.setVisibility(View.GONE);
                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");
                            strMsg = response.getString("msg");
                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jsonCart = mainClassificationJsonArray.getJSONObject(i);

                                innerMovies1.add(new Cart_Item(jsonCart.getString("normal_cart_shop_id"),
                                        jsonCart.getString("product_id"), jsonCart.getString("product_name"),
                                        jsonCart.getString("normal_shop_name"), jsonCart.getDouble("product_price")
                                        , jsonCart.getString("product_qty"), jsonCart.getString("product_image"),
                                        jsonCart.getString("type"), jsonCart.getString("available_product_quantity")));
                                outofstock_cardAdapter = new CardAdapter_OutOfStock(innerMovies1);
                                mRecyclerView.setAdapter(outofstock_cardAdapter);
                            }
                            tvMsg.setText(strMsg);
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                   // gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(AddToCart.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().setPriority(Request.Priority.LOW);
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    private void fetchUserAddress() { //TODO  server method here
        if (Connectivity.isConnected(this)) {
            final GateWay gateWay = new GateWay(this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);


            //arr_user_info.clear();
            JSONObject params = new JSONObject();
            try {
                params.put("id", "");//
                params.put("tag", "checkout");
                params.put("contact", strContact);
                params.put("email", strEmail);
                params.put("tag_for_Cart_Count", "normal_cart");
                Log.e("fetchAddress_params",params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetuserdata, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        Log.d("fetchAddress",res);
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");

                        if (status.equals("success")) {
                            JSONArray addressArray = jsonObject.getJSONArray("user_data");

                            for (int i = 0; i < addressArray.length(); i++) {
                                JSONObject json_event_list = addressArray.getJSONObject(i);


                                       latitude= json_event_list.getString("latitude");
                                        longitude=json_event_list.getString("longitude");

                            }

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();


                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(AddToCart.this)) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, AddToCart.this);*/

            GateWay gateWay1 = new GateWay(AddToCart.this);

            /*SyncWishListItems UT2 = new SyncWishListItems(SyncWishListItems.context);
            UT2.syncWishListItems(gateWay1.getContact(), gateWay1.getUserEmail(), AddToCart.this);*/
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (MasterSearch.searchView != null) {
            MasterSearch.searchView.setQuery("", false);
            MasterSearch.searchView.setIconified(false);
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder1> {

        private List<Normal_Cart_Item> mArraylist;

        public CardAdapter(List<Normal_Cart_Item> innerMovies) {
            mArraylist = innerMovies;
        }

        @NonNull
        @Override
        public CardAdapter.ViewHolder1 onCreateViewHolder(@NonNull final ViewGroup viewGroup, int viewType) {
            View v;
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_add_to_cart_new, viewGroup, false);
            relativeLayoutPrice.setVisibility(View.VISIBLE);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                        Normal_Cart_Item movie = mArraylist.get(itemPosition);
                        if (movie.getStrtype().equals("combo")) {
                        } else {
                            if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                /*
                                UserTracking UT = new UserTracking(UserTracking.context);
                                UT.user_tracking(class_name + ": clicked on view & product is: " + movie.getStr_productName(), AddToCart.this);*/

                                Intent intent = new Intent(AddToCart.this, SingleProductInformation.class);
                                intent.putExtra("product_name", movie.getP_Name());
                                intent.putExtra("shop_id", movie.getStr_normal_cart_shopId());
                                startActivity(intent);
                            } else {
                                GateWay gateWay = new GateWay(AddToCart.this);
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return new CardAdapter.NormalProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull CardAdapter.ViewHolder1 viewHolder, int position) {
            final Normal_Cart_Item cart_item = mArraylist.get(position);

            if (cart_item != null) {
                final CardAdapter.NormalProductViewHolder holder1 = (CardAdapter.NormalProductViewHolder) viewHolder;
                strType = cart_item.getStrtype();

                DBHelper db = new DBHelper(AddToCart.this);
                HashMap AddToCartInfo = db.getWishListDetails(cart_item.getStr_product_Id());
                String btnCheckStatus = (String) AddToCartInfo.get("pId");
                if (btnCheckStatus == null) {
                    holder1.layout_action2.setVisibility(View.VISIBLE);
                } else {
                    holder1.layout_action2.setVisibility(View.INVISIBLE);
                }

                if (strType.equals("combo")) {
                    holder1.layout_action2.setVisibility(View.INVISIBLE);

                    //TODO here combo offer products images setup to glide
                    /*Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/
                    Picasso.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.productImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                dialogPlus = DialogPlus.newDialog(AddToCart.this)
                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .create();

                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                //TODO here when user clicks on combo offer product image setup to glide
                               /* Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                                        .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(alertProductImage1);*/

                               Picasso.with(AddToCart.this).load(cart_item.getStr_productImg())
                                       .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(alertProductImage1);

                                dialogPlus.show();
                            } else {
                                GateWay gateWay = new GateWay(AddToCart.this);
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        }
                    });
                } else {
                    //TODO here products images setup to glide
                    /*Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/

                    Picasso.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.productImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                dialogPlus = DialogPlus.newDialog(AddToCart.this)
                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .create();

                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                //TODO here when user clicks on product image setup to glide
                               /* Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                                        .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(alertProductImage1);*/

                                Picasso.with(AddToCart.this).load(cart_item.getStr_productImg())
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(alertProductImage1);

                                dialogPlus.show();
                            } else {
                                GateWay gateWay = new GateWay(AddToCart.this);
                                gateWay.displaySnackBar(addToCartMainLayout);
                            }
                        }
                    });
                }

                double normalPrice;
                double productTotal;

                holder1.tvProductName.setText(cart_item.getStr_productName());
                //holder1.tvNormalSellerName.setText("By: " + cart_item.getStr_shopName());
                String mrp = String.valueOf(cart_item.getProduct_mrp());
                double productMrp = cart_item.getProduct_mrp();
                String discountPrice;

                holder1.tvSuperSaverTitle.setVisibility(View.GONE);
                normalPrice = cart_item.getStr_price();
                productTotal = cart_item.getProduct_total();

                discountPrice = String.valueOf(cart_item.getStr_price());
                String dis = cart_item.getProduct_discount();
                String discount = cart_item.setProduct_discount(dis);
                holder1.tvDiscount.setText(discount + " % OFF");

                if (mrp.equals(discountPrice)) {
                    holder1.tvProductPrice.setVisibility(View.VISIBLE);
                    holder1.tvDiscount.setVisibility(View.GONE);
                    holder1.tvNormalPrice.setVisibility(View.GONE);
                } else {
                    holder1.tvProductPrice.setVisibility(View.VISIBLE);
                    holder1.tvDiscount.setVisibility(View.VISIBLE);
                    holder1.tvNormalPrice.setVisibility(View.VISIBLE);
                    holder1.tvNormalPrice.setBackgroundResource(R.drawable.dash);
                }
                holder1.tvNormalPrice.setText("\u20B9 " + productMrp);
                holder1.tvProductPrice.setText("\u20B9 " + normalPrice);
                holder1.etQuantity.setText(cart_item.getStr_qty());

                if (cart_item.getStrHindi_Name().equals("")) {
                    holder1.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    holder1.tvProductHindiName.setText(" ( " + cart_item.getStrHindi_Name() + " )");
                }
                holder1.tvProductTotal.setText("Net \u20B9: " + productTotal);

                holder1.layout_action2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                            presentQty = 0;

                            DBHelper db = new DBHelper(AddToCart.this);

                            CardAdapter.ViewHolder1 holder = (CardAdapter.ViewHolder1) (v.getTag());
                            int pos = holder.getAdapterPosition();
                            //mArraylist.remove(pos);
                            //notifyItemRemoved(pos);

                            pId = cart_item.getStr_product_Id();

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": moved to wishlist & product is: " + cart_item.getStr_productName(), AddToCart.this);*/

                            String itemsCount = String.valueOf(mArraylist.size());
                            tvCount.setText("ITEMS" + " " + "(" + itemsCount + ")");

                            int iCount = Integer.parseInt(itemsCount);
                            if (iCount == 0) {
                                if (cnt == 0 && holder2iCount == 0) {
                                    tvPrice.setText("0");
                                    item.setVisible(false);
                                    relativeLayoutPrice.setVisibility(View.GONE);
                                    tv_add_more_items.setVisibility(View.GONE);
                                    relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                                }
                              /*  btnShopNow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                            Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                            intent.putExtra("tag", "");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            GateWay gateWay = new GateWay(AddToCart.this);
                                            gateWay.displaySnackBar(addToCartMainLayout);
                                        }
                                    }
                                });*/
                            }
                            Toast.makeText(AddToCart.this, "Moving product to wishlist.", Toast.LENGTH_SHORT).show();
                            //db.deleteProductItem(pId);  // deleting item from the wishlist
                            MoveToWishlist();
                            holder1.layout_action2.setVisibility(View.INVISIBLE);
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    void MoveToWishlist() { //TODo Server method here
                        shopId = cart_item.getStr_normal_cart_shopId();
                        pId = cart_item.getStr_product_Id();

                        final GateWay gateWay = new GateWay(AddToCart.this);
                        //gateWay.progressDialogStart();
                        simpleProgressBar.setVisibility(View.VISIBLE);

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("versionCode", ApplicationUrlAndConstants.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);

                            Log.e("moveToWish",""+params.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddCartItemToWishlist, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                if (!response.isNull("posts")) {
                                    try {
                                        final_price = response.getString("Total_Price");
                                        double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                        tvPrice.setText( Double.toString(finalValue));
                                        Saving_Amount = response.getString("productSavingOnMrp");
                                        if (!Saving_Amount.equals("0")) {
                                            tvTotalSaving.setVisibility(View.VISIBLE);
                                            SpannableStringBuilder builder = new SpannableStringBuilder();
                                            SpannableString str1 = new SpannableString("Your total savings:");
                                            str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                            builder.append(str1);
                                            SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                            str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                            builder.append(str2);

                                            tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                        } else {
                                            tvTotalSaving.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                //gateWay.progressDialogStop();
                                simpleProgressBar.setVisibility(View.INVISIBLE);
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                //gateWay.progressDialogStop();
                                simpleProgressBar.setVisibility(View.INVISIBLE);

                                //GateWay gateWay = new GateWay(AddToCart.this);
                                //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                showAlertDialog("Error",error.toString(),"OK");
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    }
                });

                holder1.imgPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on plus button & product is: " + cart_item.getStr_productName(), AddToCart.this);*/

                            int qty = Integer.parseInt(holder1.etQuantity.getText().toString());
                            qty++;
                            checkQuantityCartItemOnPlus(qty);
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    private void checkQuantityCartItemOnPlus(final int qty) { //TODO Server method here
                        shopId = cart_item.getStr_normal_cart_shopId();

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", cart_item.getStr_product_Id());
                            params.put("shop_id", shopId);
                            params.put("size", cart_item.getStr_Size());
                            params.put("qty", qty);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            Log.e("checkQuantityCartItem:",""+params.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.e("cechqtyItemOnPlusRes",""+response);


                                if (response.isNull("posts")) {
                                } else {
                                    try {
                                        int AllowedQty = Integer.parseInt(cart_item.getStrAllowedQty());
                                        int Available_Qty = Integer.parseInt(cart_item.getStr_AvailableProductQuantity());
                                        if (AllowedQty == 0) {
                                            if (qty > Available_Qty) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddToCart.this);
                                                alertDialogBuilder.setMessage("You cannot add more than " + Available_Qty + " quantities of this product.");
                                                alertDialogBuilder.setPositiveButton("OK",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                dialogInterface.dismiss();
                                                            }
                                                        });
                                                AlertDialog alertDialog = alertDialogBuilder.create();
                                                alertDialog.show();
                                            } else {
                                                updateMethodCall(qty);
                                            }
                                        } else {
                                            if (qty > AllowedQty) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddToCart.this);
                                                alertDialogBuilder.setMessage("You cannot add more than " + AllowedQty + " quantities of this product.");
                                                alertDialogBuilder.setPositiveButton("OK",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                dialogInterface.dismiss();
                                                            }
                                                        });
                                                AlertDialog alertDialog = alertDialogBuilder.create();
                                                alertDialog.show();
                                            } else {
                                                updateMethodCall(qty);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            private void updateMethodCall(int qty) {
                                try {
                                    cart_item.setStr_qty(String.valueOf(qty));
                                    holder1.etQuantity.setText("" + qty);
                                    strType = cart_item.getStrtype();

                                    if (strType.equals("combo")) {
                                        //updateQuantityInComboCartItemOnPlus();
                                    } else {
                                        updateQuantityInNormalCartItemOnPlus();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            private void updateQuantityInNormalCartItemOnPlus() { //TODO Server method here
                                if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                    shopId = cart_item.getStr_normal_cart_shopId();

                                    final GateWay gateWay = new GateWay(AddToCart.this);
                                    //gateWay.progressDialogStart();
                                    simpleProgressBar.setVisibility(View.VISIBLE);

                                    final JSONObject params = new JSONObject();
                                    try {
                                        params.put("product_id", cart_item.getStr_product_Id());
                                        params.put("shop_id", shopId);
                                        params.put("v_city",v_city);
                                        params.put("Qty", holder1.etQuantity.getText().toString());
                                        params.put("contactNo", strContact);
                                        params.put("email", strEmail);
                                        if (cart_item.is_supersaver.equals("Yes")) {
                                            params.put("tag", "super_saver");
                                        } else {
                                            params.put("tag", "");
                                        }
                                        Log.e("param_CartItem1plus",""+params);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {

                                            Log.e("res_CartItem1plus",""+response);
                                            if (!response.isNull("posts")) {
                                                try {
                                                    final_price = response.getString("Total_Price");
                                                    double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                                    tvPrice.setText( Double.toString(finalValue));
                                                    Saving_Amount = response.getString("productSavingOnMrp");
                                                    if (!Saving_Amount.equals("0")) {
                                                        tvTotalSaving.setVisibility(View.VISIBLE);
                                                        SpannableStringBuilder builder = new SpannableStringBuilder();
                                                        SpannableString str1 = new SpannableString("Your total savings:");
                                                        str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                                        builder.append(str1);
                                                        SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                                        str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                                        builder.append(str2);
                                                        tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                                    }
                                                    product_total = response.getString("product_total");
                                                    double productValue = Math.round(Double.parseDouble(product_total) * 100.0) / 100.0;
                                                    holder1.tvProductTotal.setText("Net \u20B9: " + Double.toString(productValue));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            //gateWay.progressDialogStop();
                                            simpleProgressBar.setVisibility(View.INVISIBLE);
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            error.printStackTrace();

                                            //gateWay.progressDialogStop();
                                            simpleProgressBar.setVisibility(View.INVISIBLE);

                                           // GateWay gateWay = new GateWay(AddToCart.this);
                                           // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                            showAlertDialog("Error",error.toString(),"OK");
                                        }
                                    });
                                    AppController.getInstance().addToRequestQueue(request);
                                } else {
                                    GateWay gateWay = new GateWay(AddToCart.this);
                                    gateWay.displaySnackBar(addToCartMainLayout);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                //GateWay gateWay = new GateWay(AddToCart.this);
                                //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                showAlertDialog("Error",error.toString(),"OK");
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    }
                });

                holder1.imgMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on minus button & product is: " + cart_item.getStr_productName(), AddToCart.this);*/

                            int qty = Integer.parseInt(holder1.etQuantity.getText().toString());
                            qty--;
                            if (qty > 0) {
                                cart_item.setStr_qty(String.valueOf(qty));
                                holder1.etQuantity.setText("" + qty);
                                strType = cart_item.getStrtype();

                                if (strType.equals("combo")) {
                                    //updateQuantityInComboCartItemOnPlus();
                                } else {
                                    updateQuantityInNormalCartItemOnPlus();
                                }
                            } else if (qty == 0) {
                                strType = cart_item.getStrtype();
                                if (strType.equals("combo")) {
                                    //deleteComboProductFromCartItem(offerCode);
                                } else {
                                    deleteNormalProductFromCartItem(qty);
                                }
                            }
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    private void updateQuantityInNormalCartItemOnPlus() { //TODO Server method here
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                            shopId = cart_item.getStr_normal_cart_shopId();

                            final GateWay gateWay = new GateWay(AddToCart.this);
                            //gateWay.progressDialogStart();
                            simpleProgressBar.setVisibility(View.VISIBLE);

                            JSONObject params = new JSONObject();
                            try {
                                params.put("product_id", cart_item.getStr_product_Id());
                                params.put("shop_id", shopId);
                                params.put("v_city", v_city);
                                params.put("Qty", holder1.etQuantity.getText().toString());
                                params.put("contactNo", strContact);
                                params.put("email", strEmail);
                                Log.e("update_cart_param:",""+params);

                                if (cart_item.is_supersaver.equals("Yes")) {
                                    params.put("tag", "super_saver");
                                } else {
                                    params.put("tag", "");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    if (!response.isNull("posts")) {
                                        try {
                                            final_price = response.getString("Total_Price");
                                            double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                            tvPrice.setText(Double.toString(finalValue));
                                            Saving_Amount = response.getString("productSavingOnMrp");
                                            if (!Saving_Amount.equals("0")) {
                                                tvTotalSaving.setVisibility(View.VISIBLE);
                                                SpannableStringBuilder builder = new SpannableStringBuilder();
                                                SpannableString str1 = new SpannableString("Your total savings:");
                                                str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                                builder.append(str1);
                                                SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                                str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                                builder.append(str2);

                                                tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                            }
                                            product_total = response.getString("product_total");
                                            double productValue = Math.round(Double.parseDouble(product_total) * 100.0) / 100.0;
                                            holder1.tvProductTotal.setText("Net \u20B9: " + Double.toString(productValue));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    //gateWay.progressDialogStop();
                                    simpleProgressBar.setVisibility(View.INVISIBLE);
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();

                                    //gateWay.progressDialogStop();
                                    simpleProgressBar.setVisibility(View.INVISIBLE);

                                    //GateWay gateWay = new GateWay(AddToCart.this);
                                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                    showAlertDialog("Error",error.toString(),"OK");
                                }
                            });
                            AppController.getInstance().addToRequestQueue(request);
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    private void deleteNormalProductFromCartItem(int qty) { //TODO Server method here
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                            final GateWay gateWay = new GateWay(AddToCart.this);
                            //gateWay.progressDialogStart();
                            simpleProgressBar.setVisibility(View.VISIBLE);

                            final DBHelper db = new DBHelper(AddToCart.this);

                            JSONObject params = new JSONObject();
                            try {
                                params.put("product_id", cart_item.getStr_product_Id());
                                params.put("contactNo", strContact);
                                params.put("email", strEmail);
                                params.put("qty", qty);
                                params.put("tag", "delete_one_item");
                                Log.e("DeleOneItem:",""+params);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {

                                    Log.e("DelOne_Item:res",""+response);

                                    if (response.isNull("posts")) {
                                    } else {
                                        db.deleteProductItem(cart_item.getStr_product_Id());
                                        try {
                                            int pos = holder1.getAdapterPosition();
                                            mArraylist.remove(pos);
                                            notifyItemRemoved(pos);

                                            //SearchProducts.addToCartArrayList.remove(cart_item.getStr_product_Id());

                                            Saving_Amount = response.getString("productSavingOnMrp");
                                            if (Saving_Amount.equals("0")) {
                                                tvTotalSaving.setVisibility(View.GONE);
                                            }
                                            int itemsCount = mArraylist.size();
                                            if (itemsCount == 0) {
                                                if (cnt == 0 && holder2iCount == 0) {
                                                    tvPrice.setText("0");
                                                    item.setVisible(false);
                                                    relativeLayoutPrice.setVisibility(View.GONE);
                                                    relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                                                }
                                               /* btnShopNow.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                                            Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                                            intent.putExtra("tag", "");
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                            finish();
                                                        } else {
                                                            GateWay gateWay = new GateWay(AddToCart.this);
                                                            gateWay.displaySnackBar(addToCartMainLayout);
                                                        }
                                                    }
                                                });*/
                                            } else {
                                                tvCount.setText("ITEMS" + " " + "(" + itemsCount + ")");

                                                final_price = response.getString("Total_Price");
                                                double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                                tvPrice.setText(Double.toString(finalValue));

                                                Saving_Amount = response.getString("productSavingOnMrp");
                                                if (!Saving_Amount.equals("0")) {
                                                    tvTotalSaving.setVisibility(View.VISIBLE);
                                                    SpannableStringBuilder builder = new SpannableStringBuilder();
                                                    SpannableString str1 = new SpannableString("Your total savings:");
                                                    str1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.totalColor)), 0, str1.length(), 0);
                                                    builder.append(str1);
                                                    SpannableString str2 = new SpannableString(" ₹ " + Saving_Amount);
                                                    str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
                                                    builder.append(str2);

                                                    tvTotalSaving.setText(builder, TextView.BufferType.SPANNABLE);
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    //gateWay.progressDialogStop();
                                    simpleProgressBar.setVisibility(View.INVISIBLE);
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //gateWay.progressDialogStop();
                                    simpleProgressBar.setVisibility(View.INVISIBLE);
                                    error.printStackTrace();

                                   // GateWay gateWay = new GateWay(AddToCart.this);
                                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                    showAlertDialog("Error",error.toString(),"OK");
                                }
                            });
                            AppController.getInstance().addToRequestQueue(request);
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mArraylist.size();
        }

        class ViewHolder1 extends RecyclerView.ViewHolder {
            ViewHolder1(View itemView) {
                super(itemView);
            }
        }

        public class NormalProductViewHolder extends CardAdapter.ViewHolder1 {

            final TextView tvProductName;
            final TextView tvNormalSellerName;
            final TextView tvNormalPrice;
            final TextView tvProductPrice;
            final TextView tvDiscount;
            final TextView tvProductTotal;
            final TextView tvProductHindiName;
            final TextView tvMovetoWishlist;
            final TextView removeItem;
            final TextView tvSuperSaverTitle;
            final ImageView productImg;
            final ImageView imgMinus;
            final ImageView imgPlus;
            final TextView etQuantity;
            final LinearLayout layout_action1;
            final LinearLayout layout_action2;

            NormalProductViewHolder(View v) {
                super(v);
                tvProductName = v.findViewById(R.id.txtProductName);
                tvProductHindiName = v.findViewById(R.id.txtProductHindiName);
                tvNormalSellerName = v.findViewById(R.id.txtNormalSellerName);
                tvNormalPrice = v.findViewById(R.id.txtNormalPrice);
                tvProductPrice = v.findViewById(R.id.txtProductPrice);
                tvDiscount = v.findViewById(R.id.txtDiscount);
                removeItem = v.findViewById(R.id.removeItem);
                etQuantity = v.findViewById(R.id.editQuantity);
                tvSuperSaverTitle = v.findViewById(R.id.txtSuperSaverTitle);
                productImg = v.findViewById(R.id.productImage);
                imgMinus = v.findViewById(R.id.imgMinus);
                imgPlus = v.findViewById(R.id.imgPlus);
                tvProductTotal = v.findViewById(R.id.txtProductTotal);
                tvMovetoWishlist = v.findViewById(R.id.txtMovetoWishlist);
                layout_action1 = v.findViewById(R.id.layout_action1);
                layout_action2 = v.findViewById(R.id.layout_action2);
                imgMinus.setTag(this);
                imgPlus.setTag(this);
                layout_action1.setTag(this);
                layout_action2.setTag(this);
            }
        }
    }

    private class CardAdapter_OutOfStock extends RecyclerView.Adapter<CardAdapter_OutOfStock.ViewHolder> {

        DialogPlus dialog;
        List<Cart_Item> mArraylist;

        CardAdapter_OutOfStock(List<Cart_Item> innerMovies) {
            mArraylist = innerMovies;
        }

        @NonNull
        @Override
        public CardAdapter_OutOfStock.ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int viewType) {
            View v;
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_add_to_cart, viewGroup, false);
            relativeLayoutPrice.setVisibility(View.GONE);
            return new CardAdapter_OutOfStock.NormalProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
            final Cart_Item cart_item = mArraylist.get(position);

            if (cart_item != null) {
                final CardAdapter_OutOfStock.NormalProductViewHolder holder1 = (CardAdapter_OutOfStock.NormalProductViewHolder) viewHolder;
                strType = cart_item.getStrtype();

                if (strType.equals("combo")) {
                    //TODO here combo offer products images setup to glide, if product is out of stock
                   /* Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/
                    Picasso.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                } else {
                    //TODO here products images setup to glide, if product is out of stock
                    /*Glide.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(AddToCart.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/

                    Picasso.with(AddToCart.this).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                }
                holder1.tvProductName.setText(cart_item.getStr_productName());
               // holder1.tvNormalSellerName.setText("By: " + cart_item.getStr_shopName());
                holder1.tvNormalPrice.setText("" + cart_item.getStr_price());
                holder1.etQuantity.setText(String.valueOf(cart_item.getStr_qty()));

                holder1.tvMovetoWishlist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                            presentQty = 0;
                            CardAdapter.ViewHolder1 holder = (CardAdapter.ViewHolder1) (v.getTag());
                            int pos = holder.getAdapterPosition();
                            mArraylist.remove(pos);
                            notifyItemRemoved(pos);

                            pId = cart_item.getStr_product_Id();

                           /* UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": moved product to wishlist & product is: " + cart_item.getStr_productName(), AddToCart.this);*/

                            String itemsCount = String.valueOf(mArraylist.size());
                            tvCount.setText("ITEMS" + " " + "(" + itemsCount + ")");

                            int iCount = Integer.parseInt(itemsCount);
                            if (iCount == 0) {
                                if (cnt == 0 && holder2iCount == 0) {
                                    tvPrice.setText("0");
                                    item.setVisible(false);
                                    relativeLayoutPrice.setVisibility(View.GONE);
                                    relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                                }
                               /* btnShopNow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                                            Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                                            intent.putExtra("tag", "");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            GateWay gateWay = new GateWay(AddToCart.this);
                                            gateWay.displaySnackBar(addToCartMainLayout);
                                        }
                                    }
                                });*/
                            }
                            Toast.makeText(AddToCart.this, "Moving product to wishlist.", Toast.LENGTH_SHORT).show();
                            MoveToWishlist();
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    void MoveToWishlist() { //TODO Server method here
                        final DBHelper db = new DBHelper(AddToCart.this);

                        shopId = cart_item.getStr_normal_cart_shopId();
                        pId = cart_item.getStr_product_Id();

                        final GateWay gateWay = new GateWay(AddToCart.this);
                        //gateWay.progressDialogStart();
                        simpleProgressBar.setVisibility(View.VISIBLE);

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("versionCode", ApplicationUrlAndConstants.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddCartItemToWishlist, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {
                                } else {
                                    db.deleteProductItem(pId);
                                    try {
                                        final_price = response.getString("Total_Price");
                                        double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                                        tvPrice.setText( Double.toString(finalValue));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                //gateWay.progressDialogStop();
                                simpleProgressBar.setVisibility(View.INVISIBLE);
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                //gateWay.progressDialogStop();
                                simpleProgressBar.setVisibility(View.INVISIBLE);

                                //GateWay gateWay = new GateWay(AddToCart.this);
                                //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                showAlertDialog("Error",error.toString(),"OK");
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    }
                });

                holder1.etQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog = DialogPlus.newDialog(AddToCart.this)
                                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.quantity_layout))
                                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .setPadding(20, 20, 20, 20)
                                .create();
                        dialog.show();
                        final TextView tvOne = (TextView) dialog.findViewById(R.id.txtOne);
                        final TextView tvTwo = (TextView) dialog.findViewById(R.id.txtTwo);
                        final TextView tvThree = (TextView) dialog.findViewById(R.id.txtThree);
                        final TextView tvFour = (TextView) dialog.findViewById(R.id.txtFour);
                        final TextView tvFive = (TextView) dialog.findViewById(R.id.txtFive);
                        final TextView tvSix = (TextView) dialog.findViewById(R.id.txtSix);
                        final TextView tvSeven = (TextView) dialog.findViewById(R.id.txtSeven);
                        final TextView tvEight = (TextView) dialog.findViewById(R.id.txtEight);
                        final TextView tvNine = (TextView) dialog.findViewById(R.id.txtNine);
                        final TextView tvTen = (TextView) dialog.findViewById(R.id.txtTen);
                        LinearLayout extraQuantityLinearLayout = (LinearLayout) dialog.findViewById(R.id.extraQuantityLinearLayout);
                        if (cart_item.getStr_price() <= 50) {
                            extraQuantityLinearLayout.setVisibility(View.VISIBLE);
                        }
                        tvOne.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String one = tvOne.getText().toString();
                                setQuantityMethod(one);
                            }
                        });
                        tvTwo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String two = tvTwo.getText().toString();
                                setQuantityMethod(two);
                            }
                        });
                        tvThree.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String three = tvThree.getText().toString();
                                setQuantityMethod(three);
                            }
                        });
                        tvFour.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String four = tvFour.getText().toString();
                                setQuantityMethod(four);
                            }
                        });
                        tvFive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String five = tvFive.getText().toString();
                                setQuantityMethod(five);
                            }
                        });
                        tvSix.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String six = tvSix.getText().toString();
                                setQuantityMethod(six);
                            }
                        });
                        tvSeven.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String seven = tvSeven.getText().toString();
                                setQuantityMethod(seven);
                            }
                        });
                        tvEight.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String eight = tvEight.getText().toString();
                                setQuantityMethod(eight);
                            }
                        });
                        tvNine.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String nine = tvNine.getText().toString();
                                setQuantityMethod(nine);
                            }
                        });
                        tvTen.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String ten = tvTen.getText().toString();
                                setQuantityMethod(ten);
                            }
                        });
                    }

                    private void setQuantityMethod(String qty) {
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                            int Available_Qty = Integer.parseInt(cart_item.getStr_AvailableProductQuantity());
                            if (Integer.parseInt(qty) > Available_Qty) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddToCart.this);
                                alertDialogBuilder.setMessage("" + Available_Qty + " quantities available right now.");
                                alertDialogBuilder.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int arg1) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                        dialog.dismiss();
                    }
                });

                try {
                    int qty = Integer.parseInt(cart_item.getStr_qty());
                    double productPrice = cart_item.getStr_price();
                    if (qty > 1) {
                        double total_price = qty * productPrice;
                        double finalValue = Math.round(total_price * 100.0) / 100.0;
                        holder1.tvProductTotal.setText("" + finalValue);
                    } else {
                        holder1.tvProductTotal.setText("" + cart_item.getStr_price());
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                holder1.removeItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Connectivity.isConnected(AddToCart.this)) { // Internet connection is not present, Ask user to connect to Internet
                            presentQty = 0;
                            CardAdapter_OutOfStock.ViewHolder holder = (CardAdapter_OutOfStock.ViewHolder) (v.getTag());
                            int pos = holder.getAdapterPosition();
                            mArraylist.remove(pos);
                            notifyItemRemoved(pos);

                            offerCode = cart_item.getStr_product_Id();
                            pId = cart_item.getStr_product_Id();

                            String itemsCount = String.valueOf(mArraylist.size());
                            int iCount = Integer.parseInt(itemsCount);
                            if (iCount == 0) {
                                if (cnt == 0 && holder2iCount == 0) {
                                    msgLayout.setVisibility(View.GONE);
                                    tv_add_more_items.setVisibility(View.VISIBLE);
                                    //when user remove product one by one in that case this method call and get fresh data from server
                                    fetch_cart_item();
                                }
                            }
                            strType = cart_item.getStrtype();
                            if (strType.equals("combo")) {
                                //deleteComboProductFromCartItem(offerCode);
                            } else {
                                deleteNormalProductFromCartItem(pId);
                            }
                        } else {
                            GateWay gateWay = new GateWay(AddToCart.this);
                            gateWay.displaySnackBar(addToCartMainLayout);
                        }
                    }

                    public void deleteNormalProductFromCartItem(String p_id) { //TODO Server method here
                        final DBHelper db = new DBHelper(AddToCart.this);

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", p_id);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("tag", "delete_one_item");
                            Log.e("DelSingle_parm:",""+params);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                Log.e("DelSingle:res",""+response);


                                if (response.isNull("posts")) {
                                } else {
                                    db.deleteProductItem(pId);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                //GateWay gateWay = new GateWay(AddToCart.this);
                                //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                showAlertDialog("Error",error.toString(),"OK");
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    }

                    public void deleteComboProductFromCartItem(String offer_code) { //TODO Server method here
                        final DBHelper db = new DBHelper(AddToCart.this);

                        JSONObject params = new JSONObject();
                        try {
                            params.put("offer_code", offer_code);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("tag", "delete_one_item");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {
                                } else {
                                    db.deleteProductItem(pId);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                               // GateWay gateWay = new GateWay(AddToCart.this);
                               // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                showAlertDialog("Error",error.toString(),"OK");
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mArraylist.size();
        }

        public void add(Cart_Item cart_item) {
            mArraylist.add(cart_item);
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(View itemView) {
                super(itemView);
            }
        }

        class NormalProductViewHolder extends CardAdapter_OutOfStock.ViewHolder {

            final TextView tvProductName;
            final TextView tvNormalSellerName;
            final TextView tvNormalPrice;
            final TextView tvProductTotal;
            final TextView tvMovetoWishlist;
            final ImageView removeItem;
            final ImageView productImg;
            final EditText etQuantity;

            NormalProductViewHolder(View v) {
                super(v);
                tvProductName = v.findViewById(R.id.txtProductName);
                tvNormalSellerName = v.findViewById(R.id.txtNormalSellerName);
                tvNormalPrice = v.findViewById(R.id.txtNormalPrice);
                removeItem = v.findViewById(R.id.removeItem);
                etQuantity = v.findViewById(R.id.editQuantity);
                productImg = v.findViewById(R.id.productImage);
                tvProductTotal = v.findViewById(R.id.txtProductTotal);
                tvMovetoWishlist = v.findViewById(R.id.txtMovetoWishlist);
                removeItem.setTag(this);
                tvMovetoWishlist.setTag(this);
            }
        }
    }

    public static class Normal_Cart_Item {

        final String str_normal_cart_shopId;
        final String str_product_Id;
        final String str_productName;
        final String str_shopName;
        final String str_productImg;
        final String strtype;
        final String str_AvailableProductQuantity;
        final String strHindi_Name;
        final String strAllowedQty;
        final double str_price;
        final String str_Size;
        final String p_Name;
        final String is_supersaver;
        final String supersaver_price;
        final String supersaver_qty;
        final String proPrice;
        final double product_mrp;
        final double product_total;
        final String product_maincat;
        String str_qty;
        String product_discount;


        Normal_Cart_Item(String normal_cart_shopId, String productId, String productName,
                         String shopName, double price, String qty, String productImg,
                         String type, String AvailableProductQuantity, String AllowedQty, String hindiname,
                         String str_Size, String p_Name, String is_supersaver, String supersaver_price, String supersaver_qty,
                         String proPrice, double product_mrp, String product_discount, String product_maincat,double product_total) {
            this.str_normal_cart_shopId = normal_cart_shopId;
            this.str_product_Id = productId;
            this.str_productName = productName;
            this.str_shopName = shopName;
            this.str_price = price;
            this.str_qty = qty;
            this.str_productImg = productImg;
            this.strtype = type;
            this.str_AvailableProductQuantity = AvailableProductQuantity;
            this.strAllowedQty = AllowedQty;
            this.strHindi_Name = hindiname;
            this.str_Size = str_Size;
            this.p_Name = p_Name;
            this.is_supersaver = is_supersaver;
            this.supersaver_price = supersaver_price;
            this.supersaver_qty = supersaver_qty;
            this.proPrice = proPrice;
            this.product_mrp = product_mrp;
            this.product_discount = product_discount;
            this.product_maincat=product_maincat;
            this.product_total = product_total;
        }

        public String getStrHindi_Name() {
            return strHindi_Name;
        }

        public String getP_Name() {
            return p_Name;
        }

        public String getStr_Size() {
            return str_Size;
        }

        public String getStrAllowedQty() {
            return strAllowedQty;
        }

        public String getStr_normal_cart_shopId() {
            return str_normal_cart_shopId;
        }

        public String getStr_product_Id() {
            return str_product_Id;
        }

        public String getStr_productName() {
            return str_productName;
        }

        public String getStr_shopName() {
            return str_shopName;
        }

        public double getStr_price() {
            return str_price;
        }

        public String getStr_qty() {
            return str_qty;
        }

        public void setStr_qty(String str_qty) {
            this.str_qty = str_qty;
        }

        public String getStr_productImg() {
            return str_productImg;
        }

        public String getStrtype() {
            return strtype;
        }

        public String getStr_AvailableProductQuantity() {
            return str_AvailableProductQuantity;
        }

        public double getProduct_mrp() {
            return product_mrp;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
            return product_discount;
        }

        public double getProduct_total() {
            return product_total;
        }

        public String getProduct_maincat() {
            return product_maincat;
        }
    }

    private class Cart_Item {

        final String str_normal_cart_shopId;
        final String str_product_Id;
        final String str_productName;
        final String str_shopName;
        final String str_productImg;
        final String strtype;
        final String str_AvailableProductQuantity;
        final double str_price;
        String str_qty;

        Cart_Item(String normal_cart_shopId, String productId, String productName, String shopName, double price, String qty, String productImg, String type, String AvailableProductQuantity) {
            this.str_normal_cart_shopId = normal_cart_shopId;
            this.str_product_Id = productId;
            this.str_productName = productName;
            this.str_shopName = shopName;
            this.str_price = price;
            this.str_qty = qty;
            this.str_productImg = productImg;
            this.strtype = type;
            this.str_AvailableProductQuantity = AvailableProductQuantity;
        }

        public String getStr_normal_cart_shopId() {
            return str_normal_cart_shopId;
        }

        public String getStr_product_Id() {
            return str_product_Id;
        }

        public String getStr_productName() {
            return str_productName;
        }

        public String getStr_shopName() {
            return str_shopName;
        }

        public double getStr_price() {
            return str_price;
        }

        public String getStr_qty() {
            return str_qty;
        }

        public String getStr_productImg() {
            return str_productImg;
        }

        public String getStrtype() {
            return strtype;
        }

        public String getStr_AvailableProductQuantity() {
            return str_AvailableProductQuantity;
        }
    }

    private void popupLimitbyCat(String mainCat , String final_price, Double chkPrice){

        final Dialog dialog = new Dialog(AddToCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout_popup_order_details);

        RadioGroup rg = dialog.findViewById(R.id.popup20RG);
        Button add_more = dialog.findViewById(R.id.btn_add_more);
        Button btn_continue = dialog.findViewById(R.id.btn_continue);
        TextView tv_cat_type = dialog.findViewById(R.id.tv_cat_type);
        TextView tv_great_msg = dialog.findViewById(R.id.tv_great_msg);
        TextView tv_expt_msg = dialog.findViewById(R.id.tv_expt_msg);

        String strPrice = String.format("%.2f", chkPrice);

        String vegmsg="Fresh Fruits & Vegetables Products\nDelivery charges Rs.30/- below 300/- order";

        String grocery_msg="Groceries Products \nDelivery charges Rs.30/- below 500/- order";

        String mesg =  "Great, you are just INR "+ strPrice +" away from Free Delivery! ";//Pls add more items

        //Double finalprice = Double.parseDouble(final_price);

        String MESSAGE="";
        if(mainCat.equals("Grocery")){

            MESSAGE = grocery_msg+mesg;
            tv_cat_type.setText(grocery_msg);

        }else {
            MESSAGE=vegmsg+mesg;
            tv_cat_type.setText(vegmsg);
        }

        tv_great_msg.setText(mesg);


        tv_expt_msg.setText("Expected Delivery time within 3 hours.");



        add_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String localFinalPrice = tvPrice.getText().toString();
                    Intent intent = new Intent(AddToCart.this, Order_Details_StepperActivity.class);
                    intent.putExtra("finalPrice", localFinalPrice);
                    startActivity(intent);
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();

    }

    public void popupLimitbyCat2(String messageG, String messageFre, String expectedDel, final String oklimit){
        final Dialog dialog = new Dialog(AddToCart.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout_popup_order_details);

        RadioGroup rg = dialog.findViewById(R.id.popup20RG);
        Button add_more = dialog.findViewById(R.id.btn_add_more);
        Button btn_continue = dialog.findViewById(R.id.btn_continue);
        TextView tv_cat_type = dialog.findViewById(R.id.tv_cat_type);
        TextView tv_great_msg = dialog.findViewById(R.id.tv_great_msg);
        TextView tv_expt_msg = dialog.findViewById(R.id.tv_expt_msg);

               tv_cat_type.setText(messageG);
               tv_great_msg.setText(messageFre);
              tv_expt_msg.setText(expectedDel);



        add_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddToCart.this, BaseActivity.class);
                intent.putExtra("tag", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if(oklimit.equalsIgnoreCase("false")){
                        dialog.dismiss();
                    }else {

                        /*if (checkout_status.equalsIgnoreCase("active")) {
                            termsConditonDialog();   //checkout_status
                            dialog.dismiss();
                        } else {*/
                            String localFinalPrice = tvPrice.getText().toString();
                            Intent intent = new Intent(AddToCart.this, Order_Details_StepperActivity.class);
                            intent.putExtra("finalPrice", localFinalPrice);
                            startActivity(intent);
                            dialog.dismiss();
                        //}
                    }



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();

    }


    //Method to show the Tersms and Condition
    public void termsConditonDialog() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dailog_terms_conditions, null);

        Button btn_submitreson = view.findViewById(R.id.btn_search);




        TextView tv_first_condition = view.findViewById(R.id.tv_first_condition);
        TextView tv_second_condition = view.findViewById(R.id.tv_second_condition);

        final RadioGroup radioGroup = view.findViewById(R.id.radioGroup);
        RadioGroup radioGroup2 = view.findViewById(R.id.radioGroup2);
        final RadioGroup radioGroup_shop = view.findViewById(R.id.radioGroup_shop);
        RadioButton radio_Anyshop = view.findViewById(R.id.radio_Anyshop);
        RadioButton radio_Specific = view.findViewById(R.id.radio_Specific);
        final Spinner sp_shopList = view.findViewById(R.id.sp_shopList);

        radio_Specific.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //sp_shopList.setVisibility(View.VISIBLE);
            }
        });

        radio_Anyshop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //sp_shopList.setVisibility(View.GONE);
            }
        });

        //final RecyclerView rv_terms_conditions = findViewById(R.id.rv_terms_conditions);
        final RecyclerView rv_terms_conditions = (RecyclerView) view.findViewById(R.id.rv_terms_conditions);
        rv_terms_conditions.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(AddToCart.this);
        rv_terms_conditions.setLayoutManager(layoutManager);

        Log.e("CartList Size:",""+termCondModelArrayList.size());
         termsCondAdaptor = new TermsCondAdaptor(termCondModelArrayList, AddToCart.this);
        rv_terms_conditions.setAdapter(termsCondAdaptor);
        termsCondAdaptor.notifyDataSetChanged();

        final RecyclerView rv_shoplist = view.findViewById(R.id.rv_shoplist);
        rv_shoplist.setHasFixedSize(true);
        final LinearLayoutManager layoutManager2 = new LinearLayoutManager(AddToCart.this);
        rv_shoplist.setLayoutManager(layoutManager2);

        /*shopListAdapter = new ShopListAdapter(orderArrayList,AddToCart.this);
        rv_shoplist.setAdapter(shopListAdapter);
        shopListAdapter.notifyDataSetChanged();*/


        //getShopList();

        orderArrayList.clear();
        arrayshoplist.clear();

        ShopModel model0 = new ShopModel();
        model0.setShop_id("NA");
        model0.setSellername("Any Seller");
        model0.setShop_name("Any Shop");
        model0.setArea("Any Area");
        orderArrayList.add(model0);
        arrayshoplist.add("Any Shop");

        String targetUrlShopList = null;
        try {
            targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/nearbyShopList.php?latitude="+latitude+"&longitude="+longitude+"&sessionMainCat="+ URLEncoder.encode(product_maincat,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+orderId+"&shopListType="+shopListType;

        Log.d("targetUrlShopList",targetUrlShopList);
        final StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ShopListRes",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        //progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        ShopModel model = new ShopModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setContact(olderlist.getString("contact"));
                                        model.setEmail(olderlist.getString("email"));
                                        model.setAdmin_type(olderlist.getString("admin_type"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setRecharge_amount(olderlist.getString("recharge_amount"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_category(olderlist.getString("shop_category"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setPreferredDeliveryAreaKm(olderlist.getString("preferred_delivery_area_km"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        orderArrayList.add(model);
                                        arrayshoplist.add(olderlist.getString("shop_name"));
                                    }
                                    Log.e("ShopListSize:",""+orderArrayList.size());
                                    /*ArrayAdapter<String> adapter = new ArrayAdapter(AddToCart.this,android.R.layout.simple_spinner_item,arrayshoplist);
                                    sp_shopList.setAdapter(adapter);*/
                                    /*shopListAdapter = new ShopListAdapter(orderArrayList,AddToCart.this);
                                    rv_shoplist.setAdapter(shopListAdapter);
                                    shopListAdapter.notifyDataSetChanged();*/
                                }else {

                                }
                            }else if(strStatus=false){
                                /*tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");*/
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter(AddToCart.this,android.R.layout.simple_spinner_item,arrayshoplist);
                            sp_shopList.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(this).addToRequestQueue(orderListRequest);

        Log.e("ShopListSize:",""+orderArrayList.size());
        shopListAdapter = new ShopListAdapter(orderArrayList,AddToCart.this);
        rv_shoplist.setAdapter(shopListAdapter);
        shopListAdapter.notifyDataSetChanged();


        if(termCondModelArrayList.size()!=0) {
            condition_types.add(termCondModelArrayList.get(0).getOption1());
            condition_types.add(termCondModelArrayList.get(1).getOption2());
        }
        rv_terms_conditions.addOnItemTouchListener(new RecyclerTouchListener(AddToCart.this, rv_terms_conditions, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                    String value_campare = termCondModelArrayList.get(position).getOption1();
                    String value_campare2 = termCondModelArrayList.get(position).getOption2();
                if(condition_types.contains(value_campare)){
                    condition_types.remove(value_campare);
                }else {
                    condition_types.add(value_campare);
                }if(condition_types.contains(value_campare2)) {
                    condition_types.remove(value_campare2);

                }else {
                    condition_types.add(value_campare2);
                }
                Log.e("Final_payment_option",""+condition_types.toString());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rv_shoplist.addOnItemTouchListener(new RecyclerTouchListener(AddToCart.this, rv_shoplist, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                nearbyShopID = orderArrayList.get(position).getShop_id();
                Toast.makeText(AddToCart.this,"ShopId:"+nearbyShopID,Toast.LENGTH_LONG).show();

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("user_terms", condition_types.toString());
                editor.putString("nearbyshopid", nearbyShopID);
                editor.apply();
                editor.commit();

                String localFinalPrice = tvPrice.getText().toString();
                Intent intent = new Intent(AddToCart.this, Order_Details_StepperActivity.class);
                intent.putExtra("finalPrice", localFinalPrice);
                startActivity(intent);
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        //GET SHOP ID From the selected shop by user
        sp_shopList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String value = sp_shopList.getSelectedItem().toString();

                for(int i=0; i<orderArrayList.size();i++){

                    if(orderArrayList.get(i).getShop_name().contains(value)){

                        nearbyShopID = orderArrayList.get(i).getShop_id();
                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("user_terms", condition_types.toString());
                        editor.putString("nearbyshopid", nearbyShopID);
                        editor.putString("product_maincat", product_maincat);
                        editor.apply();
                        editor.commit();

                        Toast.makeText(AddToCart.this,"ShopID:"+orderArrayList.get(i).getShop_name(),Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                Toast.makeText(AddToCart.this, rb.getText(), Toast.LENGTH_SHORT).show();*/

                //radioGroup.c
                //if (condition_types.size() < 1 )
                if (radioGroup_shop.getCheckedRadioButtonId() == -1 )
                {
                    // no radio buttons are checked
                    Toast.makeText(AddToCart.this, "Please Select Shop Option", Toast.LENGTH_LONG).show();
                }else {
                    //Toast.makeText(AddToCart.this,"Values:"+""+condition_types.toString(),Toast.LENGTH_LONG).show();
                    //Toast.makeText(AddToCart.this, "Values:" + "" + condition_types.size(), Toast.LENGTH_LONG).show();

                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("user_terms", condition_types.toString());
                    editor.putString("nearbyshopid", nearbyShopID);
                    editor.putString("product_maincat", product_maincat);
                    editor.apply();
                    editor.commit();

                    String localFinalPrice = tvPrice.getText().toString();
                    Intent intent = new Intent(AddToCart.this, Order_Details_StepperActivity.class);
                    intent.putExtra("finalPrice", localFinalPrice);
                    startActivity(intent);
                    dialog.dismiss();

                }

            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

    // Method get the Response of Terms and Condition Ins User App
    private void get_terms_cond() { //TODO Server method here
        if (Connectivity.isConnected(AddToCart.this)) {
            GateWay gateWay = new GateWay(AddToCart.this);

            String email = gateWay.getUserEmail();
            String user_contact = gateWay.getContact();

            JSONObject params = new JSONObject();
            try {
                params.put("email", email);
                params.put("user_contact", user_contact);

                Log.d("get_checkout_condition", ";params: " + params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.get_terms_conditions, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("REG_checkout_Res", "onResponse: " + response.toString());
                        checkout_status = response.getString("checkout_status");
                        JSONArray jsonArray = response.getJSONArray("post");

                        for(int i =0 ;i<jsonArray.length();i++){

                            TermCondModel model = new TermCondModel();
                            String VALU = jsonArray.getJSONObject(i).getString("title");
                            model.setTitle(jsonArray.getJSONObject(i).getString("title"));
                            model.setOption1(jsonArray.getJSONObject(i).getString("option1"));
                            model.setOption2(jsonArray.getJSONObject(i).getString("option2"));
                            Log.e("title_value",""+VALU);
                            termCondModelArrayList.add(model);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    Log.d("Get_Terms_Condition", "onErr: " + error.toString());
                   // GateWay gateWay = new GateWay(AddToCart.this);
                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(AddToCart.this, "Please check Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    //shop list based on current location
    public void getShopList(){

        //progressDialog.show();
        orderArrayList.clear();
        arrayshoplist.clear();

        String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/nearbyShopList.php?latitude="+latitude+"&longitude="+longitude+"&sessionMainCat="+product_maincat;
        //String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+orderId+"&shopListType="+shopListType;

        Log.d("targetUrlShopList",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ShopListRes",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        //progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        ShopModel model = new ShopModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setContact(olderlist.getString("contact"));
                                        model.setEmail(olderlist.getString("email"));
                                        model.setAdmin_type(olderlist.getString("admin_type"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setRecharge_amount(olderlist.getString("recharge_amount"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_category(olderlist.getString("shop_category"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setPreferredDeliveryAreaKm(olderlist.getString("preferred_delivery_area_km"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        orderArrayList.add(model);
                                    }
                                    Log.e("ShopListSize:",""+orderArrayList.size());
                                    /*orderlistAdapter = new ShopListAdapter(orderArrayList,mContext);
                                    rv_home_orderlist.setAdapter(orderlistAdapter);
                                    orderlistAdapter.notifyDataSetChanged();*/
                                }else {

                                }
                            }else if(strStatus=false){
                                /*tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(this).addToRequestQueue(orderListRequest);
    }

    //error Handling dialog
    private void showAlertDialog(String title, String message, String buttonText) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddToCart.this);
        alertDialog.setIcon(R.drawable.app_icon_new); //Setting Dialog icon Image
        alertDialog.setTitle(title);  // Setting Dialog Title
        alertDialog.setMessage(message); // Setting Dialog Message
        alertDialog.setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { // Setting OK Button
                //context.finish();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.show(); // Showing Alert Message
        TextView messageText = dialog.findViewById(android.R.id.message);
        assert messageText != null;
        messageText.setGravity(Gravity.CENTER);
        dialog.show();
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
}