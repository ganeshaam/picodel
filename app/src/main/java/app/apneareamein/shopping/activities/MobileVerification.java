package app.apneareamein.shopping.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.textfield.TextInputLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.NotificationUtils;
import app.apneareamein.shopping.utils.VolleySingleton;

public class MobileVerification extends AppCompatActivity implements TextWatcher {

    private GateWay gateWay;
    private CoordinatorLayout mobileVerificationMainLayout;
    private String mobNumber,notificationId,user_password,reffer_code="NA";
    private String email;
    private String userName;
    private String userLastName;
    private String finalAddress,referrerCode="NA",androidId,ref_by_id;
    private EditText etContact, etPassword, etUserName, etUserLastName, etAddress, etPinCode, etLandmark, etVerificationCodeAlert,
            etPasswordAlert, etReEnterPasswordAlert, etCity, etArea,et_password,et_password2,et_contact,editRefferalCode;
    private TextInputLayout TextInputPasswordAlert, TextInputReEnterPasswordAlert;
    private TextInputLayout input_layout_password;
    private TextInputLayout input_layout_phone;
    private TextInputLayout input_layout_otp;
    private TextInputLayout input_layout_email,input_layout_refferal_cdde;
    private TextInputLayout input_layout_user_name;
    private TextInputLayout input_layout_user_lastname;
    private TextInputLayout input_layout_user_city;
    private TextInputLayout input_layout_user_area;
    private TextInputLayout input_layout_user_address;
    private TextInputLayout input_layout_pincode;
    private EditText etOtp;
    private TextView tvForgotPassword, tvVerificationMessage,txtRegisterHere,txtResendOTP,tex_message,txtForgotPassword2;
    private CheckBox chkWhatsApp, chkFriends, chkNewsPaper, chkFacebook, chkEvent, chkMarketingTeam;
    private Button btnSubmit;
    private AutoCompleteTextView mEmailView;
    private LinearLayout registrationLinearLayout,layout_forgot_register;
    private final ArrayList<String> fName = new ArrayList<>();
    private final ArrayList<String> emailId = new ArrayList<>();
    private final ArrayList<String> contactNo = new ArrayList<>();
    private final ArrayList<String> address = new ArrayList<>();
    private final ArrayList<String> vCode = new ArrayList<>();
    private final ArrayList<String> password = new ArrayList<>();
    private final ArrayList<String> user_id = new ArrayList<>();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private  TextView text_head;
    private ProgressBar simpleProgressBar;
    public static final String MY_PREFS_NAME = "PICoDEL";
    private String refer_Code_valid ="NA", refer_type="NA", v_city = "NA", v_area ="NA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);

        mobileVerificationMainLayout = findViewById(R.id.mobileVerificationMainLayout);
        registrationLinearLayout = findViewById(R.id.registrationLinearLayout);

        input_layout_phone = findViewById(R.id.input_layout_phone);
        input_layout_password = findViewById(R.id.input_layout_password);
        input_layout_otp = findViewById(R.id.input_layout_otp);
        input_layout_email = findViewById(R.id.input_layout_email);
        input_layout_user_name = findViewById(R.id.input_layout_user_name);
        input_layout_user_lastname = findViewById(R.id.input_layout_user_lastname);
        input_layout_user_city = findViewById(R.id.input_layout_user_city);
        input_layout_user_area = findViewById(R.id.input_layout_user_area);
        input_layout_user_address = findViewById(R.id.input_layout_user_address);
        input_layout_pincode = findViewById(R.id.input_layout_pincode);
        input_layout_refferal_cdde = findViewById(R.id.input_layout_refferal_cdde);

        etPassword = findViewById(R.id.editPassword);
        mEmailView = findViewById(R.id.editUserEmail);
        etUserName = findViewById(R.id.editUserName);
        etUserLastName = findViewById(R.id.editUserLastName);
        etAddress = findViewById(R.id.editUserAddress);
        etPinCode = findViewById(R.id.editUserPinCode);
        etCity = findViewById(R.id.editUserCity);
        etArea = findViewById(R.id.editUserArea);
        etLandmark = findViewById(R.id.editLandmark);
        etOtp = findViewById(R.id.etOtp);
        txtResendOTP = findViewById(R.id.txtResendOTP);
        tex_message = findViewById(R.id.tex_message);
        editRefferalCode = findViewById(R.id.editRefferalCode);

        etContact = findViewById(R.id.etUserContact);
        etContact.addTextChangedListener(this);
        editRefferalCode.addTextChangedListener(this);

        chkWhatsApp = findViewById(R.id.chkWhatsApp);
        chkFriends = findViewById(R.id.chkFriends);
        chkNewsPaper = findViewById(R.id.chkNewsPaper);
        chkFacebook = findViewById(R.id.chkFacebook);
        chkEvent = findViewById(R.id.chkEvent);
        chkMarketingTeam = findViewById(R.id.chkMarketingTeam);

        tvForgotPassword = findViewById(R.id.txtForgotPassword);
        txtForgotPassword2 = findViewById(R.id.txtForgotPassword2);
        txtRegisterHere = findViewById(R.id.txtRegisterHere);
        btnSubmit = findViewById(R.id.btnSubmit);
        text_head = findViewById(R.id.text_head);
        layout_forgot_register = findViewById(R.id.layout_forgot_register);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

        txtResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!mobNumber.isEmpty()) {
                    Toast.makeText(MobileVerification.this,"Please enter mobile number",Toast.LENGTH_LONG).show();
                }else {
                    forgetPassword(mobNumber);
                }
            }
        });

        txtForgotPassword2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //forgotPasswordAlertDialog();
                //forgotPasswordDialog();
               Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.picodel.com/form/forgotpassword"));
                startActivity(intent);
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(ApplicationUrlAndConstants.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered now subscribe to `global` topic to receive app wide notifications
                } else if (intent.getAction().equals(ApplicationUrlAndConstants.PUSH_NOTIFICATION)) {
                    // new push notification is received
                }
            }
        };

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobNumber = etContact.getText().toString();
                if (etContact.getText().toString().equals("")) {
                    input_layout_phone.setError(getString(R.string.error_mobile));
                    requestFocus(etContact);
                } else if (etContact.getText().toString().length() < 10) {
                    input_layout_phone.setError("Please enter valid mobile number");
                    requestFocus(etContact);
                } else {
                    forgetPassword(mobNumber);
                }
            }
        });

        txtRegisterHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrationLinearLayout.setVisibility(View.VISIBLE);
                text_head.setText("Registration");
                btnSubmit.setText("Register");
                layout_forgot_register.setVisibility(View.GONE);
                //etContact.setText("");

            }
        });

        // Get token
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d("firebaseTokan", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        notificationId = task.getResult().getToken();

                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("firebaseTokan", notificationId);
                        //Toast.makeText(RegistrationActivity.this, firebaseTokan, Toast.LENGTH_SHORT).show();
                    }
                });
        // [END retrieve_current_token]

        FirebaseTokenData();

        try{
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            v_area = prefs.getString("v_area", "");
            v_city = prefs.getString("v_city", "");

        }catch (Exception e){

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        mobNumber = etContact.getText().toString();
        reffer_code = editRefferalCode.getText().toString();

        Pattern p_phone = Pattern.compile(ApplicationUrlAndConstants.MobileregEx);
        Matcher m_phone = p_phone.matcher(mobNumber);
        if (mobNumber.length() == 10 && reffer_code.length() == 0) {
            /*if (!m_phone.find()) {
                input_layout_phone.setError("Invalid mobile number");
                requestFocus(etContact);
            } else {*/
                input_layout_phone.setErrorEnabled(false);
                getUserDetailsWithMobVerification(mobNumber);
            //Toast.makeText(this, "Call method", Toast.LENGTH_SHORT).show();
           // }
        } else {
            //registrationLinearLayout.setVisibility(View.GONE);
            tvForgotPassword.setVisibility(View.GONE);
            txtRegisterHere.setVisibility(View.VISIBLE);
            //btnSubmit.setVisibility(View.GONE);
        }

        if(reffer_code.length() == 10){
            getRefferalVerification(reffer_code);
        }
    }











    private void getUserDetailsWithMobVerification(final String mobNumber) { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) { // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(MobileVerification.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            /*SharedPreferences sharedPreferences = getSharedPreferences(ApplicationUrlAndConstants.SHARED_PREF, 0);
            String notificationId = sharedPreferences.getString("regId", null);*/
          //  Log.e("MobileVerification",""+ApplicationUrlAndConstants.urlCheckUserDetails+" "+mobNumber+notificationId+" "+ApplicationUrlAndConstants.versionName);
            JSONObject params = new JSONObject();
            try {
                  params.put("mob_number", mobNumber);
                  params.put("notification_id", notificationId);
                  params.put("user_version_name", ApplicationUrlAndConstants.versionName);
                  Log.e("NotificationIdParm:",""+notificationId);
                  Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckUserDetails, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("MobileVerificationRes",""+response.toString());

                    try {
                        String refer_visibility = response.getString("refer_visibility");
                        if(refer_visibility.equalsIgnoreCase("active")){
                            input_layout_refferal_cdde.setVisibility(View.VISIBLE);
                        }else {
                            input_layout_refferal_cdde.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (response.isNull("posts")) {

                       getVerificationCode(mobNumber);

                        etPassword.addTextChangedListener(new MyTextWatcher(etPassword));
                        mEmailView.addTextChangedListener(new MyTextWatcher(mEmailView));
                        etUserName.addTextChangedListener(new MyTextWatcher(etUserName));
                        etUserLastName.addTextChangedListener(new MyTextWatcher(etUserLastName));
                        etAddress.addTextChangedListener(new MyTextWatcher(etAddress));
                        etPinCode.addTextChangedListener(new MyTextWatcher(etPinCode));
                        etArea.addTextChangedListener(new MyTextWatcher(etArea));
                        etCity.addTextChangedListener(new MyTextWatcher(etCity));
                        etLandmark.addTextChangedListener(new MyTextWatcher(etLandmark));
                        etOtp.addTextChangedListener(new MyTextWatcher(etOtp));

                        btnSubmit.setText("Register");
                        text_head.setText("Registration");
                        tex_message.setVisibility(View.GONE);
                        tvForgotPassword.setVisibility(View.GONE);
                        txtRegisterHere.setVisibility(View.GONE);
                        registrationLinearLayout.setVisibility(View.VISIBLE);
                        btnSubmit.setVisibility(View.VISIBLE);
                        btnSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                hideKeyBoard();
                                registrationValidation();
                            }
                        });
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        try {
                            //gateWay.progressDialogStop();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                            forgetPassword(mobNumber);
                            Log.d("posts",""+response.getJSONArray("posts"));
                            JSONArray userInfoJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < userInfoJsonArray.length(); i++) {
                                JSONObject jSonUserInfoData = userInfoJsonArray.getJSONObject(i);

                                fName.add(jSonUserInfoData.getString("fname"));
                                emailId.add(jSonUserInfoData.getString("emailid"));
                                contactNo.add(jSonUserInfoData.getString("contactno"));
                                address.add(jSonUserInfoData.getString("address"));
                                password.add(jSonUserInfoData.getString("password"));
                                user_id.add(jSonUserInfoData.getString("user_id"));
                                if (fName.size() > 0 && emailId.size() > 0 && contactNo.size() > 0 && address.size() > 0 && password.size() > 0) {
                                    hideKeyBoard();
                                    registrationLinearLayout.setVisibility(View.GONE);
                                    tvForgotPassword.setVisibility(View.GONE);
                                    layout_forgot_register.setVisibility(View.VISIBLE);
                                    txtRegisterHere.setVisibility(View.VISIBLE);
                                    btnSubmit.setText("Login");
                                    text_head.setText("Login");
                                    btnSubmit.setVisibility(View.VISIBLE);
                                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //String strPassword = etPassword.getText().toString();
                                            String strPassword = etOtp.getText().toString();
                                            if (strPassword.equals("")) {
                                                /*try {
                                                    etPassword.requestFocus();
                                                    etPassword.setError("Please enter your password");
                                                }catch (Exception e){
                                                    e.getMessage();
                                                    Toast.makeText(MobileVerification.this,"Please enter Password",Toast.LENGTH_LONG).show();
                                                }*/
                                                try {
                                                    etOtp.requestFocus();
                                                    etOtp.setError("Please enter your OTP");
                                                }catch (Exception e){
                                                    e.getMessage();
                                                    Toast.makeText(MobileVerification.this,"Please enter OTP",Toast.LENGTH_LONG).show();
                                                }
                                            } else {
                                                //loginValidation();
                                                String OTP = vCode.get(0);
                                                if(etOtp.getText().toString().equalsIgnoreCase(OTP)){
                                                //if(etPassword.getText().toString().equalsIgnoreCase(user_password)){
                                                    DBHelper db = new DBHelper(MobileVerification.this);
                                                    db.updateUserInfoAgain(emailId.get(0), fName.get(0), address.get(0), contactNo.get(0));

                                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                    editor.putString("user_id", user_id.get(0));
                                                    editor.apply();
                                                    editor.commit();

                                                    GateWay gateWay = new GateWay(MobileVerification.this);
                                                    String City = gateWay.getCity();
                                                    String Area = gateWay.getArea();

                                                    if (City == null && Area == null) {
                                                        Intent intent = new Intent(MobileVerification.this, LocationActivity.class);
                                                        intent.putExtra("tag", "registration");
                                                        startActivity(intent);
                                                    } else {
                                                        Intent intent = new Intent(MobileVerification.this, BaseActivity.class);
                                                        intent.putExtra("tag", "");
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                }else {
                                                    Toast.makeText(MobileVerification.this,"Please enter Valid OTP",Toast.LENGTH_LONG).show();
                                                    //Toast.makeText(MobileVerification.this,"Please enter Valid Password",Toast.LENGTH_LONG).show();
                                                }
                                                // Calling login and
                                            }
                                        }
                                    });
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //gateWay.progressDialogStop();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        }

                       // gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                   // gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(MobileVerification.this);
                    // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }

    //urlReferInsert

    private void ReferInsert(final String mobNumber) { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) { // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(MobileVerification.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);
            try {

                androidId = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);


                Log.e("androidId:",""+androidId);

            }catch (Exception e){
                e.getMessage();
            }

            JSONObject params = new JSONObject();
            try {
                params.put("contact", mobNumber);
                params.put("ref_code", referrerCode);
                params.put("device_id", androidId);
                params.put("ref_by_id", ref_by_id);
                params.put("ref_type", refer_type);

                Log.e("user_ref_Param:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlReferInsert, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("ReferInsertRes", "" + response.toString());

                    if (response.isNull("posts")) {

                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }else{

                        try {
                            String res = response.getString("posts");
                            Log.e("insert_res:",""+res);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    //GateWay gateWay = new GateWay(MobileVerification.this);
                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }

    private void loginValidation() { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) {
            String mobNumber = etContact.getText().toString();
            String strPassword = etPassword.getText().toString();

            JSONObject params = new JSONObject();
            try {
                params.put("mob_number", mobNumber);
                params.put("password", strPassword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckPassword, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String result = response.getString("posts");
                        if (result.equals("true")) {
                            DBHelper db = new DBHelper(MobileVerification.this);
                            db.updateUserInfoAgain(emailId.get(0), fName.get(0), address.get(0), contactNo.get(0));

                            GateWay gateWay = new GateWay(MobileVerification.this);
                            String City = gateWay.getCity();
                            String Area = gateWay.getArea();

                            if (City == null && Area == null) {
                                Intent intent = new Intent(MobileVerification.this, LocationActivity.class);
                                intent.putExtra("tag", "registration");
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(MobileVerification.this, BaseActivity.class);
                                intent.putExtra("tag", "");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Toast.makeText(MobileVerification.this, "Please enter valid password", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //GateWay gateWay = new GateWay(MobileVerification.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }

    private void forgetPassword(String mobNumber) { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) { // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(MobileVerification.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("mob_number", mobNumber);
                params.put("tag", "forget_password");
                //params.put("tag", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlForgetPassword, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                       // Log.e("Verefy_CodeRESPONSE:",""+vCode.get(0).toString());

                        JSONArray mainClassificationJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                            JSONObject jSonClassificationData = mainClassificationJsonArray.getJSONObject(i);
                            vCode.add(jSonClassificationData.getString("vCode"));
                            vCode.add(0,jSonClassificationData.getString("vCode"));
                           // user_password =(jSonClassificationData.getString("password"));
                            Log.e("Verefy_Code:",""+vCode.get(0).toString());
                            etOtp.setText(vCode.get(0).toString());
                        }

                        if (vCode.size() > 0) {
                            //forgotPasswordAlertDialog();
                            //tex_message.setVisibility(View.VISIBLE);
                            //gateWay.progressDialogStop();
                            //simpleProgressBar.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                       // gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                  //  gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(MobileVerification.this);
                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }

    private void forgotPasswordDialog(){

        LayoutInflater layoutInflater = LayoutInflater.from(MobileVerification.this);
        View forgotPassword = layoutInflater.inflate(R.layout.layout_forgot_password, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MobileVerification.this);
        alertDialogBuilder.setView(forgotPassword);
        alertDialogBuilder.setTitle("Forgot Password");

        et_password = forgotPassword.findViewById(R.id.et_password);
        et_password2 = forgotPassword.findViewById(R.id.et_password2);
        et_contact = forgotPassword.findViewById(R.id.et_contact);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        String strNewPassword = et_password.getText().toString();
                        String strReEnterPassword = et_password2.getText().toString();
                        String strcontact = et_contact.getText().toString();

                        if (strcontact.isEmpty()) {
                            et_contact.setError("Please enter Contact");
                            requestFocus(et_contact);
                        }else if (strcontact.length()<10) {
                            et_contact.setError("Please enter Valid Contact");
                            requestFocus(et_contact);
                        }else if (strNewPassword.isEmpty()) {
                            et_password.setError("Please enter password");
                            requestFocus(et_password);
                        } else if (strNewPassword.isEmpty()) {
                            et_password2.setError("Please re-enter password");
                            requestFocus(et_password2);
                        } else if (strNewPassword.equalsIgnoreCase(strReEnterPassword)) {
                            //successfullyChangePassword(strNewPassword);
                            changePasswordAPI(strcontact,strNewPassword);
                            dialog.dismiss();
                        } else {
                            Toast.makeText(MobileVerification.this, "Please enter same password", Toast.LENGTH_LONG).show();
                        }

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void forgotPasswordAlertDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(MobileVerification.this);
        View forgotPassword = layoutInflater.inflate(R.layout.forgot_password_layout, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MobileVerification.this);
        alertDialogBuilder.setView(forgotPassword);
        alertDialogBuilder.setTitle("Forgot Password");
        //alertDialogBuilder.setTitle("Verify Code");

        etVerificationCodeAlert = forgotPassword.findViewById(R.id.editVerificationCodeAlert);
        TextInputPasswordAlert = forgotPassword.findViewById(R.id.textInputLayoutPasswordAlert);
        TextInputReEnterPasswordAlert = forgotPassword.findViewById(R.id.textInputLayoutReEnterPasswordAlert);
        final TextInputLayout input_layout_verification_code_alert = forgotPassword.findViewById(R.id.input_layout_verification_code_alert);
        final LinearLayout passwordLayoutAlert = forgotPassword.findViewById(R.id.passwordLayoutAlert);
        etPasswordAlert = forgotPassword.findViewById(R.id.editPasswordAlert);
        etReEnterPasswordAlert = forgotPassword.findViewById(R.id.editReEnterPasswordAlert);
        tvVerificationMessage = forgotPassword.findViewById(R.id.txtVerificationMessage);


        etPasswordAlert.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String strNewPassword = etPasswordAlert.getText().toString();

                if (strNewPassword.length() < 6) {
                    TextInputPasswordAlert.setError("Password Must be Min. 6 characters.");
                    requestFocus(etPasswordAlert);
                } else {
                    TextInputPasswordAlert.setErrorEnabled(false);
                }
            }
        });

        etReEnterPasswordAlert.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String strReEnterPassword = etReEnterPasswordAlert.getText().toString();

                if (strReEnterPassword.length() < 6) {
                    TextInputReEnterPasswordAlert.setError("Password Must be Min. 6 characters.");
                    requestFocus(etReEnterPasswordAlert);
                } else {
                    TextInputReEnterPasswordAlert.setErrorEnabled(false);
                }

            }
        });

        etVerificationCodeAlert.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String strVCode = etVerificationCodeAlert.getText().toString();

                if (strVCode.length() == 5) {
                    if (!vCode.contains(strVCode)) {
                        input_layout_verification_code_alert.setError("Invalid verification code");
                        requestFocus(etVerificationCodeAlert);
                    } else {
                        input_layout_verification_code_alert.setErrorEnabled(false);
                    }
                } else {
                    input_layout_verification_code_alert.setErrorEnabled(false);
                }
            }
        });

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(Color.parseColor("#3F51B5"));
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strVCode = etVerificationCodeAlert.getText().toString();

                if (strVCode.isEmpty()) {
                    input_layout_verification_code_alert.setError("Please enter verification code you received");
                    requestFocus(etVerificationCodeAlert);
                } else if (vCode.contains(strVCode)) {
                    etVerificationCodeAlert.setVisibility(View.GONE);
                    tvVerificationMessage.setVisibility(View.GONE);
                    input_layout_verification_code_alert.setVisibility(View.GONE);
                    passwordLayoutAlert.setVisibility(View.VISIBLE);
                    TextInputPasswordAlert.setVisibility(View.VISIBLE);
                    TextInputReEnterPasswordAlert.setVisibility(View.VISIBLE);
                    input_layout_verification_code_alert.setErrorEnabled(false);

                    String strNewPassword = etPasswordAlert.getText().toString();
                    String strReEnterPassword = etReEnterPasswordAlert.getText().toString();


                    if (strNewPassword.isEmpty()) {
                        TextInputPasswordAlert.setError("Please enter password");
                        requestFocus(etPasswordAlert);
                    } else if (strNewPassword.isEmpty()) {
                        TextInputReEnterPasswordAlert.setError("Please re-enter password");
                        requestFocus(etReEnterPasswordAlert);
                    } else if (strNewPassword.equalsIgnoreCase(strReEnterPassword)) {
                        successfullyChangePassword(strNewPassword);
                        alertDialog.dismiss();
                    } else {
                        Toast.makeText(MobileVerification.this, "Please enter same password", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void changePasswordAPI(String contact, String password) { //TODO Server method here
        final GateWay gateWay = new GateWay(MobileVerification.this);
        JSONObject params = new JSONObject();
        simpleProgressBar.setVisibility(View.VISIBLE);
        try {
            params.put("contact", contact);
            params.put("password", password);

            Log.e("getShopsParam:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/update_new_password.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("UpdateRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {

                    simpleProgressBar.setVisibility(View.GONE);
                }else{
                    simpleProgressBar.setVisibility(View.GONE);

                    try {
                        String res = response.getString("posts");

                        Toast.makeText(MobileVerification.this,""+res,Toast.LENGTH_LONG).show();



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                simpleProgressBar.setVisibility(View.GONE);

            }
        });
        VolleySingleton.getInstance(MobileVerification.this).addToRequestQueue(request);

    }

    private void successfullyChangePassword(String strNewPassword) { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) { // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(MobileVerification.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            final String mobNumber = etContact.getText().toString();

            JSONObject params = new JSONObject();
            try {
                params.put("contact", mobNumber);
                params.put("password", strNewPassword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlSavePassword, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(MobileVerification.this, "Password Changed Successfully", Toast.LENGTH_LONG).show();
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(MobileVerification.this);
                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }

    private void getVerificationCode(String mobNumber) { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) {
            JSONObject params = new JSONObject();
            try {
                params.put("mob_number", mobNumber);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlRegistrationVerificationCode, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        try {
                            JSONArray verificationCodeJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < verificationCodeJsonArray.length(); i++) {
                                JSONObject jSonVCodeData = verificationCodeJsonArray.getJSONObject(i);
                                vCode.add(jSonVCodeData.getString("vCode"));
                                Log.e("Vcode",""+vCode.toString());
                                etOtp.setText(vCode.get(0));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }

    public void registrationValidation() {
        try {
            if (!validateMobile()) {
                return;
            }
           /* if (!validatePassword()) {
                return;
            }*/
            /*if (!validateOtp()) {
                return;
            }*/
            if (!validateEmail()) {
                return;
            }
            if (!validateUserName()) {
                return;
            }
            /*if (!validateUserLastName()) {
                return;
            }*/
          /*  if (!validateCity()) {
                return;
            }
            if (!validateUserArea()) {
                return;
            }
            if (!validateUserAddress()) {
                return;
            }
            if (!validatePincode()) {
                return;
            }*/
            userRegistration();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userRegistration() { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) {
            gateWay = new GateWay(MobileVerification.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            email = mEmailView.getText().toString();
            userName = etUserName.getText().toString();
            userLastName = etUserLastName.getText().toString();
            mobNumber = etContact.getText().toString();
            String otp = etOtp.getText().toString();
            String pwd = etPassword.getText().toString();
            String add = etAddress.getText().toString();
            String landmark = etLandmark.getText().toString();
            String pinCode = etPinCode.getText().toString();
            String strCity = etCity.getText().toString();
            String strArea = etArea.getText().toString();

            ArrayList<String> knowFrom = new ArrayList<>();
            String whatsApp = chkWhatsApp.getText().toString();
            String friends = chkFriends.getText().toString();
            String newsPaper = chkNewsPaper.getText().toString();
            String facebook = chkFacebook.getText().toString();
            String event = chkEvent.getText().toString();
            //String marketingTeam = chkMarketingTeam.getText().toString();
            String marketingTeam = "Marketing Team";
            if (chkWhatsApp.isChecked()) {
                knowFrom.add(whatsApp);
            }
            if (chkFriends.isChecked()) {
                knowFrom.add(friends);
            }
            if (chkNewsPaper.isChecked()) {
                knowFrom.add(newsPaper);
            }
            if (chkFacebook.isChecked()) {
                knowFrom.add(facebook);
            }
            if (chkEvent.isChecked()) {
                knowFrom.add(event);
            }
            if (chkMarketingTeam.isChecked()) {
                knowFrom.add(marketingTeam);
            }
            knowFrom.add(marketingTeam);
            knowFrom.remove(Collections.singleton(""));
            JSONArray knowFromArray = new JSONArray(knowFrom);

            //SharedPreferences sharedPreferences = getSharedPreferences(ApplicationUrlAndConstants.SHARED_PREF, 0);
           // String notificationId = sharedPreferences.getString("regId", null);
            androidId = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            finalAddress = add + ", " + landmark + ", Pincode- " + pinCode;

            JSONObject params = new JSONObject();
            try {
                //vasundhara
                params.put("email", email);
                params.put("userName", userName);
                params.put("userLastName", " ");
                params.put("mob_number", mobNumber);
                params.put("pwd", "999999");
                //params.put("pwd", pwd);
                params.put("address", "");
                if(v_city !=null || v_area!=null ){
                    params.put("city", v_city);
                    params.put("area", v_area);
                }else {
                    params.put("city", "");
                    params.put("area", "");
                }
                params.put("landmark", "");
                params.put("pinCode", "");
                params.put("knowFrom", knowFromArray);
                params.put("notification_id", notificationId);
                params.put("refercode", referrerCode);
                params.put("device_id", androidId);

                Log.d("REG", ";params: " + params.toString());
              /*  params.put("email", email);
                params.put("userName", userName);
                params.put("userLastName", userLastName);
                params.put("mob_number", mobNumber);
                params.put("pwd", pwd);
                params.put("address", finalAddress);
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("landmark", landmark);
                params.put("pinCode", pinCode);
                params.put("knowFrom", knowFromArray);
                params.put("notification_id", notificationId);*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlRegistration2, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("REG", "onResponse: " + response.toString());
                        String result = response.getString("posts");
                        String user_id = response.getString("user_id");
                        if (result.equals("already_registered")) {
                            Toast.makeText(MobileVerification.this, "You are already registered", Toast.LENGTH_LONG).show();
                            //gateWay.progressDialogStop();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        } else {
                            // storing new register user id to share preferences
                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            if(user_id!=null) {
                                editor.putString("user_id", user_id);
                            }
                            editor.apply();
                            editor.commit();
                            ReferInsert(mobNumber);
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                            String full_name = userName + " " + userLastName;
                            DBHelper db = new DBHelper(MobileVerification.this);
                            db.updateUserInfoAgain(email, full_name, finalAddress, mobNumber);
                            registrationSuccessfulAlertDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    Log.d("REG", "onResponse: " + error.toString());
                    //GateWay gateWay = new GateWay(MobileVerification.this);
                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }

    private void registrationSuccessfulAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Congratulations!");
        builder.setMessage("You have successfully registered on PICODEL Online Grocery Shopping. Your account details have been sent to your registered email id.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        GateWay gateWay = new GateWay(MobileVerification.this);
                        String City = gateWay.getCity();
                        String Area = gateWay.getArea();
                        if (City == null && Area == null) {
                            Intent intent = new Intent(MobileVerification.this, LocationActivity.class);
                            intent.putExtra("tag", "registration");
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(MobileVerification.this, BaseActivity.class);
                            intent.putExtra("tag", "");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() < 6;
    }

    private void hideKeyBoard() {
        // Check if no view has focus:
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(ApplicationUrlAndConstants.REGISTRATION_COMPLETE));

        // register new push message receiver by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(ApplicationUrlAndConstants.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.editPassword:
                    //validatePassword();
                    break;
                case R.id.etOtp:
                    validateOtp();
                    break;
                case R.id.editUserEmail:
                    validateEmail();
                    break;
                case R.id.editUserName:
                    validateUserName();
                    break;
                case R.id.editUserLastName:
                    validateUserLastName();
                    break;
                case R.id.editUserCity:
                    validateCity();
                    break;
                case R.id.editUserArea:
                    validateUserArea();
                    break;
                case R.id.editUserAddress:
                    validateUserAddress();
                    break;
                case R.id.editUserPinCode:
                    validatePincode();
                    break;
            }
        }
    }

    private boolean validateMobile() {
        String mobile = etContact.getText().toString().trim();


        Pattern p_phone = Pattern.compile(ApplicationUrlAndConstants.MobileregEx);
        Matcher m_phone = p_phone.matcher(mobile);
        if (mobile.isEmpty()) {
            input_layout_phone.setError("Enter mobile number");
            requestFocus(etContact);
            return false;
        } else if (!m_phone.find()) {
            input_layout_phone.setError("Invalid mobile number");
            requestFocus(etContact);
            return false;
        } else {
            input_layout_phone.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        String password = etPassword.getText().toString().trim();
        if (password.isEmpty()) {
            input_layout_password.setError("Please enter password");
            requestFocus(etPassword);
            return false;
        } else if (isPasswordValid(password)) {
            input_layout_password.setError(getString(R.string.validationPassword));
            requestFocus(etPassword);
            return false;
        } else {
            input_layout_password.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePincode() {
        if (etPinCode.getText().toString().trim().isEmpty()) {
            input_layout_pincode.setError(getString(R.string.error_pinCode));
            requestFocus(etPinCode);
            return false;
        } else if (etPinCode.getText().toString().length() < 6) {
            input_layout_pincode.setError("Invalid pincode");
            requestFocus(etPinCode);
            return false;
        } else {
            input_layout_pincode.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateUserAddress() {
        if (etAddress.getText().toString().trim().isEmpty()) {
            input_layout_user_address.setError(getString(R.string.error_address));
            requestFocus(etAddress);
            return false;
        } else {
            input_layout_user_address.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateUserArea() {
        if (etArea.getText().toString().trim().isEmpty()) {
            input_layout_user_area.setError(getString(R.string.error_area));
            requestFocus(etArea);
            return false;
        } else {
            input_layout_user_area.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateCity() {
        if (etCity.getText().toString().trim().isEmpty()) {
            input_layout_user_city.setError(getString(R.string.error_city));
            requestFocus(etCity);
            return false;
        } else {
            input_layout_user_city.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateUserName() {
        if (etUserName.getText().toString().trim().isEmpty()) {
            input_layout_user_name.setError(getString(R.string.error_user_name));
            requestFocus(etUserName);
            return false;
        } else {
            input_layout_user_name.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateUserLastName() {
        if (etUserLastName.getText().toString().trim().isEmpty()) {
            input_layout_user_lastname.setError(getString(R.string.error_user_lastname));
            requestFocus(etUserLastName);
            return false;
        } else {
            input_layout_user_lastname.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateEmail() {
        String email = mEmailView.getText().toString().trim();
        if (email.isEmpty()) {
            input_layout_email.setError("Please enter Email ID");
            requestFocus(mEmailView);
            return false;
        } else if (!isEmailValid(email)) {
            input_layout_email.setError(getString(R.string.error_invalid_email));
            requestFocus(mEmailView);
            return false;
        } else {
            input_layout_email.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateOtp() {
        String strVCode = etOtp.getText().toString();
        if (etOtp.getText().toString().trim().isEmpty()) {
            input_layout_otp.setError("Please enter verification code");
            requestFocus(etOtp);
            return false;
        } else if (!vCode.contains(strVCode)) {
            input_layout_otp.setError(getString(R.string.error_invalid_vCode));
            requestFocus(etOtp);
            return false;
        } else {
            input_layout_otp.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void FirebaseTokenData(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e("FirebaseToken2", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        //Log.d(TAG, msg);
                        //Toast.makeText(MobileVerification.this, ""+token, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showAlertDialog(String title, String message, String buttonText) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MobileVerification.this);
        alertDialog.setIcon(R.drawable.app_icon_new); //Setting Dialog icon Image
        alertDialog.setTitle(title);  // Setting Dialog Title
        alertDialog.setMessage(message); // Setting Dialog Message
        alertDialog.setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { // Setting OK Button
                //context.finish();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.show(); // Showing Alert Message
        TextView messageText = dialog.findViewById(android.R.id.message);
        assert messageText != null;
        messageText.setGravity(Gravity.CENTER);
        dialog.show();
    }

    private void getRefferalVerification(final String mobNumber) { //TODO Server method here
        if (Connectivity.isConnected(MobileVerification.this)) { // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(MobileVerification.this);
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {

                params.put("mob_number", mobNumber);

                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckRefferalCode, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("MobileVerificationRes",""+response.toString());

                    if (response.isNull("posts")) {
                        Toast.makeText(MobileVerification.this,"Not Valid Code",Toast.LENGTH_LONG).show();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        // gateWay.progressDialogStop();
                        try {
                            JSONArray verificationCodeJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < verificationCodeJsonArray.length(); i++) {
                                JSONObject jSonVCodeData = verificationCodeJsonArray.getJSONObject(i);

                                refer_Code_valid = jSonVCodeData.getString("isValid");
                                refer_type = jSonVCodeData.getString("type");
                                referrerCode = jSonVCodeData.getString("refer_by_contact");

                                Log.e("RferValues:",""+refer_Code_valid +" "+refer_type+" "+referrerCode);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(MobileVerification.this,"Valid Code",Toast.LENGTH_LONG).show();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay = new GateWay(this);
            gateWay.displaySnackBar(mobileVerificationMainLayout);
        }
    }
}
