package app.apneareamein.shopping.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class AutoDashboard extends AppCompatActivity {

    private CoordinatorLayout dashBoardLayout;
    private Class aClass = null;
    private ArrayList<MenuPojo> menuPojoArrayList = new ArrayList<>();
    private MenusCardAdapter menusCardAdapter;
    private RecyclerView menuRecyclerView;
    private String download_count, cashback_points;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_dashboard);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_auto_dashboard);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dashBoardLayout = findViewById(R.id.dashBoardLayout);

        menuRecyclerView = findViewById(R.id.menuRecyclerView);
        menuRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        menuRecyclerView.setLayoutManager(mLayoutManager);
        menuRecyclerView.setItemAnimator(new DefaultItemAnimator());

        loadHomeMenus();
    }

    private void loadHomeMenus() {
        if (Connectivity.isConnected(AutoDashboard.this)) {
            final GateWay gateWay = new GateWay(AutoDashboard.this);
            gateWay.progressDialogStart();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("contact", gateWay.getContact());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlLoadMenus, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    menuPojoArrayList = new ArrayList<>();
                    try {
                        JSONArray jsonArray = response.getJSONArray("");

                        JSONObject object1 = jsonArray.getJSONObject(0);
                        JSONObject object2 = jsonArray.getJSONObject(1);

                        JSONArray cityJsonArray = object1.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCart = cityJsonArray.getJSONObject(i);

                            menuPojoArrayList.add(new MenuPojo(jsonCart.getString("menu_name"),
                                    jsonCart.getString("url")));
                        }
                        JSONArray object2JSONArray = object2.getJSONArray("allCounts");
                        for (int i = 0; i < object2JSONArray.length(); i++) {
                            JSONObject jsonCart = object2JSONArray.getJSONObject(i);

                            download_count = jsonCart.getString("download_count");
                            cashback_points = jsonCart.getString("cashback_points");
                        }
                        menusCardAdapter = new MenusCardAdapter(menuPojoArrayList);
                        menuRecyclerView.setAdapter(menusCardAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();
                }
            });
            AppController.getInstance().setPriority(Request.Priority.IMMEDIATE);
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(AutoDashboard.this);
            gateWay.displaySnackBar(dashBoardLayout); //TODO SnackBar method here
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class MenuPojo {

        String menu_name;
        String url;

        MenuPojo(String menu_name, String url) {
            this.menu_name = menu_name;
            this.url = url;
        }

        String getMenu_name() {
            return menu_name;
        }

        String getUrl() {
            return url;
        }
    }

    private class MenusCardAdapter extends RecyclerView.Adapter<MenuViewHolder> {

        private List<MenuPojo> menuPojoList;

        MenusCardAdapter(ArrayList<MenuPojo> menuPojoArrayList) {
            this.menuPojoList = menuPojoArrayList;
        }

        @NonNull
        @Override
        public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_dashboard, parent, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int itemPosition = menuRecyclerView.getChildAdapterPosition(view);
                    MenuPojo menuPojo = menuPojoList.get(itemPosition);
                    try {
                        switch (menuPojo.getMenu_name()) {
                            case "App Downloads Count":
                                break;
                            case "Cashback Points":
                                break;
                            default:
                                aClass = Class.forName("app.apneareamein.shopping.activities." + menuPojo.getUrl());
                                Intent intent = new Intent(AutoDashboard.this, aClass);
                                if (menuPojo.getMenu_name().equals("History") || menuPojo.getMenu_name().equals("Above 300 CB History")) {
                                    intent.putExtra("type", menuPojo.getMenu_name());
                                }
                                startActivity(intent);
                                break;
                        }
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
            return new MenuViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
            MenuPojo menuPojo = menuPojoList.get(position);

            holder.txtTitle.setText(menuPojo.getMenu_name());
            if (menuPojo.getMenu_name().equals("App Downloads Count")) {
                holder.txtCount.setVisibility(View.VISIBLE);
                holder.txtCount.setText(download_count);
            }
            if (menuPojo.getMenu_name().equals("Cashback Points")) {
                holder.txtCount.setVisibility(View.VISIBLE);
                holder.txtCount.setText(cashback_points);
            }
            if (menuPojo.getMenu_name().equals("History")) {
                holder.txtCount.setVisibility(View.GONE);
            }
            if (menuPojo.getMenu_name().equals("Above 300 CB History")) {
                holder.txtCount.setVisibility(View.GONE);
            }
            if (menuPojo.getMenu_name().equals("Show Users")) {
                holder.txtCount.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return menuPojoList.size();
        }
    }

    private class MenuViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private TextView txtTitle, txtCount;

        MenuViewHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtCount = itemView.findViewById(R.id.txtCount);
            cardView.setTag(this);
        }
    }
}
