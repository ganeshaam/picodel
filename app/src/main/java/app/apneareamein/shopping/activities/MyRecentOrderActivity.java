package app.apneareamein.shopping.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.PaymentModeModel;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.MySingleton;
import app.apneareamein.shopping.utils.VolleySingleton;
import io.fabric.sdk.android.services.common.SafeToast;

public class MyRecentOrderActivity extends AppCompatActivity {

    public static final String MY_PREFS_NAME = "PICoDEL";
    ProgressDialog progressDialog;

    ArrayList<String> arrayList;
    TextView tv_orderid,order_date,tv_accepted_by,tv_assigned_del_boy,order_status,order_category,txt_shop_ch_heading;
    CheckBox shop_ch_qty,shop_ch_qlty,shop_ch_packing,shop_ch_price;
    Button change_status,btn_feedback;
    LinearLayout ll_feedback,ll_status,layout_shop_ch;
    String status,shopname,payment_type,del_boy_name;
    float shop_rating ;
    Spinner sp_payment;
    Context mcontext;
    RatingBar rb_shopkeeper;
    CardView cv_accepted,cv_outfordelivery,cv_delivered;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_recentorder);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("My Order Status");

        mcontext = MyRecentOrderActivity.this;
        progressDialog = new ProgressDialog(this);

        arrayList = new ArrayList<>();


        tv_orderid = findViewById(R.id.tv_orderid);
        order_category = findViewById(R.id.order_category);
        order_date = findViewById(R.id.order_date);
        tv_accepted_by = findViewById(R.id.tv_accepted_by);
        tv_assigned_del_boy = findViewById(R.id.tv_assigned_del_boy);
        order_status = findViewById(R.id.order_status);
        change_status = findViewById(R.id.change_status);
        btn_feedback = findViewById(R.id.btn_feedback);

        ll_feedback = findViewById(R.id.ll_feedback);
        ll_status = findViewById(R.id.ll_status);
        sp_payment = findViewById(R.id.sp_payment);
        rb_shopkeeper = findViewById(R.id.rb_shopkeeper);
        layout_shop_ch = findViewById(R.id.layout_shop_ch);
        txt_shop_ch_heading = findViewById(R.id.txt_shop_ch_heading);
        shop_ch_qty = findViewById(R.id.shop_ch_qty);
        shop_ch_qlty = findViewById(R.id.shop_ch_qlty);
        shop_ch_packing = findViewById(R.id.shop_ch_packing);
        shop_ch_price = findViewById(R.id.shop_ch_price);
        cv_accepted = findViewById(R.id.cv_accepted);
        cv_outfordelivery = findViewById(R.id.cv_outfordelivery);
        cv_delivered = findViewById(R.id.cv_delivered);


        btn_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(shop_rating<1){

                    Toast.makeText(MyRecentOrderActivity.this,"Please Give Ratings", Toast.LENGTH_LONG).show();

                }else {
                    FeedbackReviewDataToServer();
                }
            }
        });


        getMyLastOrder();
        paymentModeList();

        change_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(payment_type.isEmpty()){
                    Toast.makeText(MyRecentOrderActivity.this,"Please Select Payment Option",Toast.LENGTH_LONG).show();
                }else {

                    changeStatus(payment_type);
                }

            }
        });

        sp_payment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                payment_type = sp_payment.getSelectedItem().toString();
                //payment_type = arrayList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rb_shopkeeper.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if(rating>=4){
                    layout_shop_ch.setVisibility(View.GONE);
                    shop_rating= rating;
                    txt_shop_ch_heading.setText("What did you like?");
                    Uri uri = Uri.parse("market://details?id=" + mcontext.getPackageName());
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our application, we need to add following flags to intent.
                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    try {
                        startActivity(goToMarket);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=" + mcontext.getPackageName())));
                    }
                }else {
                    shop_rating= rating;
                    layout_shop_ch.setVisibility(View.VISIBLE);
                    txt_shop_ch_heading.setText("What all will you like us to improve?");
                }

            }
        });

    }

    // Method to get the User Last order Details

       private void getMyLastOrder() { //TODO Server method here
         final GateWay gateWay = new GateWay(MyRecentOrderActivity.this);
        JSONObject params = new JSONObject();
        progressDialog.show();
        try {
            params.put("contact", gateWay.getContact());

            Log.e("getShopsParam:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getOrderStatus.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ShopFranchiseeRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {

                      progressDialog.dismiss();
                }else{
                    progressDialog.dismiss();

                    try {
                        JSONArray jsonArray = response.getJSONArray("posts");


                        Log.e("user_RS:",""+jsonArray.toString());
                        if(jsonArray.length()>0){
                            // Log.d("orderlist",""+jsonArray);
                            for(int i = 0; i < jsonArray.length(); i++){
                                JSONObject shopList = jsonArray.getJSONObject(i);
                                //ShopEnquireModel model = new ShopEnquireModel();
                                tv_orderid.setText(shopList.getString("order_id"));
                                order_category.setText(shopList.getString("sessionMainCat"));
                                order_date.setText(shopList.getString("order_date"));
                                tv_accepted_by.setText(shopList.getString("shop_name"));
                                shopname = shopList.getString("shop_name");
                                tv_assigned_del_boy.setText(shopList.getString("del_boy_name"));
                                del_boy_name = shopList.getString("del_boy_name");
                                order_status.setText(shopList.getString("status"));
                                status = shopList.getString("status");

                                if(shopname.isEmpty()){
                                    cv_accepted.setVisibility(View.GONE);
                                }else {
                                    cv_accepted.setVisibility(View.VISIBLE);
                                }

                                if(del_boy_name.isEmpty()|| del_boy_name == null || del_boy_name .equalsIgnoreCase("")){
                                    cv_outfordelivery.setVisibility(View.GONE);
                                }else {
                                    cv_outfordelivery.setVisibility(View.VISIBLE);
                                }

                                if(status.equalsIgnoreCase("Delivered")){
                                    cv_delivered.setVisibility(View.VISIBLE);
                                }


                                  if(status.equalsIgnoreCase("Delivered")){
                                      ll_status.setVisibility(View.GONE);
                                      ll_feedback.setVisibility(View.VISIBLE);
                                  }else {
                                      if(shopname.isEmpty()|| shopname == null){

                                      }else {
                                          ll_status.setVisibility(View.VISIBLE);
                                          ll_feedback.setVisibility(View.GONE);
                                      }
                                  }


                            }
                            Log.e("ShopList_Size",""+jsonArray.length());
                            //shopEnquireAdapter.notifyDataSetChanged();
                        }else {
                            //tv_network_con.setVisibility(View.VISIBLE);
                            //tv_network_con.setText("No Records Found");
                        }


                        //referralPointsAdaptor.notifyDataSetChanged();
                    }catch (Exception e){

                        progressDialog.dismiss();
                    }



                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();

            }
        });
        VolleySingleton.getInstance(MyRecentOrderActivity.this).addToRequestQueue(request);

    }

     // Method to Change status Delivered

        private void changeStatus(String payment_type) { //TODO Server method here
        final GateWay gateWay = new GateWay(MyRecentOrderActivity.this);
        JSONObject params = new JSONObject();
        progressDialog.show();
        try {
            params.put("order_id", tv_orderid.getText().toString());
            params.put("payment_type", payment_type);

            Log.e("getShopsParam:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/updateOrderStatus.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("UpdateRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {

                    progressDialog.dismiss();
                }else{
                    progressDialog.dismiss();

                    try {
                        String res = response.getString("posts");

                        Toast.makeText(MyRecentOrderActivity.this,""+res,Toast.LENGTH_LONG).show();

                        getMyLastOrder();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();

            }
        });
        VolleySingleton.getInstance(MyRecentOrderActivity.this).addToRequestQueue(request);

    }

    //Get paymentmode Api method
       public void paymentModeList(){

        if(Connectivity.isConnected(this)){

            //progressDialog.show();

            String urlAcceptOrder = ApplicationUrlAndConstants.paymentmode;

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  progressDialog.dismiss();
                            try {
                                Log.e("PaymentModeRES:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONArray jsonArray =  jsonObject.getJSONArray("data");

                                    for (int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        PaymentModeModel paymentMode_model = new PaymentModeModel();
                                        paymentMode_model.setPaymentType(jsonObject1.getString("payment_type"));
                                        paymentMode_model.setPayment_details(" ");
                                        //paymentModeList.add(paymentMode_model);
                                        arrayList.add(jsonObject1.getString("payment_type"));
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter(MyRecentOrderActivity.this,android.R.layout.simple_spinner_item,arrayList);
                                    sp_payment.setAdapter(adapter);

                                    //  Toast.makeText(mContext,"Payment List Successfully fetched",Toast.LENGTH_SHORT).show();
                                }else if(status=false){
                                    Toast.makeText(MyRecentOrderActivity.this,"Sorry. No PaymentMode Found.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(this).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(this,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    // put the Review and Ratings to server
    private void FeedbackReviewDataToServer() { //TODO Server method here
        String pId = null;
        if (Connectivity.isConnected(MyRecentOrderActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(MyRecentOrderActivity.this);
            //gateWay.progressDialogStart();
            progressDialog.show();

            JSONObject params = new JSONObject();
            try {

                String   shop_chks = shop_checkOperations().toString();

                params.put("delboy_id", "NA");
                params.put("order_id", tv_orderid.getText().toString());
                params.put("shop_id", "NA");
                params.put("shop_review", shop_chks);
                params.put("shop_rating", shop_rating);
                params.put("delboy_review", "NA");
                params.put("delboy_rating", "NA");
                params.put("picodel_review", "NA");
                params.put("picodel_rating", shop_rating);
                params.put("known_from", "NA");

                Log.e("review_params:",params.toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlReviewAll, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.e("feedback_Response:",""+response);
                        String result = response.getString("posts");
                        Log.e("success_res:",""+result);
                        if (result.equals("success")) {
                            Toast.makeText(MyRecentOrderActivity.this, "Thanks for giving rating & review.", Toast.LENGTH_LONG).show();


                        }else if(result.equalsIgnoreCase("Already given the Feedback")){
                            Toast.makeText(MyRecentOrderActivity.this, "Already given the Feedback.", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                   progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    // gateWay.progressDialogStop();
                    progressDialog.dismiss();

                    GateWay gateWay = new GateWay(MyRecentOrderActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(MyRecentOrderActivity.this,"Please check internet connection",Toast.LENGTH_LONG).show();
        }
    }

    public ArrayList<String> shop_checkOperations(){
        ArrayList<String> shopReviews = new ArrayList<>();
        String qty = shop_ch_qty.getText().toString();
        String qlty = shop_ch_qlty.getText().toString();
        String placking = shop_ch_packing.getText().toString();
        String price = shop_ch_price.getText().toString();


        if (shop_ch_qty.isChecked()) {
            shopReviews.add(qty);
        }
        if (shop_ch_qlty.isChecked()) {
            shopReviews.add(qlty);
        }
        if (shop_ch_packing.isChecked()) {
            shopReviews.add(placking);
        }
        if (shop_ch_price.isChecked()) {
            shopReviews.add(price);
        }
        shopReviews.remove(Collections.singleton(""));
        return  shopReviews;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
