package app.apneareamein.shopping.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.fragments.SubCatProductFragment;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.InnerMovie;
import app.apneareamein.shopping.model.ProductCodeWiseProduct;
import app.apneareamein.shopping.sync.SyncNormalCartItems;
import app.apneareamein.shopping.sync.SyncWishListItems;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.Movie;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class DataSegregationFourTabLayout extends AppCompatActivity {

    private static final String ARG_PAGE_NUMBER = "page_number";
    private static final String ARG_SHOP_INFO = "shop_id";
    private String strCity, strArea, shop_id, product_mainCat, strProductCat, shopStrProductCat,shopProductMainCat,productSuperCat;
    public  static ArrayList<String> subCat_Name;//  = new ArrayList<>();
    private TabsPagerAdapter adapter;
    private ViewPager viewPager;
    InnerMovie innerMovie;
    private TabLayout tabLayout,tabLayout_shopsubcat;
    private int WishListCount, CartCount;
    private String class_name = this.getClass().getSimpleName();
    private RelativeLayout datsegMainLayoyut;
    private LinearLayout Main_Layout_NoInternet;
    private TextView txtNoConnection,tvMessage;
    private Network_Change_Receiver myReceiver;
    int position = 0;
    //private final ArrayList<productCodeWithProductList> FinalList = new ArrayList<>();
    private ArrayList<ProductCodeWiseProduct> tempProductWiseList = new ArrayList<>();
    private LinkedHashMap productListHashMap = new LinkedHashMap(); // will contain all
    private LinkedHashMap tempProductListHashMap = new LinkedHashMap(); // will contain all
    private JSONArray priceArray;
    private JSONArray discountArray;
    private JSONArray SortArray;
    private Set<String> productIdSet = new TreeSet<>();
    private ArrayList<ProductCodeWiseProduct> ProductWiseList = new ArrayList<>();
    private final ArrayList<String> product_id = new ArrayList<>();
     private ListView sortListView;
    private List<Movie> sortMovies;
    private final ArrayList<String> sortNameList = new ArrayList<>();
    private RecyclerView recyclerView;
    private final String[] sortArray = {"Price:Low to High", "Price:High to Low"};
   // private CardAdapter cardAdapter;
    private DialogPlus dialogPlus;
    private String pId;
    private String strId;
    private String strContact;
    private String strEmail;
    private String pName;
    private String product_size;
    private String strCheckQty;
    private TextView tvYes;
    private TextView tvNo;
    //private AlertDialog alertDialog;
    private ProgressBar progressBar;

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                datsegMainLayoyut.setVisibility(View.GONE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                datsegMainLayoyut.setVisibility(View.VISIBLE);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
    //    unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_segregation_four_tab_layout);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myReceiver = new Network_Change_Receiver();
        tvMessage = findViewById(R.id.txtMessage);

        Bundle bundle = getIntent().getExtras();
        shop_id = bundle.getString("shop_id");
        Log.d("abhi", "DataSegregationFourTabLayout onCreate shop_id: " + shop_id);
        strProductCat = bundle.getString("product_cat");
        tvTitle.setText(strProductCat);
        product_mainCat = bundle.getString("product_mainCat");
        //For shop sub cat
        shopStrProductCat = bundle.getString("shopStrProductCat");
        shopProductMainCat = bundle.getString("shopProductMainCat");
        position=  bundle.getInt("position");

        productSuperCat = bundle.getString("productSuperCat");
        progressBar = findViewById(R.id.progressBar);

        GateWay gateWay = new GateWay(this);
        strCity = gateWay.getCity();
        strArea = gateWay.getArea();

        txtNoConnection = findViewById(R.id.txtNoConnection);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        datsegMainLayoyut = findViewById(R.id.datsegMainLayoyut);
        viewPager = findViewById(R.id.viewPager1);
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout_shopsubcat = findViewById(R.id.tabLayout_shopsubcat);

          //subCat_Name = new ArrayList<>();

         getShopSubCat();
        getSubCategories(strProductCat);
         Log.d("strProductCat",""+strProductCat);
        //tabLayout_shopsubcat.getTabAt(2).select();

        tabLayout_shopsubcat.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.isSelected();
                shopStrProductCat = InnerMovieArraylist.get(tab.getPosition()).getProduct_subCat();
                //shopProductMainCat=shopProductMainCat;
                try {
                    tabLayout.removeAllTabs();
                    getSubCategories(shopStrProductCat);
                    Log.e("subCatCalling", "Hello");
                }catch (Exception e){

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

      /*  recyclerView = findViewById(R.id.recycler_view);
        SortArray = new JSONArray(sortNameList);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(DataSegregationFourTabLayout.this);
        recyclerView.setLayoutManager(mLayoutManager);
*/
        //for sort Range
        sortMovies = new ArrayList<>();
        for (String aSortArray : sortArray) {
            Movie priceItem = new Movie(aSortArray);
            sortMovies.add(priceItem);
        }



    }

    ArrayList<InnerMovie> InnerMovieArraylist = new ArrayList<>();

    private void getShopSubCat() { //TODO Server method here
        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) { // Internet connection is not present, Ask user to connect to Internet
            //final GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
            //gateWay.progressDialogStart();
            //progressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("product_cat", shopStrProductCat);
                params.put("product_maincat", shopProductMainCat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tab_param",""+params);
            Log.d("shopStrProductCat",shopStrProductCat);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlShopSubCatResult, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    //Log.d("getShopSubCat",""+response);
                    //progressBar.setVisibility(View.GONE);

                    ArrayList<String> product_subCat = new ArrayList<>();
                    ArrayList<String> product_subCatImage = new ArrayList<>();
                    ArrayList<String> title = new ArrayList<>();

                    if (!response.isNull("posts")) {
                        try {
                            JSONArray mainCatArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);
                                product_subCat.add(jsonMainCatObject.getString("product_subcat"));
                                product_subCatImage.add(jsonMainCatObject.getString("product_subcat_image"));
                                title.add(jsonMainCatObject.getString("title"));
                            }

                                if (product_subCat.size() > 0) {
                                    for (int i = 0; i < product_subCat.size(); i++) {

                                        innerMovie = new InnerMovie();
                                        innerMovie.setProduct_subCat(product_subCat.get(i));
                                        innerMovie.setProduct_subCatImage(product_subCatImage.get(i));
                                        InnerMovieArraylist.add(innerMovie);
                                        //customAdapter.add(innerMovie);

                                        tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText(InnerMovieArraylist.get(i).getProduct_subCat()));
                                    }
                                    /*new Handler().postDelayed(
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    // tabLayout_shopsubcat.getTabAt(2).select();
                                                    tabLayout_shopsubcat.setScrollPosition(position, 0f, true);
                                                    //
                                                }
                                            }, 90);*/

                                }
                                    tabLayout_shopsubcat.getTabAt(2).select();
                                    tabLayout_shopsubcat.setScrollPosition(position, 0f, true);
                                    //tabLayout_shopsubcat.setSelectedTabIndicatorColor(getResources().getColor(R.color.ColorPrimary));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //gateWay.progressDialogStop();
                        }
                    }
                    //gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    //progressBar.setVisibility(View.GONE);
                    //gateWay.progressDialogStop();

                    //GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
           // progressBar.setVisibility(View.GONE);
            Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    private void getSubCategories(String strProductCat) { //TODO Server method here
        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) {

            //progressBar.setVisibility(View.VISIBLE);
            /*subCat_Name = new ArrayList<>();
            subCat_Name.clear();*/
           final ArrayList<String> temp = new ArrayList<>();
            //subCat_Name.clear();
            //tabLayout.removeAllTabs();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("shop_id", shop_id);
                params.put("product_mainCat", product_mainCat);
                params.put("product_cat", strProductCat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("SubCategories",""+params);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetSubCategory, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("SubCategoriesRes",""+response);
                   // progressBar.setVisibility(View.GONE);
                    try {
                        if (response.isNull("sub_cat")) {
                        } else {
                            JSONArray mainCat = response.getJSONArray("sub_cat");
                            for (int i = 0; i < mainCat.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCat.get(i);
                                //subCat_Name.add(jsonMainCatObject.getString("product_sub_subcat"));
                                temp.add(jsonMainCatObject.getString("product_sub_subcat"));
                                Log.d("subCat_Name_size",""+subCat_Name);
                            }//subCat_Name.add(jsonMainCatObject.getString("product_subcat"));
                            subCat_Name = temp;
                            //adapter.notifyDataSetChanged();
                            setViewPager();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                   // progressBar.setVisibility(View.GONE);
                    //GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);

            if(product_mainCat.equalsIgnoreCase("Fruits and Veggies")){
                tabLayout.setVisibility(View.GONE);
                //tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText("Fruits"));
            }
        } else {
          //  progressBar.setVisibility(View.GONE);
            AlertDialog.Builder builder = new AlertDialog.Builder(DataSegregationFourTabLayout.this);
            builder.setMessage("Cannot proceed with the operation,No network connection! Please check your Internet connection")
                    .setTitle("Connection Offline")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            Intent intent = new Intent(DataSegregationFourTabLayout.this, BaseActivity.class);
                            intent.putExtra("tag", "");
                            startActivity(intent);
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void setViewPager() {
        try {
            adapter = new TabsPagerAdapter(getSupportFragmentManager(), shop_id);
            //adapter.notifyDataSetChanged();
             viewPager.setAdapter(adapter);
             tabLayout.setupWithViewPager(viewPager);
             viewPager.setOffscreenPageLimit(5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class TabsPagerAdapter extends  FragmentStatePagerAdapter {
        final String shop_id;

        public TabsPagerAdapter(FragmentManager fm, String shop_id) {
            super(fm);
            this.shop_id = shop_id;
        }

        @Override
        public Fragment getItem(int position) {

               SubCatProductFragment fragment = null;
            try {
                int size = subCat_Name.size();
                if (position < size) {
                    fragment = new SubCatProductFragment();
                    Bundle args = new Bundle();
                    args.putInt(ARG_PAGE_NUMBER, position);
                    args.putString(ARG_SHOP_INFO, shop_id);
                    args.putString("productSuperCat", productSuperCat);
                    args.putString("tag", "normal_tag");
                    fragment.setArguments(args);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            if(subCat_Name!=null) {
                return subCat_Name.size();
            }else {
                return 0;
            }
        }


        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return subCat_Name.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard(DataSegregationFourTabLayout.this);
        //registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

         adapter = new TabsPagerAdapter(getSupportFragmentManager(), shop_id);  //for notifying card adapter
         viewPager.setAdapter(adapter);

        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) {
            GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
            SyncNormalCartItems UT1 = new SyncNormalCartItems(SyncNormalCartItems.context);
            UT1.syncNormalCartItems(gateWay.getContact(), gateWay.getUserEmail(), DataSegregationFourTabLayout.this);

            GateWay gateWay1 = new GateWay(DataSegregationFourTabLayout.this);
            SyncWishListItems UT2 = new SyncWishListItems(SyncWishListItems.context);
            UT2.syncWishListItems(gateWay1.getContact(), gateWay1.getUserEmail(), DataSegregationFourTabLayout.this);

            SyncData();
        }
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void SyncData() { //TODO Server method here
        GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);

        JSONObject params = new JSONObject();
        try {
            params.put("contact", gateWay.getContact());
            params.put("email", gateWay.getUserEmail());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    CartCount = response.getInt("normal_cart_count");
                    WishListCount = response.getInt("wishlist_count");

                    if (CartCount >= 0) {
                        SubCatProductFragment subCatProductFragment = new SubCatProductFragment();
                        subCatProductFragment.updateAddToCartCount(CartCount);
                    }
                    if (WishListCount >= 0) {
                        SubCatProductFragment subCatProductFragment = new SubCatProductFragment();
                        subCatProductFragment.updateWishListCount(WishListCount);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

/*    //get product list
    private void getProducts(JSONArray price, JSONArray discount, JSONArray Sort, String normal_tag, int position, String tag) { //TODO Server method here

        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) {
            FinalList.clear();
            tempProductListHashMap = new LinkedHashMap();
            tempProductWiseList = new ArrayList<>();

            //int page = getArguments().getInt(ARG_PAGE_NUMBER, -1);
            int page = position;
            String tabName = subCat_Name.get(page);
            //String tabName = ProductListTabLayouts.subCat_Name.get(page);

            final GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
            gateWay.progressDialogStart();

            JSONArray pIdArray;
            if (SortArray.length() == 0) {
                pIdArray = new JSONArray();
            } else {
                pIdArray = new JSONArray();
                pIdArray = new JSONArray(productIdSet);
            }

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("shop_id", shop_id);
                params.put("product_subCat", tabName);
                params.put("tag", tag);
                params.put("price", price);
                params.put("discount", discount);
                params.put("sort", Sort);
                params.put("sort_product_id", pIdArray);
                params.put("contactno", gateWay.getContact());

                Log.e("getProducts_param:",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetSubCategoryProductsShopWise, params, new Response.Listener<JSONObject>() {
            //                @Override
            //                public void onResponse(JSONObject response) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    ApplicationUrlAndConstants.urlGetSubCategoryProductsShopWise11,
                    params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response.isNull("posts")) {
                                tvMessage.setVisibility(View.VISIBLE);
                                gateWay.progressDialogStop();
                            } else {
                                try {
                                    Log.e("ProductList_BrandTest:",""+response.toString());
                                    JSONArray mainShopJsonArray = response.getJSONArray("posts");
                                    for (int i = 0; i < mainShopJsonArray.length(); i++) {
                                        JSONObject jSonShopData = mainShopJsonArray.getJSONObject(i);

                                        product_id.add(jSonShopData.getString("product_id"));

                                        tempProductListHashMap.put(jSonShopData.getString("code"), "");

                                        tempProductWiseList.add(new ProductCodeWiseProduct(jSonShopData.getString("code"), jSonShopData.getString("shop_id"),
                                                jSonShopData.getString("shop_name"), jSonShopData.getString("shop_category"),
                                                jSonShopData.getString("product_name"), jSonShopData.getString("product_brand"),
                                                jSonShopData.getString("product_id"), jSonShopData.getString("product_image"),
                                                jSonShopData.getString("product_image1"), jSonShopData.getString("product_image2"),
                                                jSonShopData.getString("product_image3"), jSonShopData.getString("product_image4"),
                                                jSonShopData.getString("product_size"), jSonShopData.getString("product_mrp"),
                                                jSonShopData.getString("product_price"), jSonShopData.getString("product_discount"),
                                                jSonShopData.getString("product_description"), jSonShopData.getString("similar_product_status"), jSonShopData.getString("type"),
                                                jSonShopData.getString("available_product_quantity"), jSonShopData.getString("hindi_name"),jSonShopData.getString("cart_pstatus")));
                                    }
                                    productListHashMap = tempProductListHashMap;
                                    ProductWiseList = tempProductWiseList;
                                    productIdSet = new TreeSet<>();
                                    productIdSet.addAll(product_id);

                                    if (product_id.size() > 0) {
                                        try {
                                            for (Object o : productListHashMap.keySet()) {
                                                String key = (String) o;
                                                productCodeWithProductList withProductCode = new productCodeWithProductList();
                                                withProductCode.code = key;
                                                withProductCode.productCodeWiseProducts = new ArrayList<>();
                                                for (ProductCodeWiseProduct pp : ProductWiseList) {
                                                    if (pp.code.equals(key)) {
                                                        withProductCode.productCodeWiseProducts.add(pp);
                                                    }
                                                }
                                                FinalList.add(withProductCode);
                                            }
                                            tvMessage.setVisibility(View.GONE);
                                            cardAdapter = new CardAdapter(FinalList);
                                            recyclerView.setAdapter(cardAdapter);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                //displayTuto("1");
                            }
                            gateWay.progressDialogStop();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            AppController.getInstance().addToRequestQueue(request);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(DataSegregationFourTabLayout.this);
            builder.setMessage("Cannot proceed with the operation,No network connection! Please check your Internet connection")
                    .setTitle("Connection Offline")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            *//*Intent intent = new Intent(DataSegregationFourTabLayout.this, BaseActivity.class);
                            intent.putExtra("tag", "");
                            startActivity(intent);
                            DataSegregationFourTabLayout.this.finish();
*//*                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
    //adapter 
    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.MainViewHolder> {

        int selectedPosition;
        String selectCondition = "";
        ArrayList<productCodeWithProductList> allProductItemsList = new ArrayList<>();

        public CardAdapter(ArrayList<productCodeWithProductList> finalList) {
            allProductItemsList = finalList;
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_search_products, parent, false);
            final MainViewHolder viewHolder = new MainViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = recyclerView.getChildAdapterPosition(v);
                            productCodeWithProductList movie = allProductItemsList.get(itemPosition);
                            selectedPosition = movie.getPosition();
                            ProductCodeWiseProduct forClickEvent = movie.productCodeWiseProducts.get(selectedPosition);
                            if (forClickEvent.getProduct_name().equals("")) {
                            } else {
                                Intent intent = new Intent(DataSegregationFourTabLayout.this, SingleProductInformation.class);
                                intent.putExtra("product_name", forClickEvent.getProduct_name());
                                intent.putExtra("shop_id", forClickEvent.getShop_id());
                                startActivity(intent);
                            }

                        } else {
                           // GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                          //  gateWay.displaySnackBar();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder viewHolder, final int position) {
            final DBHelper db = new DBHelper(DataSegregationFourTabLayout.this);
            final ArrayList<String> sizesArrayList = new ArrayList<>();
            final ArrayList<String> pricesArrayList = new ArrayList<>();
            ArrayList<String> DiscountpricesArrayList = new ArrayList<>();
            try {

                productCodeWithProductList newProductList = allProductItemsList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                    sizesArrayList.add(tp.getProduct_size());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = allProductItemsList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                    DiscountpricesArrayList.add(tp.getProduct_price());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = allProductItemsList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                    pricesArrayList.add(tp.getProduct_mrp());

                    if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                        viewHolder.LayoutSpinner.setVisibility(View.VISIBLE);
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(DataSegregationFourTabLayout.this, pricesArrayList, DiscountpricesArrayList, sizesArrayList, "");
                        viewHolder.spinner.setAdapter(customSpinnerAdapter);
      
                    } else {
                        viewHolder.LayoutSpinner.setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            *//*--------------------------------for default value------------------------------*//*
            try {
                productCodeWithProductList movie = allProductItemsList.get(position);
                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);


                String cart_server_status = tp.getCart_pstatus();


                if (cart_server_status.equalsIgnoreCase("0")) {
                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                    viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                } else {
                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                    viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                }

                HashMap AddToCartInfo1 = db.getWishListDetails(tp.getProduct_id());
                String btnCheckStatus1 = (String) AddToCartInfo1.get("pId");
                if (btnCheckStatus1 == null) {
                    viewHolder.imgWishList.setVisibility(View.VISIBLE);
                    viewHolder.imgWishListSelected.setVisibility(View.GONE);
                } else {
                    viewHolder.imgWishList.setVisibility(View.GONE);
                    viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                }

                switch (tp.getSelect_type()) {
                    case "Size":
                        viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                        viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                        viewHolder.linearLayoutSelectSize.setVisibility(View.VISIBLE);
                        break;
                    case "Condition":
                        viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                        viewHolder.linearLayoutSelectType.setVisibility(View.VISIBLE);
                        viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                        break;
                    default:
                        viewHolder.linearLayoutSelectCondition.setVisibility(View.GONE);
                        viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                        viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                        break;
                }

                if (tp.getStrHindiName().equals("")) {
                    viewHolder.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    viewHolder.tvProductHindiName.setVisibility(View.VISIBLE);
                    viewHolder.tvProductHindiName.setText(tp.getStrHindiName());
                }

                //TODO show product images setup to glide
               *//* Glide.with(DataSegregationFourTabLayout.this).load("http://simg.picodel.com/" + tp.getProduct_image())
                        .thumbnail(Glide.with(DataSegregationFourTabLayout.this).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.img);*//*

                Picasso.with(DataSegregationFourTabLayout.this).load("https://simg.picodel.com/" + tp.getProduct_image())
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.ic_app_transparent)
                        .into(viewHolder.img);

                //viewHolder.tvSellerName.setText("By:" + " " + tp.getShop_name());
                viewHolder.tvSellerName.setText(tp.getProduct_brand());
                String size = tp.getProduct_size();
                viewHolder.tvProductName.setText(tp.getProduct_name() + " " + size);
                String dis = tp.getProduct_discount();
                String discount = tp.setProduct_discount(dis);
                viewHolder.tvDiscount.setText(discount + "% off");
                String productMrp = tp.getProduct_mrp();
                String discountPrice = tp.getProduct_price();
                String QTY = tp.getStrAvailable_Qty();

                if (QTY.equals("0")) {
                    viewHolder.btnAddToCart.setVisibility(View.GONE);
                    viewHolder.imgOutOfStock.setVisibility(View.VISIBLE);

                    //TODO here out of stock GIF image setup to glide
                   *//* Glide.with(DataSegregationFourTabLayout.this)
                            .load(R.drawable.outofstock)
                            .error(R.drawable.icerror_outofstock)
                            .into(viewHolder.imgOutOfStock);*//*
                    Picasso.with(DataSegregationFourTabLayout.this)
                            .load(R.drawable.outofstock)
                            .error(R.drawable.icerror_outofstock)
                            .into(viewHolder.imgOutOfStock);
                } else {
                    viewHolder.btnAddToCart.setVisibility(View.VISIBLE);
                    viewHolder.imgOutOfStock.setVisibility(View.GONE);
                }

                if (productMrp.equals(discountPrice)) {
                    viewHolder.tvPrice.setVisibility(View.VISIBLE);
                    viewHolder.frameimgMsg.setVisibility(View.GONE);
                    viewHolder.tvMrp.setVisibility(View.GONE);
                } else {
                    viewHolder.tvPrice.setVisibility(View.VISIBLE);
                    viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                    viewHolder.tvMrp.setVisibility(View.VISIBLE);
                    viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                }
                viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
            } catch (Exception e) {
                e.printStackTrace();
            }

            viewHolder.tvRipeType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvRipeType.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvRawType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvRawType.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvSmallSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvSmallSize.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvMediumSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvMediumSize.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvLargeSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvLargeSize.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                    try {
                        productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                        ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);

                        movie.setPosition(pos); //set here position when user any wants to buy multiple size products

                        switch (tp.getSelect_type()) {
                            case "Size":
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.VISIBLE);
                                break;
                            case "Condition":
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                            default:
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                        }

                        if (tp.getStrHindiName().equals("")) {
                            viewHolder.tvProductHindiName.setVisibility(View.GONE);
                        } else {
                            viewHolder.tvProductHindiName.setVisibility(View.VISIBLE);
                            viewHolder.tvProductHindiName.setText(tp.getStrHindiName());
                        }

                        viewHolder.img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogPlus = DialogPlus.newDialog(DataSegregationFourTabLayout.this)
                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .create();

                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);

                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                //TODO here when user clicks on product image setup to glide
                                *//*Glide.with(DataSegregationFourTabLayout.this).load("http://simg.picodel.com/" + tp.getProduct_image())
                                        .thumbnail(Glide.with(DataSegregationFourTabLayout.this).load(R.drawable.loading))
                                        .error(R.drawable.ic_app_transparent)
                                        .fitCenter()
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(alertProductImage1);*//*

                                Picasso.with(DataSegregationFourTabLayout.this).load("https://simg.picodel.com/" + tp.getProduct_image())
                                        .placeholder(R.drawable.loading)

                                        .error(R.drawable.ic_app_transparent)
                                        .into(alertProductImage1);

                                dialogPlus.show();
                            }
                        });

                        //TODO show product images setup to glide
                        *//*Glide.with(DataSegregationFourTabLayout.this).load("http://simg.picodel.com/" + tp.getProduct_image())
                                .thumbnail(Glide.with(DataSegregationFourTabLayout.this).load(R.drawable.loading))
                                .error(R.drawable.ic_app_transparent)
                                .fitCenter()
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(viewHolder.img);*//*

                        Picasso.with(DataSegregationFourTabLayout.this).load("https://simg.picodel.com/" + tp.getProduct_image())
                                .placeholder(R.drawable.loading)

                                .error(R.drawable.ic_app_transparent)
                                .into(viewHolder.img);

                        String size = tp.getProduct_size();
                        viewHolder.tvProductName.setText(tp.getProduct_name() + " " + size);
                        String dis = tp.getProduct_discount();
                        String discount = tp.setProduct_discount(dis);
                        viewHolder.tvDiscount.setText(discount + "% off");
                        String productMrp = tp.getProduct_mrp();
                        String discountPrice = tp.getProduct_price();
                        if (productMrp.equals(discountPrice)) {
                            viewHolder.tvPrice.setVisibility(View.VISIBLE);
                            viewHolder.frameimgMsg.setVisibility(View.GONE);
                            viewHolder.tvMrp.setVisibility(View.GONE);
                        } else {
                            viewHolder.tvPrice.setVisibility(View.VISIBLE);
                            viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                            viewHolder.tvMrp.setVisibility(View.VISIBLE);
                            viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                        }
                        viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                        viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                        String QTY = tp.getStrAvailable_Qty();
                        HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                        String btnCheckStatus = (String) AddToCartInfo.get("new_pid");
                        String cart_server_status = tp.getCart_pstatus();

                        if (cart_server_status.equalsIgnoreCase("0")) {
                            tp.setStatus(false);
                            viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                        } else {
                            tp.setStatus(true);
                            if (QTY.equals("0")) {
                                viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                            } else {
                                viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                            }
                        }

                        if (QTY.equals("0")) {
                            viewHolder.btnAddToCart.setVisibility(View.GONE);
                            viewHolder.imgOutOfStock.setVisibility(View.VISIBLE);

                            //TODO here out of stock GIF image setup to glide
                           *//* Glide.with(DataSegregationFourTabLayout.this)
                                    .load(R.drawable.outofstock)
                                    .error(R.drawable.icerror_outofstock)
                                    .into(viewHolder.imgOutOfStock);*//*
                            Picasso.with(DataSegregationFourTabLayout.this)
                                    .load(R.drawable.outofstock)
                                    .error(R.drawable.icerror_outofstock)
                                    .into(viewHolder.imgOutOfStock);
                        } else {
                            viewHolder.btnAddToCart.setVisibility(View.VISIBLE);
                            viewHolder.imgOutOfStock.setVisibility(View.GONE);
                        }

                        HashMap AddToCartInfo1 = db.getWishListDetails(tp.getProduct_id());
                        String btnCheckStatus1 = (String) AddToCartInfo1.get("pId");
                        if (btnCheckStatus1 == null) {
                            viewHolder.imgWishList.setVisibility(View.VISIBLE);
                            viewHolder.imgWishListSelected.setVisibility(View.GONE);
                        } else {
                            viewHolder.imgWishList.setVisibility(View.GONE);
                            viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                        }

                        viewHolder.tvRipeType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvRipeType.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvRawType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvRawType.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvSmallSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvSmallSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvMediumSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvMediumSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvLargeSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvLargeSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

            viewHolder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogPlus = DialogPlus.newDialog(DataSegregationFourTabLayout.this)
                            .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                            .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                            .setGravity(Gravity.CENTER)
                            .create();

                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);

                    ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                    //TODO here when user clicks on product image setup to glide
                   *//* Glide.with(DataSegregationFourTabLayout.this).load("http://simg.picodel.com/" + tp.getProduct_image())
                            .thumbnail(Glide.with(DataSegregationFourTabLayout.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(alertProductImage1);*//*

                    Picasso.with(DataSegregationFourTabLayout.this).load("https://simg.picodel.com/" + tp.getProduct_image())
                            .placeholder(R.drawable.loading)

                            .error(R.drawable.ic_app_transparent)
                            .into(alertProductImage1);

                    dialogPlus.show();
                }
            });

            viewHolder.imgWishList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                        selectedPosition = movie.getPosition();
                        ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                        HashMap wishListHashMap;
                        pId = forWishList.getProduct_id();

                        wishListHashMap = db.getWishListDetails(pId);
                        strId = (String) wishListHashMap.get("pId");

                        if (strId == null) {
                            db.insertProductIntoWishList(pId);
                            int cnt = (int) db.fetchWishListCount();
                           // updateWishListCount(cnt);

                            HashMap infoChangeButtonName;
                            String pIdForButtonChange = forWishList.getProduct_id();
                            infoChangeButtonName = db.getWishListDetails(pIdForButtonChange);

                            List<String> listProductNameAlreadyHave = new ArrayList<>(infoChangeButtonName.values());
                            boolean val = listProductNameAlreadyHave.contains(pId);

                            if (val) {
                                viewHolder.imgWishList.setVisibility(View.GONE);
                                viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                            } else {
                                viewHolder.imgWishList.setVisibility(View.VISIBLE);
                            }
                            if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) {
                                addWishListItemsToServer();
                            } else {
                              //  GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                              //  gateWay.displaySnackBar(getView());
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void addWishListItemsToServer() { //TODO Server method here
                    GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                    strContact = gateWay.getContact();
                    strEmail = gateWay.getUserEmail();

                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    selectedPosition = movie.getPosition();
                    ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                    pId = forWishList.getProduct_id();

                    JSONObject params = new JSONObject();
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shop_id);
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", strContact);
                        params.put("email", strEmail);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddMyWishListItems, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                            gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });

            viewHolder.imgWishListSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                        selectedPosition = movie.getPosition();
                        ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);
                        pId = forWishList.getProduct_id();
                        db.deleteWishListProductItem(pId);
                        int cnt = (int) db.fetchWishListCount();
                        //updateWishListCount(cnt);
                        //uniqueWishList.remove(pId);
                        viewHolder.imgWishList.setVisibility(View.VISIBLE);
                        viewHolder.imgWishListSelected.setVisibility(View.GONE);
                        deleteoneProductFromWishlist(pId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void deleteoneProductFromWishlist(String pid) { //TODO Server method here
                    if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) {
                        JSONObject params = new JSONObject();
                        final GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                        try {
                            params.put("contactNo", strContact);
                            params.put("email", gateWay.getUserEmail());
                            params.put("product_id", pid);
                            params.put("tag", "delete_one_item");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteMyWishListItems, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {

                                } else {
                                    // db.deleteWishListProductItem(pId);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                gateWay.progressDialogStop();

                                error.printStackTrace();

                                GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                        //GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                        //gateWay.displaySnackBar(getView());
                    }
                }
            });

            viewHolder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) { // Internet connection is not present, Ask user to connect to Internet
                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            final ProductCodeWiseProduct forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);
                            shop_id = forAddToCart.getShop_id();

                            HashMap AddToCartInfo;
                            pId = forAddToCart.getProduct_id();

                            AddToCartInfo = db.getCartDetails(pId);
                            strId = (String) AddToCartInfo.get("new_pid");

                            if (strId == null) { //TODO Server method here
                                forAddToCart.setStatus(false);

                                pName = viewHolder.tvProductName.getText().toString();
                                product_size = forAddToCart.getProduct_size();

                                JSONObject params = new JSONObject();
                                try {
                                    params.put("product_id", pId);
                                    params.put("shop_id", shop_id);
                                    params.put("size", product_size);
                                    params.put("qty", 1);
                                    params.put("contactNo", strContact);
                                    params.put("email", strEmail);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (response.isNull("posts")) {

                                        } else {
                                            try {
                                                strCheckQty = response.getString("posts");
                                                if (strCheckQty.equals("true")) {
                                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DataSegregationFourTabLayout.this);
                                                    alertDialogBuilder.setMessage("You cannot add more than 1000 gm of this product.");
                                                    alertDialogBuilder.setPositiveButton("OK",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                    dialogInterface.dismiss();
                                                                }
                                                            });
                                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                                    alertDialog.show();
                                                } else {
                                                   *//* db.insertCount(pId);
                                                    int count = (int) db.fetchAddToCartCount();
                                                    updateAddToCartCount(count);
                                                    if (count >= 0) {
                                                        forAddToCart.setStatus(true);
                                                        if (forAddToCart.isStatus(true)) {
                                                            viewHolder.btnAddToCart.setText("Go To Cart");
                                                        }
                                                    } else {
                                                        forAddToCart.setStatus(false);
                                                        if (forAddToCart.isStatus(false)) {
                                                            viewHolder.btnAddToCart.setText("Add To Cart");
                                                        }
                                                    }*//*
                                                    db.insertCount(pId);
                                                    int count = SyncDataCart();//(int) db.fetchAddToCartCount();
                                                    if (count >= 0) {
                                                        forAddToCart.setStatus(true);
                                                        // if (forAddToCart.isStatus(true)) {
                                                        // viewHolder.btnAddToCart.setText("Go To Cart");
                                                        //  }
                                                    } else {
                                                        forAddToCart.setStatus(false);
                                                        //if (forAddToCart.isStatus(false)) {
                                                        // viewHolder.btnAddToCart.setText("Add To Cart");
                                                    }
                                                    addCartItemsToServer();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();

                                        GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                                        gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(request);
                            } else {
                                viewHolder.btnAddToCart.setText("Go To Cart");
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                                deleteCartItemDialog(pId);
                            }
                        } else {
                            GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                            gateWay.displaySnackBar(v);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void deleteCartItemDialog(final String pId) {
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    selectedPosition = movie.getPosition();
                    ProductCodeWiseProduct forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);

                    LayoutInflater layoutInflater = LayoutInflater.from(DataSegregationFourTabLayout.this);
                    View review = layoutInflater.inflate(R.layout.same_shop_cart, null);
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DataSegregationFourTabLayout.this);
                    alertDialogBuilder.setView(review);

                    tvMessage = review.findViewById(R.id.txtMessage);
                    tvYes = review.findViewById(R.id.btnYes);
                    tvNo = review.findViewById(R.id.btnNo);
                    tvMessage.setText("Product is " + forAddToCart.getProduct_name() + ". Do you want remove or Go To Cart.");
                    // set dialog message
                    alertDialogBuilder.setCancelable(true);
                    // create alert dialog
                    alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Intent intent = new Intent(DataSegregationFourTabLayout.this, AddToCart.class);
                            startActivity(intent);
                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            deleteNormalProductFromCartItem(pId);
                        }
                    });
                }

                private void deleteNormalProductFromCartItem(String p_id) { //TODO Server method here
                    final GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                    gateWay.progressDialogStart();

                    final DBHelper db = new DBHelper(DataSegregationFourTabLayout.this);

                    if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) { // Internet connection is not present, Ask user to connect to Internet
                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", p_id);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("tag", "delete_one_item");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {

                                } else {
                                    db.deleteProductItem(pId);
                                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                }
                                int count = SyncDataCart();//(int) db.fetchAddToCartCount();
                                //updateAddToCartCount(count);
                                gateWay.progressDialogStop();
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                gateWay.progressDialogStop();

                                error.printStackTrace();

                                GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                      //  gateWay.displaySnackBar(getView());
                    }
                }

                private void addCartItemsToServer() { //TODO Server method here
                    JSONObject params = new JSONObject();
                    if (pName.contains("Small")) {
                        selectCondition = "Small";
                    } else if (pName.contains("Large")) {
                        selectCondition = "Large";
                    } else if (pName.contains("Medium")) {
                        selectCondition = "Medium";
                    } else if (pName.contains("Ripe")) {
                        selectCondition = "Ripe";
                    } else if (pName.contains("Raw")) {
                        selectCondition = "Raw";
                    } else {
                        selectCondition = "";
                    }
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shop_id);
                        params.put("selectedType", selectCondition);
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", strContact);
                        params.put("email", strEmail);
                        params.put("sessionMainCat", productSuperCat);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("productSuperCat",""+params);
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            Log.d("addCartItemsToServer1",""+response);
                            try {
                                String posts = response.getString("posts");

                                if (posts.equals("true")){
                                    db.insertCount(pId);

                                    int count = SyncDataCart();//(int) db.fetchAddToCartCount();
                                    //updateAddToCartCount(count);
                                    if (count >= 0) {

                                        viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                        viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));

                                    } else {

                                        viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                        viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));

                                    }

                                    Log.d("addCartItemsToServer1",""+posts);
                                    Toast toast = Toast.makeText(DataSegregationFourTabLayout.this, "Adding product to cart.", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    openSessionDialog(posts);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                            gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });
        }

        @Override
        public int getItemCount() {
            return allProductItemsList.size();
        }

        class MainViewHolder extends RecyclerView.ViewHolder {

            final ImageView img;
            final TextView btnAddToCart;
            final ImageView imgWishList;
            final ImageView imgWishListSelected;
            final ImageView imgOutOfStock;
            final TextView tvProductName;
            final TextView tvSellerName;
            final TextView tvMrp;
            final TextView tvDiscount;
            final TextView tvPrice;
            final TextView tvRipeType;
            final TextView tvRawType;
            final TextView tvSmallSize;
            final TextView tvMediumSize;
            final TextView tvLargeSize;
            final TextView tvProductHindiName;
            final RelativeLayout linearLayoutProduct;
            final CardView cardView;
            final LinearLayout linearLayoutSelectCondition;
            final LinearLayout linearLayoutSelectType;
            final LinearLayout linearLayoutSelectSize;
            final Spinner spinner;
            final RelativeLayout LayoutSpinner;
            final FrameLayout frameimgMsg;

            public MainViewHolder(View itemView) {
                super(itemView);

                cardView = itemView.findViewById(R.id.view);
                img = itemView.findViewById(R.id.imgProduct);
                tvProductName = itemView.findViewById(R.id.txtProductName);
                tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
                tvSellerName = itemView.findViewById(R.id.txtSellerName);
                tvMrp = itemView.findViewById(R.id.txtTotal);
                tvDiscount = itemView.findViewById(R.id.txtDiscount);
                frameimgMsg = itemView.findViewById(R.id.frameimgMsg);
                tvPrice = itemView.findViewById(R.id.txtPrice);
                tvRipeType = itemView.findViewById(R.id.txtRipeType);
                tvRawType = itemView.findViewById(R.id.txtRawType);
                tvSmallSize = itemView.findViewById(R.id.txtSmallSize);
                tvMediumSize = itemView.findViewById(R.id.txtMediumSize);
                tvLargeSize = itemView.findViewById(R.id.txtLargeSize);
                btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
                imgWishList = itemView.findViewById(R.id.wishList);
                imgWishListSelected = itemView.findViewById(R.id.wishList1);
                linearLayoutProduct = itemView.findViewById(R.id.linearLayoutProduct);
                linearLayoutSelectCondition = itemView.findViewById(R.id.selectCondition);
                linearLayoutSelectType = itemView.findViewById(R.id.linearLayoutSelectType);
                linearLayoutSelectSize = itemView.findViewById(R.id.linearLayoutSelectSize);
                imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
                spinner = itemView.findViewById(R.id.spinner);
                LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
                btnAddToCart.setTag(this);
                imgWishList.setTag(this);
                imgWishListSelected.setTag(this);
                linearLayoutProduct.setTag(this);
            }
        }
    }
    //productCodeWithProductList
    private class productCodeWithProductList {
        String code;
        int position;

        ArrayList<ProductCodeWiseProduct> productCodeWiseProducts;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
    //show dialog when If he tries to select something in other category
    private void openSessionDialog(String message) {
        final Dialog dialog = new Dialog(DataSegregationFourTabLayout.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sessoin);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        TextView tv_clearcart = dialog.findViewById(R.id.tv_clearcart);
        TextView tv_continue = dialog.findViewById(R.id.tv_continue);
        tv_message.setText(message);
        tv_clearcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DataSegregationFourTabLayout.this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            // GateWay gateWay = new GateWay(getActivity());
                            //gateWay.displaySnackBar(homeMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void deleteAllProductFromCartItem() {
        if (Connectivity.isConnected(DataSegregationFourTabLayout.this)) {
            final GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(DataSegregationFourTabLayout.this);
                        db.deleteOnlyCartTable();

                        //after remove cart items from local and online db
                        //then sync online cart count again
                        SyncData();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(DataSegregationFourTabLayout.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }
    public void updateAddToCartCount(final int count) {
        if (tvCartNumber == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCartNumber.setVisibility(View.VISIBLE);
                if (count == 0) {
                    tvCartNumber.setText("" + count);
                } else {
                    tvCartNumber.setText("" + count);
                }
            }
        });
    }
    public void updateWishListCount(final int count) {
        if (tvWishList == null) return;
       runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvWishList.setVisibility(View.VISIBLE);
                if (count == 0) {
                    tvWishList.setText("" + count);
                } else {
                    tvWishList.setText("" + count);
                }
            }
        });
    }
    private int SyncDataCart() { //TODO Server method here
        JSONObject params = new JSONObject();
        try {
            params.put("contact", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    CartCount = response.getInt("normal_cart_count");
                    WishListCount = response.getInt("wishlist_count");

                    if (CartCount >= 0) {
                     //   updateAddToCartCount(CartCount);
                    }
                    if (WishListCount >= 0) {
                     //   updateWishListCount(WishListCount);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
        return CartCount;
    }*/
}
