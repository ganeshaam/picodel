package app.apneareamein.shopping.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.CustomSpinnerAdapter;
import app.apneareamein.shopping.adapters.ProductListAdaptor;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.model.ProductListModel;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.HidingScrollListenerForFab;
import app.apneareamein.shopping.utils.Movie;
import app.apneareamein.shopping.utils.VolleySingleton;

public class ProductListActity extends AppCompatActivity {

    private static final String ARG_PAGE_NUMBER = "page_number";
    private static final String ARG_SHOP_INFO = "shop_id";
    private TextView tvWishList;
    private String strCity, strArea, shop_id, product_mainCat, strProductCat, shopStrProductCat,shopProductMainCat,productSuperCat;
    private final String[] priceRangeArray = {"0-100", "100-500", "500-1000", "1000-10000", "10000-20000", "20000 and higher"};
    private final String[] discountRangeArray = {"40% and Higher", "30% and Higher", "20% and Higher", "10% and Higher", "5% and Higher"};
    private final ArrayList<String> product_id = new ArrayList<>();
    private final ArrayList<productCodeWithProductList> FinalList = new ArrayList<>();

    private final ArrayList<Object> priceRangeSelectedList = new ArrayList<>();
    private final ArrayList<Object> discountRangeSelectedList = new ArrayList<>();
    private final ArrayList<String> priceNameList = new ArrayList<>();
    private final ArrayList<String> discountNameList = new ArrayList<>();
    private final String[] sortArray = {"Price:Low to High", "Price:High to Low"};
    private final ArrayList<String> sortNameList = new ArrayList<>();
    private final ArrayList<Object> sortRangeSelectedList = new ArrayList<>();
    private AlertDialog alertDialog;
    //Start of Dialog data
    private ListView priceListView, discountListView;
    private PriceCustomListViewAdapter priceAdapter;
    private PriceCustomListViewAdapter discountAdapter;
    private ProductListAdaptor productListAdaptor;
    private CheckBox priceCheckBox;
    private RadioButton discountRadioButton;
    private DialogPlus dialogPlus;
    private String shopId;
    private String product_size;
    private String pId;
    private String strContact;
    private String pName;
    private String strId;
    private String strEmail;
    private String TAG;
    private String strName;
    private TextView tvMessage;
    private TextView tvYes;
    private TextView tvNo;
    private TextView tvMsg;
    private TextView tvAPPLY;
    private TextView tvNO;
    private TextView tvCartNumber;
    private RecyclerView recyclerView;
    private  CardAdapter cardAdapter;
    private ArrayList<ProductCodeWiseProduct> ProductWiseList = new ArrayList<>();
    private ArrayList<ProductCodeWiseProduct> tempProductWiseList = new ArrayList<>();
    public ArrayList<ProductListModel> product_list_value = new ArrayList<>();
    private LinkedHashMap productListHashMap = new LinkedHashMap(); // will contain all
    private LinkedHashMap tempProductListHashMap = new LinkedHashMap(); // will contain all
    private List<Movie> priceMovies;
    private List<Movie> discountMovies;
    private int positionForRadioButton = -1;
    private int countForDiscountRadioButton = 0;
    private JSONArray priceArray;
    private JSONArray discountArray;
    private JSONArray SortArray;
    private Set<String> productIdSet = new TreeSet<>();
    private ListView sortListView;
    private List<Movie> sortMovies;
    private  SortCustomListViewAdapter sortAdapter;
    private RadioButton sortRadioButton;
    private FrameLayout frameFABLayout;
    private int WishListCount, CartCount,position;
    private String strCheckQty;
    //private ProductListTabLayouts homePageActivity;
    ProgressBar progressBar;
    Context mContext;
    public static final String MY_PREFS_NAME = "PICoDEL";
    private String Zone_Area ="",v_state="",v_city="",sessionMainCat="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_product_list);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = ProductListActity.this;
        Bundle bundle = getIntent().getExtras();
        shop_id = bundle.getString("shop_id");
        Log.d("abhi", "DataSegregationFourTabLayout onCreate shop_id: " + shop_id);
        strProductCat = bundle.getString("product_cat");
        tvTitle.setText(strProductCat);
        product_mainCat = bundle.getString("product_mainCat");
        //For shop sub cat
        shopStrProductCat = bundle.getString("shopStrProductCat");
        shopProductMainCat = bundle.getString("shopProductMainCat");
        position=  bundle.getInt("position");

        Intent cat_intent = getIntent();

        if(cat_intent!=null){
            productSuperCat =  cat_intent.getStringExtra("productSuperCat");
        }

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");
        v_city = prefs.getString("v_city", "");
        v_state = prefs.getString("v_state", "");
        sessionMainCat = prefs.getString("sessionMainCat", "");

        GateWay gateWay = new GateWay(mContext);
        strCity = gateWay.getCity();
        strArea = gateWay.getArea();
        strContact = gateWay.getContact();
        strName = gateWay.getUserName();
        strEmail = gateWay.getUserEmail();

        tvCartNumber =  findViewById(R.id.cartNumber);
        FloatingActionButton floatingActionButton =  findViewById(R.id.userCart);
        frameFABLayout =  findViewById(R.id.frameFABLayout);
        tvMessage =  findViewById(R.id.txtMessage);
        TextView tvFilter =  findViewById(R.id.txtFilter);
        TextView tvSort =  findViewById(R.id.txtSort);
        progressBar =  findViewById(R.id.progressBar);

        recyclerView =   findViewById(R.id.recycler_view);
        SortArray = new JSONArray(sortNameList);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setOnScrollListener(new HidingScrollListenerForFab() {
            @Override
            public void onHide() {
                hideViews();
            }
            @Override
            public void onShow() {
                showViews();
            }
        });

        if (Connectivity.isConnected(mContext)) {
            getProducts(null, null, null, TAG);
        }

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                    //DBHelper db = new DBHelper(context);
                    int count = SyncData();//(int) db.fetchAddToCartCount();
                    if (count == 0) {
                        // GateWay gateWay = new GateWay(mContext);
                        //gateWay.
                        cartAlertDialog();
                    } else {
                        Intent intent = new Intent(mContext, AddToCart.class);
                        startActivity(intent);
                    }
                } else {
                    //GateWay gateWay = new GateWay(mContext);
                    //gateWay.displaySnackBar(getView());
                }
            }
        });

    }

    private void hideViews() {
        frameFABLayout.animate().translationY(frameFABLayout.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    private void showViews() {
        frameFABLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    private void getProducts(JSONArray price, JSONArray discount, JSONArray Sort, String tag) { //TODO Server method here

        if (Connectivity.isConnected(mContext)) {
            progressBar.setVisibility(View.VISIBLE);
            FinalList.clear();
            tempProductListHashMap = new LinkedHashMap();
            tempProductWiseList = new ArrayList<>();

            //int page = getArguments().getInt(ARG_PAGE_NUMBER, -1);
            //String tabName = DataSegregationFourTabLayout.subCat_Name.get(page);
            //String tabName = ProductListTabLayouts.subCat_Name.get(page);

            final GateWay gateWay = new GateWay(mContext);
            //gateWay.progressDialogStart();
            JSONArray pIdArray;
            if (SortArray.length() == 0) {
                pIdArray = new JSONArray();
            } else {
                pIdArray = new JSONArray();
                pIdArray = new JSONArray(productIdSet);
            }
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String Zone_Area = prefs.getString("Zone_Area", "");

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("areaname", Zone_Area);
                params.put("shop_id", shopId);
                params.put("product_subCat", strProductCat);
                params.put("tag", "normal_tag");
                params.put("price", price);
                params.put("discount", discount);
                params.put("sort", Sort);
                params.put("sort_product_id", pIdArray);
                params.put("contactno", gateWay.getContact());

                Log.e("getSubCatProducta_param:",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetSubCategoryProductsShopWise, params, new Response.Listener<JSONObject>() {
            //@Override
            //public void onResponse(JSONObject response) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    ApplicationUrlAndConstants.urlGetSubCategoryProductsShopWise,
                    params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response.isNull("posts")) {
                                tvMessage.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                            } else {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    Log.e("ProductLista_BrandTest:",""+response.toString());
                                    JSONArray mainShopJsonArray = response.getJSONArray("posts");
                                    for (int i = 0; i < mainShopJsonArray.length(); i++) {
                                        JSONObject jSonShopData = mainShopJsonArray.getJSONObject(i);

                                        product_id.add(jSonShopData.getString("product_id"));

                                        tempProductListHashMap.put(jSonShopData.getString("code"), "");

                                        tempProductWiseList.add(new ProductCodeWiseProduct(jSonShopData.getString("code"), jSonShopData.getString("shop_id"),
                                                jSonShopData.getString("shop_name"), jSonShopData.getString("shop_category"),
                                                jSonShopData.getString("product_name"), jSonShopData.getString("product_brand"),
                                                jSonShopData.getString("product_id"), jSonShopData.getString("product_image"),
                                                jSonShopData.getString("product_image1"), jSonShopData.getString("product_image2"),
                                                jSonShopData.getString("product_image3"), jSonShopData.getString("product_image4"),
                                                jSonShopData.getString("product_size"), jSonShopData.getString("product_mrp"),
                                                jSonShopData.getString("product_price"), jSonShopData.getString("product_discount"),
                                                jSonShopData.getString("product_description"), jSonShopData.getString("similar_product_status"), jSonShopData.getString("type"),
                                                jSonShopData.getString("available_product_quantity"), jSonShopData.getString("hindi_name"),jSonShopData.getString("cart_pstatus")));

                                     /*   product_list_value.add(new ProductListModel(jSonShopData.getString("code"), jSonShopData.getString("shop_id"),
                                                jSonShopData.getString("shop_name"), jSonShopData.getString("shop_category"),
                                                jSonShopData.getString("product_name"), jSonShopData.getString("product_brand"),
                                                jSonShopData.getString("product_id"), jSonShopData.getString("product_image"),
                                                jSonShopData.getString("product_image1"), jSonShopData.getString("product_image2"),
                                                jSonShopData.getString("product_image3"), jSonShopData.getString("product_image4"),
                                                jSonShopData.getString("product_size"), jSonShopData.getString("product_mrp"),
                                                jSonShopData.getString("product_price"), jSonShopData.getString("product_discount"),
                                                jSonShopData.getString("product_description"), jSonShopData.getString("similar_product_status"), jSonShopData.getString("type"),
                                                jSonShopData.getString("available_product_quantity"), jSonShopData.getString("hindi_name"),jSonShopData.getString("cart_pstatus")));*/
                                    }
                                    productListHashMap = tempProductListHashMap;
                                    ProductWiseList = tempProductWiseList;

                                    productIdSet = new TreeSet<>();
                                    productIdSet.addAll(product_id);

                                   /* productListAdaptor = new ProductListAdaptor(product_list_value,mContext,"");
                                    recyclerView.setAdapter(productListAdaptor);
                                    productListAdaptor.notifyDataSetChanged();*/
                                    if (product_id.size() > 0) {
                                        try {
                                            for (Object o : productListHashMap.keySet()) {
                                                String key = (String) o;
                                                productCodeWithProductList withProductCode = new productCodeWithProductList();
                                                withProductCode.code = key;
                                                withProductCode.productCodeWiseProducts = new ArrayList<>();
                                                for (ProductCodeWiseProduct pp : ProductWiseList) {
                                                    if (pp.code.equals(key)) {
                                                        withProductCode.productCodeWiseProducts.add(pp);
                                                    }
                                                    Log.e("product_check:",""+pp.getProduct_name());
                                                }
                                                FinalList.add(withProductCode);
                                            }
                                            Log.e("product_check:",""+FinalList);
                                            tvMessage.setVisibility(View.GONE);
                                            cardAdapter = new CardAdapter(FinalList);
                                            recyclerView.setAdapter(cardAdapter);

                                          /*productListAdaptor = new ProductListAdaptor(FinalList2,mContext);
                                            recyclerView.setAdapter(productListAdaptor); */
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                //displayTuto("1");
                            }
                            //  gateWay.progressDialogStop();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    progressBar.setVisibility(View.GONE);
                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            /*request.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));*/
            //AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
            Log.e("URL:REQUEST",""+request);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Cannot proceed with the operation,No network connection! Please check your Internet connection")
                    .setTitle("Connection Offline")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            Intent intent = new Intent(mContext, BaseActivity.class);
                            intent.putExtra("tag", "");
                            startActivity(intent);
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void updateAddToCartCount(final int count) {
        if (tvCartNumber == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCartNumber.setVisibility(View.VISIBLE);
                if (count == 0) {
                    tvCartNumber.setText("" + count);
                } else {
                    tvCartNumber.setText("" + count);
                }
            }
        });
    }

    public void updateWishListCount(final int count) {
        if (tvWishList == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvWishList.setVisibility(View.VISIBLE);
                if (count == 0) {
                    tvWishList.setText("" + count);
                } else {
                    tvWishList.setText("" + count);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Connectivity.isConnected(mContext)) {
            SyncData();
        }
    }

    private int SyncData() { //TODO Server method here
        JSONObject params = new JSONObject();
        try {
            params.put("contact", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    CartCount = response.getInt("normal_cart_count");
                    WishListCount = response.getInt("wishlist_count");

                    if (CartCount >= 0) {
                        updateAddToCartCount(CartCount);
                    }
                    if (WishListCount >= 0) {
                        updateWishListCount(WishListCount);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
        return CartCount;
    }



    public class productCodeWithProductList {
        String code;
        int position;

        ArrayList<ProductCodeWiseProduct> productCodeWiseProducts;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

    public class ProductCodeWiseProduct {
        final String code;
        final String shop_id;
        final String shop_name;
        final String shop_category;
        final String product_name;
        final String product_brand;
        final String product_id;
        final String product_image;
        final String product_image1;
        final String product_image2;
        final String product_image3;
        final String product_image4;
        final String product_size;
        final String product_mrp;
        final String product_price;
        final String product_description;
        final String similar_product_status;
        final String select_type;
        final String strAvailable_Qty;
        final String strHindiName;
        String product_discount;
        String cart_pstatus;
        boolean status = false;

        public ProductCodeWiseProduct(String code, String shop_id, String shop_name, String shop_category,
                                      String product_name, String product_brand, String product_id, String product_image,
                                      String product_image1, String product_image2, String product_image3,
                                      String product_image4, String product_size, String product_mrp, String product_price,
                                      String product_discount, String product_description, String similar_product_status, String type,
                                      String available_qty, String hindiname , String cart_pstatus ) {
            this.code = code;
            this.shop_id = shop_id;
            this.shop_name = shop_name;
            this.shop_category = shop_category;
            this.product_name = product_name;
            this.product_brand = product_brand;
            this.product_id = product_id;
            this.product_image = product_image;
            this.product_image1 = product_image1;
            this.product_image2 = product_image2;
            this.product_image3 = product_image3;
            this.product_image4 = product_image4;
            this.product_size = product_size;
            this.product_mrp = product_mrp;
            this.product_price = product_price;
            this.product_discount = product_discount;
            this.product_description = product_description;
            this.similar_product_status = similar_product_status;
            this.select_type = type;
            this.strAvailable_Qty = available_qty;
            this.strHindiName = hindiname;
            this.cart_pstatus = cart_pstatus;
        }

        public String getStrAvailable_Qty() {
            return strAvailable_Qty;
        }

        public String getStrHindiName() {
            return strHindiName;
        }

        public String getCode() {
            return code;
        }

        public String getShop_id() {
            return shop_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getProduct_brand() {
            return product_brand;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_image() {
            return product_image;
        }

        public String getProduct_size() {
            return product_size;
        }

        public String getProduct_mrp() {
            return product_mrp;
        }

        public String getProduct_price() {
            return product_price;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String getSelect_type() {
            return select_type;
        }

        public boolean isStatus(boolean check) {
            return status;
        }

        public String getSimilar_product_status() {
            return similar_product_status;
        }

        public String setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
            return product_discount;
        }

        public boolean setStatus(boolean status) {
            this.status = status;
            return status;
        }

        public String getCart_pstatus() {
            return cart_pstatus;
        }

        public void setCart_pstatus(String cart_pstatus) {
            this.cart_pstatus = cart_pstatus;
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.MainViewHolder> {

        int selectedPosition;
        String selectCondition = "";
        ArrayList<productCodeWithProductList> allProductItemsList;

        public CardAdapter(ArrayList<productCodeWithProductList> finalList) {
            allProductItemsList = finalList;
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_search_products, parent, false);
            final MainViewHolder viewHolder = new MainViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = recyclerView.getChildAdapterPosition(v);
                            productCodeWithProductList movie = allProductItemsList.get(itemPosition);
                            selectedPosition = movie.getPosition();
                            ProductCodeWiseProduct forClickEvent = movie.productCodeWiseProducts.get(selectedPosition);
                            if (forClickEvent.getProduct_name().equals("")) {
                            } else {
                                Intent intent = new Intent(mContext, SingleProductInformation.class);
                                intent.putExtra("product_name", forClickEvent.getProduct_name());
                                intent.putExtra("shop_id", forClickEvent.getShop_id());
                                startActivity(intent);
                            }
                        } else {
                            //GateWay gateWay = new GateWay(mContext);
                            // gateWay.displaySnackBar(getView());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder viewHolder, final int position) {
            final DBHelper db = new DBHelper(mContext);
            final ArrayList<String> sizesArrayList = new ArrayList<>();
            final ArrayList<String> pricesArrayList = new ArrayList<>();
            ArrayList<String> DiscountpricesArrayList = new ArrayList<>();
            try {
                productCodeWithProductList newProductList = allProductItemsList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                    sizesArrayList.add(tp.getProduct_size());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = allProductItemsList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                    DiscountpricesArrayList.add(tp.getProduct_price());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = allProductItemsList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    ProductCodeWiseProduct tp = newProductList.productCodeWiseProducts.get(l);
                    pricesArrayList.add(tp.getProduct_mrp());

                    if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                        viewHolder.LayoutSpinner.setVisibility(View.VISIBLE);
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(mContext, pricesArrayList, DiscountpricesArrayList, sizesArrayList, "");
                        viewHolder.spinner.setAdapter(customSpinnerAdapter);
                    } else {
                        viewHolder.LayoutSpinner.setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*--------------------------------for default value------------------------------*/
            try {
                productCodeWithProductList movie = allProductItemsList.get(position);
                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);

                // String cart_server_status = tp.getCart_pstatus();
                String cart_server_status = tp.getCart_pstatus();

                if (cart_server_status.equalsIgnoreCase("0")) {
                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                    viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                } else {
                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                    viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                }

                HashMap AddToCartInfo1 = db.getWishListDetails(tp.getProduct_id());
                String btnCheckStatus1 = (String) AddToCartInfo1.get("pId");
                String status_from_server = tp.getCart_pstatus();
                if (btnCheckStatus1 == null) {
                    viewHolder.imgWishList.setVisibility(View.VISIBLE);
                    viewHolder.imgWishListSelected.setVisibility(View.GONE);
                } else {
                    viewHolder.imgWishList.setVisibility(View.GONE);
                    viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                }

                switch (tp.getSelect_type()) {
                    case "Size":
                        viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                        viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                        viewHolder.linearLayoutSelectSize.setVisibility(View.VISIBLE);
                        break;
                    case "Condition":
                        viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                        viewHolder.linearLayoutSelectType.setVisibility(View.VISIBLE);
                        viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                        break;
                    default:
                        viewHolder.linearLayoutSelectCondition.setVisibility(View.GONE);
                        viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                        viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                        break;
                }

                if (tp.getStrHindiName().equals("")) {
                    viewHolder.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    viewHolder.tvProductHindiName.setVisibility(View.VISIBLE);
                    viewHolder.tvProductHindiName.setText(tp.getStrHindiName());
                }

                //TODO show product images setup to glide
               /* Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.img);*/

                Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.ic_app_transparent)
                        .into(viewHolder.img);

                //viewHolder.tvSellerName.setText("By:" + " " + tp.getShop_name());
                viewHolder.tvSellerName.setText(tp.getProduct_brand());
                String size = tp.getProduct_size();
                viewHolder.tvProductName.setText(tp.getProduct_name() + " " + size);
                String dis = tp.getProduct_discount();
                String discount = tp.setProduct_discount(dis);
                viewHolder.tvDiscount.setText(discount + "% off");
                String productMrp = tp.getProduct_mrp();
                String discountPrice = tp.getProduct_price();
                String QTY = tp.getStrAvailable_Qty();

                if (QTY.equals("0")) {
                    viewHolder.btnAddToCart.setVisibility(View.GONE);
                    viewHolder.imgOutOfStock.setVisibility(View.VISIBLE);

                    //TODO here out of stock GIF image setup to glide
                   /* Glide.with(mContext)
                            .load(R.drawable.outofstock)
                            .error(R.drawable.icerror_outofstock)
                            .into(viewHolder.imgOutOfStock);*/
                    Picasso.with(mContext)
                            .load(R.drawable.outofstock)
                            .error(R.drawable.icerror_outofstock)
                            .into(viewHolder.imgOutOfStock);
                } else {
                    viewHolder.btnAddToCart.setVisibility(View.VISIBLE);
                    viewHolder.imgOutOfStock.setVisibility(View.GONE);
                }

                if (productMrp.equals(discountPrice)) {
                    viewHolder.tvPrice.setVisibility(View.VISIBLE);
                    viewHolder.frameimgMsg.setVisibility(View.GONE);
                    viewHolder.tvMrp.setVisibility(View.GONE);
                } else {
                    viewHolder.tvPrice.setVisibility(View.VISIBLE);
                    viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                    viewHolder.tvMrp.setVisibility(View.VISIBLE);
                    viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                }
                viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
            } catch (Exception e) {
                e.printStackTrace();
            }

            viewHolder.tvRipeType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvRipeType.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvRawType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvRawType.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvSmallSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvSmallSize.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvMediumSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvMediumSize.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.tvLargeSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = viewHolder.tvLargeSize.getText().toString();
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);
                    viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                }
            });

            viewHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                    try {
                        productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                        ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);

                        movie.setPosition(pos); //set here position when user any wants to buy multiple size products

                        switch (tp.getSelect_type()) {
                            case "Size":
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.VISIBLE);
                                break;
                            case "Condition":
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.VISIBLE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                            default:
                                viewHolder.linearLayoutSelectCondition.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectType.setVisibility(View.GONE);
                                viewHolder.linearLayoutSelectSize.setVisibility(View.GONE);
                                break;
                        }

                        if (tp.getStrHindiName().equals("")) {
                            viewHolder.tvProductHindiName.setVisibility(View.GONE);
                        } else {
                            viewHolder.tvProductHindiName.setVisibility(View.VISIBLE);
                            viewHolder.tvProductHindiName.setText(tp.getStrHindiName());
                        }

                        viewHolder.img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogPlus = DialogPlus.newDialog(mContext)
                                        .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .create();

                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);

                                ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                                //TODO here when user clicks on product image setup to glide
                                Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                        .placeholder(R.drawable.loading)

                                        .error(R.drawable.ic_app_transparent)
                                        .into(alertProductImage1);

                                dialogPlus.show();
                            }
                        });

                        //TODO show product images setup to glide
                        Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                                .placeholder(R.drawable.loading)
                                .error(R.drawable.ic_app_transparent)
                                .into(viewHolder.img);

                        String size = tp.getProduct_size();
                        viewHolder.tvProductName.setText(tp.getProduct_name() + " " + size);
                        String dis = tp.getProduct_discount();
                        String discount = tp.setProduct_discount(dis);
                        viewHolder.tvDiscount.setText(discount + "% off");
                        String productMrp = tp.getProduct_mrp();
                        String discountPrice = tp.getProduct_price();
                        if (productMrp.equals(discountPrice)) {
                            viewHolder.tvPrice.setVisibility(View.VISIBLE);
                            viewHolder.frameimgMsg.setVisibility(View.GONE);
                            viewHolder.tvMrp.setVisibility(View.GONE);
                        } else {
                            viewHolder.tvPrice.setVisibility(View.VISIBLE);
                            viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                            viewHolder.tvMrp.setVisibility(View.VISIBLE);
                            viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                        }
                        viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                        viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                        String QTY = tp.getStrAvailable_Qty();
                        HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                        String btnCheckStatus = (String) AddToCartInfo.get("new_pid");
                        String cart_server_status = tp.getCart_pstatus();

                        if (cart_server_status.equalsIgnoreCase("0")) {
                            tp.setStatus(false);
                            viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                        } else {
                            tp.setStatus(true);
                            if (QTY.equals("0")) {
                                viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                            } else {
                                viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                            }
                        }

                        if (QTY.equals("0")) {
                            viewHolder.btnAddToCart.setVisibility(View.GONE);
                            viewHolder.imgOutOfStock.setVisibility(View.VISIBLE);

                            //TODO here out of stock GIF image setup to glide

                            Picasso.with(mContext)
                                    .load(R.drawable.outofstock)
                                    .error(R.drawable.icerror_outofstock)
                                    .into(viewHolder.imgOutOfStock);
                        } else {
                            viewHolder.btnAddToCart.setVisibility(View.VISIBLE);
                            viewHolder.imgOutOfStock.setVisibility(View.GONE);
                        }

                        HashMap AddToCartInfo1 = db.getWishListDetails(tp.getProduct_id());
                        String btnCheckStatus1 = (String) AddToCartInfo1.get("pId");
                        if (btnCheckStatus1 == null) {
                            viewHolder.imgWishList.setVisibility(View.VISIBLE);
                            viewHolder.imgWishListSelected.setVisibility(View.GONE);
                        } else {
                            viewHolder.imgWishList.setVisibility(View.GONE);
                            viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                        }

                        viewHolder.tvRipeType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvRipeType.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvRawType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvRawType.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvSmallSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvSmallSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvMediumSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvMediumSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });

                        viewHolder.tvLargeSize.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectCondition = viewHolder.tvLargeSize.getText().toString();
                                productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                                ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(pos);
                                viewHolder.tvProductName.setText(selectCondition + " " + tp.getProduct_name() + " " + tp.getProduct_size());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

            viewHolder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogPlus = DialogPlus.newDialog(mContext)
                            .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                            .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                            .setGravity(Gravity.CENTER)
                            .create();

                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    ProductCodeWiseProduct tp = movie.productCodeWiseProducts.get(0);

                    ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                    //TODO here when user clicks on product image setup to glide
                   /* Glide.with(mContext).load("http://simg.picodel.com/" + tp.getProduct_image())
                            .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(alertProductImage1);*/

                    Picasso.with(mContext).load("https://simg.picodel.com/" + tp.getProduct_image())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(alertProductImage1);

                    dialogPlus.show();
                }
            });

            viewHolder.imgWishList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                        selectedPosition = movie.getPosition();
                        ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                        HashMap wishListHashMap;
                        pId = forWishList.getProduct_id();

                        wishListHashMap = db.getWishListDetails(pId);
                        strId = (String) wishListHashMap.get("pId");

                        if (strId == null) {
                            db.insertProductIntoWishList(pId);
                            int cnt = (int) db.fetchWishListCount();
                            updateWishListCount(cnt);

                            HashMap infoChangeButtonName;
                            String pIdForButtonChange = forWishList.getProduct_id();
                            infoChangeButtonName = db.getWishListDetails(pIdForButtonChange);

                            List<String> listProductNameAlreadyHave = new ArrayList<>(infoChangeButtonName.values());
                            boolean val = listProductNameAlreadyHave.contains(pId);

                            if (val) {
                                viewHolder.imgWishList.setVisibility(View.GONE);
                                viewHolder.imgWishListSelected.setVisibility(View.VISIBLE);
                            } else {
                                viewHolder.imgWishList.setVisibility(View.VISIBLE);
                            }
                            if (Connectivity.isConnected(mContext)) {
                                addWishListItemsToServer();
                            } else {
                                Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
                                // GateWay gateWay = new GateWay(mContext);
                                // gateWay.displaySnackBar(getView());
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void addWishListItemsToServer() { //TODO Server method here
                    GateWay gateWay = new GateWay(mContext);
                    strContact = gateWay.getContact();
                    strEmail = gateWay.getUserEmail();

                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    selectedPosition = movie.getPosition();
                    ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);

                    pId = forWishList.getProduct_id();

                    JSONObject params = new JSONObject();
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shopId);
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", strContact);
                        params.put("email", strEmail);
                        Log.e("add_wish_param:",""+params);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddMyWishListItems, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            ErrorHandlingMethod(error); //TODO ServerError Method here
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });

            viewHolder.imgWishListSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                        selectedPosition = movie.getPosition();
                        ProductCodeWiseProduct forWishList = movie.productCodeWiseProducts.get(selectedPosition);
                        pId = forWishList.getProduct_id();
                        db.deleteWishListProductItem(pId);
                        int cnt = (int) db.fetchWishListCount();
                        updateWishListCount(cnt);
                        //uniqueWishList.remove(pId);
                        viewHolder.imgWishList.setVisibility(View.VISIBLE);
                        viewHolder.imgWishListSelected.setVisibility(View.GONE);
                        deleteoneProductFromWishlist(pId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void deleteoneProductFromWishlist(String pid) { //TODO Server method here
                    if (Connectivity.isConnected(mContext)) {
                        JSONObject params = new JSONObject();
                        final GateWay gateWay = new GateWay(mContext);
                        try {
                            params.put("contactNo", strContact);
                            params.put("email", gateWay.getUserEmail());
                            params.put("product_id", pid);
                            params.put("tag", "delete_one_item");
                            Log.e("del_wish_list_param:",""+params);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteMyWishListItems, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {

                                } else {
                                    // db.deleteWishListProductItem(pId);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                gateWay.progressDialogStop();

                                error.printStackTrace();

                                ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                       /* GateWay gateWay = new GateWay(mContext);
                        gateWay.displaySnackBar(getView());*/
                        Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            viewHolder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                            productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            final ProductCodeWiseProduct forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);
                            shopId = forAddToCart.getShop_id();

                            HashMap AddToCartInfo;
                            pId = forAddToCart.getProduct_id();

                           /* AddToCartInfo = db.getCartDetails(pId);
                            strId = (String) AddToCartInfo.get("new_pid");*/

                            String cart_status = forAddToCart.getCart_pstatus();
                            if(cart_status.equalsIgnoreCase("1")){
                                forAddToCart.setStatus(true);
                                Intent intent = new Intent(mContext, AddToCart.class);
                                startActivity(intent);
                            }

                            if (cart_status.equalsIgnoreCase("0")) { //TODO Server method here
                                forAddToCart.setStatus(false);

                                pName = viewHolder.tvProductName.getText().toString();
                                product_size = forAddToCart.getProduct_size();

                                JSONObject params = new JSONObject();
                                try {
                                    params.put("product_id", pId);
                                    params.put("shop_id", shopId);
                                    params.put("size", product_size);
                                    params.put("qty", 1);
                                    params.put("contactNo", strContact);
                                    params.put("email", strEmail);
                                    Log.e("cheq_qty_param:",""+params);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (response.isNull("posts")) {

                                        } else {
                                            try {
                                                strCheckQty = response.getString("posts");
                                                if (strCheckQty.equals("true")) {
                                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                                                    alertDialogBuilder.setMessage("You cannot add more than 1000 gm of this product.");
                                                    alertDialogBuilder.setPositiveButton("OK",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                    dialogInterface.dismiss();
                                                                }
                                                            });
                                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                                    alertDialog.show();
                                                } else {
                                                   /* db.insertCount(pId);
                                                    int count = (int) db.fetchAddToCartCount();
                                                    updateAddToCartCount(count);
                                                    if (count >= 0) {
                                                        forAddToCart.setStatus(true);
                                                        if (forAddToCart.isStatus(true)) {
                                                            viewHolder.btnAddToCart.setText("Go To Cart");
                                                        }
                                                    } else {
                                                        forAddToCart.setStatus(false);
                                                        if (forAddToCart.isStatus(false)) {
                                                            viewHolder.btnAddToCart.setText("Add To Cart");
                                                        }
                                                    }*/
                                                    //db.insertCount(pId);
                                                    int count = SyncData();//(int) db.fetchAddToCartCount();
                                                    if (count >= 0) {
                                                        forAddToCart.setStatus(true);
                                                        // if (forAddToCart.isStatus(true)) {
                                                        // viewHolder.btnAddToCart.setText("Go To Cart");
                                                        //  }
                                                    } else {
                                                        forAddToCart.setStatus(false);
                                                        //if (forAddToCart.isStatus(false)) {
                                                        // viewHolder.btnAddToCart.setText("Add To Cart");
                                                    }
                                                    addCartItemsToServer();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();
                                        ErrorHandlingMethod(error); //TODO ServerError method here
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(request);
                            } else {
                                viewHolder.btnAddToCart.setText("Go to Cart");
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                                deleteCartItemDialog(pId);
                            }
                        } else {
                            // GateWay gateWay = new GateWay(mContext);
                            //gateWay.displaySnackBar(v);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void deleteCartItemDialog(final String pId) {
                    productCodeWithProductList movie = allProductItemsList.get(viewHolder.getAdapterPosition());
                    selectedPosition = movie.getPosition();
                    ProductCodeWiseProduct forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);

                    LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                    View review = layoutInflater.inflate(R.layout.same_shop_cart, null);
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                    alertDialogBuilder.setView(review);

                    tvMessage = review.findViewById(R.id.txtMessage);
                    tvYes = review.findViewById(R.id.btnYes);
                    tvNo = review.findViewById(R.id.btnNo);
                    tvMessage.setText("Product is " + forAddToCart.getProduct_name() + ". Do you want remove or Go to Cart.");
                    // set dialog message
                    alertDialogBuilder.setCancelable(true);
                    // create alert dialog
                    alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Intent intent = new Intent(mContext, AddToCart.class);
                            startActivity(intent);
                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            deleteNormalProductFromCartItem(pId);
                        }
                    });
                }

                private void deleteNormalProductFromCartItem(String p_id) { //TODO Server method here
                    final GateWay gateWay = new GateWay(mContext);
                    gateWay.progressDialogStart();

                    final DBHelper db = new DBHelper(mContext);

                    if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", p_id);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("tag", "delete_one_item");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {

                                } else {
                                    db.deleteProductItem(pId);
                                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                }
                                int count = SyncData();//(int) db.fetchAddToCartCount();
                                updateAddToCartCount(count);
                                gateWay.progressDialogStop();
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                gateWay.progressDialogStop();

                                error.printStackTrace();

                                ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                        Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
                    }
                }

                private void addCartItemsToServer() { //TODO Server method here
                    JSONObject params = new JSONObject();
                    if (pName.contains("Small")) {
                        selectCondition = "Small";
                    } else if (pName.contains("Large")) {
                        selectCondition = "Large";
                    } else if (pName.contains("Medium")) {
                        selectCondition = "Medium";
                    } else if (pName.contains("Ripe")) {
                        selectCondition = "Ripe";
                    } else if (pName.contains("Raw")) {
                        selectCondition = "Raw";
                    } else {
                        selectCondition = "";
                    }
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shopId);
                        params.put("areaname", Zone_Area);
                        params.put("v_city", v_city);
                        params.put("v_state", v_state);
                        params.put("selectedType", selectCondition);
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", strContact);
                        params.put("email", strEmail);
                        if(sessionMainCat.equalsIgnoreCase("Both")){
                            params.put("sessionMainCat", sessionMainCat);
                        }else {
                            params.put("sessionMainCat", productSuperCat);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("productSuperCat",""+params);
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            Log.d("addCartItemsToServer1",""+response);
                            try {
                                String posts = response.getString("posts");
                                String promotional = response.getString("promotional");
                                String promotional_message = response.getString("promo_message");
                                if(promotional.equalsIgnoreCase("promotional")){

                                    openSessionDialog(promotional_message,"promo");

                                }else if (posts.equals("true")){
                                    // db.insertCount(pId);

                                    int count = SyncData();//(int) db.fetchAddToCartCount();
                                    updateAddToCartCount(count);
                                    if (count >= 0) {

                                        viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                        viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));

                                    } else {

                                        viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                        viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));

                                    }

                                    Log.d("addCartItemsToServer1",""+posts);
                                    Toast toast = Toast.makeText(mContext, "Adding product to cart.", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    openSessionDialog(posts,"session");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            ErrorHandlingMethod(error); //TODO ServerError Method here
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });
        }

        @Override
        public int getItemCount() {
            Log.d("getItemCount",""+allProductItemsList.size());
            return allProductItemsList.size();
        }

        class MainViewHolder extends RecyclerView.ViewHolder {

            final ImageView img;
            final TextView btnAddToCart;
            final ImageView imgWishList;
            final ImageView imgWishListSelected;
            final ImageView imgOutOfStock;
            final TextView tvProductName;
            final TextView tvSellerName;
            final TextView tvMrp;
            final TextView tvDiscount;
            final TextView tvPrice;
            final TextView tvRipeType;
            final TextView tvRawType;
            final TextView tvSmallSize;
            final TextView tvMediumSize;
            final TextView tvLargeSize;
            final TextView tvProductHindiName;
            final RelativeLayout linearLayoutProduct;
            final CardView cardView;
            final LinearLayout linearLayoutSelectCondition;
            final LinearLayout linearLayoutSelectType;
            final LinearLayout linearLayoutSelectSize;
            final Spinner spinner;
            final RelativeLayout LayoutSpinner;
            final FrameLayout frameimgMsg;

            public MainViewHolder(View itemView) {
                super(itemView);

                cardView = itemView.findViewById(R.id.view);
                img = itemView.findViewById(R.id.imgProduct);
                tvProductName = itemView.findViewById(R.id.txtProductName);
                tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
                tvSellerName = itemView.findViewById(R.id.txtSellerName);
                tvMrp = itemView.findViewById(R.id.txtTotal);
                tvDiscount = itemView.findViewById(R.id.txtDiscount);
                frameimgMsg = itemView.findViewById(R.id.frameimgMsg);
                tvPrice = itemView.findViewById(R.id.txtPrice);
                tvRipeType = itemView.findViewById(R.id.txtRipeType);
                tvRawType = itemView.findViewById(R.id.txtRawType);
                tvSmallSize = itemView.findViewById(R.id.txtSmallSize);
                tvMediumSize = itemView.findViewById(R.id.txtMediumSize);
                tvLargeSize = itemView.findViewById(R.id.txtLargeSize);
                btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
                imgWishList = itemView.findViewById(R.id.wishList);
                imgWishListSelected = itemView.findViewById(R.id.wishList1);
                linearLayoutProduct = itemView.findViewById(R.id.linearLayoutProduct);
                linearLayoutSelectCondition = itemView.findViewById(R.id.selectCondition);
                linearLayoutSelectType = itemView.findViewById(R.id.linearLayoutSelectType);
                linearLayoutSelectSize = itemView.findViewById(R.id.linearLayoutSelectSize);
                imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
                spinner = itemView.findViewById(R.id.spinner);
                LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
                btnAddToCart.setTag(this);
                imgWishList.setTag(this);
                imgWishListSelected.setTag(this);
                linearLayoutProduct.setTag(this);
            }
        }
    }



    public class PriceCustomListViewAdapter extends ArrayAdapter<Movie> {
        final Context context;
        private final List<Movie> items;
        private final int stepCount;


        public PriceCustomListViewAdapter(Context context, int resourceId, List<Movie> items, int stepCount) {
            super(context, resourceId, items);
            this.context = context;
            this.items = items;
            this.stepCount = stepCount;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            Movie movie = getItem(position);
            if (stepCount == 0) {
                convertView = null;
                LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(R.layout.price_range_list_filter, null);
                CheckBox cbprice = convertView.findViewById(R.id.checkBox1);
                TextView priceRange = convertView.findViewById(R.id.checkBoxName);

                if (priceRangeSelectedList.contains(position))
                    cbprice.setChecked(true);
                else
                    cbprice.setChecked(false);
                priceRange.setText(movie.getTitle());
            }
            if (stepCount == 1) {
                convertView = null;
                LayoutInflater dInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = dInflater.inflate(R.layout.discount_range_list_filter, null);
                TextView discountRange = convertView.findViewById(R.id.radioButtonName);
                RadioButton rb = convertView.findViewById(R.id.radioButtonDiscount);
                discountRange.setText(movie.getTitle());
                if (positionForRadioButton == position && (discountRangeSelectedList.contains(position)))
                    rb.setChecked(true);
                else
                    rb.setChecked(false);
            }
            return convertView;
        }
    }

    public class SortCustomListViewAdapter extends ArrayAdapter<Movie> {
        final Context context;
        private final List<Movie> items;
        RadioButton rb = null;

        public SortCustomListViewAdapter(Context context, List<Movie> items) {
            super(context, R.layout.discount_range_list_filter, items);
            this.context = context;
            this.items = items;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            Movie movie = getItem(position);
            convertView = null;
            LayoutInflater dInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = dInflater.inflate(R.layout.discount_range_list_filter, null);
            TextView discountRange = convertView.findViewById(R.id.radioButtonName);
            discountRange.setText(movie != null ? movie.getTitle() : null);
            rb = convertView.findViewById(R.id.radioButtonDiscount);
            if (sortRangeSelectedList.contains(position)) {
                rb.setChecked(true);
            } else {
                rb.setChecked(false);
            }
            return convertView;
        }
    }

    //show dialog when If he tries to select something in other category
    private void openSessionDialog(String message, String type) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sessoin);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        TextView tv_clearcart = dialog.findViewById(R.id.tv_clearcart);
        TextView tv_continue = dialog.findViewById(R.id.tv_continue);
        tv_message.setText(message);
        if(type.equalsIgnoreCase("promo")){
            tv_clearcart.setVisibility(View.GONE);
        }
        tv_clearcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(mContext)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            // GateWay gateWay = new GateWay(mContext);
                            //gateWay.displaySnackBar(homeMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteAllProductFromCartItem() {
        if (Connectivity.isConnected(mContext)) {
            final GateWay gateWay = new GateWay(mContext);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(mContext);
                        db.deleteOnlyCartTable();

                        //after remove cart items from local and online db
                        //then sync online cart count again
                        SyncData();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        final DBHelper db = new DBHelper(ProductListActity.this);
        getMenuInflater().inflate(R.menu.menu_local_search_product, menu);
        super.onCreateOptionsMenu(menu);

        MenuItem item = menu.findItem(R.id.action_login);
        item.setVisible(false);

        MenuItem item2 = menu.findItem(R.id.search);
        item2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(mContext, MasterSearch.class));
                return false;
            }
        });

        MenuItem item1 = menu.findItem(R.id.action_wish_list);
        MenuItemCompat.setActionView(item1, R.layout.wish_list_notification_icon);
        RelativeLayout notifyCount1 = (RelativeLayout) MenuItemCompat.getActionView(item1);

        tvWishList = notifyCount1.findViewById(R.id.cartNumber);
        notifyCount1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int cnt = (int) db.fetchWishListCount();
                if (cnt == 0) {
                    GateWay gateWay = new GateWay(mContext);
                    gateWay.wishListAlertDialog();
                } else {
                    Intent intent = new Intent(mContext, WishList.class);
                    startActivity(intent);
                }
            }
        });

        int wishListCount = (int) db.fetchWishListCount();
        if (wishListCount == 0) {
            tvWishList.setVisibility(View.INVISIBLE);
        } else {
            tvWishList.setVisibility(View.VISIBLE);
            tvWishList.setText("" + wishListCount);
        }

        notifyCount1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast toast = Toast.makeText(mContext, "WishList", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.RIGHT, 0, 110);
                toast.show();
                return true;
            }
        });
        return true;
    }
    public void cartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Your Cart seems to be empty, SHOP NOW.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void ErrorHandlingMethod(VolleyError error) {
        if (error instanceof ServerError) {
            String title = "Server Error";
            String message = "Sorry for the inconvenience, the web server is not responding. Please try again after some time.";
            String buttonText = "Ok";

            showAlertDialog(title, message, buttonText);
        } else if (error instanceof NoConnectionError) {
            String title = "No Connection Error";
            String message = "Communication Error! Please try again after some time.";
            String buttonText = "Ok";

            showAlertDialog(title, message, buttonText);
        } else if (error instanceof TimeoutError) {
            String title = "Timeout Error";
            String message = "Connection TimeOut! Please check your internet connection.";
            String buttonText = "Ok";

            showAlertDialog(title, message, buttonText);
        } else if (error instanceof ParseError) {
            String title = "Parse Error";
            String message = "Parsing error! Please try again after some time.";
            String buttonText = "Ok";
            showAlertDialog(title, message, buttonText);
        }
    }
    private void showAlertDialog(String title, String message, String buttonText) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setIcon(R.drawable.app_icon_new); //Setting Dialog icon Image
        alertDialog.setTitle(title);  // Setting Dialog Title
        alertDialog.setMessage(message); // Setting Dialog Message
        alertDialog.setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { // Setting OK Button
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.show(); // Showing Alert Message
        TextView messageText = dialog.findViewById(android.R.id.message);
        assert messageText != null;
        messageText.setGravity(Gravity.CENTER);
        dialog.show();
    }
}

