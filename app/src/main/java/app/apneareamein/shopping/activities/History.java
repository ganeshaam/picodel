package app.apneareamein.shopping.activities;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.GateWay;

public class History extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private CardAdapter cardAdapter;
    TextView txtMessage;
    String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtMessage = findViewById(R.id.txtMessage);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            tag = bundle.getString("type");

            if (tag.equals("Above 300 CB History")) {
                tvTitle.setText(R.string.title_activity_above_300_cb_history);
            } else {
                tvTitle.setText(R.string.title_activity_history);
            }
        }
        getAutoHistory(tag);
    }

    private void getAutoHistory(String type) {
        cardAdapter = new CardAdapter();

        final GateWay gateWay = new GateWay(History.this);
        gateWay.progressDialogStart();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAutoHistory, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                gateWay.progressDialogStop();
                try {
                    if (!response.isNull("posts")) {
                        txtMessage.setVisibility(View.GONE);
                        JSONArray jsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            POJOClass pojoClass = new POJOClass(jsonObject1.getString("user_name"),
                                    jsonObject1.getString("purchase_or_fare"),
                                    jsonObject1.getString("created_date"),
                                    jsonObject1.getString("message"));

                            cardAdapter.add(pojoClass);
                        }
                        mRecyclerView.setAdapter(cardAdapter);
                    } else {
                        txtMessage.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                gateWay.progressDialogStop();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.IMMEDIATE);
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class CardAdapter extends RecyclerView.Adapter<HistoryViewHolder> {

        List<POJOClass> rowItems = new ArrayList<>();

        @NonNull
        @Override
        public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_auto_history, parent, false);
            return new HistoryViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {

            holder.Name.setText(rowItems.get(position).getUser_name());
            holder.fare_or_purchase.setText("Fare - " + rowItems.get(position).getPurchase_or_fare());

            String strdate = rowItems.get(position).getCreated_date();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date;
            try {
                date = dateFormat.parse(strdate);

                String strnewdate = date.toString();

                String[] words = strnewdate.split("\\s");//splits the string based on whitespace

                String dte = words[1];
                String strMonth = words[0];
                String strYear = words[5];

                holder.date.setText(dte);
                holder.month.setText(strMonth);
                holder.year.setText(strYear);
                if (tag.equals("Above 300 CB History")) {
                    holder.message.setVisibility(View.VISIBLE);
                    holder.message.setText(rowItems.get(position).getMessage());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return rowItems.size();
        }

        public void add(POJOClass pojoClass) {
            rowItems.add(pojoClass);
        }
    }

    private class HistoryViewHolder extends RecyclerView.ViewHolder {

        TextView Name, month, date, year, fare_or_purchase, message;
        RelativeLayout li;

        HistoryViewHolder(View itemView) {
            super(itemView);

            Name = itemView.findViewById(R.id.txt_name);
            fare_or_purchase = itemView.findViewById(R.id.txt_purchase_fare);
            month = itemView.findViewById(R.id.txt_month);
            year = itemView.findViewById(R.id.txt_year);
            date = itemView.findViewById(R.id.txt_date);
            message = itemView.findViewById(R.id.txt_msg);
        }
    }

    private class POJOClass {

        String user_name;
        String purchase_or_fare;
        String created_date;
        String message;

        POJOClass(String user_name, String purchase_or_fare, String created_date, String message) {
            this.user_name = user_name;
            this.purchase_or_fare = purchase_or_fare;
            this.created_date = created_date;
            this.message = message;
        }

        public String getUser_name() {
            return user_name;
        }

        public String getPurchase_or_fare() {
            return purchase_or_fare;
        }

        public String getCreated_date() {
            return created_date;
        }

        public String getMessage() {
            return message;
        }
    }
}
