package app.apneareamein.shopping.activities;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.MySingleton;
import app.apneareamein.shopping.utils.VolleySingleton;
import dmax.dialog.SpotsDialog;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class EditAddress extends AppCompatActivity implements View.OnClickListener , GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener{

    //boolean defaultValue = false;
    private Bundle bundle;
    private EditText etName, etPincode, etAddress, editRoadName, editAreaName, etContact, etCity, etFlatNo, editBuildingName;//editColonyname,
    private String str_id, str_type, finalPrice, tag;
    private CoordinatorLayout addressMainLayout;
    private final ArrayList<UserInfo> arr_user_info = new ArrayList<>();
    private LinearLayout Main_Layout_NoInternet;
    private final String class_name = this.getClass().getSimpleName();
    TextInputLayout tvName, tvPincode, tvAddress,tvRoadName,tvColonyname, tvPhone, tvCity, tvFlatNo, txtInputBuilding, tvState,tvAreaname;
    AutoCompleteTextView etState;
    LinearLayout location_button;
    public static final String TAG = "EditAddress";
    android.app.AlertDialog progressDialog2;
    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;
    //private Location mCurrentLocation;

    private Network_Change_Receiver myReceiver;
    ScrollView scrollView;
    private TextView txtNoConnection;

    private PermissionRequestErrorListener errorListener;
    private PermissionListener gpsPermissionListener;
    Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private LocationManager locationManager;
    String latitude, longitude,address_key,version;
    public static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    private ProgressBar simpleProgressBar;
    private Spinner sp_city;
    private ArrayList<String> cityList;
    public static final String MY_PREFS_NAME = "PICoDEL";
    private String v_city ="",v_state="",Zone_Area;

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                scrollView.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);

        myReceiver = new Network_Change_Receiver();

        //here we initialized all weight
        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog2 = new SpotsDialog.Builder().setContext(EditAddress.this).build();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");
        v_city = prefs.getString("v_city", "");
        v_state = prefs.getString("v_state", "");

        cityList = new ArrayList<>();

        getKeys2();

        txtNoConnection = findViewById(R.id.txtNoConnection);
        scrollView = findViewById(R.id.scrollView);
        addressMainLayout = findViewById(R.id.addressMainLayout);
        etName = findViewById(R.id.editUserName);
        etPincode = findViewById(R.id.editUserPinCode);
        etAddress = findViewById(R.id.editAddress);
        editRoadName = findViewById(R.id.editRoadName);
        //editColonyname = findViewById(R.id.editColonyname);
        etFlatNo = findViewById(R.id.editFlat);
        editBuildingName = findViewById(R.id.editBuildingName);
        etCity = findViewById(R.id.editCity);
        editAreaName = findViewById(R.id.editAreaName);
        etState = findViewById(R.id.editState);
        etContact = findViewById(R.id.editContact);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

        tvName = findViewById(R.id.txtInputname);
        tvPincode = findViewById(R.id.txtInputpincode);
        tvAddress = findViewById(R.id.txtInputaddress);
        tvRoadName = findViewById(R.id.txtInputroadname);
        tvColonyname = findViewById(R.id.txtInputcolonyname);
        tvPhone = findViewById(R.id.txtInputcontact);
        tvCity = findViewById(R.id.txtInputcity);
        tvAreaname= findViewById(R.id.txtInputareaname);
        tvFlatNo = findViewById(R.id.txtInputFlat);
        txtInputBuilding = findViewById(R.id.txtInputBuilding);
        tvState = findViewById(R.id.txtInputState);
        sp_city = findViewById(R.id.sp_city);

        mContext = EditAddress.this;

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);

        checkLocation(); //check whether location service is enable or not in your  phone

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(EditAddress.this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }

        String[] mTestArray = getResources().getStringArray(R.array.india_states);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, mTestArray);
        etState.setAdapter(adapter);

        //Get city List
        getCityList();

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etName.getText().toString().trim().length() > 2) {
                    tvName.setErrorEnabled(false);
                } else {
                    tvName.setError("Enter your full name");
                    requestFocus(etName);
                }
            }
        });

        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                /*if (etCity.getText().toString().trim().length() > 2) {
                    tvCity.setErrorEnabled(false);
                } else {
                    tvCity.setError("Enter city name");
                    requestFocus(etCity);
                }*/
            }
        });
        //area name
        editAreaName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editAreaName.getText().toString().trim().length() > 2) {
                    tvAreaname.setErrorEnabled(false);
                } else {
                    editAreaName.setError("Enter Area name");
                    requestFocus(editAreaName);
                }
            }
        });

        etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
              /*  if (etAddress.getText().toString().trim().length() > 1) {
                    tvAddress.setErrorEnabled(false);
                } else {
                    tvAddress.setError("Enter locality, area or street");
                    requestFocus(etAddress);
                }*/

            }
        });

        editRoadName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
              /*  if (editRoadName.getText().toString().trim().length() > 1) {
                    tvRoadName.setErrorEnabled(false);
                } else {
                    editRoadName.setError("Enter Road Name");
                    requestFocus(editRoadName);
                }
*/
            }
        });
       /* editColonyname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editColonyname.getText().toString().trim().length() > 1) {
                    tvColonyname.setErrorEnabled(false);
                } else {
                    editColonyname.setError("Enter Road Name");
                    requestFocus(editColonyname);
                }

            }
        });*/

        etFlatNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etFlatNo.getText().toString().trim().length() > 1) {
                    tvFlatNo.setErrorEnabled(false);
                } else {
                    tvFlatNo.setError("Flat No. / House No.");
                    requestFocus(etFlatNo);
                }

            }
        });

        editBuildingName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editBuildingName.getText().toString().trim().length() > 1) {
                    txtInputBuilding.setErrorEnabled(false);
                } else {
                    txtInputBuilding.setError("Enter House Name / Building name / Society Name*");
                    requestFocus(editBuildingName);
                }

            }
        });

        etPincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
              /*  if (etPincode.getText().toString().length() < 6) {
                    tvPincode.setError("Invalid Pincode");
                    requestFocus(etPincode);
                } else if (etPincode.getText().toString().length() == 6) {
                    checkPincode(etPincode.getText().toString(), "");
                } else if (etPincode.getText().toString().length() == 0) {
                    tvPincode.setErrorEnabled(false);
                } else {
                    tvPincode.setErrorEnabled(false);
                }*/
            }
        });

        etState.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etState.getText().toString().trim().length() > 2) {
                    tvState.setErrorEnabled(false);
                } else {
                    tvState.setError("Enter state");
                    requestFocus(etState);
                }

            }
        });

        etContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etContact.getText().toString().length() < 10) {
                    tvPhone.setError("Invalid phone number");
                    requestFocus(etContact);
                } else {
                    tvPhone.setErrorEnabled(false);
                }
            }
        });

        /*SwitchCompat sBDefault = findViewById(R.id.sBDefault);
        sBDefault.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                defaultValue = isChecked;
            }
        });*/

        Button btnsave = findViewById(R.id.btnSave);
        Button btncancel = findViewById(R.id.btnCancel);

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        location_button = findViewById(R.id.location_button);

        bundle = getIntent().getExtras();
        str_type = bundle.getString("type");
        tag = bundle.getString("tag");
        finalPrice = bundle.getString("finalPrice");
        /*defaultValue = bundle.getBoolean("defaultValue");

        if (defaultValue) {
            sBDefault.setChecked(true);
        } else {
            sBDefault.setChecked(false);
        }*/
        if (str_type.equals("edit")) {
            tvTitle.setText("Edit Address");
            str_id = bundle.getString("id");
            fetchuserAddress(str_id);
        } else {
            tvTitle.setText("Add New Address");
        }

        location_button.setOnClickListener(this);
        btnsave.setOnClickListener(this);
        btncancel.setOnClickListener(this);


        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setCountry("IN");

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() +", "+place.getLatLng());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        final List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        etAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                /*Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(EditAddress.this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);*/
            }
        });

        etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

               if(s.length()==1){

                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(EditAddress.this);
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
               }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                // Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() +", "+place.getLatLng());
                if (place != null) {
                    LatLng latLng = place.getLatLng();
                    latitude  = String.valueOf(latLng.latitude);
                    longitude =  String.valueOf(latLng.longitude);
                    // Log.d("latlong",latitude+" "+mStringLongitude);
                    etAddress.setText(place.getName());
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnSave:
                Validation();
                break;

            case R.id.location_button:
                hideKeyboard(EditAddress.this);
                LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
               /* if (!gps_enabled) {
                    checkRequiredPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
                } else if (!checkPermissions()) {
                    checkRequiredPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
                }*/
                break;

            case R.id.btnCancel:
                if (tag != null && tag.equals("simple")) {
                    Intent intent = new Intent(EditAddress.this, BaseActivity.class);
                    bundle = new Bundle();
                    bundle.putString("tag", "my_address");
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else if (tag != null && tag.equals("resto")) {
                    Intent intent = new Intent(EditAddress.this, BaseActivity.class);
                    bundle = new Bundle();
                    bundle.putString("tag", "resto");
                    bundle.putString("finalPrice", finalPrice);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else if (tag != null && tag.equals("address")) {
                    Intent intent = new Intent(EditAddress.this, BaseActivity.class);
                    bundle = new Bundle();
                    bundle.putString("tag", "address");
                    bundle.putString("finalPrice", finalPrice);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(EditAddress.this, BaseActivity.class);
                    bundle = new Bundle();
                    bundle.putString("tag", "easy_order_address");
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                break;
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void Validation() {
        Pattern p_phone = Pattern.compile(ApplicationUrlAndConstants.MobileregEx);
        Matcher m_phone = p_phone.matcher(etContact.getText().toString());

        if (etName.getText().toString().trim().isEmpty()) {
            //tvName.setError("Enter your full name");
            requestFocus(etName);
        } else if (etFlatNo.getText().toString().trim().isEmpty()) {
           // tvFlatNo.setError("Enter Flat No. / House No.");
            requestFocus(etFlatNo);
        } else if (editBuildingName.getText().toString().trim().isEmpty()) {
           // txtInputBuilding.setError("Enter House Name / Building name / Society Name");
            requestFocus(editBuildingName);
        }else if (etAddress.getText().toString().trim().isEmpty()) {
            tvAddress.setError("Select Nearest Location of Delivery Address");
            requestFocus(etAddress);
        }/*else if (editRoadName.getText().toString().trim().isEmpty()) {
            editRoadName.setError("Enter Road Name");
            requestFocus(etAddress);
        }else if (editColonyname.getText().toString().trim().isEmpty()) {
            editColonyname.setError("Enter Colony Name");
            requestFocus(etAddress);
        }*/else if (sp_city.getSelectedItem().toString().trim().isEmpty()) {
            // tvCity.setError("Enter city");
            requestFocus(etCity);
        }
           /*else if (etCity.getText().toString().trim().isEmpty()) {
           // tvCity.setError("Enter city");
            requestFocus(etCity);
        }*/ else if (etState.getText().toString().trim().isEmpty()) {
          //  tvState.setError("Enter state");
            requestFocus(etState);
        }
        /*if (etPincode.getText().toString().trim().isEmpty()) {
            tvPincode.setError("Enter pincode");
            requestFocus(etPincode);
        } else if (etPincode.getText().toString().length() < 6) {
            tvPincode.setError("Invalid pincode");
            requestFocus(etPincode);
        } else*///else if (etContact.getText().toString().isEmpty()) {
           // tvPhone.setError("Enter phone number");
            //requestFocus(etContact);
        /*else if (etContact.getText().toString().length() < 10) {
            tvPhone.setError("Invalid phone number");
            requestFocus(etContact);
        }*/ /*else if (!m_phone.find()) {
            tvPhone.setError("Invalid phone number");
            requestFocus(etContact);
        } */else {
            if (str_type.equals("edit")) {
                 addnewAddress("update", str_id);
                //checkPincode(etPincode.getText().toString(), "update");
            } else {
                 addnewAddress("add", "");
                //checkPincode(etPincode.getText().toString(), "add");
            }
        }
    }

    private void addnewAddress(final String type, String id) {//TODO Server method here
        Log.d("EditAddress", "addnewAddress: "+type+"String id"+id);
        if (Connectivity.isConnected(EditAddress.this)) {
            final GateWay gateWay = new GateWay(EditAddress.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("name", etName.getText().toString());
               // params.put("pincode", etPincode.getText().toString());
                params.put("pincode", "411028");
                params.put("address", etFlatNo.getText().toString()+" "+editBuildingName.getText().toString()+" "+etAddress.getText().toString()+" "+editRoadName.getText().toString()+" "+editAreaName.getText().toString());
                params.put("address3", etAddress.getText().toString());//address3
                params.put("address4", editRoadName.getText().toString()); //address 4
                params.put("address5", editRoadName.getText().toString());//address 5
                params.put("area", editAreaName.getText().toString());//address colunm name and landmark replace with area
                params.put("city", etCity.getText().toString());
                params.put("address1", etFlatNo.getText().toString());//flat_no replace with address1
                params.put("address2", editBuildingName.getText().toString());//building_name replace with address2
                params.put("state", etState.getText().toString());
                //params.put("mobile", etContact.getText().toString());
                params.put("mobile", gateWay.getContact());
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("id", id);
                params.put("tag", type);
                //params.put("address_type", defaultValue);
                params.put("latitude", latitude);
                params.put("longitude", longitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("addnewAddress",""+params);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddNewAddress, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        Log.e("AddAdressRES:",""+res);
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("message");

                        if (status.equals("fail")) {
                            Toast.makeText(EditAddress.this, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            switch (tag) {
                                case "simple": {
                                    Intent intent = new Intent(EditAddress.this, DeliveryAddress.class);
                                   // Intent intent = new Intent();
                                    bundle = new Bundle();
                                    bundle.putString("tag", "my_address");
                                    intent.putExtras(bundle);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    //setResult(RESULT_OK, intent);
                                    //finish();
                                    break;
                                }
                                case "resto": {
                                    Intent intent = new Intent(EditAddress.this, DeliveryAddress.class);
                                    bundle = new Bundle();
                                    bundle.putString("tag", "resto");
                                    bundle.putString("finalPrice", finalPrice);
                                    intent.putExtras(bundle);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                   // finish();
                                    break;
                                }
                                case "address": {
                                    Intent intent = new Intent(EditAddress.this, DeliveryAddress.class);
                                    bundle = new Bundle();
                                    bundle.putString("tag", "address");
                                    bundle.putString("finalPrice", finalPrice);
                                    intent.putExtras(bundle);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    //finish();
                                    break;
                                }
                                default: {
                                    Intent intent = new Intent(EditAddress.this, DeliveryAddress.class);
                                    bundle = new Bundle();
                                    bundle.putString("tag", "easy_order_address");
                                    intent.putExtras(bundle);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                   // finish();
                                    break;
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(EditAddress.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(EditAddress.this);
            gateWay.displaySnackBar(addressMainLayout);
        }
    }

    private void fetchuserAddress(String id) { //TODO Server method here
        if (Connectivity.isConnected(EditAddress.this)) {
            final GateWay gateWay = new GateWay(EditAddress.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("id", id);
                params.put("tag", "");
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetuserdata, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        Log.d("userAddress",res);
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("message");

                        if (status.equals("success")) {
                            JSONArray addressArray = jsonObject.getJSONArray("user_data");

                            for (int i = 0; i < addressArray.length(); i++) {
                                JSONObject json_event_list = addressArray.getJSONObject(i);
                                UserInfo ui = new UserInfo(json_event_list.getString("name"),
                                        json_event_list.getString("address"),
                                        json_event_list.getString("contact"),
                                        json_event_list.getString("pincode"),
                                        //json_event_list.getString("landmark"),
                                        json_event_list.getString("city"),
                                        json_event_list.getString("state"),
                                        //json_event_list.getString("flat_no"),
                                       // json_event_list.getString("building_name"),
                                        json_event_list.getString("address1"),
                                        json_event_list.getString("address2"),
                                        json_event_list.getString("address3"),
                                        json_event_list.getString("address4"),
                                        json_event_list.getString("address5"),
                                        json_event_list.getString("area"),
                                        json_event_list.getString("latitude"),
                                        json_event_list.getString("longitude"));
                                arr_user_info.add(ui);
                            }
                            etName.setText(arr_user_info.get(0).getStrName());
                            etPincode.setText(arr_user_info.get(0).getStrPincode());
                            etAddress.setText(arr_user_info.get(0).getAddress3());
                            editAreaName.setText(arr_user_info.get(0).getArea());
                            etContact.setText(arr_user_info.get(0).getStrContact());
                            etCity.setText(arr_user_info.get(0).getStrCity());
                            etFlatNo.setText(arr_user_info.get(0).getAddress1());
                            editBuildingName.setText(arr_user_info.get(0).getAddress2());
                            etState.setText(arr_user_info.get(0).getStrState());
                            editRoadName.setText(arr_user_info.get(0).getAddress4());
                            latitude = arr_user_info.get(0).getStrLatitude();
                            longitude= arr_user_info.get(0).getStrLongitude();
                           // editRoadName.setText(arr_user_info.get(0).getAddress5());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(EditAddress.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void checkPincode(String strPincode, final String edit_type) { //TODO Server method here
        if (Connectivity.isConnected(EditAddress.this)) { // Internet connection is not present, Ask user to connect to Internet
            JSONObject params = new JSONObject();
            try {
                params.put("pinCode", strPincode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckPincode, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");

                        if (status.equals("fail")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(EditAddress.this);
                            builder.setTitle("Apne Are Mein Online Shopping");
                            builder.setMessage("We are very sorry! Currently we do not serve in this area.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else if (status.equals("success")) {
                            tvPincode.setErrorEnabled(false);
                            if (edit_type.equals("update")) {
                                addnewAddress(edit_type, str_id);
                            } else if (edit_type.length() > 0) {
                                addnewAddress(edit_type, "");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(EditAddress.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(this);
            gateWay.displaySnackBar(addressMainLayout);
        }
    }

    private class UserInfo {

        final String strName;
        final String strAddress;
        final String strContact;
        final String strPincode;
        //final String strLandmark;
        final String strCity;
        final String strState;
        //final String flat_no;
        //final String building_name;
        final String address1;
        final String address2;
        final String address3;
        final String address4;
        final String address5;
        final String area;
        final String strLatitude;
        final String strLongitude;


        UserInfo(String name, String address, String contact, String strPincode, String strCity, String strState, String address1,String address2,String address3,String address4,String address5,String area,String strLatitude,String strLongitude) {//String landmark, String flat_no, String building_name,
            this.strName = name;
            this.strAddress = address;
            this.strContact = contact;
            this.strPincode = strPincode;
           // this.strLandmark = landmark;
            this.strCity = strCity;
            this.strState = strState;
            //this.flat_no = flat_no;
            //this.building_name = building_name;
            this.address1 = address1;
            this.address2 = address2;
            this.address3 = address3;
            this.address4 = address4;
            this.address5 = address5;
            this.area = area;
            this.strLatitude = strLatitude;
            this.strLongitude = strLongitude;
        }

        public String getStrName() {
            return strName;
        }

        String getStrAddress() {
            return strAddress;
        }

        public String getStrCity() {
            return strCity;
        }

        String getStrState() {
            return strState;
        }

       /* public String getFlat_no() {
            return flat_no;
        }

        public String getBuilding_name() {
            return building_name;
        }*/

        public String getStrContact() {
            return strContact;
        }

        public String getStrPincode() {
            return strPincode;
        }

       /* String getStrLandmark() {
            return strLandmark;
        }*/

        public String getAddress1() {
            return address1;
        }

        public String getAddress2() {
            return address2;
        }

        public String getAddress3() {
            return address3;
        }

        public String getAddress4() {
            return address4;
        }

        public String getAddress5() {
            return address5;
        }

        public String getArea() {
            return area;
        }

        public String getStrLatitude() {
            return strLatitude;
        }

        public String getStrLongitude() {
            return strLongitude;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        /*startLocation();*/

        if (Connectivity.isConnected(EditAddress.this)) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, EditAddress.this);*/
        }
    }


    //get current Location

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            Log.d("Location",mLocation.getLatitude()+" "+mLocation.getLongitude());
           // latitude = String.valueOf(mLocation.getLatitude());
            //longitude = String.valueOf(mLocation.getLongitude());
            //mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));

            //geocoder
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                addresses = geocoder.getFromLocation(mLocation.getLatitude(), mLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
               // addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

               // etPincode.setText(postalCode);
                etState.setText(state);
                etCity.setText(city);
                 /*if (knownName.equals(null)) {
                    etAddress.setText(addresses.get(0).getSubLocality());
                } else {
                    etAddress.setText(addresses.get(0).getFeatureName() + "," + addresses.get(0).getSubLocality());
                }*/
                // String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                //String state = addresses.get(0).getAdminArea();
                //String country = addresses.get(0).getCountryName();
                //String postalCode = addresses.get(0).getPostalCode();
                //String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //  Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }
/*
    @Override
    public void onLocationChanged(Location location) {

    }*/

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
     /*   mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);*/
        mLocationRequest = LocationRequest.create();//.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }



    @Override
    public void onLocationChanged(Location location) {

        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.d("UpdatedLocation",msg);
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        //mLongitudeTextView.setText(String.valueOf(location.getLongitude() ));
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        //LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }



    private void getKeys2() { //TODO Server method here


            JSONObject params = new JSONObject();
            try {
                params.put("app", "shopkeeper");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.GetAndroidKey, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("cartCountRes:",""+response.toString());
                        address_key = response.getString("address_key");
                        version = response.getString("version");

                        // Create a new Places client instance
                        Places.initialize(getApplicationContext(), address_key);
                        // Create a new Places client instance
                        PlacesClient placesClient = Places.createClient(EditAddress.this);

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().setPriority(Request.Priority.HIGH);
            AppController.getInstance().addToRequestQueue(request);

    }

    private void testApiRes () { //TODO Server method here


        JSONObject params = new JSONObject();
        try {
            params.put("app", "shopkeeper");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.GetAndroidKey, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.e("cartCountRes:",""+response.toString());
                    address_key = response.getString("address_key");
                    version = response.getString("version");

                    // Create a new Places client instance
                    Places.initialize(getApplicationContext(), address_key);
                    // Create a new Places client instance
                    PlacesClient placesClient = Places.createClient(EditAddress.this);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.HIGH);
        AppController.getInstance().addToRequestQueue(request);

    }
    private void getCityList() { //TODO Server method here
        if (Connectivity.isConnected(EditAddress.this)) {
            cityList.clear();

            final GateWay gateWay = new GateWay(EditAddress.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCityConfiguration, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    try {

                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            cityList.add(jsonCityData.getString("city"));
                        }
                        /*CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, areaList, null, null, "location");
                        sp_Area_spinner.setAdapter(customSpinnerAdapter);*/

                        ArrayAdapter<String> adapter = new ArrayAdapter(EditAddress.this,android.R.layout.simple_spinner_item,cityList);
                        sp_city.setAdapter(adapter);

                        if (v_city != null && v_city.length()>2) {
                            int spinnerPosition = adapter.getPosition(v_city);
                            //stateCityModelArrayList.contains(v_city);
                            sp_city.setSelection(spinnerPosition);

                            if(v_state!=null && v_state.length()>2) {
                                etState.setText(v_state);
                            }
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(EditAddress.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(EditAddress.this).addToRequestQueue(request);
        }
    }

}
