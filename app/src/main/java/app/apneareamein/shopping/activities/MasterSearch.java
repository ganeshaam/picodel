package app.apneareamein.shopping.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.CustomSpinnerAdapter;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.Helper;
import app.apneareamein.shopping.utils.SharedPreferencesUtils;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class MasterSearch extends AppCompatActivity {

    public static SearchView searchView;
    private LinkedHashMap productListHashMap = new LinkedHashMap();
    private ArrayList<UltimateProductList> productWiseList = new ArrayList<>();
    private LinearLayout progressViewLinearLayout;
    private ListView searchResultListView;
    private LinearLayout Main_Layout_NoInternet;
    private Button btnSearchAgain;
    private EditText et;
    private RecyclerView recyclerView;
    private final ArrayList<productCodeWithProductList> FinalList = new ArrayList<>();
    private UltimateCardAdapter ultimateCardAdapter;
    private String pId;
    private String shopId;
    private String strId;
    private String strCity, strArea, strContact, strEmail,Zone_Area,v_state,v_city,sessionMainCat="";
    private LinearLayout LayoutProducts;
    private TextView tvTitle;
    private LinearLayout LayNoResult;
    private TextView txtNoConnection;
    private Network_Change_Receiver myReceiver;
    ImageView closeButton;
    private ProgressBar simpleProgressBar;
    public static final String MY_PREFS_NAME = "PICoDEL";
    public SharedPreferences prefs;
    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                hideKeyBoard();
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                hideKeyBoard();
                Main_Layout_NoInternet.setVisibility(View.GONE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);

                String searchText = prefs.getString("search_value","Farmengg");
                if(searchText.equalsIgnoreCase("")||searchText == null){

                }else if(searchText.length() >= 2){
                    Log.e("textValue:",""+searchText);
                    requestFromServer(searchText);
                }


                /*if (et.getText().toString().length() >= 3) {
                    hideKeyBoard();

                    requestFromServer(et.getText().toString());

                } else {
                    requestFromServer("");
                }*/
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master_search);
        MasterSearch.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //MasterSearch.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        hideKeyBoard();
        myReceiver = new Network_Change_Receiver();

        txtNoConnection = findViewById(R.id.txtNoConnection);

        GateWay gateWay = new GateWay(this);
        strCity = gateWay.getCity();
        strArea = gateWay.getArea();
        strContact = gateWay.getContact();
        strEmail = gateWay.getUserEmail();

        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");
        v_city = prefs.getString("v_city", "");
        v_state = prefs.getString("v_state", "");
        sessionMainCat = prefs.getString("sessionMainCat", "");

        progressViewLinearLayout = findViewById(R.id.progressbar_view);
        LayoutProducts = findViewById(R.id.LayoutProducts);
        tvTitle = findViewById(R.id.txtT);
        LayNoResult = findViewById(R.id.LayNoResult);
        searchResultListView = findViewById(R.id.listView);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

        btnSearchAgain = findViewById(R.id.btnSearchAgain);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MasterSearch.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (Connectivity.isConnected(MasterSearch.this)) {
            LayNoResult.setVisibility(View.GONE);

            //requestFromServer(""); //onCreate call
            String searchText = prefs.getString("search_value","Farmengg");
            if(searchText.equalsIgnoreCase("")||searchText == null){

            }else if(searchText.length() >= 2){
                Log.e("textValue:",""+searchText);
                requestFromServer(searchText);
            }

        }

    }

    private void hideKeyBoard() {
        View view = getCurrentFocus();  // Check if no view has focus:
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyBoard();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            et.setText(query);
            et.setSelection(et.getText().length());
            doSearch(query);
        }
    }

    private void doSearch(String query) {
        String class_name = this.getClass().getSimpleName();

        /*UserTracking UT = new UserTracking(UserTracking.context);
        UT.user_tracking(class_name + " :searched for keyword: " + query, MasterSearch.this);*/

        Toast.makeText(this, "Searching for: " + query, Toast.LENGTH_LONG).show();
        hideKeyBoard();
        requestFromServer(query); //this fo voice search
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);

        if (Connectivity.isConnected(this)) {  // check for Internet status
            final MenuItem searchItem = menu.findItem(R.id.master_search_icon);
            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

            searchView.setQueryHint("Search for Products, Brands and More");
            searchView.setIconified(false);
            searchView.setMaxWidth(Integer.MAX_VALUE);
            closeButton = searchView.findViewById(R.id.search_close_btn);

            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            et = searchView.findViewById(R.id.search_src_text); //Find EditText view
            et.setFocusableInTouchMode(false);
            et.clearFocus();
            et.setFocusableInTouchMode(true);
            btnSearchAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(MasterSearch.this)) {
                        if (et.getText().toString().length() >= 2) {
                            requestFromServer(et.getText().toString()); //if no internet in that case use this method cal
                        } else {
                            Toast.makeText(MasterSearch.this, "Please enter at least 3 characters !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            if (searchView != null) {
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        progressViewLinearLayout.setVisibility(View.GONE);
                        hideKeyBoard();
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(final String newText) {
                        try {
//                             if (newText.length() == 0) {
//                                LayNoResult.setVisibility(View.GONE);
//                                requestFromServer(et.getText().toString()); //when length 0
//                                progressViewLinearLayout.setVisibility(View.GONE);
//                            }
                            if (et.getText().toString().length() > 2) {
                                LayNoResult.setVisibility(View.GONE);
                                requestFromServer(et.getText().toString()); //if no internet in that case use this method cal
                                progressViewLinearLayout.setVisibility(View.GONE);
                                SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(MasterSearch.this);
                                //String searchText =
                                //sharedPreferencesUtils.ClearText();
                                Log.e("editTextVlaue:",""+et.getText().toString());
                                Log.e("editTextVlaue:",""+et.getText().toString());
                                sharedPreferencesUtils.setSearchText(et.getText().toString());
                                Log.e("LogData:",""+sharedPreferencesUtils.getSearchText());
                            } else {
                             //   Toast.makeText(MasterSearch.this, "Please enter at least 3 characters !!!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                });
            }
        } else {
            Toast toast = Toast.makeText(MasterSearch.this, "Sorry, You have no internet connection", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void requestFromServer(final String newText) { //TODO Server method here

        SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(MasterSearch.this);
        String seeesionCat = sharedPreferencesUtils.getCategory();
        if(seeesionCat.equalsIgnoreCase("nocat"))
        {
            seeesionCat=newText;
        }
        GateWay gateWay = new GateWay(this);

        progressViewLinearLayout.setVisibility(View.VISIBLE);
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");

        JSONObject params = new JSONObject();
        try {
            params.put("name", newText);
            params.put("city", strCity);
            params.put("area", strArea);
            params.put("areaname", Zone_Area);
            params.put("v_state", v_state);
            params.put("v_city", v_city);
           // params.put("seeesionCat", seeesionCat);
            params.put("contactNo", gateWay.getContact());
            Log.e("param_master_search",""+params.toString());

            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("search_value",newText);
            editor.apply();
            editor.commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlMasterSearch, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Log.d("masterSearch",""+response);

                ArrayList<String> dataList = new ArrayList<>();
                productListHashMap = new LinkedHashMap();
                productWiseList = new ArrayList<>();
                FinalList.clear();
                if (response.isNull("products") && response.isNull("data")) {
                    LayNoResult.setVisibility(View.VISIBLE);
                } else {
                    LayNoResult.setVisibility(View.GONE);
                    progressViewLinearLayout.setVisibility(View.GONE);
                }
                try {
                    if (!response.isNull("data")) {
                        JSONArray mainJsonArray = response.getJSONArray("data");
                        for (int i = 0; i < mainJsonArray.length(); i++) {
                            JSONObject jsonObject = mainJsonArray.getJSONObject(i);
                            dataList.add(jsonObject.getString("data"));
                        }
                    } else {
                        searchResultListView.setVisibility(View.GONE);
                        progressViewLinearLayout.setVisibility(View.GONE);
                    }

                    if (!response.isNull("products")) {
                        String tt = response.getString("title");
                        tvTitle.setText(tt);
                        tvTitle.setTextSize(16);
                        tvTitle.setTextColor(Color.RED);
                        LayoutProducts.setVisibility(View.VISIBLE);

                        JSONArray mainClassificationJsonArray1 = response.getJSONArray("products");
                        Log.e("master_brand",""+mainClassificationJsonArray1.toString());

                        for (int i = 0; i < mainClassificationJsonArray1.length(); i++) {
                            JSONObject jSonClassificationData = mainClassificationJsonArray1.getJSONObject(i);

                            productListHashMap.put(jSonClassificationData.getString("code"), "");

                            productWiseList.add(new UltimateProductList(jSonClassificationData.getString("code"),
                                    jSonClassificationData.getString("product_id"),
                                    jSonClassificationData.getString("shop_id"),
                                    jSonClassificationData.getString("deliveryavailable"),
                                    jSonClassificationData.getString("product_name"),
                                    jSonClassificationData.getString("product_image"),
                                    jSonClassificationData.getString("product_mrp"),
                                    jSonClassificationData.getString("product_price"),
                                    jSonClassificationData.getString("product_discount"),
                                    jSonClassificationData.getString("product_size"),
                                    jSonClassificationData.getString("pName"),
                                    jSonClassificationData.getString("product_hindi_name"),
                                    jSonClassificationData.getString("shop_name"),
                                    jSonClassificationData.getString("product_brand"),
                                    jSonClassificationData.getString("cart_pstatus"),
                                    jSonClassificationData.getString("main_category")));
                        }

                        for (Object o : productListHashMap.keySet()) {
                            String key = (String) o;
                            productCodeWithProductList withProductCode = new productCodeWithProductList();
                            withProductCode.code = key;
                            withProductCode.productCodeWiseProducts = new ArrayList<>();
                            for (UltimateProductList pp : productWiseList) {
                                if (pp.code.equals(key)) {
                                    withProductCode.productCodeWiseProducts.add(pp);
                                }
                            }
                            FinalList.add(withProductCode);
                        }
                        ultimateCardAdapter = new UltimateCardAdapter(FinalList);
                        recyclerView.setAdapter(ultimateCardAdapter);
                    } else {
                        LayoutProducts.setVisibility(View.GONE);
                    }
                    if (dataList.size() > 0) {
                        progressViewLinearLayout.setVisibility(View.GONE);
                        CustomAdapter customAdapter = new CustomAdapter(MasterSearch.this, dataList);
                        //searchResultListView.setAdapter(customAdapter);
                        Helper.getListViewSize(searchResultListView);
                        //searchResultListView.setVisibility(View.VISIBLE);
                        customAdapter.getFilter().filter(newText);              // very important for filtering
                        searchResultListView.setTextFilterEnabled(true);

                        searchResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                TextView tvTitle = view.findViewById(R.id.txtItem);
                                String selectedName = (String) tvTitle.getText();
                                try {
                                    if (selectedName.equals("Search History") || selectedName.equals("Popular Search") ||
                                            selectedName.equals("Brands") || selectedName.equals("Category") ||
                                            selectedName.equals("Store Name") || selectedName.equals("Department")) {
                                    } else {
                                        searchView.setQuery(selectedName, true);
                                        searchView.clearFocus();

                                        Intent intent = new Intent(MasterSearch.this, SearchProducts.class);
                                        intent.putExtra("selectedName", selectedName);
                                        intent.putExtra("tag", "next");
                                        startActivity(intent);
                                        //storeSearchHistory(selectedName);
                                    }
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                progressViewLinearLayout.setVisibility(View.GONE);
                            }
                        });
                    }

                    try {
                        et.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                    }catch ( Exception e){
                        e.getMessage();
                    }

                    //if(!et.getText().toString().isEmpty()) {
                        try {
                            et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                @Override
                                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                        if (et.getText().toString().length() > 2) {
                                            hideKeyBoard();
                                            requestFromServer(et.getText().toString());
                                            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(MasterSearch.this);
                                            //String searchText =
                                            sharedPreferencesUtils.setSearchText(et.getText().toString());
                                            Log.e("editTextVlaue:",""+et.getText().toString());
                                        } else {
                                            Toast.makeText(MasterSearch.this, "Please enter atleast 3 characters !!!", Toast.LENGTH_SHORT).show();
                                        }
                                        return true;
                                    }

                                    return false;
                                }
                            });
                        }catch (Exception e){
                            e.getMessage();
                        }
                    //}

                    /*closeButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            et.setText(""); //Clear the text from EditText view
                            searchView.setIconified(false);
                            searchView.clearFocus();
                        }
                    });*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                progressViewLinearLayout.setVisibility(View.GONE);

                GateWay gateWay = new GateWay(MasterSearch.this);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void storeSearchHistory(String selectedName) { //TODO Server method here
        GateWay gateWay = new GateWay(this);
        String strContact = gateWay.getContact();

        JSONObject params = new JSONObject();
        try {
            params.put("contactNo", strContact);
            params.put("searchKeyword", selectedName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddAppSearchHistory, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("posts").equals("true")) {
                        Toast.makeText(MasterSearch.this, "Your search keyword added into you history...", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyBoard();

        MasterSearch.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        if (Connectivity.isConnected(MasterSearch.this)) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            String searchText = prefs.getString("search_value","Farmengg");
            if(searchText.equalsIgnoreCase("")||searchText == null){

            }else if(searchText.length() >= 2){
                Log.e("textValue:",""+searchText);
                //MasterSearch.this.et.setText(searchText);
                requestFromServer(searchText);
            }

            String class_name = this.getClass().getSimpleName();

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, MasterSearch.this);*/
        }
    }

    private class CustomAdapter extends ArrayAdapter {

        private ArrayList<String> dataList;
        private Context context;

        CustomAdapter(MasterSearch masterSearch, ArrayList<String> dataList) {
            super(masterSearch, R.layout.list_item, dataList);
            context = masterSearch;
            this.dataList = dataList;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            convertView = null;
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item, null);
            TextView textView = convertView.findViewById(R.id.txtItem);
            ImageView imgLeftArrow = convertView.findViewById(R.id.imgLeftArrow);
            if (dataList.get(position).equals("Search History") ||
                    dataList.get(position).equals("Brands") || dataList.get(position).equals("Category") ||
                    dataList.get(position).equals("Popular Search") || dataList.get(position).equals("Store Name")
                    || dataList.get(position).equals("Department")) {
                textView.setText(dataList.get(position));
                textView.setTextSize(14);
                textView.setTextColor(Color.RED);
            } else {
                imgLeftArrow.setVisibility(View.VISIBLE);
                textView.setTextSize(12);
                textView.setText(dataList.get(position));
            }
            return convertView;
        }
    }

    private class UltimateProductList {

        private String str_PId;
        private String code;
        private String str_ShopId;
        private String str_DeliveryAvailable;
        private String str_PName;
        private String str_PImg;
        private String str_PMrp;
        private String str_Pprice;
        private String str_PDiscount;
        private String str_PSize;
        private String str_product_name;
        private String str_product_hindiname;
        private String str_shop_name;
        private String str_product_brand;
        private String str_maincategory;
        private String cart_pstatus;

        boolean status = false;

        UltimateProductList(String code, String str_PId, String str_ShopId, String str_DeliveryAvailable,
                            String str_PName, String str_PImg, String str_PMrp, String str_Pprice,
                            String str_PDiscount, String str_PSize, String str_product_name, String str_product_hindiname,
                            String str_shop_name,String str_product_brand, String cart_pstatus ,String str_maincategory) //,String str_maincategory
         {

            this.code = code;
            this.str_PId = str_PId;
            this.str_ShopId = str_ShopId;
            this.str_DeliveryAvailable = str_DeliveryAvailable;
            this.str_PName = str_PName;
            this.str_PImg = str_PImg;
            this.str_PMrp = str_PMrp;
            this.str_Pprice = str_Pprice;
            this.str_PDiscount = str_PDiscount;
            this.str_PSize = str_PSize;
            this.str_product_name = str_product_name;
            this.str_product_hindiname = str_product_hindiname;
            this.str_shop_name = str_shop_name;
            this.cart_pstatus = cart_pstatus;
            this.str_product_brand = str_product_brand;
             this.str_maincategory = str_maincategory;
        }

        public String getStr_PId() {
            return str_PId;
        }

        public String getStr_ShopId() {
            return str_ShopId;
        }

        public String getStr_PName() {
            return str_PName;
        }

        public String getStr_PImg() {
            return str_PImg;
        }

        public String getStr_PMrp() {
            return str_PMrp;
        }

        public String getStr_Pprice() {
            return str_Pprice;
        }

        public String getStr_PDiscount() {
            return str_PDiscount;
        }

        public String getStr_PSize() {
            return str_PSize;
        }

        public String getStr_product_name() {
            return str_product_name;
        }

        public String getStr_product_hindiname() {
            return str_product_hindiname;
        }

        public boolean setStatus(boolean status) {
            this.status = status;
            return status;
        }

        public String getStr_shop_name() {
            return str_shop_name;
        }

        public String getStr_maincategory() {
            return str_maincategory;
        }

        public void setStr_maincategory(String str_maincategory) {
            this.str_maincategory = str_maincategory;
        }

        public String getStr_product_brand() {
            return str_product_brand;
        }

        public void setStr_product_brand(String str_product_brand) {
            this.str_product_brand = str_product_brand;
        }

        public String getCart_pstatus() {
            return cart_pstatus;
        }

        public void setCart_pstatus(String cart_pstatus) {
            this.cart_pstatus = cart_pstatus;
        }
    }

    public class productCodeWithProductList {

        public ArrayList<UltimateProductList> productCodeWiseProducts;

        String code;
        int position;

        public int getPosition() {
            return position;
        }


        public void setPosition(int position) {
            this.position = position;
        }
    }

    private class UltimateCardAdapter extends RecyclerView.Adapter<MainViewHolder> {

        int selectedPosition;
        private ArrayList<productCodeWithProductList> mItems;
        List<productCodeWithProductList> areaArrayList;
        String searchString = "";

        UltimateCardAdapter(ArrayList<productCodeWithProductList> finalList) {
            this.areaArrayList = finalList;
            this.mItems = new ArrayList<>();
            this.mItems.addAll(areaArrayList);
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_of_master_search_products, parent, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(MasterSearch.this)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = recyclerView.getChildAdapterPosition(v);
                            productCodeWithProductList movie = areaArrayList.get(itemPosition);
                            selectedPosition = movie.getPosition();
                            UltimateProductList tp = movie.productCodeWiseProducts.get(selectedPosition);
                            if (!tp.getStr_PName().equals("")) {
                                Intent intent = new Intent(MasterSearch.this, SingleProductInformation.class);
                                intent.putExtra("product_name", tp.getStr_product_name());
                                intent.putExtra("shop_id", tp.getStr_ShopId());
                                startActivity(intent);
                            }
                        } else {
                            Toast toast = Toast.makeText(MasterSearch.this, "Sorry, You have no internet connection", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return new MainViewHolder(v);

        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder holder, final int position) {
            final DBHelper db = new DBHelper(MasterSearch.this);

            ArrayList<String> sizesArrayList = new ArrayList<>();
            ArrayList<String> pricesArrayList = new ArrayList<>();
            ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

            productCodeWithProductList tp = areaArrayList.get(position);
            UltimateProductList movie = tp.productCodeWiseProducts.get(0);


            String server_cart_status = movie.getCart_pstatus();

            if (server_cart_status.equalsIgnoreCase("0")) {
                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
            } else {
                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
            }

            //TODO here products image setup to glide
           /* Glide.with(MasterSearch.this)
                    .load(movie.getStr_PImg())
                    .thumbnail(Glide.with(MasterSearch.this).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProduct);*/
            Picasso.with(MasterSearch.this)
                    .load(movie.getStr_PImg())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgProduct);
            Log.e("MasterSerarchurl:",""+movie.getStr_PImg());

            holder.tvProductName.setText(movie.getStr_PName());

            String name = movie.getStr_PName().toLowerCase(Locale.getDefault());
            if (name.contains(searchString)) {
                int startPos = name.indexOf(searchString);
                int endPos = startPos + searchString.length();

                Spannable spanString = Spannable.Factory.getInstance().newSpannable(holder.tvProductName.getText());
                spanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.ColorPrimaryDark)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvProductName.setText(spanString);
            }

            if (!movie.getStr_product_hindiname().equals("")) {
                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                holder.tvProductHindiName.setText(movie.getStr_product_hindiname());
            }
            String dis = movie.getStr_PDiscount();
            holder.tvDiscount.setText(dis + "% off");
            //holder.tvShopName.setText("By:" + " " + movie.getStr_shop_name());
            holder.tvShopName.setText(movie.getStr_product_brand());
            Log.e("prduct_brand:",""+movie.getStr_product_brand());
            String productMrp = movie.getStr_PMrp();
            String discountPrice = movie.getStr_Pprice();
            if (productMrp.equals(discountPrice)) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.frameimgMsg.setVisibility(View.GONE);
                holder.tvMrp.setVisibility(View.GONE);
            } else {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.frameimgMsg.setVisibility(View.VISIBLE);
                holder.tvMrp.setVisibility(View.VISIBLE);
                holder.tvMrp.setBackgroundResource(R.drawable.dash);
            }
            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

            try {
                productCodeWithProductList newProductList = areaArrayList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp1 = newProductList.productCodeWiseProducts.get(l);
                    sizesArrayList.add(tp1.getStr_PSize());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = areaArrayList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp3 = newProductList.productCodeWiseProducts.get(l);
                    DiscountpricesArrayList.add(tp3.getStr_Pprice());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = areaArrayList.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp2 = newProductList.productCodeWiseProducts.get(l);
                    pricesArrayList.add(tp2.getStr_PMrp());

                    if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                        holder.LayoutSpinner.setVisibility(View.VISIBLE);
                        //CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(MasterSearch.this, pricesArrayList, DiscountpricesArrayList, sizesArrayList, "homepage");
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(MasterSearch.this, pricesArrayList, DiscountpricesArrayList, sizesArrayList, "");
                        holder.spinner.setAdapter(customSpinnerAdapter);
                    } else {
                        holder.LayoutSpinner.setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                    try {
                        if (Connectivity.isConnected(MasterSearch.this)) { // Internet connection is not present, Ask user to connect to Internet
                            productCodeWithProductList tp = areaArrayList.get(holder.getAdapterPosition());
                            UltimateProductList movie = tp.productCodeWiseProducts.get(pos);

                            tp.setPosition(pos); //set here position when user any wants to buy multiple size products

                            HashMap addToCartInfo;
                            String str_PIdForCart = movie.getStr_PId();
                            addToCartInfo = db.getCartDetails(str_PIdForCart);

                            List<String> cartProductsList = new ArrayList<>(addToCartInfo.values());
                            boolean addToCartCheckVal = cartProductsList.contains(movie.getStr_PId());

                            if (addToCartCheckVal) {
                                movie.setStatus(true);
                                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                            } else {
                                movie.setStatus(false);
                                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                            }

                            //TODO here product image setup to glide when user select different product variant from spinner
                           /* Glide.with(MasterSearch.this)
                                    .load(movie.getStr_PImg())
                                    .thumbnail(Glide.with(MasterSearch.this).load(R.drawable.loading))
                                    .error(R.drawable.ic_app_transparent)
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(holder.imgProduct);*/
                           Picasso.with(MasterSearch.this)
                                    .load(movie.getStr_PImg())
                                   .placeholder(R.drawable.loading)
                                    .error(R.drawable.ic_app_transparent)
                                    .into(holder.imgProduct);

                            holder.tvProductName.setText(movie.getStr_PName());

                            if (!movie.getStr_product_hindiname().equals("")) {
                                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                                holder.tvProductHindiName.setText(movie.getStr_product_hindiname());
                            }
                            String dis = movie.getStr_PDiscount();
                            holder.tvDiscount.setText(dis + "% off");
                           // holder.tvShopName.setText("By:" + " " + movie.getStr_shop_name());
                            holder.tvShopName.setText(movie.getStr_product_brand());
                            Log.e("prduct_brand:",""+movie.getStr_product_brand());
                            String productMrp = movie.getStr_PMrp();
                            String discountPrice = movie.getStr_Pprice();
                            String cart_status = movie.getCart_pstatus();
                            if (productMrp.equals(discountPrice)) {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.frameimgMsg.setVisibility(View.GONE);
                                holder.tvMrp.setVisibility(View.GONE);
                            } else {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.frameimgMsg.setVisibility(View.VISIBLE);
                                holder.tvMrp.setVisibility(View.VISIBLE);
                                holder.tvMrp.setBackgroundResource(R.drawable.dash);
                            }
                            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

            holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (Connectivity.isConnected(MasterSearch.this)) { // Internet connection is not present, Ask user to connect to Internet
                            productCodeWithProductList movie = areaArrayList.get(holder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            UltimateProductList forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);
                            String maicategory = movie.productCodeWiseProducts.get(selectedPosition).getStr_maincategory();
                            Log.d("maicategory",""+maicategory);
                            shopId = forAddToCart.getStr_ShopId();
                            pId = forAddToCart.getStr_PId();

                            /*HashMap AddToCartInfo;
                            AddToCartInfo = db.getCartDetails(forAddToCart.getStr_PId());
                            strId = (String) AddToCartInfo.get("new_pid");*/

                            if(movie.productCodeWiseProducts.get(selectedPosition).getCart_pstatus().equalsIgnoreCase("1")){
                                Intent intent = new Intent(MasterSearch.this, AddToCart.class);
                                startActivity(intent);
                            }

                            forAddToCart.setStatus(true);
                            addCartItemsToServer1(pId, shopId, forAddToCart.getStr_PName(),maicategory);


                            /* if (strId == null) {
                              //  forAddToCart.setStatus(false);
                                db.insertCount(pId);
                                int count = (int) db.fetchAddToCartCount();
                                //updateAddToCartCount(count);

                                if (count >= 0) {
                                    holder.btnAddToCart.setText("Go To Cart");
                                } else {
                                    holder.btnAddToCart.setText("Add To Cart");
                                }
                               *//* Toast toast = Toast.makeText(MasterSearch.this, forAddToCart.getStr_PName() + " adding to cart", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast.show();*//*
                                addCartItemsToServer(pId, shopId, forAddToCart.getStr_PName());
                            } else {
                                Intent intent = new Intent(MasterSearch.this, AddToCart.class);
                                startActivity(intent);
                            }*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void addCartItemsToServer1(final String pId, String shopId, final String str_pName,String mainCategory) { //TODO Server method here
                    JSONObject params = new JSONObject();
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shopId);
                        params.put("areaname", Zone_Area);
                        params.put("v_city", v_city);
                        params.put("v_state", v_state);
                        params.put("selectedType", "");
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", strContact);
                        params.put("email", strEmail);
                        if(sessionMainCat.equalsIgnoreCase("Both")){
                            params.put("sessionMainCat", "Both");
                        }else {
                            params.put("sessionMainCat", mainCategory);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("mastersearchParams",""+params);
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                               /* if (response.getString("posts").equals("true")) {
                                    Toast toast = Toast.makeText(MasterSearch.this, str_pName + " successfully added to cart", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                } else {
                                    Toast toast = Toast.makeText(MasterSearch.this, str_pName + " failed to add in cart", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }*/
                            try {
                                Log.e("Add_toCart_response:",""+response.toString());
                                String posts = response.getString("posts");
                                String promotional = response.getString("promotional");
                                String promotional_message = response.getString("promo_message");
                                if(promotional.equalsIgnoreCase("promotional")){

                                    Toast.makeText(MasterSearch.this,"Promotional Product",Toast.LENGTH_LONG).show();
                                    openSessionDialog(promotional_message,"promo");

                                }
                                else if (posts.equals("true")) {
                                    if (strId == null) {
                                        //  forAddToCart.setStatus(false);
                                       // db.insertCount(pId);
                                        int count = SyncData();//(int) db.fetchAddToCartCount();
                                        //updateAddToCartCount(count);

                                        if (count >= 0) {
                                            holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                            holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                                        } else {
                                            holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                            holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                                        }
                                    } else {
                                        Intent intent = new Intent(MasterSearch.this, AddToCart.class);
                                        startActivity(intent);
                                    }
                                    Log.d("addCartItemsToServer1", "" + posts);
                                    Toast toast = Toast.makeText(MasterSearch.this, str_pName + " successfully added to cart", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    openSessionDialog(posts,"session"); //promotional
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            GateWay gateWay = new GateWay(MasterSearch.this);
                            gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });
        }

        @Override
        public int getItemCount() {
            return areaArrayList.size();
        }

        public void filter(String charText) { // Filter Class
            charText = charText.toLowerCase();
            this.searchString = charText;

            areaArrayList.clear();
            if (charText.length() == 0) {
                areaArrayList.addAll(mItems);
            } else {
                for (productCodeWithProductList s : mItems) {
                    selectedPosition = s.getPosition();
                    UltimateProductList forAddToCart = s.productCodeWiseProducts.get(selectedPosition);
                    if (forAddToCart.getStr_PName().toLowerCase().contains(charText)) {
                        areaArrayList.add(s);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    private class MainViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgProduct;
        private TextView tvProductName;
        private TextView tvProductHindiName;
        private TextView tvMrp;
        private TextView tvDiscount;
        private TextView tvPrice;
        private TextView btnAddToCart;
        private TextView tvShopName;
        private Spinner spinner;
        private RelativeLayout LayoutSpinner;
        private FrameLayout frameimgMsg;

        public MainViewHolder(View itemView) {
            super(itemView);

            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvProductName = itemView.findViewById(R.id.txtProductName);
            tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
            tvMrp = itemView.findViewById(R.id.txtTotal);
            tvDiscount = itemView.findViewById(R.id.txtDiscount);
            tvPrice = itemView.findViewById(R.id.txtPrice);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            tvShopName = itemView.findViewById(R.id.txtShopName);
            spinner = itemView.findViewById(R.id.spinner);
            LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
            frameimgMsg = itemView.findViewById(R.id.frameimgMsg);
            btnAddToCart.setTag(this);
        }
    }

    //show dialog when If he tries to select something in other category
    private void openSessionDialog(String message, String type) {
        final Dialog dialog = new Dialog(MasterSearch.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sessoin);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        TextView tv_clearcart = dialog.findViewById(R.id.tv_clearcart);
        TextView tv_continue = dialog.findViewById(R.id.tv_continue);
        tv_message.setText(message);
        if(type.equalsIgnoreCase("promo")){
            tv_clearcart.setVisibility(View.GONE);
        }
        tv_clearcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MasterSearch.this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(MasterSearch.this)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            // GateWay gateWay = new GateWay(getActivity());
                            //gateWay.displaySnackBar(homeMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void deleteAllProductFromCartItem() {
        if (Connectivity.isConnected(MasterSearch.this)) {
            final GateWay gateWay = new GateWay(MasterSearch.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.INVISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(MasterSearch.this);
                        db.deleteOnlyCartTable();

                        //after remove cart items from local and online db
                        //then sync online cart count again
                        SyncData();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(MasterSearch.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }
    //this is for sync the cart count
    private int CartCount;
    private int SyncData() { //TODO Server method here
        if (Connectivity.isConnected(MasterSearch.this)) {
            GateWay gateWay = new GateWay(MasterSearch.this);

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("cartCountRes:",""+response.toString());

                       // updateAddToCartCount(response.getInt("normal_cart_count"));
                        CartCount = response.getInt("normal_cart_count");

                       /* if(CartCount == 0){
                            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(MasterSearch.this);
                            sharedPreferencesUtils.setCategory("nocat");
                        }*/

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().setPriority(Request.Priority.LOW);
            AppController.getInstance().addToRequestQueue(request);
        }
        return CartCount;

    }
    /*public void updateAddToCartCount(final int count) {
        if (tvCart == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCart.setVisibility(View.VISIBLE);
                if (count == 0) {
                    tvCart.setText("" + count);
                } else {
                    tvCart.setText("" + count);
                }
            }
        });
    } */
}