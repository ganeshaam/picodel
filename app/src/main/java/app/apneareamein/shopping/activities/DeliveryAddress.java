package app.apneareamein.shopping.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.services.EasyOrderDetails;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.SharedPreferencesUtils;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;

public class DeliveryAddress extends AppCompatActivity {

    boolean defaultValue = false;
    private Bundle bundle;
    private String tag, finalPrice;
    private RecyclerView mRecyclerView;
    private CardAdapter cardAdapter;
    private CoordinatorLayout DeliveryAddressedMainLayout;
    private RelativeLayout relativeLayoutaddresses, relativeLayoutEmptyAddress;
    private final String class_name = this.getClass().getSimpleName();
    Intent i = null;
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;
    Context mContext;
    TextView tvTitle;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_address);
        mContext = DeliveryAddress.this;

        //initializeViews();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Select Address");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            tag = bundle.getString("tag");
            finalPrice = bundle.getString("finalPrice");
        }

        Button btnAddAddress = findViewById(R.id.btnAddAddress);
        Button btnAddAddress1 = findViewById(R.id.btnAddAddress1);
        DeliveryAddressedMainLayout = findViewById(R.id.myaddressedMainLayout);
        relativeLayoutEmptyAddress = findViewById(R.id.relativeLayoutEmptyAddress);
        relativeLayoutaddresses = findViewById(R.id.relativeLayoutaddresses);
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //cardAdapter = new DeliveryAddress.CardAdapter();
        //mRecyclerView.setAdapter(cardAdapter);

        relativeLayoutaddresses.setVisibility(View.GONE);
        relativeLayoutEmptyAddress.setVisibility(View.GONE);

        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tag) {
                    case "address":
                        if (Connectivity.isConnected(mContext)) {
                            i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "address");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;

                    case "easy_order_address":
                        if (Connectivity.isConnected(mContext)) {
                            i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "easy_order_address");
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;

                    case "resto":
                        if (Connectivity.isConnected(mContext)) {
                            i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "resto");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;

                    default:
                        if (Connectivity.isConnected(mContext)) {
                         i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "simple");
                            i.putExtras(bundle);
                            startActivity(i);
                            //startActivityForResult(i,1);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;
                }
            }
        });

        btnAddAddress1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tag) {
                    case "address":
                        if (Connectivity.isConnected(mContext)) {
                            i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "address");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;

                    case "easy_order_address":
                        if (Connectivity.isConnected(mContext)) {
                            i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "easy_order_address");
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;

                    case "resto":
                        if (Connectivity.isConnected(mContext)) {
                            i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "resto");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;

                    default:
                        if (Connectivity.isConnected(mContext)) {
                            i = new Intent(mContext, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "simple");
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(mContext);
                            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                        }
                        break;
                }
            }
        });
       // myReceiver = new DeliveryAddress.Network_Change_Receiver();
        RequestforAddresses();
    }
    @Override
    public void onPause() {
        super.onPause();
     /*   mContext.unregisterReceiver(myReceiver);
        GateWay gateWay = new GateWay(mContext);
        gateWay.hide();*/
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                relativeLayoutaddresses.setVisibility(View.GONE);
              /*  mContext.Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);*/
              Toast.makeText(mContext,"No connection",Toast.LENGTH_SHORT).show();
            } else {
               /* BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(BaseActivity.txtNoConnection);*/
                Toast.makeText(mContext,"Back online",Toast.LENGTH_SHORT).show();
               //RequestforAddresses();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {
        public Network_Change_Receiver() {
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
           // Activity activity = mContext;
            if ( mContext != null) {
                dialog(status);
            }
        }
    }

   /* private void initializeViews() {
        homePageActivity = (BaseActivity) mContext;
    }*/
 /*  @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
       super.onActivityResult(requestCode, resultCode, data);
       if (requestCode == 1) {
           if(resultCode == RESULT_OK) {
               bundle = getIntent().getExtras();
               if (bundle != null) {
                   tag = bundle.getString("tag");
               }
              RequestforAddresses();
           }
       }
   }*/

    @Override
    public void onResume() {
        super.onResume();
    /*   mContext.registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        ((BaseActivity)mContext).changeTitle(getString(R.string.title_my_address));

        if (Connectivity.isConnected(mContext)) {
            GateWay gateWay = new GateWay(mContext);
            gateWay.SyncData();
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, mContext);
        }
        GateWay gateWay = new GateWay(mContext);
        gateWay.hide();*/
       // RequestforAddresses();
       // myReceiver = new DeliveryAddress.Network_Change_Receiver();
    }

    private void RequestforAddresses() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) {
            final GateWay gateWay = new GateWay(mContext);
            gateWay.progressDialogStart();

            cardAdapter = new DeliveryAddress.CardAdapter();

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "fetch");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddNewAddress, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        Log.d("RequestforAddresses",res);
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");

                        if (status.equals("success")) {
                            JSONArray addressArray = jsonObject.getJSONArray("user_data");

                            for (int i = 0; i < addressArray.length(); i++) {
                                JSONObject json_event_list = addressArray.getJSONObject(i);
                                addressList e = new addressList(json_event_list.getString("id"),
                                        json_event_list.getString("name"),
                                        json_event_list.getString("contact"),
                                        json_event_list.getString("address"),
                                        json_event_list.getString("address1"),
                                        json_event_list.getString("address2"),
                                        json_event_list.getString("address3"),
                                        json_event_list.getString("address4"),
                                        json_event_list.getString("address5"),
                                        json_event_list.getString("area"));
                                cardAdapter.add(e);
                            }
                            relativeLayoutaddresses.setVisibility(View.VISIBLE);
                            relativeLayoutEmptyAddress.setVisibility(View.GONE);
                            mRecyclerView.setAdapter(cardAdapter);
                            cardAdapter.notifyDataSetChanged();
                        } else {
                            relativeLayoutaddresses.setVisibility(View.GONE);
                            relativeLayoutEmptyAddress.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
          // ((BaseActivity) mContext).Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    private void deleteAddress(String id) {
        if (Connectivity.isConnected(mContext)) {
            GateWay gateWay = new GateWay(mContext);

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("id", id);
                params.put("tag", "delete");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddNewAddress, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("message");

                        if (status.equals("fail")) {
                            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                            RequestforAddresses();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(mContext);
            gateWay.displaySnackBar(DeliveryAddressedMainLayout);
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<DeliveryAddress.ViewHolder> {

        final ArrayList<DeliveryAddress.addressList> mItems = new ArrayList<>();

        public CardAdapter() {
        }

        @Override
        public void onBindViewHolder(@NonNull final DeliveryAddress.ViewHolder viewHolder, final int position) {
            final DeliveryAddress.addressList ad = mItems.get(position);

            try {
                /*if (ad.getAddress_type().equals("default")) {
                    viewHolder.viewLine.setVisibility(View.VISIBLE);
                    viewHolder.txtDefault.setVisibility(View.VISIBLE);
                    viewHolder.txtDefault.setText("To be Delivered");
                } else {
                    viewHolder.txtDefault.setVisibility(View.GONE);
                    viewHolder.viewLine.setVisibility(View.GONE);
                }*/
                viewHolder.tvName.setText(ad.getStr_name());
                viewHolder.tvContact.setText(ad.getStr_contact());
                //viewHolder.tvAddress.setText(ad.getStr_address());
                viewHolder.tvAddress.setText(ad.getAddress1()+" "+ad.getAddress2()+" "+ad.getAddress3()+" "+ad.getAddress4()+" "+ad.getAddress5()+" "+ad.area);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            viewHolder.rbSelectAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                            //Log.d("rbSelectAddress",""+position);
                            Log.d("address_id_test",ad.getStr_id());
                            Intent intent = new Intent();
                            intent.putExtra("addressPosition", position);
                            intent.putExtra("address_id_test",ad.getStr_id());
                            intent.putExtra("address1",ad.getAddress1());
                            intent.putExtra("address1",ad.getAddress1());
                            intent.putExtra("address2",ad.getAddress2());
                            intent.putExtra("address3",ad.getAddress3());
                            intent.putExtra("address4",ad.getAddress4());
                            intent.putExtra("address5",ad.getAddress5());
                            intent.putExtra("area",ad.getArea());
                            intent.putExtra("address",ad.getStr_address());
                            sharedPreferencesUtils.setCategory(ad.getStr_id());
                            setResult(RESULT_OK, intent);
                            finish();
                            //onBackPressed();
                }
            });

            viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //defaultValue = ad.getAddress_type().equals("default");
                    String address_id = ad.getStr_id();

                    switch (tag) {
                        case "address":
                            if (Connectivity.isConnected(mContext)) {
                                Intent i = new Intent(mContext, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "address");
                                bundle.putString("finalPrice", finalPrice);
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                            }
                            break;

                        case "easy_order_address":
                            if (Connectivity.isConnected(mContext)) {
                                Intent i = new Intent(mContext, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "easy_order_address");
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                            }
                            break;

                        case "resto":
                            if (Connectivity.isConnected(mContext)) {
                                Intent i = new Intent(mContext, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "resto");
                                bundle.putString("finalPrice", finalPrice);
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                            }
                            break;

                        default:
                            if (Connectivity.isConnected(mContext)) {
                                Intent i = new Intent(mContext, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "simple");
                                bundle.putString("finalPrice", "");
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(mContext);
                                gateWay.displaySnackBar(DeliveryAddressedMainLayout);
                            }
                            break;
                    }
                }
            });

            viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String address_id = ad.getStr_id();
                   /* UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on remove To be Delivered address", mContext);*/
                    deleteAlertDialog(address_id);
                   // mItems.remove(position);
                    /*if (ad.getAddress_type().equals("default")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle("To be Delivered address");
                        builder.setMessage("Are you sure, you want to delete To be Delivered address?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (Connectivity.isConnected(mContext)) {
                                            dialog.dismiss();

                                            deleteAddress(address_id);
                                        } else {
                                            dialog.dismiss();
                                        }
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {

                    }*/
                }
            });

            viewHolder.btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tag == null) {
                    } else if (tag.equals("address")) {
                        DeliveryAddress.addressList movie = mItems.get(viewHolder.getAdapterPosition());
                        String id = movie.getStr_id();

                        Intent intent = new Intent(mContext, Order_Details_StepperActivity.class);
                        intent.putExtra("finalPrice", finalPrice);
                        intent.putExtra("address_id", id);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else if (tag.equals("easy_order_address")) {
                        DeliveryAddress.addressList movie = mItems.get(viewHolder.getAdapterPosition());
                        String id = movie.getStr_id();

                        Intent intent = new Intent(mContext, EasyOrderDetails.class);
                        intent.putExtra("address_id", id);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }
            });

        }

        @NonNull
        @Override
        public DeliveryAddress.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_view_del_address, viewGroup, false);
            return new DeliveryAddress.ViewHolder(v);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(DeliveryAddress.addressList e) {
            mItems.add(e);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvName;
        final TextView tvContact;
        final TextView tvAddress;
        final TextView txtDefault;
        Button btnEdit, btnDelete, btnConfirm;
        View viewLine;
        RadioButton rbSelectAddress;

        public ViewHolder(View itemView) {
            super(itemView);

            viewLine = itemView.findViewById(R.id.viewLine);
            tvName = itemView.findViewById(R.id.txtName);
            tvContact = itemView.findViewById(R.id.txtContact);
            tvAddress = itemView.findViewById(R.id.txtAddress);
            txtDefault = itemView.findViewById(R.id.txtDefault);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            rbSelectAddress = itemView.findViewById(R.id.rb_select_address);
            btnConfirm = itemView.findViewById(R.id.btnConfirm);
            btnEdit.setTag(this);
            btnDelete.setTag(this);
            btnConfirm.setTag(this);
        }
    }

    private void deleteAlertDialog(final String address_id) { //this is calling from HomePage to check the combo offers available or not
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure you want to delete this address")
                .setTitle("Remove Address")
                .setCancelable(true)
                .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": clicked on remove address", mContext);*/

                        deleteAddress(address_id);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private class addressList {

        final String str_id;
        final String str_name;
        final String str_contact;
        final String str_address;
        final String address1;
        final String address2;
        final String address3;
        final String address4;
        final String address5;
        final String area;

        addressList(String str_id, String str_name, String str_contact, String str_address,String address1,String address2,String address3,String address4,String address5,String area) {//,
            this.str_id = str_id;
            this.str_name = str_name;
            this.str_contact = str_contact;
            this.str_address = str_address;
             this.address1 = address1;
            this.address2 = address2;
            this.address3 = address3;
            this.address4 = address4;
            this.address5 = address5;
            this.area = area;
        }

        public String getStr_id() {
            return str_id;
        }

        public String getStr_name() {
            return str_name;
        }

        public String getStr_contact() {
            return str_contact;
        }

        public String getStr_address() {
            return str_address;
        }

        public String getAddress1() {
            return address1;
        }
        public String getAddress2() {
            return address2;
        }
        public String getAddress3() {
            return address3;
        }
        public String getAddress4() {
            return address4;
        }
        public String getAddress5() {
            return address5;
        }
        public String getArea() {
            return area;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
