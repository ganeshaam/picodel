package app.apneareamein.shopping.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class DyanamicActivity extends AppCompatActivity {

    WebView wb_dyanamic;
    TextView tvTitle;
    ProgressBar pb_progressbar;
    String option ="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dynamic_layout);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("Activity");

        wb_dyanamic = findViewById(R.id.wb_dyanamic);
        pb_progressbar = findViewById(R.id.pb_progressbar);
        option = getIntent().getStringExtra("option");
        getDyanamicValues();


    }

    private void getDyanamicValues() { //TODO Server method here
        if (Connectivity.isConnected(DyanamicActivity.this)) {
            GateWay gateWay = new GateWay(DyanamicActivity.this);

            String email = gateWay.getUserEmail();
            String user_contact = gateWay.getContact();

            JSONObject params = new JSONObject();
            try {
                params.put("email", email);
                params.put("user_contact", user_contact);
                params.put("option", option);

                Log.d("dynamic", ";params: " + params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urldyanamic_activity, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.e("DyanamicAPI", "onResponse: " + response.toString());
                        String result = response.getString("result");

                        wb_dyanamic.loadData(result, "text/html", "UTF-8");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    Log.d("SCRATCHED", "onErr: " + error.toString());
                    GateWay gateWay = new GateWay(DyanamicActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(DyanamicActivity.this, "Please check Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
