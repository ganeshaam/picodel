package app.apneareamein.shopping.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.VolleySingleton;

public class PromotionalPartner extends AppCompatActivity {

    CheckBox cbCategory;
    List<String> checkedCategory;
    Button btnSumbit;
    ArrayList<String> categoryList;
    CategoryAdapter categoryAdapter;
    ListView categoriesList;
    TextView txtCategoryMessage;
    private DialogPlus filterDialogPlus;
    TextView txtFilter;
    WebView terms;
    TextView tvAcceptTerms;
    DialogPlus dialogPlus;
    TextView txtMessage;
    TextView txtShoppingMessage, txtTransferShoppingMessage;
    EditText editShopOrAutoContact, editShopOrAutoPurchaseAmt;
    EditText editTransferUserName, editTransferMobileNo, editTransferCashBack;
    Button btnFromShopOrAutoSubmit, btnTransferSubmit, btnOk, btnShare;
    Button btnBack, btnFromBack, btnTransferBack;
    LinearLayout defaultLayout, fromShopOrAuto, transferCashBackPoints, shoppingLayout,
            shoppingTransferLayout;
    RecyclerView recycler_view_cash_back;
    DialogPlus cashBackDialog;
    RecyclerView recycler_view;
    CashBackAdapter cashBackAdapter;
    CardAdapter cardAdapter;
    TextView tvTitle;
    String cash_back_points;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotional_partner);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            cash_back_points = bundle.getString("cash_back_points");
        }
        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_promotional_shop);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtFilter = findViewById(R.id.txtFilter);

        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());

        getCashBackType();

        getDataOfShopkeeper();

        txtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkedCategory = new ArrayList<>();

                filterDialogPlus = DialogPlus.newDialog(PromotionalPartner.this)
                        .setContentHolder(new ViewHolder(R.layout.dialog_for_categories_filter))
                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                        .setGravity(Gravity.CENTER)
                        .create();

                categoriesList = (ListView) filterDialogPlus.findViewById(R.id.categoriesList);
                categoriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        cbCategory = view.findViewById(R.id.checkBox1);
                        TextView chkName = view.findViewById(R.id.checkBoxName);
                        if (!cbCategory.isChecked()) {
                            cbCategory.setChecked(true);
                            checkedCategory.add((String) chkName.getText());
                        } else {
                            cbCategory.setChecked(false);
                            checkedCategory.remove(chkName.getText());
                        }
                    }
                });

                txtCategoryMessage = (TextView) filterDialogPlus.findViewById(R.id.txtCategoryMessage);
                btnSumbit = (Button) filterDialogPlus.findViewById(R.id.btnSubmit);
                btnSumbit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        filterDialogPlus.dismiss();
                        filterCategoryWiseShop();
                    }
                });
                getCategories();
            }
        });
    }

    private void filterCategoryWiseShop() {
        cardAdapter = new CardAdapter();

        JSONArray jsonArray = new JSONArray(checkedCategory);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("category", jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetShopkeeperData, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("posts");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        POJOClass pojoClass = new POJOClass(jsonObject.getString("contact_no"),
                                jsonObject.getString("contact_person"),
                                jsonObject.getString("logo"),
                                jsonObject.getString("shop_id"),
                                jsonObject.getString("shop_name"),
                                jsonObject.getString("address"),
                                jsonObject.getString("products"),
                                jsonObject.getString("terms_condition"),
                                jsonObject.getString("adjustment_in_percentage"));

                        cardAdapter.add(pojoClass);
                    }
                    recycler_view.setAdapter(cardAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void getCategories() {
        categoryList = new ArrayList<>();
        categoryAdapter = new CategoryAdapter(PromotionalPartner.this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetShopCategories, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (!response.isNull("posts")) {
                    txtCategoryMessage.setVisibility(View.GONE);
                    filterDialogPlus.show();

                    try {
                        JSONArray jsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            categoryList.add(jsonObject1.getString("category"));

                            CategoryPOJO categoryPOJO = new CategoryPOJO(jsonObject1.getString("category"),
                                    jsonObject1.getString("category_cnt"));
                            categoryAdapter.add(categoryPOJO);
                        }
                        categoriesList.setAdapter(categoryAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    txtCategoryMessage.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        //AppController.getInstance().addToRequestQueue(request);
        VolleySingleton.getInstance(PromotionalPartner.this).addToRequestQueue(request);
    }

    private void getCashBackType() {
        cashBackAdapter = new CashBackAdapter();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCashBackType, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (!response.isNull("posts")) {
                        cashBackDialog = DialogPlus.newDialog(PromotionalPartner.this)
                                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_cash_back))
                                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .setCancelable(false)
                                .setPadding(20, 20, 20, 20)
                                .create();
                        cashBackDialog.show();

                        defaultLayout = (LinearLayout) cashBackDialog.findViewById(R.id.defaultLayout);
                        fromShopOrAuto = (LinearLayout) cashBackDialog.findViewById(R.id.fromShopOrAuto);
                        transferCashBackPoints = (LinearLayout) cashBackDialog.findViewById(R.id.transferCashBackPoints);
                        shoppingLayout = (LinearLayout) cashBackDialog.findViewById(R.id.shoppingLayout);
                        txtShoppingMessage = (TextView) cashBackDialog.findViewById(R.id.txtShoppingMessage);
                        btnOk = (Button) cashBackDialog.findViewById(R.id.btnOk);
                        btnOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                cashBackDialog.dismiss();
                                onBackPressed();
                            }
                        });

                        shoppingTransferLayout = (LinearLayout) cashBackDialog.findViewById(R.id.shoppingTransferLayout);
                        txtTransferShoppingMessage = (TextView) cashBackDialog.findViewById(R.id.txtTransferShoppingMessage);
                        btnShare = (Button) cashBackDialog.findViewById(R.id.btnShare);
                        btnShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                cashBackDialog.dismiss();

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_SEND);
                                intent.putExtra(Intent.EXTRA_TEXT, "Hey check out Apne Area Mein Online Shopping app at: https://play.google.com/store/apps/details?id=app.apneareamein.shopping&hl=en");
                                intent.setType("text/plain");
                                startActivity(intent);
                            }
                        });

                        recycler_view_cash_back = (RecyclerView) cashBackDialog.findViewById(R.id.recycler_view_cash_back);
                        recycler_view_cash_back.setHasFixedSize(true);
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PromotionalPartner.this, LinearLayoutManager.HORIZONTAL, false);
                        recycler_view_cash_back.setLayoutManager(mLayoutManager);
                        recycler_view_cash_back.setItemAnimator(new DefaultItemAnimator());

                        JSONArray jsonArray = response.getJSONArray("posts");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            POJOClass pojoClass = new POJOClass(jsonObject.getString("type"));

                            cashBackAdapter.add(pojoClass);
                        }
                        recycler_view_cash_back.setAdapter(cashBackAdapter);

                        editShopOrAutoContact = (EditText) cashBackDialog.findViewById(R.id.editShopOrAutoContact);
                        editShopOrAutoPurchaseAmt = (EditText) cashBackDialog.findViewById(R.id.editShopOrAutoPurchaseAmt);
                        editTransferUserName = (EditText) cashBackDialog.findViewById(R.id.editTransferUserName);
                        editTransferMobileNo = (EditText) cashBackDialog.findViewById(R.id.editTransferMobileNo);
                        editTransferCashBack = (EditText) cashBackDialog.findViewById(R.id.editTransferCashBack);

                        btnFromShopOrAutoSubmit = (Button) cashBackDialog.findViewById(R.id.btnFromShopOrAutoSubmit);
                        btnFromShopOrAutoSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (editShopOrAutoContact.getText().toString().trim().equals("")) {
                                    Toast.makeText(PromotionalPartner.this, "Please enter contact no", Toast.LENGTH_LONG).show();
                                } else if (editShopOrAutoPurchaseAmt.getText().toString().trim().equals("")) {
                                    Toast.makeText(PromotionalPartner.this, "Please enter amount of purchase / fare", Toast.LENGTH_LONG).show();
                                } else {
                                    updateShoppingDetails();
                                }
                            }
                        });

                        btnTransferSubmit = (Button) cashBackDialog.findViewById(R.id.btnTransferSubmit);
                        btnTransferSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (editTransferUserName.getText().toString().trim().equals("")) {
                                    Toast.makeText(PromotionalPartner.this, "Please enter name", Toast.LENGTH_LONG).show();
                                } else if (editTransferMobileNo.getText().toString().trim().equals("")) {
                                    Toast.makeText(PromotionalPartner.this, "Please enter mobile no", Toast.LENGTH_LONG).show();
                                } else if (editTransferCashBack.getText().toString().trim().equals("")) {
                                    Toast.makeText(PromotionalPartner.this, "Please enter cashback points to be transfer", Toast.LENGTH_LONG).show();
                                } else {
                                    transferCbToUser();
                                }
                            }
                        });

                        btnBack = (Button) cashBackDialog.findViewById(R.id.btnBack);
                        btnBack.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        });

                        btnFromBack = (Button) cashBackDialog.findViewById(R.id.btnFromBack);
                        btnFromBack.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editShopOrAutoContact.setText("");
                                editShopOrAutoPurchaseAmt.setText("");
                                defaultLayout.setVisibility(View.VISIBLE);
                                fromShopOrAuto.setVisibility(View.GONE);
                            }
                        });

                        btnTransferBack = (Button) cashBackDialog.findViewById(R.id.btnTransferBack);
                        btnTransferBack.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editTransferUserName.setText("");
                                editTransferMobileNo.setText("");
                                editTransferCashBack.setText("");
                                defaultLayout.setVisibility(View.VISIBLE);
                                transferCashBackPoints.setVisibility(View.GONE);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                GateWay gateWay = new GateWay(PromotionalPartner.this);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        //AppController.getInstance().setPriority(Request.Priority.IMMEDIATE);
       // AppController.getInstance().addToRequestQueue(request);
        VolleySingleton.getInstance(PromotionalPartner.this).addToRequestQueue(request);
    }

    private void transferCbToUser() {
        final GateWay gateWay = new GateWay(PromotionalPartner.this);
        gateWay.progressDialogStart();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_name", editTransferUserName.getText().toString());
            jsonObject.put("user_contact", editTransferMobileNo.getText().toString());
            jsonObject.put("cb_to_be_transfer", editTransferCashBack.getText().toString());
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("tag", "transfer");
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdateShoppingDetails, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                gateWay.progressDialogStop();
                try {
                    switch (response.getString("key")) {
                        case "transfer": {
                            String cb_message = response.getString("message");

                            transferCashBackPoints.setVisibility(View.GONE);
                            shoppingTransferLayout.setVisibility(View.VISIBLE);
                            txtTransferShoppingMessage.setText(cb_message);
                            break;
                        }
                        case "failed":
                            String cb_message = response.getString("message");

                            transferCashBackPoints.setVisibility(View.GONE);
                            shoppingTransferLayout.setVisibility(View.VISIBLE);
                            txtTransferShoppingMessage.setText(cb_message);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                gateWay.progressDialogStop();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void updateShoppingDetails() {
        final GateWay gateWay = new GateWay(PromotionalPartner.this);
        gateWay.progressDialogStart();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shop_contact", editShopOrAutoContact.getText().toString());
            jsonObject.put("purchase_or_fare", editShopOrAutoPurchaseAmt.getText().toString());
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("type", "get");
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdateShoppingDetails, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                gateWay.progressDialogStop();
                try {
                    switch (response.getString("key")) {
                        case "promotional": {
                            String cash_back_points = response.getString("message");
                            setCashbackMessage(cash_back_points);
                            break;
                        }
                        case "auto": {
                            String cash_back_points = response.getString("message");
                            setCashbackMessage(cash_back_points);
                            break;
                        }
                        default:
                            String message = response.getString("message");
                            fromShopOrAuto.setVisibility(View.GONE);
                            shoppingLayout.setVisibility(View.VISIBLE);
                            txtShoppingMessage.setText(message);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                gateWay.progressDialogStop();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void setCashbackMessage(String cash_back_points) {
        fromShopOrAuto.setVisibility(View.GONE);
        shoppingLayout.setVisibility(View.VISIBLE);
        txtShoppingMessage.setText(cash_back_points);
    }

    private void getDataOfShopkeeper() {
        cardAdapter = new CardAdapter();

        final GateWay gateWay = new GateWay(PromotionalPartner.this);
        gateWay.progressDialogStart();

        JSONObject params = new JSONObject();
        try {
            params.put("city", gateWay.getCity());
            params.put("area", gateWay.getArea());
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetShopkeeperData, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("posts");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        POJOClass pojoClass = new POJOClass(jsonObject.getString("contact_no"),
                                jsonObject.getString("contact_person"),
                                jsonObject.getString("logo"),
                                jsonObject.getString("shop_id"),
                                jsonObject.getString("shop_name"),
                                jsonObject.getString("address"),
                                jsonObject.getString("products"),
                                jsonObject.getString("terms_condition"),
                                jsonObject.getString("adjustment_in_percentage"));

                        cardAdapter.add(pojoClass);
                    }
                    recycler_view.setAdapter(cardAdapter);

                    gateWay.progressDialogStop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                gateWay.progressDialogStop();

                error.printStackTrace();

                GateWay gateWay = new GateWay(PromotionalPartner.this);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        AppController.getInstance().setPriority(Request.Priority.LOW);
        AppController.getInstance().addToRequestQueue(request);
    }

    private class POJOClass {

        String contact_no;
        String contact_person;
        String logo;
        String shop_id;
        String shop_name;
        String address;
        String products;
        String terms_condition;
        String adjustment_in_percentage;

        String type;

        POJOClass(String contact_no, String contact_person, String logo, String shop_id,
                  String shop_name, String address, String products, String terms_condition, String adjustment_in_percentage) {
            this.contact_no = contact_no;
            this.contact_person = contact_person;
            this.logo = logo;
            this.shop_id = shop_id;
            this.shop_name = shop_name;
            this.address = address;
            this.products = products;
            this.terms_condition = terms_condition;
            this.adjustment_in_percentage = adjustment_in_percentage;
        }

        POJOClass(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public String getContact_no() {
            return contact_no;
        }

        public String getContact_person() {
            return contact_person;
        }

        public String getLogo() {
            return logo;
        }

        public String getShop_id() {
            return shop_id;
        }

        public String getAddress() {
            return address;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getProducts() {
            return products;
        }

        public String getTerms_condition() {
            return terms_condition;
        }

        public String getAdjustment_in_percentage() {
            return adjustment_in_percentage;
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<ShopViewHolder> {

        List<POJOClass> pojoClasses = new ArrayList<>();

        @NonNull
        @Override
        public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_promotional_shop, viewGroup, false);
            final ShopViewHolder shopViewHolder = new ShopViewHolder(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cash_back_points.equals("0")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PromotionalPartner.this);
                        builder.setTitle("You have ( " + cash_back_points + " ) Cash Back Points");
                        builder.setMessage("You do not have Cash Back points, please Order and collect Cash Back Points.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                        onBackPressed();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        int itemPosition = recycler_view.getChildAdapterPosition(view);

                        POJOClass pojoClass = pojoClasses.get(itemPosition);

                        if (pojoClass.getShop_name().equals("Vighnaharta Gold Membership Card")) {
                            dialogPlus = DialogPlus.newDialog(PromotionalPartner.this)
                                    .setContentHolder(new ViewHolder(R.layout.dialog_cashback_terms))
                                    .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                    .setGravity(Gravity.CENTER)
                                    .setHeader(R.layout.terms_header)
                                    .setPadding(20, 20, 20, 20)
                                    .setFooter(R.layout.terms_footer)
                                    .setCancelable(true)
                                    .create();
                            dialogPlus.show();
                            terms = (WebView) dialogPlus.findViewById(R.id.webview);
                            txtMessage = (TextView) dialogPlus.findViewById(R.id.txtMessage);
                            tvAcceptTerms = (TextView) dialogPlus.findViewById(R.id.txtAcceptTerms);
                            Pattern p = Pattern.compile(ApplicationUrlAndConstants.URL_REGEX);
                            Matcher m = p.matcher(pojoClass.getTerms_condition());//replace with string to compare
                            if (m.find()) {
                                terms.setVisibility(View.VISIBLE);
                                terms.loadUrl(pojoClass.getTerms_condition());
                                tvAcceptTerms.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        sendUserDetailsToShop();
                                    }
                                });
                            }
                        } else {
                            Intent intent = new Intent(PromotionalPartner.this, RedeemPoints.class);
                            intent.putExtra("shop_id", pojoClass.getShop_id());
                            intent.putExtra("shop_name", pojoClass.getShop_name());
                            intent.putExtra("contact_no", pojoClass.getContact_no());
                            intent.putExtra("cash_back_points", cash_back_points);
                            intent.putExtra("adjustment_in_percentage", pojoClass.getAdjustment_in_percentage());
                            startActivity(intent);
                        }
                    }
                }
            });
            return shopViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ShopViewHolder holder, int position) {
            final POJOClass pojoClass = pojoClasses.get(position);

            if (pojoClass.getTerms_condition().equals("")) {
                holder.info.setVisibility(View.GONE);
            } else {
                holder.info.setVisibility(View.VISIBLE);
                holder.info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final DialogPlus dialogPlus = DialogPlus.newDialog(PromotionalPartner.this)
                                .setContentHolder(new ViewHolder(R.layout.dialog_cashback_terms))
                                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .setHeader(R.layout.terms_header)
                                .setPadding(20, 20, 20, 20)
                                .setFooter(R.layout.terms_footer)
                                .setCancelable(true)
                                .create();
                        dialogPlus.show();
                        WebView terms = (WebView) dialogPlus.findViewById(R.id.webview);
                        terms.setVisibility(View.VISIBLE);
                        TextView tvAcceptTerms = (TextView) dialogPlus.findViewById(R.id.txtAcceptTerms);
                        terms.loadUrl(pojoClass.getTerms_condition());
                        tvAcceptTerms.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogPlus.dismiss();
                            }
                        });
                    }
                });
            }

            //TODO here show combo offer product images setup to gilde
           /* Glide.with(PromotionalPartner.this).load(pojoClass.getLogo())
                    .thumbnail(Glide.with(PromotionalPartner.this).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.logo);
*/
            Picasso.with(PromotionalPartner.this).load(pojoClass.getLogo())
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.logo);


            holder.txtShopName.setText(pojoClass.getShop_name());
            holder.txtContactPerson.setText(pojoClass.getContact_person());
            holder.txtShopAddress.setText(pojoClass.getAddress());
            if (!pojoClass.getProducts().equals("")) {
                holder.txtProducts.setVisibility(View.VISIBLE);
                holder.txtProducts.setText(pojoClass.getProducts());
            } else {
                holder.txtProducts.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return pojoClasses.size();
        }

        public void add(POJOClass pojoClass) {
            pojoClasses.add(pojoClass);
        }
    }

    private void sendUserDetailsToShop() {
        GateWay gateWay = new GateWay(PromotionalPartner.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("tag", "send_details");
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdateShoppingDetails, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("key").equals("promotional_vgold")) {
                        dialogPlus.dismiss();
                        Toast.makeText(PromotionalPartner.this, response.getString("message"), Toast.LENGTH_LONG).show();
                        getCashBackType();
                    } else if (response.getString("key").equals("failed")) {
                        terms.setVisibility(View.GONE);
                        txtMessage.setVisibility(View.VISIBLE);
                        txtMessage.setText(response.getString("message"));
                        tvAcceptTerms.setText("Ok, got it");
                        tvAcceptTerms.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogPlus.dismiss();
                                getCashBackType();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private class ShopViewHolder extends RecyclerView.ViewHolder {

        ImageView info;
        ImageView logo;
        TextView txtShopName;
        TextView txtContactPerson;
        TextView txtShopAddress;
        TextView txtProducts;

        ShopViewHolder(View v) {
            super(v);

            info = v.findViewById(R.id.info);
            logo = v.findViewById(R.id.logo);
            txtShopName = v.findViewById(R.id.txtShopName);
            txtContactPerson = v.findViewById(R.id.txtContactPerson);
            txtShopAddress = v.findViewById(R.id.txtShopAddress);
            txtProducts = v.findViewById(R.id.txtProducts);
            info.setTag(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class CashBackAdapter extends RecyclerView.Adapter<CashBackViewHolder> {

        List<POJOClass> pojoClasses = new ArrayList<>();

        @NonNull
        @Override
        public CashBackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_casback_type, parent, false);
            return new CashBackViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final CashBackViewHolder holder, int position) {
            POJOClass pojoClass = pojoClasses.get(position);

            holder.rbCashBackType.setText(pojoClass.type);

            holder.rbCashBackType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.rbCashBackType.getText().toString().equals("Transfer Cashback to Someone.")) {
                        transferCashBackPoints.setVisibility(View.VISIBLE);
                        holder.rbCashBackType.setChecked(false);
                    } else {
                        defaultLayout.setVisibility(View.GONE);
                    }
                    if (holder.rbCashBackType.getText().toString().equals("Get Cashback from Shop / Auto.")) {
                        fromShopOrAuto.setVisibility(View.VISIBLE);
                        holder.rbCashBackType.setChecked(false);
                    } else {
                        defaultLayout.setVisibility(View.GONE);
                    }
                    if (holder.rbCashBackType.getText().toString().equals("Use Cashback to get Discount\n" +
                            "(Applicable only on 1st time Shopping).")) {
                        cashBackDialog.dismiss();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return pojoClasses.size();
        }

        public void add(POJOClass pojoClass) {
            pojoClasses.add(pojoClass);
        }
    }

    private class CashBackViewHolder extends RecyclerView.ViewHolder {
        RadioButton rbCashBackType;

        CashBackViewHolder(View v) {
            super(v);

            rbCashBackType = v.findViewById(R.id.rbCashBackType);
            rbCashBackType.setTag(this);
        }
    }

    private class CategoryPOJO {

        String category;
        String category_cnt;

        CategoryPOJO(String category, String category_cnt) {
            this.category = category;
            this.category_cnt = category_cnt;
        }

        public String getCategory() {
            return category;
        }

        public String getCategory_cnt() {
            return category_cnt;
        }
    }

    private class CategoryAdapter extends ArrayAdapter<CategoryPOJO> {

        Context context;
        List<CategoryPOJO> categoryPOJOS;

        CategoryAdapter(Context context) {
            super(context, R.layout.dialog_cancel_order_of_my_order);
            this.context = context;
            categoryPOJOS = new ArrayList<>();
        }

        public void add(CategoryPOJO categoryPOJO) {
            categoryPOJOS.add(categoryPOJO);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            final CategoryPOJO categoryPOJO = categoryPOJOS.get(position);
            convertView = null;
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.category_list_with_cnt, null);

                final TextView priceRange = convertView.findViewById(R.id.checkBoxName);
                final TextView txtCategoryCnt = convertView.findViewById(R.id.txtCategoryCnt);
                priceRange.setText(categoryPOJO.getCategory());
                txtCategoryCnt.setText("( " + categoryPOJO.getCategory_cnt() + " )");
            }
            return convertView;
        }

        @Override
        public int getCount() {
            return categoryPOJOS.size();
        }
    }
}
