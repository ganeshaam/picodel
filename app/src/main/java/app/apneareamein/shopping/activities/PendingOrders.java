package app.apneareamein.shopping.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class PendingOrders extends AppCompatActivity {

    private CoordinatorLayout pendingOrderLayout;
    private ManageOrderCardAdapter manageOrderCardAdapter;
    private TextView txtMessage;
    private MenuItem search;
    private List<Card> innerMovies;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_orders);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_pending_orders);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pendingOrderLayout = findViewById(R.id.pendingOrderLayout);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        txtMessage = findViewById(R.id.txtMessage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class ManageOrderCardAdapter extends RecyclerView.Adapter<ManageOrderCardAdapter.ViewHolder> {

        List<Card> mArraylist;
        String searchString = "";

        ManageOrderCardAdapter(List<Card> innerMovies) {
            mArraylist = innerMovies;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_pending_orders, viewGroup, false);
            final ViewHolder viewHolder = new ViewHolder(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemPosition = recyclerView.getChildPosition(v);
                    Card card = mArraylist.get(itemPosition);
                    Intent intent = new Intent(PendingOrders.this, CheckCartDetails.class);
                    intent.putExtra("order_id", card.getOrderId());
                    intent.putExtra("user_id", card.getUserId());
                    intent.putExtra("delivery_date", card.getDelivery_date());
                    intent.putExtra("delivery_time", card.getDelivery_time());
                    startActivity(intent);
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final Card card = mArraylist.get(position);

            viewHolder.tvId.setText("Order id - " + card.getOrderId());
            viewHolder.tvUserName.setText("Name / Mobile no. - " + card.getUserName() + " / " + card.getContactno());
            viewHolder.tvUserName.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("MissingPermission")
                @Override
                public void onClick(View view) {
                    String myPhoneNo = viewHolder.tvUserName.getText().toString();

                    //Use this to extract the digits
                    myPhoneNo = myPhoneNo.replaceAll("[^0-9]", ""); //Now only 244892501 remains

                    //Start the call
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + myPhoneNo));
                    startActivity(intent);
                }

            });
            viewHolder.tvTotalAmt.setText("Total Amt - " + "₹ " + card.getTotalAmt() + "/-");
            viewHolder.txtAddress.setText("Delivery Address - " + card.getAddress());
            viewHolder.txtDeliveryOrderDate.setText(card.getDelivery_date());
            viewHolder.txtDeliveryOrderTime.setText(card.getDelivery_time());
        }

        @Override
        public int getItemCount() {
            return mArraylist.size();
        }

        void setFilter(List<Card> countryModels, String searchString) {
            this.searchString = searchString;
            mArraylist = new ArrayList<>();
            mArraylist.addAll(countryModels);
            notifyDataSetChanged();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView tvId, tvUserName, tvTotalAmt, txtDeliveryOrderDate, txtDeliveryOrderTime, txtAddress;

            ViewHolder(View itemView) {
                super(itemView);

                tvId = itemView.findViewById(R.id.txtUserId);
                tvUserName = itemView.findViewById(R.id.txtUserName);
                tvTotalAmt = itemView.findViewById(R.id.txtTotalAmt);
                txtDeliveryOrderDate = itemView.findViewById(R.id.txtDeliveryOrderDate);
                txtDeliveryOrderTime = itemView.findViewById(R.id.txtDeliveryOrderTime);
                txtAddress = itemView.findViewById(R.id.txtAddress);
            }
        }
    }

    private class Card {
        String orderId;
        String userId;
        String userName;
        String contactno;
        String totalAmt;
        String address;
        String delivery_date;
        String delivery_time;

        Card(String orderId, String userId, String userName, String contactno,
             String totalAmt, String address, String delivery_date, String delivery_time) {
            this.orderId = orderId;
            this.userId = userId;
            this.userName = userName;
            this.contactno = contactno;
            this.totalAmt = totalAmt;
            this.address = address;
            this.delivery_date = delivery_date;
            this.delivery_time = delivery_time;
        }

        public String getUserId() {
            return userId;
        }

        public String getContactno() {
            return contactno;
        }

        public String getOrderId() {
            return orderId;
        }

        public String getUserName() {
            return userName;
        }

        public String getTotalAmt() {
            return totalAmt;
        }

        public String getAddress() {
            return address;
        }

        public String getDelivery_date() {
            return delivery_date;
        }

        public String getDelivery_time() {
            return delivery_time;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search, menu);

        search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setQueryHint("Search by Order ID / Name");
        search(searchView);

        MenuItemCompat.setOnActionExpandListener(search,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        manageOrderCardAdapter.setFilter(innerMovies, "");
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
        return true;
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<Card> filteredModelList = filter(innerMovies, newText);
                manageOrderCardAdapter.setFilter(filteredModelList, newText);
                return true;
            }
        });
    }

    private List<Card> filter(List<Card> models, String query) {
        query = query.toLowerCase();
        final List<Card> filteredModelList = new ArrayList<>();
        for (Card model : models) {
            final String text = model.getOrderId();
            final String text1 = model.getUserName().toLowerCase();

            if (text.toLowerCase().contains(query)) {
                filteredModelList.add(model);
            } else if (text1.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestForManageOrders();
    }

    private void requestForManageOrders() { //TODO Server method here
        if (Connectivity.isConnected(PendingOrders.this)) {
            final GateWay gateWay = new GateWay(PendingOrders.this);
            gateWay.progressDialogStart();

            innerMovies = new ArrayList<>();

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlManageOrders,  null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            txtMessage.setVisibility(View.VISIBLE);
                            search.setVisible(false);
                        } else {
                            txtMessage.setVisibility(View.GONE);
                            search.setVisible(true);
                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = mainClassificationJsonArray.getJSONObject(i);

                                innerMovies.add(new Card(jSonClassificationData.getString("order_id"), jSonClassificationData.getString("user_id"),
                                        jSonClassificationData.getString("user_name"), jSonClassificationData.getString("contactno"),
                                        jSonClassificationData.getString("total_amt"), jSonClassificationData.getString("address"),
                                        jSonClassificationData.getString("delivery_date"), jSonClassificationData.getString("delivery_time")));
                                manageOrderCardAdapter = new ManageOrderCardAdapter(innerMovies);
                                recyclerView.setAdapter(manageOrderCardAdapter);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(PendingOrders.this);
            gateWay.displaySnackBar(pendingOrderLayout); //TODO SnackBar method here
        }
    }
}
