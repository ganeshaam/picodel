package app.apneareamein.shopping.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.ShopListAdapter;
import app.apneareamein.shopping.model.ShopModel;
import app.apneareamein.shopping.utils.MySingleton;

public class ShopListActivity extends AppCompatActivity {

    RecyclerView recycler_view_shops;
    TextView txtNoConnection;
    ShopListAdapter shopListAdapter;
    ArrayList<ShopModel> orderArrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoplist);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recycler_view_shops = findViewById(R.id.recycler_view_shops);
        txtNoConnection = findViewById(R.id.txtNoConnection);

        orderArrayList = new ArrayList<>();

        recycler_view_shops.setNestedScrollingEnabled(false);
        recycler_view_shops.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ShopListActivity.this);
        recycler_view_shops.setLayoutManager(linearLayoutManager);
    // method to call shoplist api
            Log.e("Test Commit_08-12-2020","ok");
        getshoplist();

    }

    public void getshoplist(){
        orderArrayList.clear();

        ShopModel model0 = new ShopModel();
        model0.setShop_id("00");
        model0.setSellername("Any Seller");
        model0.setShop_name("Any Shop");
        model0.setArea("Any Area");
        orderArrayList.add(model0);
        //arrayshoplist.add("Any Shop");

        String targetUrlShopList = null;
        try {
            targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/nearbyShopList.php?latitude="+"18.5726054"+"&longitude="+"73.8782079"+"&sessionMainCat="+ URLEncoder.encode("Grocery","utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+orderId+"&shopListType="+shopListType;

        Log.d("targetUrlShopList",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ShopListRes",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        //progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            boolean s_status = jsonObject.getBoolean("s_status");



                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        ShopModel model = new ShopModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setContact(olderlist.getString("contact"));
                                        model.setEmail(olderlist.getString("email"));
                                        model.setAdmin_type(olderlist.getString("admin_type"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setRecharge_amount(olderlist.getString("recharge_amount"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_category(olderlist.getString("shop_category"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setPreferredDeliveryAreaKm(olderlist.getString("preferred_delivery_area_km"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        orderArrayList.add(model);
                                        //arrayshoplist.add(olderlist.getString("shop_name"));
                                    }
                                    Log.e("ShopListSize:",""+orderArrayList.size());
                                    shopListAdapter = new ShopListAdapter(orderArrayList,ShopListActivity.this);
                                    recycler_view_shops.setAdapter(shopListAdapter);
                                    shopListAdapter.notifyDataSetChanged();
                                }else {

                                }
                            }else if(strStatus=false){
                                /*tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");*/

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(ShopListActivity.this).addToRequestQueue(orderListRequest);

    }

}
