package app.apneareamein.shopping.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class ContinueShopingActivity extends AppCompatActivity {

    Button btnShopNow,btnOffers;
    TextView tvTitle;
    WebView wv_scratchcard;
    ImageView imgView;
    String scratch_heading = "Get Scratch Card Up to INR ";
    public static final String MY_PREFS_NAME = "PICoDEL";
    String base_url = "https://www.picodel.com/onerupee",order_id,button_name,offer_status,v_city="Pune";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_continue_shoping);

        btnShopNow = findViewById(R.id.btnShopNow);
        btnOffers = findViewById(R.id.btnOffers);
        imgView = findViewById(R.id.imgView);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_continue_shop);
        wv_scratchcard = findViewById(R.id.wv_scratchcard);

       /* setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       */

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        v_city = prefs.getString("v_city", "NA");

        getScratch_amount();
        getbannerImages();

        btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContinueShopingActivity.this, BaseActivity.class);
                intent.putExtra("tag", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(base_url.isEmpty()){

                }else {
                    //https://www.picodel.com/onerupee/1180/7987
                    SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                    String user_id = prefs.getString("user_id", "");
                    // find MenuItem you want to change

                    String param1 = "/" + user_id + "/" + order_id;
                    Uri webpage = Uri.parse(base_url + param1);
                    Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                    startActivity(myIntent);
                    Log.e("drawer_url:", "" + base_url + param1);
                }
            }
        });
    }


    private void getScratch_amount() { //TODO Server method here
        if (Connectivity.isConnected(ContinueShopingActivity.this)) {
            GateWay gateWay = new GateWay(ContinueShopingActivity.this);

            String email = gateWay.getUserEmail();
            String user_contact = gateWay.getContact();

            JSONObject params = new JSONObject();
            try {
                params.put("email", email);
                params.put("user_contact", user_contact);
                params.put("order_id", "");
                params.put("order_amount", "");
                Log.d("scratch_get", ";params: " + params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.get_scratch_amount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("REG", "onResponse: " + response.toString());
                        String result = response.getString("defalut_max");
                        base_url = response.getString("base_url");
                        order_id = response.getString("order_id");
                        button_name = response.getString("button_name");
                        offer_status = response.getString("offer_status");
                        btnOffers.setText(button_name);

                        if(offer_status.equalsIgnoreCase("active")){
                            btnOffers.setVisibility(View.VISIBLE);
                        }else {
                            btnOffers.setVisibility(View.GONE);
                        }

                        wv_scratchcard.loadData(result, "text/html", "UTF-8");

                        /*if (result.equals("success")) {
                            Toast.makeText(Scratched_Card_Activity.this, "Scratch is Not Applied", Toast.LENGTH_LONG).show();

                        }*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    Log.d("SCRATCHED", "onErr: " + error.toString());
                    GateWay gateWay = new GateWay(ContinueShopingActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(ContinueShopingActivity.this, "Please check Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


    private void getbannerImages() { //TODO Server method here
        if (Connectivity.isConnected(ContinueShopingActivity.this)) { // Internet connection is not present, Ask user to connect to Internet

            //simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {

                params.put("activity_name", "BrowseByCategory");//BrowseByCategory
                params.put("city", v_city);

                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlgetActivityBanners, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("BannersRes",""+response.toString());

                    if (response.isNull("posts")) {
                        Toast.makeText(ContinueShopingActivity.this,"No Banner Found",Toast.LENGTH_LONG).show();
                        //simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        // gateWay.progressDialogStop();
                        try {
                            JSONArray verificationCodeJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < verificationCodeJsonArray.length(); i++) {
                                JSONObject jSonVCodeData = verificationCodeJsonArray.getJSONObject(i);

                                String banner_url = jSonVCodeData.getString("banner_url");
                                Picasso.with(ContinueShopingActivity.this)
                                        .load(banner_url)
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(imgView);

                                Log.e("Res_banner_url:",""+banner_url);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {

        }
    }


}
