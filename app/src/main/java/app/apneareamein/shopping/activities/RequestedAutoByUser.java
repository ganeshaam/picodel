package app.apneareamein.shopping.activities;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.GateWay;

public class RequestedAutoByUser extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    CardAdapter cardAdapter;
    TextView txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requested_auto_by_user);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_requested_auto_by_user);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtMessage = findViewById(R.id.txtMessage);

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        getAutoDataFromServer();
    }

    private void getAutoDataFromServer() {
        cardAdapter = new CardAdapter();

        final GateWay gateWay = new GateWay(RequestedAutoByUser.this);
        gateWay.progressDialogStart();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("city", gateWay.getCity());
            jsonObject.put("area", gateWay.getArea());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetAutoDataFromServer, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                gateWay.progressDialogStop();
                try {
                    if (!response.isNull("posts")) {
                        txtMessage.setVisibility(View.GONE);
                        JSONArray jsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            POJOClass pojoClass = new POJOClass(jsonObject1.getString("user_id"),
                                    jsonObject1.getString("user_name"),
                                    jsonObject1.getString("from_point"),
                                    jsonObject1.getString("to_point"),
                                    jsonObject1.getString("type"),
                                    jsonObject1.getString("time"));

                            cardAdapter.add(pojoClass);
                        }
                        mRecyclerView.setAdapter(cardAdapter);
                    } else {
                        txtMessage.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                gateWay.progressDialogStop();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.IMMEDIATE);
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class POJOClass {

        String user_id;
        String user_name;
        String from_point;
        String to_point;
        String type;
        String time;

        POJOClass(String user_id, String user_name, String from_point, String to_point, String type, String time) {
            this.user_id = user_id;
            this.user_name = user_name;
            this.from_point = from_point;
            this.to_point = to_point;
            this.type = type;
            this.time = time;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public String getFrom_point() {
            return from_point;
        }

        public String getTo_point() {
            return to_point;
        }

        public String getType() {
            return type;
        }

        public String getTime() {
            return time;
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<AutoViewHolder> {
        private List<POJOClass> pojoClasses = new ArrayList<>();

        public void add(POJOClass pojoClass) {
            pojoClasses.add(pojoClass);
        }

        @NonNull
        @Override
        public AutoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_auto_list, viewGroup, false);
            return new AutoViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull AutoViewHolder holder, int position) {
            POJOClass pojoClass = pojoClasses.get(position);

            holder.txtUserName.setText("User Name - " + pojoClass.getUser_name());
            holder.txtFromPoint.setText("From Point - " + pojoClass.getFrom_point());
            holder.txtToPoint.setText("To Point - " + pojoClass.getTo_point());
            holder.txtType.setText("Type - " + pojoClass.getType());
            holder.txtTime.setText("& Time is - " + pojoClass.getTime());
        }

        @Override
        public int getItemCount() {
            return pojoClasses.size();
        }
    }

    private class AutoViewHolder extends RecyclerView.ViewHolder {

        TextView txtUserName, txtFromPoint, txtToPoint, txtType, txtTime;

        AutoViewHolder(View itemView) {
            super(itemView);

            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtFromPoint = itemView.findViewById(R.id.txtFromPoint);
            txtToPoint = itemView.findViewById(R.id.txtToPoint);
            txtType = itemView.findViewById(R.id.txtType);
            txtTime = itemView.findViewById(R.id.txtTime);
        }
    }
}
