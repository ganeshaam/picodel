package app.apneareamein.shopping.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.ReferralPointsAdaptor;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.ReferPoints;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import dmax.dialog.SpotsDialog;

public class RefererPoints extends AppCompatActivity {


    RecyclerView rv_referral_points;
    ReferralPointsAdaptor referralPointsAdaptor;
    ArrayList<ReferPoints> ReferPointsList ;
    LinearLayoutManager layoutManager;
    GateWay gateWay;
    android.app.AlertDialog progressDialog2;
    public static final String MY_PREFS_NAME = "PICoDEL";
    TextView tv_not;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referer_points);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText("PICODEL Points");


        rv_referral_points = findViewById(R.id.rv_referral_points);
        tv_not = findViewById(R.id.tv_not);
        progressDialog2 = new SpotsDialog.Builder().setContext(this).build();

        rv_referral_points.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this,2);
        rv_referral_points.setLayoutManager(layoutManager);
       // rv_referral_points.setItemAnimator(new DefaultItemAnimator());
        ReferPointsList = new ArrayList<>();
        referralPointsAdaptor = new ReferralPointsAdaptor(ReferPointsList,this);
        rv_referral_points.setAdapter(referralPointsAdaptor);

        getReferralPoints();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getReferralPoints() { //TODO Server method here
        if (Connectivity.isConnected(RefererPoints.this)) {
            // Internet connection is not present, Ask user to connect to Internet
            gateWay = new GateWay(RefererPoints.this);
            progressDialog2.show();

            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            String user_id = prefs.getString("user_id", "");

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("user_id", user_id);
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlrefgetPoints, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("ReferInsertRes", "" + response.toString());
                    progressDialog2.dismiss();
                    if (response.isNull("posts")) {

                       // progressDialog2.dismiss();
                    }else{


                        try {
                            JSONArray postsJsonArray = response.getJSONArray("posts");

                            Log.e("points_RS:",""+postsJsonArray.toString());

                            for (int i = 0; i < postsJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = postsJsonArray.getJSONObject(i);

                                ReferPoints points = new ReferPoints();
                                points.setAmount(jSonClassificationData.getString("amount"));
                                points.setBenificial_type(jSonClassificationData.getString("benificial_type"));
                                points.setContact(jSonClassificationData.getString("contact"));//contact
                                points.setCreated_at(jSonClassificationData.getString("created_at"));//created_at
                                points.setValidity(jSonClassificationData.getString("validity"));//validity
                                points.setOrder_id(jSonClassificationData.getString("order_id"));//order_id
                                if(i==0){
                                    tv_not.setText(jSonClassificationData.getString("note"));
                                }
                                ReferPointsList.add(points);
                            }
                            referralPointsAdaptor.notifyDataSetChanged();
                        }catch (Exception e){

                        }



                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    progressDialog2.dismiss();
                    GateWay gateWay = new GateWay(RefererPoints.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
            Log.e("requesturl:",""+request.toString());
        } else {
            Toast.makeText(this,"Please check the Internet Connection",Toast.LENGTH_LONG).show();
        }
    }
}
