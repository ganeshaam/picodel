package app.apneareamein.shopping.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.ShopsCategoryAdapter;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.ShopModel;
import app.apneareamein.shopping.utils.MySingleton;

public class ShopCategoryActivity extends AppCompatActivity {

    RecyclerView recycler_view_shops_category;
    TextView txtNoConnection;
    ShopsCategoryAdapter shopsCategoryAdapter;
    ArrayList<ShopModel> shopCategoryModelArrayList;
    Context mContext;
    ProgressBar simpleProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_category);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvTitle.setText("Shop Category");

        mContext = ShopCategoryActivity.this;
        simpleProgressBar = findViewById(R.id.simpleProgressBar);
        recycler_view_shops_category = findViewById(R.id.recycler_view_shops_category);

        shopCategoryModelArrayList = new ArrayList<>();

        recycler_view_shops_category.setNestedScrollingEnabled(false);
        recycler_view_shops_category.setHasFixedSize(true);
        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
         GridLayoutManager linearLayoutManager = new GridLayoutManager(mContext,2);
        recycler_view_shops_category.setLayoutManager(linearLayoutManager);
        shopsCategoryAdapter = new ShopsCategoryAdapter(shopCategoryModelArrayList,mContext);
        recycler_view_shops_category.setAdapter(shopsCategoryAdapter);

        shopsCategoryList();
    }


    public void shopsCategoryList(){
        shopCategoryModelArrayList.clear();
        simpleProgressBar.setVisibility(View.VISIBLE);

        StringRequest shopCategoryList = new StringRequest(Request.Method.POST,
                ApplicationUrlAndConstants.urlGetBannerShopCategory,
                new Response.Listener<String>(){

                    @Override
                    public void onResponse(String response) {
                        try {
                            simpleProgressBar.setVisibility(View.GONE);
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if(status){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    for(int i=0; i<jsonArray.length();i++){
                                        JSONObject shopCategoryList = jsonArray.getJSONObject(i);
                                        ShopModel shopCategoryModel = new ShopModel();
                                        shopCategoryModel.setShopCategoryImage(shopCategoryList.getString("slider_image"));
                                        shopCategoryModel.setShopCategoryName(shopCategoryList.getString("title"));
                                        shopCategoryModelArrayList.add(shopCategoryModel);
                                    }
                                    shopsCategoryAdapter.notifyDataSetChanged();
                                }else {

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
         MySingleton.getInstance(mContext).addToRequestQueue(shopCategoryList);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}