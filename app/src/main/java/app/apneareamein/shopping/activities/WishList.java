package app.apneareamein.shopping.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class WishList extends AppCompatActivity {

    private String pId, strContact, strEmail;
    private RecyclerView mRecyclerView;
    private CoordinatorLayout wishListMainLayout;
    private CardAdapter cardAdapter;
    private RelativeLayout relativeLayoutEmptyWishList;
    private Button btnShopNow;
    private MenuItem item;
    private LinearLayout Main_Layout_NoInternet;
    private DialogPlus dialogPlus;
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    private TextView txtNoConnection;
    private ProgressBar simpleProgressBar;
    public static final String MY_PREFS_NAME = "PICoDEL";
    private String Zone_Area="",sessionMainCat="";

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                item.setVisible(false);
                relativeLayoutEmptyWishList.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {

                Main_Layout_NoInternet.setVisibility(View.GONE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);

                fetch_wishlist_item();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        myReceiver = new Network_Change_Receiver();
        GateWay gateWay = new GateWay(this);
        strContact = gateWay.getContact();
        strEmail = gateWay.getUserEmail();

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_wish_list);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtNoConnection = findViewById(R.id.txtNoConnection);
        relativeLayoutEmptyWishList = findViewById(R.id.relativeLayoutEmptyWishList);
        wishListMainLayout = findViewById(R.id.wishListMainLayout);
        btnShopNow = findViewById(R.id.btnShopNow);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");
        sessionMainCat = prefs.getString("sessionMainCat", "");

        btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(WishList.this)) { // Internet connection is not present, Ask user to connect to Internet
                    Intent intent = new Intent(WishList.this, BaseActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    GateWay gateWay1 = new GateWay(WishList.this);
                    gateWay1.displaySnackBar(wishListMainLayout);
                }
            }
        });


        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

    }

    private void fetch_wishlist_item() { //TODO Server method here
        if (Connectivity.isConnected(WishList.this)) {
            cardAdapter = new CardAdapter();

            final GateWay gateWay = new GateWay(WishList.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetMyWishListItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            item.setVisible(false);
                            mRecyclerView.setVisibility(View.GONE);
                            relativeLayoutEmptyWishList.setVisibility(View.VISIBLE);
                        } else {
                            relativeLayoutEmptyWishList.setVisibility(View.GONE);
                            item.setVisible(true);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            Log.e("WisList_Response:",""+response.toString());
                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");

                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jsonCart = mainClassificationJsonArray.getJSONObject(i);

                                Movie movie = new Movie(jsonCart.getString("shop_id"),
                                        jsonCart.getString("shop_name"),
                                        jsonCart.getString("product_id"),
                                        jsonCart.getString("product_name"),
                                        jsonCart.getDouble("product_price"),
                                        jsonCart.getString("product_image"),
                                        jsonCart.getString("product_qty"),
                                        jsonCart.getString("available_product_quantity"),
                                        jsonCart.getString("product_size"),
                                        jsonCart.getString("p_name"),
                                        jsonCart.getString("product_brand"),
                                        jsonCart.getString("product_maincat"),
                                        jsonCart.getString("cart_pstatus"));

                                cardAdapter.add(movie);
                            }
                            mRecyclerView.setAdapter(cardAdapter);
                        }
                        cardAdapter.notifyDataSetChanged();
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(WishList.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_my_wishlist, menu);
        item = menu.findItem(R.id.action_empty_wishlist);
        MenuItem search = menu.findItem(R.id.search);

        search.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(WishList.this, MasterSearch.class));
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.action_empty_wishlist:
                EmptyWishlistAlertDialog();
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void EmptyWishlistAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WishList.this);
        builder.setTitle("PICODEL Online Shopping");
        builder.setMessage("Remove all the shortlist items?")
                .setCancelable(false)
                .setPositiveButton("EMPTY WISHLIST", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(WishList.this)) {
                            deleteAllProductFromWishlist();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            GateWay gateWay = new GateWay(WishList.this);
                            gateWay.displaySnackBar(wishListMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteAllProductFromWishlist() { //TODO Server method here
        final GateWay gateWay = new GateWay(WishList.this);
        //gateWay.progressDialogStart();
        simpleProgressBar.setVisibility(View.VISIBLE);

        JSONObject params = new JSONObject();
        try {
            params.put("contactNo", strContact);
            params.put("email", strEmail);
            params.put("tag", "delete_all_items");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteMyWishListItems, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (response.isNull("posts")) {
                } else {
                    item.setVisible(false);

                    DBHelper db = new DBHelper(WishList.this);
                    db.deleteOnlyWishListTable();

                    mRecyclerView.setVisibility(View.GONE);
                    relativeLayoutEmptyWishList.setVisibility(View.VISIBLE);

                    btnShopNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Connectivity.isConnected(WishList.this)) { // Internet connection is not present, Ask user to connect to Internet
                                Intent intent = new Intent(WishList.this, BaseActivity.class);
                                intent.putExtra("tag", "");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                gateWay.displaySnackBar(wishListMainLayout);
                            }
                        }
                    });
                }
                //gateWay.progressDialogStop();
                simpleProgressBar.setVisibility(View.INVISIBLE);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //gateWay.progressDialogStop();
                simpleProgressBar.setVisibility(View.INVISIBLE);

                error.printStackTrace();

                GateWay gateWay = new GateWay(WishList.this);
                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder1> {

        private List<Movie> mArraylist = new ArrayList<>();

        public CardAdapter() {
        }

        @NonNull
        @Override
        public ViewHolder1 onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_wish_list, viewGroup, false);
            final ViewHolder1 viewHolder = new ViewHolder1(v);


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(WishList.this)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = mRecyclerView.getChildAdapterPosition(v);
                            Movie movie = mArraylist.get(itemPosition);

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on view & product is: " + movie.getProductName(), WishList.this);*/

                            Intent intent = new Intent(WishList.this, SingleProductInformation.class);
                            intent.putExtra("product_name", movie.getP_Name());
                            intent.putExtra("shop_id", movie.getShop_id());
                            startActivity(intent);
                        } else {
                            GateWay gateWay = new GateWay(WishList.this);
                            gateWay.displaySnackBar(wishListMainLayout);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final CardAdapter.ViewHolder1 viewHolder, final int position) {
            final DBHelper db = new DBHelper(WishList.this);
            final Movie movie = mArraylist.get(position);

            //TODO here product image setup to glide
            /*Glide.with(WishList.this).load(movie.getProductImg())
                    .thumbnail(Glide.with(WishList.this).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.productImg);*/

            Picasso.with(WishList.this).load(movie.getProductImg())
                    .error(R.drawable.ic_app_transparent)
                    .placeholder(R.drawable.ic_app_transparent)
                    .into(viewHolder.productImg);

            viewHolder.productImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(WishList.this)) { // Internet connection is not present, Ask user to connect to Internet
                        dialogPlus = DialogPlus.newDialog(WishList.this)
                                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.image_pop_up))
                                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .create();

                        ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                        //TODO here product image setup to glide when user select different product variant from spinner
                       /* Glide.with(WishList.this).load(movie.getProductImg())
                                .thumbnail(Glide.with(WishList.this).load(R.drawable.loading))
                                .error(R.drawable.ic_app_transparent)
                                .fitCenter()
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(alertProductImage1);*/
                        Picasso.with(WishList.this).load(movie.getProductImg())
                                .error(R.drawable.ic_app_transparent)
                                .into(alertProductImage1);

                        dialogPlus.show();
                    } else {
                        GateWay gateWay = new GateWay(WishList.this);
                        gateWay.displaySnackBar(wishListMainLayout);
                    }
                }
            });
            viewHolder.tvProductName.setText(movie.getProductName());
            //viewHolder.tvSellerName.setText(movie.getShopName());
            viewHolder.tvSellerName.setText(movie.getProduct_brand());
            viewHolder.tvPrice.setText("" + movie.getPrice());

            HashMap AddToCartInfo = db.getCartDetails(movie.getProductId());
            String btnCheckStatus = (String) AddToCartInfo.get("new_pid");
            String server_cart_status = movie.getCart_pstatus();
            //viewHolder.layout_action1.setBackground(getResources().getDrawable(R.drawable.button_yellow));

            if (server_cart_status.equalsIgnoreCase("0")) {
                viewHolder.btnAddToCart.setText("Add to Cart");
                //viewHolder.layout_action2.setBackground(getResources().getDrawable(R.drawable.button_border));
            } else {
                viewHolder.btnAddToCart.setText("Go to Cart");
               //viewHolder.layout_action2.setBackground(getResources().getDrawable(R.drawable.button_green));
            }

            String QTY = movie.getStr_AvailableProductQuantity();
            if (QTY.equals("0")) {
                viewHolder.btnAddToCart.setVisibility(View.GONE);
                viewHolder.imageView.setVisibility(View.GONE);
                viewHolder.imgOutOfStock.setVisibility(View.VISIBLE);

                //TODO here out of stock GIF image setup to glide
                /*Glide.with(WishList.this)
                        .load(R.drawable.outofstock)
                        .error(R.drawable.icerror_outofstock)
                        .into(viewHolder.imgOutOfStock);*/
                Picasso.with(WishList.this)
                        .load(R.drawable.outofstock)
                        .error(R.drawable.icerror_outofstock)
                        .into(viewHolder.imgOutOfStock);
            } else {
                viewHolder.imgOutOfStock.setVisibility(View.GONE);
            }

            viewHolder.layout_action1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(WishList.this)) {
                        ViewHolder1 holder = (ViewHolder1) (v.getTag());
                        int pos = holder.getAdapterPosition();
                        mArraylist.remove(pos);
                        notifyItemRemoved(pos);

                        pId = movie.getProductId();

                        deleteoneProductFromWishlist();
                    } else {
                        GateWay gateWay = new GateWay(WishList.this);
                        gateWay.displaySnackBar(wishListMainLayout);
                    }
                }

                private void deleteoneProductFromWishlist() { //TODO Server method here
                    JSONObject params = new JSONObject();
                    try {
                        params.put("contactNo", strContact);
                        params.put("email", strEmail);
                        params.put("product_id", pId);
                        params.put("tag", "delete_one_item");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteMyWishListItems, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            if (response.isNull("posts")) {
                            } else {
                                db.deleteWishListProductItem(pId);

                                int cnt = (int) db.fetchWishListCount();
                                if (cnt == 0 && mArraylist.size() == 0) {
                                    relativeLayoutEmptyWishList.setVisibility(View.VISIBLE);
                                    item.setVisible(false);
                                    btnShopNow.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (Connectivity.isConnected(WishList.this)) { // Internet connection is not present, Ask user to connect to Internet
                                                Intent intent = new Intent(WishList.this, BaseActivity.class);
                                                intent.putExtra("tag", "");
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                GateWay gateWay = new GateWay(WishList.this);
                                                gateWay.displaySnackBar(wishListMainLayout);
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            GateWay gateWay = new GateWay(WishList.this);
                            gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });

            if (viewHolder.imgOutOfStock.getVisibility() == View.VISIBLE) {

            } else {
                viewHolder.layout_action2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Connectivity.isConnected(WishList.this)) { // Internet connection is not present, Ask user to connect to Internet
                            if (viewHolder.btnAddToCart.getText().equals("Go to Cart"))
                            {
                                startActivity(new Intent(WishList.this, AddToCart.class));
                            } else {
                                final ViewHolder1 holder = (ViewHolder1) (v.getTag());
                                int pos = holder.getAdapterPosition();

                                /*DBHelper dbHelper = new DBHelper(WishList.this);
                                HashMap cartInfo;*/
                                pId = movie.getProductId();

                                /*UserTracking UT = new UserTracking(UserTracking.context);
                                UT.user_tracking(class_name + ": clicked on add to cart & product is: " + movie.getProductName(), WishList.this);*/

                                 /*dbHelper.insertProductIntoWishList(pId);
                                  cartInfo = dbHelper.getCartDetails(pId);
                                 String strId = (String) cartInfo.get("new_pid");*/
                                //SearchProducts.addToCartArrayList.add(pId);

                               /*if (strId == null) {
                                    pId = movie.getProductId();
                                    //db.insertCount(pId);

                                    int cnt = (int) db.fetchWishListCount();

                                    if (cnt >= 0) {
                                        movie.setStatus(true);
                                        if (movie.isStatus(true)) {
                                            viewHolder.btnAddToCart.setText("Go to Cart");
                                           // viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                                        }
                                    } else {
                                        movie.setStatus(false);
                                        if (movie.isStatus(false)) {
                                            viewHolder.btnAddToCart.setText("Add to Cart");
                                            //viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                                        }
                                    }
                                } else {
                                    Intent intent = new Intent(WishList.this, AddToCart.class);
                                    startActivity(intent);
                                }*/
                            addCartToServerSide(movie.getpMainCategory(),pId);
                            }
                        } else {
                            GateWay gateWay = new GateWay(WishList.this);
                            gateWay.displaySnackBar(wishListMainLayout);
                        }
                    }

                    private void addCartToServerSide(String product_maincat,final String strId) { //TODO Server method here
                        String Qty = movie.getItemQty();

                        GateWay gateWay = new GateWay(WishList.this);
                        strContact = gateWay.getContact();

                        String selectCondition;
                        if (movie.getProductName().contains("Small")) {
                            selectCondition = "Small";
                        } else if (movie.getProductName().contains("Large")) {
                            selectCondition = "Large";
                        } else if (movie.getProductName().contains("Medium")) {
                            selectCondition = "Medium";
                        } else if (movie.getProductName().contains("Ripe")) {
                            selectCondition = "Ripe";
                        } else if (movie.getProductName().contains("Raw")) {
                            selectCondition = "Raw";
                        } else {
                            selectCondition = "";
                        }

                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", "");
                            params.put("areaname", Zone_Area);
                            params.put("selectedType", selectCondition);
                            params.put("versionCode", ApplicationUrlAndConstants.versionName);
                            params.put("Qty", Qty);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            if(sessionMainCat.equalsIgnoreCase("Both")){
                                params.put("sessionMainCat", sessionMainCat);
                            }else {
                                params.put("sessionMainCat", product_maincat);
                            }

                     /*       params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("selectedType", "");
                            params.put("versionCode", ApplicationUrlAndConstants.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("sessionMainCat", maincategory);*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //urlAddWishListItemToCart
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                             try {
                                String posts = response.getString("posts");
                                 String promotional = response.getString("promotional");
                                 String promotional_message = response.getString("promo_message");
                                 if(promotional.equalsIgnoreCase("promotional")){
                                     openSessionDialog(promotional_message,"promo");
                                 }else if (posts.equals("true")){
                                   /* if (strId == null) {
                                        //forAddToCart.setStatus(false);


                                        DBHelper dbHelper = new DBHelper(WishList.this);
                                        HashMap cartInfo;
                                       String  strId = movie.getProductId();
                                         dbHelper.insertProductIntoWishList(strId);
                                        cartInfo = dbHelper.getCartDetails(strId);
                                        String strId = (String) cartInfo.get("new_pid");
                                        //SearchProducts.addToCartArrayList.add(pId);

                                        if (strId == null) {
                                            strId = movie.getProductId();
                                            db.insertCount(strId);

                                            int cnt = (int) db.fetchWishListCount();

                                            if (cnt >= 0) {
                                                movie.setStatus(true);
                                                if (movie.isStatus(true)) {
                                                    viewHolder.btnAddToCart.setText("Go To Cart");
                                                }
                                            } else {
                                                movie.setStatus(false);
                                                if (movie.isStatus(false)) {
                                                    viewHolder.btnAddToCart.setText("Add To Cart");
                                                }
                                            }
                                        }
                                           // Log.e("CartCount:",""+count);
                                    } else {
                                        Intent intent = new Intent(WishList.this, AddToCart.class);
                                        startActivity(intent);
                                    }*/
                                    Log.d("wishlistTocart",""+posts);
                                    Toast toast = Toast.makeText(WishList.this," adding to cart", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    openSessionDialog(posts,"session");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(WishList.this);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mArraylist.size();
        }


        public void add(Movie movie) {
            mArraylist.add(movie);
        }

        class ViewHolder1 extends RecyclerView.ViewHolder {

            final TextView tvProductName;
            final TextView tvSellerName;
            final TextView tvPrice;
            final TextView removeItem;
            final ImageView productImg;
            final TextView btnAddToCart;
            final ImageView imgOutOfStock;
            final ImageView imageView;
            final LinearLayout layout_action1;
            final LinearLayout layout_action2;


            public ViewHolder1(View itemView) {
                super(itemView);
                tvProductName = itemView.findViewById(R.id.txtProductName);
                tvSellerName = itemView.findViewById(R.id.txtSellerName);
                tvPrice = itemView.findViewById(R.id.txtPrice);
                removeItem = itemView.findViewById(R.id.removeItem);
                productImg = itemView.findViewById(R.id.productImage);
                btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
                imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
                imageView = itemView.findViewById(R.id.imageView);
                layout_action1 = itemView.findViewById(R.id.layout_action1);
                layout_action2 = itemView.findViewById(R.id.layout_action2);
                layout_action2.setTag(this);
                layout_action1.setTag(this);
            }
        }
    }

    public class Movie {

        final String shop_id;
        final String shopName;
        final String productId;
        final String productName;
        final String productImg;
        final String itemQty;
        final String str_AvailableProductQuantity;
        final String product_Size;
        final String p_Name;
        final String product_brand;
        final String cart_pstatus;
        double price;
        int count = 0;
        private boolean status;
        String pMainCategory;


        public Movie(String shop_id, String shopName, String productId, String productName, double price, String productImg, String itemQty, String AvailableProductQuantity, String product_Size, String p_Name,String product_brand,String pMainCategory, String cart_pstatus) {
            this.shop_id = shop_id;
            this.shopName = shopName;
            this.productId = productId;
            this.productName = productName;
            this.price = price;
            this.productImg = productImg;
            this.itemQty = itemQty;
            this.str_AvailableProductQuantity = AvailableProductQuantity;
            this.product_Size = product_Size;
            this.p_Name = p_Name;
            this.product_brand =product_brand;
            this.pMainCategory = pMainCategory;
            this.cart_pstatus = cart_pstatus;
        }

        public String getP_Name() {
            return p_Name;
        }

        public String getProduct_Size() {
            return product_Size;
        }

        public String getShop_id() {
            return shop_id;
        }

        public String getShopName() {
            return shopName;
        }

        public String getProductId() {
            return productId;
        }

        public String getProductImg() {
            return productImg;
        }

        public String getProductName() {
            return productName;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getItemQty() {
            return itemQty;
        }

        public boolean isStatus(boolean check) {
            return status;
        }

        public boolean setStatus(boolean status) {
            this.status = status;
            return status;
        }

        public String getStr_AvailableProductQuantity() {
            return str_AvailableProductQuantity;
        }

        public String getpMainCategory() {
            return pMainCategory;
        }

        public void setpMainCategory(String pMainCategory) {
            this.pMainCategory = pMainCategory;
        }

        public String getProduct_brand() {
            return product_brand;
        }


        public String getCart_pstatus() {
            return cart_pstatus;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        hideKeyboard(WishList.this);
        if (Connectivity.isConnected(WishList.this)) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, WishList.this);*/
        }
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void openSessionDialog(String message, String type) {
        final Dialog dialog = new Dialog(WishList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sessoin);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        TextView tv_clearcart = dialog.findViewById(R.id.tv_clearcart);
        TextView tv_continue = dialog.findViewById(R.id.tv_continue);
        tv_message.setText(message);
        if(type.equalsIgnoreCase("promo")){
            tv_clearcart.setVisibility(View.GONE);
        }
        tv_clearcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void EmptyCartAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WishList.this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(WishList.this)) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            // GateWay gateWay = new GateWay(getActivity());
                            //gateWay.displaySnackBar(homeMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void deleteAllProductFromCartItem() {
        if (Connectivity.isConnected(WishList.this)) {
            final GateWay gateWay = new GateWay(WishList.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(WishList.this);
                        db.deleteOnlyCartTable();

                        //after remove cart items from local and online db
                        //then sync online cart count again
                        SyncData();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(WishList.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }
    //this is for sync the cart count
    private int CartCount;
    private void SyncData() { //TODO Server method here
        if (Connectivity.isConnected(WishList.this)) {
            GateWay gateWay = new GateWay(WishList.this);

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("cartCountRes:",""+response.toString());

                        // updateAddToCartCount(response.getInt("normal_cart_count"));
                        CartCount = response.getInt("normal_cart_count");

                       /* if(CartCount == 0){
                            SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(MasterSearch.this);
                            sharedPreferencesUtils.setCategory("nocat");
                        }*/

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().setPriority(Request.Priority.LOW);
            AppController.getInstance().addToRequestQueue(request);
        }

    }
}
