
package app.apneareamein.shopping.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.RemoteException;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.CategoryAdapter;
import app.apneareamein.shopping.fragments.BrowseByCategory;
import app.apneareamein.shopping.fragments.BrowseByMainCategory;
import app.apneareamein.shopping.fragments.HomePage;
import app.apneareamein.shopping.fragments.MyAccount;
import app.apneareamein.shopping.fragments.NotificationsHome;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.model.CategoryModel;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.MySingleton;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnClickListener {

    public Menu nav_Menu;
    private String class_name = this.getClass().getSimpleName();
    public static BottomNavigationView bottomNavigationView;
    public LinearLayout search_bar;
    public static LinearLayout Main_Layout_NoInternet;
    Handler handler=new Handler();
    Handler home_handler = new Handler();
    boolean counterFlag=true;
    String greetings="",option1,option2,option3,option4,page_url1,page_url2,page_url3,page_url4,home_page_status,home_page_message;
    public static final String MY_PREFS_NAME = "PICoDEL";
    String Sharing_content=""; //"Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 2 hours...\nDownload PICODEL App and enjoy lot more unique features !";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    boolean a = false;
    boolean b = false;
    private TextView tvTitle;
    private Bundle bundle;
    private Fragment fragment = null;
    private GateWay gateWay;
    private Intent intent;
    private boolean backPressedToExitOnce = false;
    private String strName;
    private TextView tvUserName;
    private TextView tvCity;
    private TextView tvArea;
    private DrawerLayout drawer;
    private String TAG, SelectedName, FinalPrice;
    public Toolbar toolbar;
    public AppBarLayout appBarLayout;
    private TextView tvCart;
    public ImageView imgNotify;
    public TextView tvNotification;
    public TextView tv_dyanamic;
    public TextView tvCashBackPoints;
    private Animation in;
    private Animation out;
    private Runnable mFadeOut = new Runnable() {

        @Override
        public void run() {
            imgNotify.startAnimation(out);
        }
    };
    private Handler handler1;
    public static TextView txtNoConnection;
    protected ArrayList<CategoryModel> categoryModelArrayList;
    String visitCount,visitCount2="false";

    private void init() {
        toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        appBarLayout = findViewById(R.id.appbar);

        tvTitle = findViewById(R.id.txtTitle);
        TextView txtSearchTxt = findViewById(R.id.search_text);
        txtNoConnection = findViewById(R.id.txtNoConnection);
        txtSearchTxt.setOnClickListener(this);
        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationAdapter(this));

        BottomNavigationItemView cart = (BottomNavigationItemView) ((BottomNavigationMenuView) bottomNavigationView.getChildAt(0)).getChildAt(2);
        View allBadge = LayoutInflater.from(this).inflate(R.layout.notification_badge, cart, false);
        tvCart = allBadge.findViewById(R.id.badge);
        cart.addView(allBadge);
        tvCart.setVisibility(View.INVISIBLE);

        NestedScrollView nestedScrollView = findViewById(R.id.nested_scroll_view);
        imgNotify = findViewById(R.id.imgNotify);

        /*handler1 = new Handler();
        in = new ScaleAnimation(1.0f, 1.0f, 0.0f, 1.0f, 50f, 100f);
        in.setDuration(3000);

        out = new ScaleAnimation(1.0f, 1.0f, 1.0f, 0.0f, 50f, 100f);
        out.setDuration(3000);
        out.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {
                imgNotify.startAnimation(in);
                handler1.postDelayed(mFadeOut, 3000);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        imgNotify.startAnimation(in);
        handler1.postDelayed(mFadeOut, 4000);*/


        ImageButton btn_Menu = findViewById(R.id.bt_menu);
        btn_Menu.setOnClickListener(this);
        ImageButton bt_toolmenu = findViewById(R.id.bt_toolmenu);
        bt_toolmenu.setOnClickListener(this);
        ImageButton location = findViewById(R.id.location);
        location.setOnClickListener(this);
        search_bar = findViewById(R.id.search_bar);
        nestedScrollView.setOnScrollChangeListener(new nestedScroll(this));
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);

        /*disableShiftMode(bottomNavigationView);*/
        drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        nav_Menu = navigationView.getMenu();
        //this is for by default gone the navigation menu
        //nav_Menu.findItem(R.id.nav_AddShop).setVisible(false);
      /*  nav_Menu.findItem(R.id.nav_GetAuto).setVisible(false);
        nav_Menu.findItem(R.id.nav_Auto).setVisible(false);
        nav_Menu.findItem(R.id.nav_Shopkeeper).setVisible(false);
*/
        TextView navVersion = (TextView) navigationView.getMenu().findItem(R.id.nav_AppVersion).getActionView();
        navVersion.setText("  Current Version     " + ApplicationUrlAndConstants.versionName+"         ");
        navVersion.setGravity(View.TEXT_ALIGNMENT_CENTER);


        View headerView = navigationView.getHeaderView(0);
        tvUserName = headerView.findViewById(R.id.txtUserName);
        tvArea = headerView.findViewById(R.id.txtArea);
        tvCity = headerView.findViewById(R.id.txtCity);
        LinearLayout linMyOrders = headerView.findViewById(R.id.linMyOrders);
        LinearLayout linLogout = headerView.findViewById(R.id.linLogout);
        LinearLayout locationLinearLayout = headerView.findViewById(R.id.locationLinearLayout);
        locationLinearLayout.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);

        tvNotification = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_Notification));
        tvNotification.setGravity(Gravity.CENTER_VERTICAL); //Gravity property aligns the text
        tvNotification.setTypeface(null, Typeface.BOLD);
        tvNotification.setTextColor(getResources().getColor(R.color.red));



               /* nav_Menu.findItem(R.id.nav_option1).setTitle("option1").setVisible(true);
                nav_Menu.findItem(R.id.nav_option2).setTitle("option2");
                nav_Menu.findItem(R.id.nav_option3).setVisible(false);
                nav_Menu.findItem(R.id.nav_option4).setTitle("final");*/



      /*  tvCashBackPoints = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_PromotionalPartner));
        tvCashBackPoints.setGravity(Gravity.CENTER_VERTICAL); //Gravity property aligns the text
        tvCashBackPoints.setTypeface(null, Typeface.BOLD);
        tvCashBackPoints.setTextColor(getResources().getColor(R.color.red));*/

        linMyOrders.setOnClickListener(this);
        linLogout.setOnClickListener(this);

        tvUserName.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               /* MyAccount fragment = new MyAccount();

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.commit();*/
                if (Connectivity.isConnected(BaseActivity.this)) {
                    /* DBHelper db = new DBHelper(BaseActivity.this);
                    int cnt = (int) db.fetchWishListCount();

                   if (cnt == 0) {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.wishListAlertDialog();
                    } else {*/
                        Intent intent = new Intent(BaseActivity.this, MyAccount.class);
                        startActivity(intent);
                    //}
                    check_home_menu(false, true, false, false, "navigation_profile");
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
               /* if (Connectivity.isConnected(BaseActivity.this)) {
                    fragment = new MyAccount();
                    FragmentManager myaccountHomeManager = getSupportFragmentManager();
                    boolean myaccountHomeFragmentPopped = myaccountHomeManager.popBackStackImmediate("addMyAccountHome", 0);
                    if (!myaccountHomeFragmentPopped) { //fragment not in back stack, create it.
                        FragmentTransaction myaccountHomeTransaction = myaccountHomeManager.beginTransaction();
                        myaccountHomeTransaction.replace(R.id.frame, fragment, "MyAccount");
                        myaccountHomeTransaction.addToBackStack("addMyAccountHome");
                        myaccountHomeTransaction.commit();
                    }
                    check_home_menu(false, true, false, false, "navigation_profile");
                } else {txtUserName
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }*/
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
    }
    int CartCount =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        init();

        bundle = getIntent().getExtras();
        if (bundle != null) {
            TAG = bundle.getString("tag");
            SelectedName = bundle.getString("selectedName");
            FinalPrice = bundle.getString("finalPrice");
        } else {
            TAG = "";
        }
        gateWay = new GateWay(this);
        tvCity.setText(gateWay.getCity());
        tvArea.setText(gateWay.getArea());

        strName = gateWay.getUserName();

        //this method is for getting android device basic information
        //deviceInformation();
        StroeDeiviceInfo2();
        SyncData();
        //homepopup();
       //Get Categories dynamically
        //getDrawerOption();
        getDrawerOption2();
        getCategoriesData();
       SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
       String restoredText = prefs.getString("visitCount", null);
       visitCount2 = prefs.getString("visitCount2", "false");
        if (restoredText != null) {
            visitCount = prefs.getString("visitCount", "true");//"No name defined" is the default value.
            if(visitCount.equalsIgnoreCase("false")){
                try {

                    //visitCounter();
                }catch (Exception e){
                    e.getMessage();
                }
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("visitCount", "true");
                editor.apply();
                editor.commit();
            }

            /*Log.e("visitCount2",visitCount2);
            if(visitCount2.equalsIgnoreCase("false")){
                try {
                        homepopup();
                }catch (Exception e){
                    e.getMessage();
                }
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("visitCount2", "true");
                editor.apply();
                editor.commit();
            }*/

        }

        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("sessionMainCat", "Both");
        editor.apply();
        editor.commit();

    }

    private void deviceInformation() { //TODO Server method here
        GateWay gateWay = new GateWay(BaseActivity.this);

        JSONObject params = new JSONObject();
        try {
            params.put("contact", gateWay.getContact());
            params.put("SDK", Build.VERSION.SDK);
            params.put("BOARD", Build.BOARD);
            params.put("BRAND", Build.BRAND);
            params.put("OS_VERSION", Build.VERSION.RELEASE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlStoredDeviceInfo, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.HIGH);
        //AppController.getInstance().addToRequestQueue(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                600000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(request);
    }

    public void changeTitle(String txt) {
        search_bar.setVisibility(View.GONE);//gone
        toolbar.setVisibility(View.VISIBLE);//VI
        //appBarLayout.setVisibility(View.GONE);
        tvTitle.setText(txt);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        UserTracking UT = new UserTracking(UserTracking.context);

        switch (id) {
            case R.id.location:
                /*if (Connectivity.isConnected(BaseActivity.this)) {
                    UT.user_tracking(class_name + ": clicked on change location layout", BaseActivity.this);
                    Intent intent = new Intent(BaseActivity.this, LocationActivity.class);
                    intent.putExtra("tag", "homepage");
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;*/

                if (Connectivity.isConnected(BaseActivity.this)) {
                    /*UT.user_tracking(class_name + ": clicked on master search view", BaseActivity.this);
                    startActivity(new Intent(BaseActivity.this, MasterSearch.class));*/
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            case R.id.locationLinearLayout:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    if (Connectivity.isConnected(BaseActivity.this)) {
                       // UT.user_tracking(class_name + ": clicked on change location navigation layout", BaseActivity.this);
                        Intent intent = new Intent(BaseActivity.this, LocationActivity.class);
                        intent.putExtra("tag", "homepage");
                        startActivity(intent);
                    } else {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }
                }
                break;

            case R.id.search_text:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    //UT.user_tracking(class_name + ": clicked on master search view", BaseActivity.this);
                    startActivity(new Intent(BaseActivity.this, MasterSearch.class));
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            case R.id.bt_menu:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //UT.user_tracking(class_name + ": drawer opened", BaseActivity.this);
                }
                break;

            case R.id.bt_toolmenu:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);

                    /*UT.user_tracking(class_name + ": drawer opened", BaseActivity.this);*/
                }
                break;

            case R.id.linMyOrders:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    if (Connectivity.isConnected(BaseActivity.this)) {
                        Intent intent1 = new Intent(this, MyOrder.class);
                        startActivity(intent1);
                    } else {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }
                }
                break;

            case R.id.linLogout:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    if (Connectivity.isConnected(BaseActivity.this)) {
                        logoutAlertDialog();
                    } else {
                        gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }
                }
                break;

            default:
                break;
        }
    }

    class BottomNavigationAdapter implements BottomNavigationView.OnNavigationItemSelectedListener {
        BaseActivity bottomNavigationLight;

        BottomNavigationAdapter(BaseActivity bottomNavigationLight) {
            this.bottomNavigationLight = bottomNavigationLight;
        }

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.navigation_home:
                    if (Connectivity.isConnected(BaseActivity.this)) {
                        check_home_menu(true, false, false, false, "navigation_home");
                    } else {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }
                    break;
                case R.id.navigation_profile:
                    if (Connectivity.isConnected(BaseActivity.this)) {
                       /* fragment = new MyAccount();
                        FragmentManager myaccountHomeManager = getSupportFragmentManager();
                        boolean myaccountHomeFragmentPopped = myaccountHomeManager.popBackStackImmediate("addMyAccountHome", 0);
                        if (!myaccountHomeFragmentPopped) { //fragment not in back stack, create it.
                            FragmentTransaction myaccountHomeTransaction = myaccountHomeManager.beginTransaction();
                            myaccountHomeTransaction.replace(R.id.frame, fragment, "MyAccount");
                            myaccountHomeTransaction.addToBackStack("addMyAccountHome");
                            myaccountHomeTransaction.commit();
                        }*/
                        if (Connectivity.isConnected(BaseActivity.this)) {
                         /*   DBHelper db = new DBHelper(BaseActivity.this);
                            int cnt = (int) db.fetchWishListCount();
                            if (cnt == 0) {
                                GateWay gateWay = new GateWay(BaseActivity.this);
                                gateWay.wishListAlertDialog();
                            } else {
                                Intent intent = new Intent(BaseActivity.this, MyAccount.class);
                                startActivity(intent);
                            }*/
                            Intent intent = new Intent(BaseActivity.this, MyAccount.class);
                            startActivity(intent);
                        } else {
                            GateWay gateWay = new GateWay(BaseActivity.this);
                            gateWay.displaySnackBar(drawer);
                        }
                        check_home_menu(false, true, false, false, "navigation_profile");
                    } else {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }
                    break;
                case R.id.navigation_cart:
                    check_home_menu(false, false, false, false, "navigation_cart");
                    if (Connectivity.isConnected(BaseActivity.this)) {
                        //DBHelper db = new DBHelper(BaseActivity.this);
                       // int count = SyncData();//(int) db.fetchAddToCartCount();
                        //Log.e("ClickHomeCARTcOUNT:",""+count);

                       // int count = SyncData();
                       //if (SyncData() > 0) {
                           intent = new Intent(BaseActivity.this, AddToCart.class);
                           startActivity(intent);
                        /*} else {
                           gateWay = new GateWay(BaseActivity.this);
                           gateWay.cartAlertDialog();

                        }*/
                    } else {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }
                    break;
                case R.id.navigation_services:
                  /*  if (Connectivity.isConnected(BaseActivity.this)) {
                        fragment = new Services();
                        bundle = new Bundle();
                        bundle.putString("type", "services");
                        fragment.setArguments(bundle);
                        FragmentManager servicesFragmentManager = getSupportFragmentManager();
                        boolean servicesFragmentPopped = servicesFragmentManager.popBackStackImmediate("addService", 0);
                        if (!servicesFragmentPopped) { //fragment not in back stack, create it.
                            FragmentTransaction serviceTransaction = servicesFragmentManager.beginTransaction();
                            serviceTransaction.replace(R.id.frame, fragment, "Service");
                            serviceTransaction.addToBackStack("addService");
                            serviceTransaction.commit();
                        }
                        check_home_menu(false, false, false, true, "navigation_services");
                    } else {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }*/
                    if (Connectivity.isConnected(BaseActivity.this)) {
                        DBHelper db = new DBHelper(BaseActivity.this);
                        int cnt = (int) db.fetchWishListCount();
                        if (cnt == 0) {
                            GateWay gateWay = new GateWay(BaseActivity.this);
                            gateWay.wishListAlertDialog();
                        } else {
                            Intent intent = new Intent(BaseActivity.this, WishList.class);
                            startActivity(intent);
                        }
                    } else {
                        GateWay gateWay = new GateWay(BaseActivity.this);
                        gateWay.displaySnackBar(drawer);
                    }
                    break;

            }
            return true;
        }
    }

    class nestedScroll implements NestedScrollView.OnScrollChangeListener {
        BaseActivity navigationLight;

        nestedScroll(BaseActivity bottomNavigationLight) {
            this.navigationLight = bottomNavigationLight;
        }

        @Override
        public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
            if (scrollY < oldScrollY) {
                //this.navigationLight.m9781b(false);
                //this.navigationLight.m9782c(false);
                //this.navigationLight.m9782d(false);
                //this.navigationLight.m9782e(false);
            }
            if (scrollY > oldScrollY) {
               // this.navigationLight.m9781b(true);
                //this.navigationLight.m9782c(true);
                //this.navigationLight.m9782d(true);
                //this.navigationLight.m9782e(true);
            }
        }
    }

    private void m9781b(boolean z) {
        if (!a || !z) {
            if (a || z) {
                a = z;
                bottomNavigationView.animate().translationY((float) (z ? bottomNavigationView.getHeight() * 2 : 0)).setStartDelay(100).setDuration(300).start();
            }
        }
    }

    private void m9782c(boolean z) {
        if (!b || !z) {
            if (b || z) {
                b = z;
                this.search_bar.animate().translationY((float) (z ? -(this.search_bar.getHeight() * 2) : 0)).setStartDelay(100).setDuration(300).start();
            }
        }
    }

    private void m9782d(boolean z) {
        if (!b || !z) {
            if (b || z) {
                b = z;
                //this.toolbar.animate().translationY((float) (z ? -(this.toolbar.getHeight()) : 0)).setStartDelay(100).setDuration(300).start();
                this.toolbar.animate().translationY((float) (z ? -(this.toolbar.getHeight() * 2) : 0)).setStartDelay(100).setDuration(300).start();
            }
        }
    }
    //appBarLayout
    private void m9782e(boolean z) {
        if (!b || !z) {
            if (b || z) {
                b = z;
                //this.toolbar.animate().translationY((float) (z ? -(this.toolbar.getHeight()) : 0)).setStartDelay(100).setDuration(300).start();
                this.appBarLayout.animate().translationY((float) (z ? -(this.appBarLayout.getHeight() * 2) : 0)).setStartDelay(100).setDuration(300).start();
            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView navigation) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navigation.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //item.setShiftingMode(false);
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    private void fragment_replace() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) { //Handle navigation view item clicks here.
        int id = item.getItemId();
        fragment = null;

        switch (id) {
            case R.id.nav_Browsebycategory:
                if (Connectivity.isConnected(BaseActivity.this)) {
                  /*  fragment = new BrowseByMainCategory();// BrowseByCategory();
                    FragmentManager browseByCatManager = getSupportFragmentManager();
                    boolean browseByCatFragmentPopped = browseByCatManager.popBackStackImmediate("addBrowse", 0);
                    if (!browseByCatFragmentPopped) { //fragment not in back stack, create it.
                        FragmentTransaction browseTransaction = browseByCatManager.beginTransaction();
                        browseTransaction.replace(R.id.frame, fragment, "Browse");
                        browseTransaction.addToBackStack("addBrowse");
                        browseTransaction.commit();
                    }*/
                        Intent intentMainCategory = new Intent(BaseActivity.this,BrowseByMainCategory.class);
                        startActivity(intentMainCategory);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            case R.id.nav_Notification:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    fragment = new NotificationsHome();
                    FragmentManager notificationHomeManager = getSupportFragmentManager();
                    boolean notificationHomeFragmentPopped = notificationHomeManager.popBackStackImmediate("addNotificationHome", 0);
                    if (!notificationHomeFragmentPopped) { //fragment not in back stack, create it.
                        FragmentTransaction notificationHomeTransaction = notificationHomeManager.beginTransaction();
                        notificationHomeTransaction.replace(R.id.frame, fragment, "Notification");
                        notificationHomeTransaction.addToBackStack("addNotificationHome");
                        notificationHomeTransaction.commit();
                    }
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            case R.id.nav_Wishlist:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    DBHelper db = new DBHelper(this);
                    int cnt = (int) db.fetchWishListCount();
                    if (cnt == 0) {
                        GateWay gateWay = new GateWay(this);
                        gateWay.wishListAlertDialog();
                    } else {
                        Intent intent = new Intent(this, WishList.class);
                        startActivity(intent);
                    }
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            /*case R.id.nav_RateApp:
                gotoPlayStore();
                break;*/

            case R.id.nav_ShareApp:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    /*intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, "Hey check out PICODEL Online Shopping app at: https://play.google.com/store/apps/details?id=app.apneareamein.shopping&hl=en");
                    intent.setType("text/plain");
                    startActivity(intent);*/
                    sendReferral(BaseActivity.this);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;


         /*   case R.id.nav_CashBackTerms:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, QuickLinks.class);
                    bundle = new Bundle();
                    bundle.putString("type", "cash_back");
                    bundle.putString("title", "Cashback Terms");
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;*/




            case R.id.nav_Help:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, QuickLinks.class);
                    bundle = new Bundle();
                    bundle.putString("type", "help");
                    bundle.putString("title", "Help Document");
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            /*case R.id.nav_Legal:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, QuickLinks.class);
                    bundle = new Bundle();
                    bundle.putString("type", "terms");
                    bundle.putString("title", "Terms and Conditions");
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;*/

            case R.id.nav_event :
                if (Connectivity.isConnected(BaseActivity.this)) {
                    /*intent = new Intent(BaseActivity.this, EventActivity.class);
                    startActivity(intent);*/

                    intent = new Intent(BaseActivity.this, OffersActivity.class);
                    startActivity(intent);


                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;
            case R.id.nav_option1 :
                if (Connectivity.isConnected(BaseActivity.this)) {
                    /*intent = new Intent(BaseActivity.this, DyanamicActivity.class);
                    intent.putExtra("option","option1");
                    startActivity(intent);*/
                    if(page_url1.isEmpty()){
                        //Toast.makeText(BaseActivity.this,"No Event",Toast.LENGTH_LONG).show();
                    }else {
                        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                        String user_id = prefs.getString("user_id", "");
                        // find MenuItem you want to change
                        MenuItem menuItem = nav_Menu.findItem(R.id.nav_option1);
                        String value = String.valueOf(menuItem.getTitle());
                        String param1="/"+user_id+"/"+value;
                        Uri webpage = Uri.parse(page_url1+param1);
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(myIntent);
                        Log.e("drawer_url:",""+page_url1+param1);
                    }



                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;
            case R.id.nav_option2 :
                if (Connectivity.isConnected(BaseActivity.this)) {

                    if(page_url2.isEmpty()){
                     //Toast.makeText(BaseActivity.this,"No Event",Toast.LENGTH_LONG).show();
                    }else {

                        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                        String user_id = prefs.getString("user_id", "");
                        MenuItem menuItem = nav_Menu.findItem(R.id.nav_option2);
                        String value = String.valueOf(menuItem.getTitle());
                        //String param2="?user_id="+user_id+"&event="+value;
                          String param2="/"+user_id+"/"+value;

                        Uri webpage = Uri.parse(page_url2+param2);
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(myIntent);
                        Log.e("drawer_url2:",""+page_url2+param2);
                    }
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;
            case R.id.nav_option3 :
                if (Connectivity.isConnected(BaseActivity.this)) {
                    /*intent = new Intent(BaseActivity.this, DyanamicActivity.class);
                    intent.putExtra("option","option3");
                    startActivity(intent);*/

                    if(page_url3.isEmpty()){
                        //Toast.makeText(BaseActivity.this,"No Event",Toast.LENGTH_LONG).show();
                    }else {
                        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                        String user_id = prefs.getString("user_id", "");
                        MenuItem menuItem = nav_Menu.findItem(R.id.nav_option3);
                        String value = String.valueOf(menuItem.getTitle());
                        //String param3="?user_id="+user_id+"&event="+value;
                        String param3="/"+user_id+"/"+value;


                        Uri webpage = Uri.parse(page_url3+param3);
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(myIntent);
                    }


                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;
            case R.id.nav_option4 :
                if (Connectivity.isConnected(BaseActivity.this)) {
                    /*intent = new Intent(BaseActivity.this, DyanamicActivity.class);
                    intent.putExtra("option","option4");
                    startActivity(intent);*/

                    if(page_url4.isEmpty()){
                        //Toast.makeText(BaseActivity.this,"No Event",Toast.LENGTH_LONG).show();
                    }else {
                        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                        String user_id = prefs.getString("user_id", "");
                        MenuItem menuItem = nav_Menu.findItem(R.id.nav_option4);
                        String value = String.valueOf(menuItem.getTitle());
                        //String param4="?user_id="+user_id+"&event="+value;
                        String param4="/"+user_id+"/"+value;

                        Uri webpage = Uri.parse(page_url4+param4);
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(myIntent);
                    }

                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            case R.id.nav_AboutUs:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, QuickLinks.class);
                    bundle = new Bundle();
                    bundle.putString("type", "about_us");
                    bundle.putString("title", "About Us");
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;
            case R.id.nav_point: //ref_point
                if (Connectivity.isConnected(BaseActivity.this)) {

                    Intent getPoints = new Intent(BaseActivity.this,RefererPoints.class);
                    startActivity(getPoints);


                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;
            case R.id.nav_feedback:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, FeedbackActivity.class);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

            case R.id.nav_myorder:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, MyRecentOrderActivity.class);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;

                //nav_knonw_from

         /*   case R.id.nav_AddShop:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, AddShop.class);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;*/

          /*  case R.id.nav_GetAuto:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, GetAuto.class);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;*/

           /* case R.id.nav_PromotionalPartner:
                if (tvCashBackPoints.getText().toString().equals("0")) {
                    cashBackPoint("zero_cash_back_points");
                } else {
                    cashBackPoint("cash_back_points");
                }
                break;*/

         /*   case R.id.nav_Auto:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, AutoDashboard.class);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;*/

          /*  case R.id.nav_Shopkeeper:
                if (Connectivity.isConnected(BaseActivity.this)) {
                    intent = new Intent(BaseActivity.this, ShopDashboard.class);
                    startActivity(intent);
                } else {
                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.displaySnackBar(drawer);
                }
                break;*/
        }
        if (fragment != null) {
            fragment_replace();
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void cashBackPoint(String tag) {
        String message;
        String btnMessage;
        if (tag.equals("zero_cash_back_points")) {
            message = "Would you like to add Redeem Cash Back Points?\n" +
                    "Order now and get 100% Cash back for 1st 5 Orders.";
            btnMessage = "Ok";
        } else {
            message = "Would you like to Redeem Cash Back Points?";
            btnMessage = "Yes";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("You have ( " + tvCashBackPoints.getText().toString() + " ) Cash Back Points");
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btnMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (Connectivity.isConnected(BaseActivity.this)) {
                            intent = new Intent(BaseActivity.this, PromotionalPartner.class);
                            intent.putExtra("cash_back_points", tvCashBackPoints.getText().toString());
                            startActivity(intent);
                        } else {
                            GateWay gateWay = new GateWay(BaseActivity.this);
                            gateWay.displaySnackBar(drawer);
                        }
                    }
                }).setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void logoutAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
        builder.setTitle("Logout");
        //builder.setMessage("Logout account will delete all your PICODEL User Login data from this device. Do you want to continue?")
        builder.setMessage(" Are you sure you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DBHelper db = new DBHelper(BaseActivity.this);
                        db.deleteAll2();
                        Intent intent = new Intent(BaseActivity.this, Splash.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void gotoPlayStore() {
        if (Connectivity.isConnected(BaseActivity.this)) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=app.apneareamein.shopping&hl=en"));
            startActivity(intent);
        } else {
            gateWay = new GateWay(BaseActivity.this);
            gateWay.displaySnackBar(drawer);
        }
    }

    public void sendReferral(Context context) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getInvitationMessage());
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "PICODEL");
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, "PICODEL REFERRAL"));
       /* Intent i = new Intent("com.android.vending.INSTALL_REFERRER");
        //Set Package name
        i.setPackage("app.apneareamein.shopping");
        //referrer is a composition of the parameter of the campaing
        i.putExtra("referrer", "OkPICODEL99");
        sendBroadcast(i);*/
    }

    private String getInvitationMessage(){
        //String SUB = "Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 2 hours...\nDownload PICODEL App and enjoy lot more unique features !";
        String playStoreLink = Sharing_content+"\n "+"https://play.google.com/store/apps/details?id=app.apneareamein.shopping&referrer=";
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String user_id = prefs.getString("user_id", "");
        Log.e("user_id",""+user_id);
        return playStoreLink + "PIC"+user_id;//utm_source=
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("BaseActivity", "onResume: "+TAG);
        if (Connectivity.isConnected(BaseActivity.this)) {

            if (TAG.equals("update")) {
                gotoPlayStore();
            } else
                if (TAG.equals("notification_Home")) {
                fragment = new NotificationsHome();
                FragmentManager notificationHomeManager = getSupportFragmentManager();
                boolean notificationHomeFragmentPopped = notificationHomeManager.popBackStackImmediate("addNotificationHome", 0);
                if (!notificationHomeFragmentPopped) { //fragment not in back stack, create it.
                    FragmentTransaction notificationHomeTransaction = notificationHomeManager.beginTransaction();
                    notificationHomeTransaction.replace(R.id.frame, fragment, "Notification");
                    notificationHomeTransaction.addToBackStack("addNotificationHome");
                    notificationHomeTransaction.commit();
                }
            } else
                if (TAG.equals(null)) {
            } else
                {
                try {
                    Log.d("BaseActivity", "backstack count: "+getSupportFragmentManager().getBackStackEntryCount());
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        if (getSupportFragmentManager().findFragmentByTag("BrowseByCategory") != null) {
                            getSupportFragmentManager().popBackStack("BrowseByCategory", 0); // I'm viewing Fragment DataSegregationLevelThree
                        } else if (getSupportFragmentManager().findFragmentByTag("DataSegregationLevelThree") != null) {
                            getSupportFragmentManager().popBackStack("addDataSegregationLevelThree", 0); // I'm viewing Fragment DataSegregationLevelThree
                        } else if (getSupportFragmentManager().findFragmentByTag("Service") != null) {
                            getSupportFragmentManager().popBackStack("addService", 0); // I'm viewing Fragment Services
                        } else if (getSupportFragmentManager().findFragmentByTag("MyAccount") != null) {
                            getSupportFragmentManager().popBackStack("addMyAccount", 0); // I'm viewing Fragment CategoryWiseProducts
                        } else if (getSupportFragmentManager().findFragmentByTag("CategoryWiseProducts") != null) {
                            getSupportFragmentManager().popBackStack("addCategoryWiseProducts", 0); // I'm viewing Fragment CategoryWiseProducts
                        } else if (getSupportFragmentManager().findFragmentByTag("CategoryWiseProductsFromDiscount") != null) {
                            getSupportFragmentManager().popBackStack("addCategoryWiseProductsFromDiscount", 0); // I'm viewing Fragment CategoryWiseProducts
                        } else if (getSupportFragmentManager().findFragmentByTag("Combo") != null) {
                            getSupportFragmentManager().popBackStack("addCombo", 0); // I'm viewing Fragment ComboOffer
                        } else {
                            if (getSupportFragmentManager().findFragmentByTag("BrowseByCategory") != null) {
                                getSupportFragmentManager().popBackStack("BrowseByCategory", 0); // I'm viewing Fragment DataSegregationLevelThree
                            } else if (getSupportFragmentManager().findFragmentByTag("DataSegregationLevelThree") != null) {
                                getSupportFragmentManager().popBackStack("addDataSegregationLevelThree", 0); // I'm viewing Fragment DataSegregationLevelThree
                            } else if (getSupportFragmentManager().findFragmentByTag("Service") != null) {
                                getSupportFragmentManager().popBackStack("addService", 0); // I'm viewing Fragment Services
                            } else if (getSupportFragmentManager().findFragmentByTag("MyAccount") != null) {
                                getSupportFragmentManager().popBackStack("addMyAccount", 0); // I'm viewing Fragment CategoryWiseProducts
                            } else if (getSupportFragmentManager().findFragmentByTag("CategoryWiseProducts") != null) {
                                getSupportFragmentManager().popBackStack("addCategoryWiseProducts", 0); // I'm viewing Fragment CategoryWiseProducts
                            } else if (getSupportFragmentManager().findFragmentByTag("CategoryWiseProductsFromDiscount") != null) {
                                getSupportFragmentManager().popBackStack("addCategoryWiseProductsFromDiscount", 0); // I'm viewing Fragment CategoryWiseProducts
                            } else if (getSupportFragmentManager().findFragmentByTag("Combo") != null) {
                                getSupportFragmentManager().popBackStack("addCombo", 0); // I'm viewing Fragment ComboOffer
                            }
                        }
                    } else {
                        check_home_menu(true, false, false, false, "else ");
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }
        if (strName == null) {
            tvUserName.setText("Welcome");
        } else {
            tvUserName.setText(strName);
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("BaseActivity", "onBackPressed:count "+getSupportFragmentManager().getBackStackEntryCount());
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (drawer.isDrawerOpen(Gravity.START)) {
                drawer.closeDrawer(Gravity.START);
            }

            if (getSupportFragmentManager().findFragmentByTag("BrowseByCategory") != null) {
                getSupportFragmentManager().popBackStack("BrowseByCategory", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment DataSegregationLevelThree
            } else
            if (getSupportFragmentManager().findFragmentByTag("DataSegregationLevelThree") != null) {
                getSupportFragmentManager().popBackStack("addDataSegregationLevelThree", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment DataSegregationLevelThree
            } else if (getSupportFragmentManager().findFragmentByTag("Browse") != null) {
                getSupportFragmentManager().popBackStack("addBrowse", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment BrowseByCategory
                check_home_menu(true, false, false, false, "Browse");
            } else if (getSupportFragmentManager().findFragmentByTag("Service") != null) {
                getSupportFragmentManager().popBackStack("addService", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment Services
                check_home_menu(true, false, false, false, "Service");
            } else if (getSupportFragmentManager().findFragmentByTag("Combo") != null) {
                getSupportFragmentManager().popBackStack("addCombo", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment ComboOffer
                check_home_menu(true, false, false, false, "Combo");
            } else if (getSupportFragmentManager().findFragmentByTag("Notification") != null) {
                getSupportFragmentManager().popBackStack("addNotificationHome", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment ComboOffer
                check_home_menu(true, false, false, false, "Notification");
            } else if (getSupportFragmentManager().findFragmentByTag("MyAddress") != null) {
                getSupportFragmentManager().popBackStack("addMyAddress", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment MyAddress
                check_home_menu(false, true, false, false, "MyAddress");
            } else if (getSupportFragmentManager().findFragmentByTag("CashBackOffer") != null) {
                getSupportFragmentManager().popBackStack("addCashBackOffer", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment CashBackOffer
                check_home_menu(false, true, false, false, "CashBackOffer");
            } else if (getSupportFragmentManager().findFragmentByTag("MyAccount") != null) {
                getSupportFragmentManager().popBackStack("addMyAccountHome", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment MyAccount
                check_home_menu(true, false, false, false, "MyAccount");
            } else if (getSupportFragmentManager().findFragmentByTag("CategoryWiseProducts") != null) {
                getSupportFragmentManager().popBackStack("addCategoryWiseProducts", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment CategoryWiseProducts
            } else if (getSupportFragmentManager().findFragmentByTag("CategoryWiseProductsFromDiscount") != null) {
                getSupportFragmentManager().popBackStack("addCategoryWiseProductsFromDiscount", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment CategoryWiseProducts
            } else if (getSupportFragmentManager().findFragmentByTag("DiscountHome") != null) {
                getSupportFragmentManager().popBackStack("addDiscountHome", FragmentManager.POP_BACK_STACK_INCLUSIVE); // I'm viewing Fragment DiscountHome
                check_home_menu(true, false, false, false, "DiscountHome");
            } else {

                check_home_menu(true, false, false, false, "  super.onBackPressed()");
            }
        } else {
            if (backPressedToExitOnce) {
                updateStatusOfCartDialog("exit_app");
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("visitCount2", "false");
                editor.putString("cartUpdateFlag", "false");
                editor.apply();
                editor.commit();
                super.onBackPressed();
            } else {
                if (drawer.isDrawerOpen(Gravity.START)) {
                    drawer.closeDrawer(Gravity.START);
                } else {
                    this.backPressedToExitOnce = true;
                    Toast toast = Toast.makeText(this, "Hit back again to close the app", Toast.LENGTH_SHORT);
                    toast.show();
                }
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        backPressedToExitOnce = false;
                    }
                }, 2000);
            }
        }
    }

    private void updateStatusOfCartDialog(String tag) {
        GateWay gateWay = new GateWay(BaseActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("tag", tag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdateStatusOfCartDialog, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.IMMEDIATE);
        //AppController.getInstance().addToRequestQueue(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                600000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateAddToCartCount(final int count) {
        if (tvCart == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCart.setVisibility(View.VISIBLE);
                if (count == 0) {
                    tvCart.setText("" + count);
                } else {
                    tvCart.setText("" + count);
                }
            }
        });
    }

    public void check_home_menu(boolean home, boolean profile, boolean cart, boolean services, String s) {
        bottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(home);
        if (home) {
            bottomNavigationView.getMenu().findItem(R.id.navigation_home).setCheckable(home);
            bottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(home);
            try {
                fragment = new HomePage();
                fragment_replace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
        bottomNavigationView.getMenu().findItem(R.id.navigation_profile).setCheckable(profile);
        bottomNavigationView.getMenu().findItem(R.id.navigation_cart).setCheckable(cart);
        bottomNavigationView.getMenu().findItem(R.id.navigation_services).setCheckable(services);
    }

    //show homw dialog
    String title,veg,vegprice,groceries,groceriesprice,Non_veg,Non_veg_price;
    private  void visitCounter(){

        try {
            handler = new Handler();

            handler.postDelayed(new Runnable() {
                public void run() {
                    try {
                        Calendar cal = Calendar.getInstance();

                        int millisecond = cal.get(Calendar.MILLISECOND);
                        int second = cal.get(Calendar.SECOND);
                        int minute = cal.get(Calendar.MINUTE);
                        //12 hour format
                        int hour = cal.get(Calendar.HOUR);
                        //24 hour format
                        int hourofday = cal.get(Calendar.HOUR_OF_DAY);
                        Log.e("currentTime:", "" + hourofday);

                        if (hourofday < 12) {
                            greetings = "Good Morning..!!";
                        } else if (hourofday >= 12 && hourofday <= 16) {

                            greetings = "Good After Noon..!!";
                        } else if (hourofday > 16 && hourofday < 24) {

                            greetings = "Good Evening..!!";
                        } else {
                            greetings = "Hello..!!";
                        }
                    } catch (Exception e) {
                        Log.e("gretingExp:", "" + e.getMessage());
                    }

                    final Dialog dialog = new Dialog(BaseActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_layout_popup20_sec);

                    final TextView tv_dialog_title = dialog.findViewById(R.id.tv_dialog_title);
                    final TextView tv_veg_price = dialog.findViewById(R.id.tv_veg_price);
                    final TextView tv_grocery_price = dialog.findViewById(R.id.tv_grocery_price);
                    final TextView tv_rb_non_veg_price = dialog.findViewById(R.id.tv_non_veg_price);
                    RadioGroup rg = dialog.findViewById(R.id.popup20RG);
                    final  RadioButton button_vg = dialog.findViewById(R.id.rbveg);
                    final  RadioButton button_grocery = dialog.findViewById(R.id.rbgrocery);
                    final  RadioButton button_rb_non_veg = dialog.findViewById(R.id.rb_non_veg);

                    final RecyclerView rv_category = dialog.findViewById(R.id.rv_category);
                    rv_category.setHasFixedSize(true);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(BaseActivity.this);
                    rv_category.setLayoutManager(layoutManager);
                    final CategoryAdapter categoryAdapter = new CategoryAdapter(categoryModelArrayList, BaseActivity.this);
                    rv_category.setAdapter(categoryAdapter);

                    TextView greetingstext = dialog.findViewById(R.id.greetings2);
                    ImageView iv_dialog_close = dialog.findViewById(R.id.iv_dialog_close);


                   // homeDialogContent();
                    if(Connectivity.isConnected(BaseActivity.this)){
                        String urlPlaceOrderLimit = ApplicationUrlAndConstants.urlPlaceOrderLimit+"?flag=home";

                         StringRequest requestPlaceOrderLimit = new StringRequest(Request.Method.GET,
                                urlPlaceOrderLimit,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String status = jsonObject.getString("status");
                                            Log.d("urlPlaceOrderLimit",response);
                                            if(status.equals("true")){
                                                JSONArray jsonArray = jsonObject.getJSONArray("homemessage");
                                                JSONObject objectMessage = jsonArray.getJSONObject(0);
                                                title = objectMessage.getString("title");
                                                Log.d("title",title);
                                                veg = objectMessage.getString("veg");
                                                vegprice = objectMessage.getString("vegprice");
                                                groceries = objectMessage.getString("groceries");
                                                groceriesprice = objectMessage.getString("groceriesprice");
                                                Non_veg = objectMessage.getString("Non_veg");
                                                Non_veg_price = objectMessage.getString("Non_veg_price");
                                                tv_dialog_title.setText(title);
                                                tv_veg_price.setText(vegprice);
                                                tv_grocery_price.setText(groceriesprice);
                                                tv_rb_non_veg_price.setText(Non_veg_price);
                                                button_vg.setText(veg);
                                                button_grocery.setText(groceries);
                                                button_rb_non_veg.setText(Non_veg);

                                            }else {
                                                title="What would you like to order today?";
                                                veg="Fresh Fruits and Vegetables";
                                                vegprice="Checkout For Free Delivery";
                                                groceries="Groceries";
                                                groceriesprice="Checkout For Free Delivery";
                                                tv_dialog_title.setText(title);
                                                tv_veg_price.setText(vegprice);
                                                tv_grocery_price.setText(groceriesprice);
                                                button_vg.setText(veg);
                                                button_grocery.setText(groceries);

                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                });
                        MySingleton.getInstance(BaseActivity.this).addToRequestQueue(requestPlaceOrderLimit);
                    }else{
                        Toast.makeText(BaseActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                    rv_category.addOnItemTouchListener(new RecyclerTouchListener(BaseActivity.this, rv_category, new RecyclerTouchListener.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {

                            if(categoryModelArrayList.get(position).getMaincategory().equalsIgnoreCase("Grocery")||categoryModelArrayList.get(position).getMaincategory().equalsIgnoreCase("Fruits and Veggies")){
                                Toast.makeText(BaseActivity.this, categoryModelArrayList.get(position).getMaincategory(), Toast.LENGTH_LONG).show();
                                Intent bundle = new Intent(BaseActivity.this, BrowseByCategory.class);
                                bundle.putExtra("product_SuperCat", categoryModelArrayList.get(position).getMaincategory());
                                startActivity(bundle);
                                dialog.dismiss();
                            }else if(categoryModelArrayList.get(position).getMaincategory().equalsIgnoreCase("Both")){
                                Toast.makeText(BaseActivity.this, categoryModelArrayList.get(position).getMaincategory(), Toast.LENGTH_LONG).show();
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("sessionMainCat", "Both");
                                editor.apply();
                                editor.commit();
                                dialog.dismiss();

                            }else {

                                Intent intent = new Intent(BaseActivity.this, SearchProducts.class);
                                intent.putExtra("tag", "next");
                                intent.putExtra("selectedName", categoryModelArrayList.get(position).getMaincategory());//categoryModelArrayList.get(position).getMaincategory()//Eggs
                                //intent.putExtra("selectedName", "Non_Veg");
                                startActivity(intent);
                                dialog.dismiss();
                                //Grocery, Fruits and Veggies
                            }
                              //Both

                             //if(categoryModelArrayList.get(position).getMaincategory().equalsIgnoreCase("Both")){
                             //if(categoryModelArrayList.get(position).getMaincategory().equalsIgnoreCase("Eggs Meat and Chicken")){
                                /*Intent intent = new Intent(BaseActivity.this, SearchProducts.class);
                                intent.putExtra("tag", "next");
                                intent.putExtra("selectedName", "Eggs");
                                //intent.putExtra("selectedName", "Non_Veg");
                                startActivity(intent);*/

                                /*SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("sessionMainCat", "Both");
                                editor.apply();
                                editor.commit();
                                dialog.dismiss();*/
                            //}
                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));

                    greetingstext.setText(greetings);
                    iv_dialog_close.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.rbveg:
                                    dialog.dismiss();
                                    Intent bundle = new Intent(BaseActivity.this,BrowseByCategory.class);
                                    bundle.putExtra("product_SuperCat", "Fruits and Veggies");
                                    startActivity(bundle);
                                    break;
                                case R.id.rbgrocery:
                                    dialog.dismiss();
                                    // do operations specific to this selection
                                    Intent bundle2 = new Intent(BaseActivity.this,BrowseByCategory.class);
                                    bundle2.putExtra("product_SuperCat", "Grocery");
                                    startActivity(bundle2);
                                    break;

                                case R.id.rb_non_veg:
                                    dialog.dismiss();
                                    /*Intent intent = new Intent(BaseActivity.this, SearchProducts.class);
                                    intent.putExtra("tag", "next");
                                    intent.putExtra("selectedName", "Eggs");
                                    //intent.putExtra("selectedName", "Non_Veg");
                                    startActivity(intent);*/
                            }
                        }
                    });
                    if(!isFinishing()) {

                        dialog.show();
                    }
                }
            }, 5000); //5000); //10000
        }catch (Exception e){
            e.getMessage();
        }

    }

    private  void homepopup(){

        home_handler = new Handler();

        home_handler.postDelayed(new Runnable() {
            @Override
            public void run() {

               // Log.e("home_page_status",""+home_page_status);
                //homeCartUpdateDialog();
                if(visitCount2.equalsIgnoreCase("false")){
                    try {
                        homeMessageDialog();
                    }catch (Exception e){
                        e.getMessage();
                    }
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("visitCount2", "true");
                    editor.apply();
                    editor.commit();
                }
            }
        },1000);
    }

    //fetch home dialog content
    public void homeDialogContent(){
        if(Connectivity.isConnected(BaseActivity.this)){
            String urlPlaceOrderLimit = ApplicationUrlAndConstants.urlPlaceOrderLimit+"?flag=home";

;            StringRequest requestPlaceOrderLimit = new StringRequest(Request.Method.GET,
                    urlPlaceOrderLimit,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                Log.d("urlPlaceOrderLimit",response);
                                if(status.equals("true")){
                                    JSONArray jsonArray = jsonObject.getJSONArray("homemessage");
                                    JSONObject objectMessage = jsonArray.getJSONObject(0);
                                    title = objectMessage.getString("title");
                                    Log.d("title",title);
                                    veg = objectMessage.getString("veg");
                                    vegprice = objectMessage.getString("vegprice");
                                    groceries = objectMessage.getString("groceries");
                                    groceriesprice = objectMessage.getString("groceriesprice");
                                }else {
                                    title="What would you like to order today?";
                                    veg="Fresh Fruits and Vegetables";
                                    vegprice="Checkout For Free Delivery";
                                    groceries="Groceries";
                                    groceriesprice="Checkout For Free Delivery";
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                    MySingleton.getInstance(BaseActivity.this).addToRequestQueue(requestPlaceOrderLimit);

        }else{
            Toast.makeText(BaseActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
    //this is for sync the cart count
    private int SyncData() { //TODO Server method here


        if (Connectivity.isConnected(BaseActivity.this)) {
            GateWay gateWay = new GateWay(BaseActivity.this);

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                Log.e("cartCountParams:",""+params);
            } catch (JSONException e) {
                e.getMessage();
                Log.e("cartCountParams:",""+e.getMessage());
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("cartCountRes:",""+response.toString());

                        (BaseActivity.this).updateAddToCartCount(response.getInt("normal_cart_count"));
                        //CartCount = response.getInt("normal_cart_count");
                    //    normalcartcount[0] = response.getInt("normal_cart_count");
                        CartCount = response.getInt("normal_cart_count");
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().setPriority(Request.Priority.HIGH);
            //AppController.getInstance().addToRequestQueue(request);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    600000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(request);
        }
        return CartCount;
    }
    private void getCategoriesData() { //TODO Server method here
        if (Connectivity.isConnected(BaseActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(BaseActivity.this);
            //gateWay.progressDialogStart();

            categoryModelArrayList = new ArrayList<CategoryModel>();


            JSONObject params = new JSONObject();
            try {
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlbrowseByMainCat_PopUp, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if (response.isNull("posts")) {

                    } else {

                        Log.e("broseByCate_BaseRes:",""+response.toString());

                        try {
                            JSONArray mainCatArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);
                                CategoryModel model = new CategoryModel();
                                model.setMaincategory(jsonMainCatObject.getString("maincategory"));
                                model.setProduct_cat_image(jsonMainCatObject.getString("product_cat_image"));
                                model.setDescription(jsonMainCatObject.getString("meta_description"));
                                categoryModelArrayList.add(model);
                            }
                            Sharing_content =response.getString("sharing_content");
                            home_page_message =response.getString("home_page_message");
                            home_page_status = response.getString("home_page_status");
                            if(home_page_status.equalsIgnoreCase("active")) {
                                homepopup();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();



                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    600000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(request);
        }
    }

    //urldrawer_options
    private void getDrawerOption() { //TODO Server method here
        if (Connectivity.isConnected(BaseActivity.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(BaseActivity.this);
            //gateWay.progressDialogStart();

            categoryModelArrayList = new ArrayList<CategoryModel>();


            JSONObject params = new JSONObject();
            try {
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urldrawer_options, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if (response.isNull("posts")) {

                    } else {

                        Log.e("Drawer_Response:",""+response.toString());

                        try {
                            JSONArray mainCatArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);
                                String key = jsonMainCatObject.getString("option_key");
                                String option_value = jsonMainCatObject.getString("option_value");
                                String status = jsonMainCatObject.getString("status");

                                if(key.equalsIgnoreCase("option1")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option1).setTitle(option_value).setVisible(true);
                                    page_url1 = jsonMainCatObject.getString("page_url");
                                }else if(key.equalsIgnoreCase("option2")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option2).setTitle(option_value).setVisible(true);
                                    page_url2 = jsonMainCatObject.getString("page_url");
                                }else if(key.equalsIgnoreCase("option3")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option3).setTitle(option_value).setVisible(true);
                                    page_url3 = jsonMainCatObject.getString("page_url");
                                }else if(key.equalsIgnoreCase("option4")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option4).setTitle(option_value).setVisible(true);
                                    page_url4 = jsonMainCatObject.getString("page_url");
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(BaseActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    600000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(request);
        }
    }
    //urldrawer_options from yii api
    public void getDrawerOption2() {

        GateWay gateWay = new GateWay(BaseActivity.this);
        String urldrawerOption = "";
        //String urlCartlist = "ApplicationUrlAndConstants.userTermsCondition";

        try {
            urldrawerOption = ApplicationUrlAndConstants.url_getdrawerOption
                    + "?contact=" + gateWay.getContact()+"&city="+ URLEncoder.encode(gateWay.getCity(),"UTF-8")+"&area="+ URLEncoder.encode(gateWay.getCity(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Log.d("urldrawerOption", urldrawerOption);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urldrawerOption,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("GetDrawerOptonRes", response);


                            JSONArray mainCatArray = jsonObject.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);
                                String key = jsonMainCatObject.getString("option_key");
                                String option_value = jsonMainCatObject.getString("option_value");
                                String status = jsonMainCatObject.getString("status");

                                if(key.equalsIgnoreCase("option1")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option1).setTitle(option_value).setVisible(true);
                                    page_url1 = jsonMainCatObject.getString("page_url");
                                }else if(key.equalsIgnoreCase("option2")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option2).setTitle(option_value).setVisible(true);
                                    page_url2 = jsonMainCatObject.getString("page_url");
                                }else if(key.equalsIgnoreCase("option3")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option3).setTitle(option_value).setVisible(true);
                                    page_url3 = jsonMainCatObject.getString("page_url");
                                }else if(key.equalsIgnoreCase("option4")&&status.equalsIgnoreCase("active")){
                                    nav_Menu.findItem(R.id.nav_option4).setTitle(option_value).setVisible(true);
                                    page_url4 = jsonMainCatObject.getString("page_url");
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        MySingleton.getInstance(BaseActivity.this).addToRequestQueue(requestCartList);

    }

    //Store Device info yii api
    public void StroeDeiviceInfo2() {

        GateWay gateWay = new GateWay(BaseActivity.this);
        String urlStoreDevice = "";
        try {
            urlStoreDevice = ApplicationUrlAndConstants.url_Setdeviceinfo
                    + "?contact=" + gateWay.getContact()+"&SDK="+ URLEncoder.encode(Build.VERSION.SDK,"UTF-8")
                    +"&BOARD="+ URLEncoder.encode(Build.BOARD,"UTF-8")
                    +"&BRAND="+ URLEncoder.encode(Build.BRAND,"UTF-8")
                    +"&OS_VERSION="+ URLEncoder.encode(Build.VERSION.RELEASE,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.d("urlStoreDevice", urlStoreDevice);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlStoreDevice,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //JSONObject jsonObject = new JSONObject(response);
                            Log.e("StoreDeviceInfoRes", response);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        MySingleton.getInstance(BaseActivity.this).addToRequestQueue(requestCartList);

    }

    // String request count
    public int getCartCount(){
        GateWay gateWay1 = new GateWay(BaseActivity.this);
        if(Connectivity.isConnected(BaseActivity.this)){

            try {

                Log.e("contact_user:", "" + gateWay1.getContact());
                Log.e("email_user:", "" + gateWay1.getUserEmail());
            }catch (Exception e){
                e.getMessage();
                Log.e("getcountParam:",""+e.getMessage());
            }

            String urlOtpsend = ApplicationUrlAndConstants.urlGetCartCount
                    +"?contact="+gateWay1.getContact()
                    +"&email="+gateWay1.getUserEmail();
            Log.e("getcounturl:",""+urlOtpsend);
            StringRequest stringRequestOtp = new StringRequest(Request.Method.POST,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {

                                Log.e("cartCountRes:",""+response.toString());
                                JSONObject jsonObject = new JSONObject(response);
                                (BaseActivity.this).updateAddToCartCount(Integer.parseInt(jsonObject.getString("normal_cart_count")));
                                CartCount = Integer.parseInt(jsonObject.getString("normal_cart_count"));


                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            MySingleton.getInstance(BaseActivity.this).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(BaseActivity.this,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }

        return CartCount;
    }

    //Method to show the Tersms and Condition
    public void homeMessageDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dailog_home_page_message, null);

        Button btn_submitreson = view.findViewById(R.id.btn_close);

        TextView tv_home_message = view.findViewById(R.id.tv_home_message);

        tv_home_message.setText(home_page_message);

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    //Method to show the Tersms and Condition
    public void homeCartUpdateDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dailog_cart_update_price, null);

        Button btn_submitreson = view.findViewById(R.id.btn_ok);

        TextView tv_home_message = view.findViewById(R.id.tv_home_message);

        tv_home_message.setText(home_page_message);

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

/*
* 6:26:40 PM: Executing task 'signingReport'...

Executing tasks: [signingReport]


> Configure project :app
Could not find google-services.json while looking in [src/nullnull/debug, src/debug/nullnull, src/nullnull, src/debug, src/nullnullDebug]
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)
Could not find google-services.json while looking in [src/nullnull/release, src/release/nullnull, src/nullnull, src/release, src/nullnullRelease]
registerResGeneratingTask is deprecated, use registerGeneratedResFolders(FileCollection)

> Task :app:signingReport
Variant: release
Config: none
----------
Variant: releaseUnitTest
Config: none
----------
Variant: debug
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------
Variant: debugAndroidTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------
Variant: debugUnitTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------

BUILD SUCCESSFUL in 0s
1 actionable task: 1 executed
6:26:41 PM: Task execution finished 'signingReport'.

* */


// From Pramod PC

    /*> Task :app:signingReport
    Variant: debug
    Config: debug
    Store: C:\Users\SURAJ\.android\debug.keystore
    Alias: AndroidDebugKey
    MD5: DA:FA:A2:95:2B:C8:B0:B3:EE:78:2E:74:F3:EB:E8:BB
    SHA1: AD:68:2F:CB:8E:03:8E:5E:28:68:17:65:67:F9:2C:5C:D1:AB:8D:03
    SHA-256: D3:7C:8A:97:D4:26:CF:64:9F:98:73:C5:B6:2C:4C:71:F0:F1:1D:35:4C:2A:BE:31:2C:10:FC:3A:DF:8A:5B:C5
    Valid until: Sunday, April 5, 2048
            ----------
    Variant: debugUnitTest
    Config: debug
    Store: C:\Users\SURAJ\.android\debug.keystore
    Alias: AndroidDebugKey
    MD5: DA:FA:A2:95:2B:C8:B0:B3:EE:78:2E:74:F3:EB:E8:BB
    SHA1: AD:68:2F:CB:8E:03:8E:5E:28:68:17:65:67:F9:2C:5C:D1:AB:8D:03
    SHA-256: D3:7C:8A:97:D4:26:CF:64:9F:98:73:C5:B6:2C:4C:71:F0:F1:1D:35:4C:2A:BE:31:2C:10:FC:3A:DF:8A:5B:C5
    Valid until: Sunday, April 5, 2048
            ----------
    Variant: debugAndroidTest
    Config: debug
    Store: C:\Users\SURAJ\.android\debug.keystore
    Alias: AndroidDebugKey
    MD5: DA:FA:A2:95:2B:C8:B0:B3:EE:78:2E:74:F3:EB:E8:BB
    SHA1: AD:68:2F:CB:8E:03:8E:5E:28:68:17:65:67:F9:2C:5C:D1:AB:8D:03
    SHA-256: D3:7C:8A:97:D4:26:CF:64:9F:98:73:C5:B6:2C:4C:71:F0:F1:1D:35:4C:2A:BE:31:2C:10:FC:3A:DF:8A:5B:C5
    Valid until: Sunday, April 5, 2048*/




}
