package app.apneareamein.shopping.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.apneareamein.shopping.R;

import app.apneareamein.shopping.fragments.SubCatProductFragment;
//import app.apneareamein.shopping.fragments.Tab1;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.model.InnerMovie;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.Connectivity;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class ProductListTabLayouts extends AppCompatActivity implements TabLayout.OnTabSelectedListener {


    TabLayout tabLayout_shopsubcat, tabLayout;
    //This is our tablayout

    //This is our viewPager
    private ViewPager viewPager;

    private static final String ARG_PAGE_NUMBER = "page_number";
    private static final String ARG_SHOP_INFO = "shop_id";
    private String strCity, strArea, shop_id, product_mainCat, strProductCat, shopStrProductCat, shopProductMainCat, productSuperCat;
    InnerMovie innerMovie;

    private int WishListCount, CartCount;
    private String class_name = this.getClass().getSimpleName();
    private RelativeLayout datsegMainLayoyut;
    private LinearLayout Main_Layout_NoInternet;
    private TextView txtNoConnection;
    private Network_Change_Receiver myReceiver;
    int position = 0;
    Pager adapter;
    public static ArrayList<String> subCat_Name;

    public ArrayList<String> value_2 = new ArrayList<>();


    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                datsegMainLayoyut.setVisibility(View.GONE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                datsegMainLayoyut.setVisibility(View.VISIBLE);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tab_test);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();


        myReceiver = new Network_Change_Receiver();

        Bundle bundle = getIntent().getExtras();
        shop_id = bundle.getString("shop_id");
        Log.d("abhi", "DataSegregationFourTabLayout onCreate shop_id: " + shop_id);
        strProductCat = bundle.getString("product_cat");
        tvTitle.setText(strProductCat);
        product_mainCat = bundle.getString("product_mainCat");
        //For shop sub cat
        shopStrProductCat = bundle.getString("shopStrProductCat");
        shopProductMainCat = bundle.getString("shopProductMainCat");
        position = bundle.getInt("position");

        productSuperCat = bundle.getString("productSuperCat");


        GateWay gateWay = new GateWay(this);
        strCity = gateWay.getCity();
        strArea = gateWay.getArea();

        //Adding the tabs using addTab() method


        /*tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText("Main Tab Title1"));
        tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText("Main Tab Title2"));
        tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText("Main Tab Title3"));
        tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText("Main Tab Title4"));
*/
       /*tabLayout_shopsubcat.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
           @Override
           public void onTabSelected(TabLayout.Tab tab) {

               if(tabLayout_shopsubcat.getTabAt(0).isSelected()) {
                   tabLayout.clearOnTabSelectedListeners();
                   tabLayout.removeAllTabs();
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title1"));
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title2"));
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title3"));
                   tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                   Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(),shop_id);
                   //Adding adapter to pager
                   viewPager.setAdapter(adapter);
               }
               else  if(tabLayout_shopsubcat.getTabAt(1).isSelected()) {

                   tabLayout.clearOnTabSelectedListeners();
                   tabLayout.removeAllTabs();
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title1"));
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title2"));

                   tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                   Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(),shop_id);
                   //Adding adapter to pager
                   viewPager.setAdapter(adapter);

               }

              else if(tabLayout_shopsubcat.getTabAt(2).isSelected()) {
                   tabLayout.clearOnTabSelectedListeners();
                   tabLayout.removeAllTabs();
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title1"));
                   //tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title2"));

                   tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                   Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(),shop_id);
                   //Adding adapter to pager
                   viewPager.setAdapter(adapter);

               }

              else if(tabLayout_shopsubcat.getTabAt(3).isSelected()) {
                   tabLayout.clearOnTabSelectedListeners();
                   tabLayout.removeAllTabs();
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title1"));
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title2"));
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title3"));
                   tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title4"));
                   tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                   Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(),shop_id);
                   //Adding adapter to pager
                   viewPager.setAdapter(adapter);
               }

           }

           @Override
           public void onTabUnselected(TabLayout.Tab tab) {

           }

           @Override
           public void onTabReselected(TabLayout.Tab tab) {

           }
       });*/

       /* tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title1"));
        tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title2"));
        tabLayout.addTab(tabLayout.newTab().setText("Your Tab Title3"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
*/
        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(), shop_id);
        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        // tabLayout.setOnTabSelectedListener(this);


        getShopSubCat();
        getSubCategories(strProductCat);
        Log.d("strProductCat", "" + strProductCat);


    }

    public void init() {

        tabLayout = findViewById(R.id.tabLayout); // second tab
        tabLayout_shopsubcat = findViewById(R.id.tabLayout_shopsubcat); // first tab

        txtNoConnection = findViewById(R.id.txtNoConnection);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        datsegMainLayoyut = findViewById(R.id.datsegMainLayoyut);
        viewPager = findViewById(R.id.viewPager1);

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    //Extending FragmentStatePagerAdapter
    public class Pager extends FragmentStatePagerAdapter {

        //integer to count number of tabs
        int tabCount;
        String shop_id = "0";

        //Constructor to the class
        public Pager(FragmentManager fm, int tabCount, String shopid) {
            super(fm);
            //Initializing tab count
            this.tabCount = tabCount;
            this.shop_id = shopid;
        }

        //Overriding method getItem
        @Override
        public Fragment getItem(int position) {


            //Returning the current tabs
           /* switch (position) {
                case 0:
                    Tab1 tab1 = new Tab1();
                    return tab1;
                case 1:
                    Tab1 tab2 = new Tab1();
                    return tab2;
                case 2:
                    Tab1 tab3 = new Tab1();
                    return tab3;
                default:
                    return null;*/


            //Returning the current tabs
            SubCatProductFragment fragment = null;
            try {
                int size = tabCount;
                if (position < size) {
                    fragment = new SubCatProductFragment();
                    Bundle args = new Bundle();
                    args.putInt(ARG_PAGE_NUMBER, position);
                    args.putString(ARG_SHOP_INFO, shop_id);
                    args.putString("productSuperCat", productSuperCat);
                    args.putString("tag", "normal_tag");
                    fragment.setArguments(args);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return fragment;

    }



        //Overriden method getCount to get the number of tabs
        @Override
        public int getCount() {
            return tabCount;
        }
    }

    ArrayList<InnerMovie> InnerMovieArraylist = new ArrayList<>();


    private void setViewPager(ArrayList<String> temp) {
        try {

            Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(),shop_id);
            //Adding adapter to pager
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.setOffscreenPageLimit(1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getShopSubCat() { //TODO Server method here
        if (Connectivity.isConnected(ProductListTabLayouts.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(ProductListTabLayouts.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("product_cat", shopStrProductCat);
                params.put("product_maincat", shopProductMainCat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tab_param",""+params);
            Log.d("shopStrProductCat",shopStrProductCat);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlShopSubCatResult, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.d("getShopSubCat",""+response);

                    ArrayList<String> product_subCat = new ArrayList<>();
                    ArrayList<String> product_subCatImage = new ArrayList<>();
                    ArrayList<String> title = new ArrayList<>();

                    if (!response.isNull("posts")) {
                        try {
                            JSONArray mainCatArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);

                                product_subCat.add(jsonMainCatObject.getString("product_subcat"));
                                product_subCatImage.add(jsonMainCatObject.getString("product_subcat_image"));
                                title.add(jsonMainCatObject.getString("title"));

                            }

                            if (product_subCat.size() > 0) {
                                for (int i = 0; i < product_subCat.size(); i++) {
                                    //innerMovie = new InnerMovie(product_subCat.get(i), product_subCatImage.get(i));
                                    tabLayout.clearOnTabSelectedListeners();
                                    innerMovie = new InnerMovie();
                                    innerMovie.setProduct_subCat(product_subCat.get(i));
                                    innerMovie.setProduct_subCatImage(product_subCatImage.get(i));
                                    InnerMovieArraylist.add(innerMovie);
                                    //customAdapter.add(innerMovie);

                                    tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText(InnerMovieArraylist.get(i).getProduct_subCat()));


                                    //strProductCat

                                    tabLayout_shopsubcat.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                        @Override
                                        public void onTabSelected(TabLayout.Tab tab) {
                                            tab.isSelected();
                                            shopStrProductCat = InnerMovieArraylist.get(tab.getPosition()).getProduct_subCat();
                                            //shopProductMainCat=shopProductMainCat;
                                            try {
                                                //tabLayout.removeAllTabs();
                                                getSubCategories(shopStrProductCat);
                                                Log.e("subCatCalling", "Hello");
                                            }catch (Exception e){

                                            }



                                        }

                                        @Override
                                        public void onTabUnselected(TabLayout.Tab tab) {

                                        }

                                        @Override
                                        public void onTabReselected(TabLayout.Tab tab) {

                                        }
                                    });
                                    //tabLayout_shopsubcat.getTabAt(2);
                                    ///tabLayout_shopsubcat.getTabAt(2).isSelected();
                                    // tabLayout_shopsubcat.select();
                                }
                                new Handler().postDelayed(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                // tabLayout_shopsubcat.getTabAt(2).select();
                                                tabLayout_shopsubcat.setScrollPosition(position, 0f, true);
                                                // tabLayout_shopsubcat.setSelectedTabIndicatorColor(getResources().getColor(R.color.ColorPrimary));
                                            }
                                        }, 100);
                                // recyclerView.setAdapter(customAdapter);
                                //tvEssential.setText(title.get(0));
                                // getEssentialProducts();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            gateWay.progressDialogStop();
                        }
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(ProductListTabLayouts.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            ((BaseActivity) getApplicationContext()).Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    private void getSubCategories(String strProductCat) { //TODO Server method here
        if (Connectivity.isConnected(ProductListTabLayouts.this)) {

            /*subCat_Name = new ArrayList<>();
            subCat_Name.clear();*/
            final ArrayList<String> temp = new ArrayList<>();
            //subCat_Name.clear();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("shop_id", shop_id);
                params.put("product_mainCat", product_mainCat);
                params.put("product_cat", strProductCat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("SubCategories",""+params);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetSubCategory, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("SubCategoriesRes",""+response);
                    try {
                        if (response.isNull("sub_cat")) {
                        } else {
                            JSONArray mainCat = response.getJSONArray("sub_cat");
                            for (int i = 0; i < mainCat.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCat.get(i);
                                //subCat_Name.add(jsonMainCatObject.getString("product_sub_subcat"));
                                temp.add(jsonMainCatObject.getString("product_sub_subcat"));
                                Log.d("subCat_Name_size",""+subCat_Name);
                            }//subCat_Name.add(jsonMainCatObject.getString("product_subcat"));
                            subCat_Name = temp;
                            //adapter.notifyDataSetChanged();
                            setViewPager(temp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(ProductListTabLayouts.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);

            if(product_mainCat.equalsIgnoreCase("Fruits and Veggies")){
                tabLayout.setVisibility(View.GONE);
                //tabLayout_shopsubcat.addTab(tabLayout_shopsubcat.newTab().setText("Fruits"));
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ProductListTabLayouts.this);
            builder.setMessage("Cannot proceed with the operation,No network connection! Please check your Internet connection")
                    .setTitle("Connection Offline")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            Intent intent = new Intent(ProductListTabLayouts.this, BaseActivity.class);
                            intent.putExtra("tag", "");
                            startActivity(intent);
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }


}
