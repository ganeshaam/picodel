package app.apneareamein.shopping.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.Connectivity;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class QuickLinks extends AppCompatActivity implements View.OnClickListener {

    private WebView help;
    private ProgressBar progressBar;
    private final String class_name = this.getClass().getSimpleName();
    LinearLayout laySocial;
    private RelativeLayout quickMainLayout;
    private LinearLayout Main_Layout_NoInternet;
    private Network_Change_Receiver myReceiver;
    private TextView txtNoConnection;
    private String tag;
    private String title;

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                quickMainLayout.setVisibility(View.GONE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                quickMainLayout.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_links);

        myReceiver = new Network_Change_Receiver();

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        txtNoConnection = findViewById(R.id.txtNoConnection);
        ImageView imgFB = findViewById(R.id.imgFB);
        ImageView imgTwitter = findViewById(R.id.imgTwitter);
        ImageView imgLinkedIn = findViewById(R.id.imgLinkedIn);
        laySocial = findViewById(R.id.laySocial);
        quickMainLayout = findViewById(R.id.quickMainLayout);
        imgFB.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("type");
            title = getIntent().getExtras().getString("title");
        }

        if (tag.equals("about_us")) {
            laySocial.setVisibility(View.VISIBLE);
        } else {
            laySocial.setVisibility(View.GONE);
        }

        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(title);
        help = findViewById(R.id.webview);

        switch (tag) {
            case "about_us":
                RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params1.addRule(RelativeLayout.ABOVE, R.id.laySocial);
                help.setLayoutParams(params1);
                initWebView();
                help.loadUrl(ApplicationUrlAndConstants.about_us);
                break;
            case "terms":
                initWebView();
                help.loadUrl(ApplicationUrlAndConstants.terms);
                break;
            case "cash_back":
                initWebView();
                help.loadUrl(ApplicationUrlAndConstants.cash_back);
                break;
            default:
                initWebView();
                help.loadUrl(ApplicationUrlAndConstants.help);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.imgFB:
                OpenFacebookPage();
                break;

            case R.id.imgTwitter:
                OpenTwitterPage();
                break;

            case R.id.imgLinkedIn:
                OpenLinkedInPage();
                break;

            default:
                break;
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        final Context context;

        MyWebChromeClient(Context context) {
            super();
            this.context = context;
        }
    }

    private void initWebView() {
        help.setWebChromeClient(new MyWebChromeClient(this));

        help.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                QuickLinks.this.progressBar.setProgress(100);
                super.onPageFinished(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
                QuickLinks.this.progressBar.setProgress(0);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

                view.loadUrl(ApplicationUrlAndConstants.blank_page);
            }
        });

        help.clearCache(true);
        help.clearHistory();
        help.setHorizontalScrollBarEnabled(false);
        help.getSettings().setSupportZoom(true);
        help.getSettings().setBuiltInZoomControls(true);
        help.getSettings().setDisplayZoomControls(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        if (Connectivity.isConnected(QuickLinks.this)) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name + ": selected is: " + title + " " + tag, QuickLinks.this);*/
        }
    }

    private void OpenFacebookPage() {
        String facebookPageID = "Picodel-Online-Grocery-and-Vegetables-431816291007173/";
        String facebookUrl = "https://www.facebook.com/" + facebookPageID;
        try {
            this.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } catch (PackageManager.NameNotFoundException e) {
            Intent i = new Intent(QuickLinks.this, Social_Link.class);
            i.putExtra("url", facebookUrl);
            i.putExtra("title", "Follow Us On Facebook");
            startActivity(i);
        }
    }

    private void OpenTwitterPage() {
        /*String twitterPageID = "ApneAreaMein_00";
        String twitterUrl = "https://twitter.com/" + twitterPageID;
        try {
            this.getPackageManager().getPackageInfo("com.twitter.android", 0);
            Uri uri = Uri.parse(twitterUrl);
            startActivity(new Intent(Intent.ACTION_VIEW, uri));

        } catch (PackageManager.NameNotFoundException e) {
            Intent i = new Intent(QuickLinks.this, Social_Link.class);
            i.putExtra("url", twitterUrl);
            i.putExtra("title", "Follow Us On Twitter");
            startActivity(i);
        }*/
    }

    /*https://www.facebook.com/Picodel-Online-Grocery-and-Vegetables-431816291007173/
                https://ch.linkedin.com/company/picodel  */

    private void OpenLinkedInPage() {
        String twitterPageID = "company/picodel";
        String twitterUrl = "https://www.linkedin.com/" + twitterPageID;
        try {
            this.getPackageManager().getPackageInfo("com.linkedin.android", 0);
            Uri uri = Uri.parse(twitterUrl);
            startActivity(new Intent(Intent.ACTION_VIEW, uri));

        } catch (PackageManager.NameNotFoundException e) {
            Intent i = new Intent(QuickLinks.this, Social_Link.class);
            i.putExtra("url", twitterUrl);
            i.putExtra("title", "Follow Us On LinkedIn");
            startActivity(i);
        }
    }
}
