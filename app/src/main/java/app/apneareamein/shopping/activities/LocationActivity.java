package app.apneareamein.shopping.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.adapters.CustomSpinnerAdapter;
import app.apneareamein.shopping.adapters.StateCityAdaptor;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.model.ShopModel;
import app.apneareamein.shopping.model.StateCityModel;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.MySingleton;
import app.apneareamein.shopping.utils.VolleySingleton;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class LocationActivity extends AppCompatActivity {

    private EditText etArea;
    private Spinner spinner,sp_city_spinner,sp_Area_spinner;
    private final ArrayList<String> cityList = new ArrayList<>();
    private final ArrayList<String> cityList2 = new ArrayList<>();
    private final ArrayList<String> zoneList = new ArrayList<>();
    private final ArrayList<String> areaList = new ArrayList<>();
    private CoordinatorLayout locationMainActivity;
    private ImageView imageView,iv_banner;
    private String strCity,sp_city_value="",v_city="Pune",v_state;
    private String strTag;
    private Button btnDone;
    private LinearLayout Main_Layout_NoInternet;
    private RelativeLayout locationMainLayout;
    private Network_Change_Receiver myReceiver;
    private TextView txtNoConnection;
    String class_name = this.getClass().getSimpleName();
    private ProgressBar simpleProgressBar;
    public static final String MY_PREFS_NAME = "PICoDEL";
    private StateCityAdaptor stateCityAdaptor;
    private ArrayList<StateCityModel> stateCityModelArrayList,stateCityModelArrayList2;
    private RecyclerView rv_cityState;
    private EditText et_search;
    private boolean searchFlag = false;

    private String Zone_Area="";
    private int lastAreaSize;
    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                locationMainLayout.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                locationMainLayout.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
                getCity(); //for  state List
                getCity2(); // for city List
                //getAreaList(); // For Area List
                //getAreaList2(v_city); // For Area List
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        myReceiver = new Network_Change_Receiver();
        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.chooseLocality);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        strTag = i.getStringExtra("tag");
        stateCityModelArrayList = new ArrayList<>();
        stateCityModelArrayList2 = new ArrayList<>();

        if (strTag.equals("registration")) {
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        txtNoConnection = findViewById(R.id.txtNoConnection);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        locationMainLayout = findViewById(R.id.locationMainLayout);

        etArea = findViewById(R.id.editArea);
        spinner = findViewById(R.id.spinner);
        locationMainActivity = findViewById(R.id.locationMainActivity);
        imageView = findViewById(R.id.img);
        btnDone = findViewById(R.id.btnDone);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);
        sp_city_spinner = findViewById(R.id.sp_city_spinner);
        sp_Area_spinner = findViewById(R.id.sp_Area_spinner);
        et_search = findViewById(R.id.et_search);
        iv_banner = findViewById(R.id.iv_banner);

        rv_cityState = findViewById(R.id.rv_cityState);
        rv_cityState.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_cityState.setLayoutManager(layoutManager);

        stateCityAdaptor = new StateCityAdaptor(LocationActivity.this,stateCityModelArrayList);
        rv_cityState.setAdapter(stateCityAdaptor);
        stateCityAdaptor.notifyDataSetChanged();



        etArea.setText("Pune");
        getbannerImages();

     /*   etArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Connectivity.isConnected(LocationActivity.this)) {
                    Intent i = new Intent(LocationActivity.this, AreaListActivity.class);
                    i.putExtra("city", strCity);
                    startActivityForResult(i, 1);
                } else {
                    GateWay gateWay = new GateWay(LocationActivity.this);
                    gateWay.displaySnackBar(locationMainActivity);
                }
            }
        });*/

            Log.e("TestCommit","09/07/2020");


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strCity = cityList.get(i);
                Log.d("strCity",strCity);
                v_state = strCity;

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                if(strCity!=null) {
                    editor.putString("v_state", strCity);
                }
                editor.apply();
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                etArea.setText(cityList2.get(position));
                sp_city_value = cityList2.get(position);
                v_city = sp_city_value;
                strCity = stateCityModelArrayList.get(position).getState();
                Log.e("v_city_value:",""+v_city);

                SharedPreferences.Editor editor2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                if(sp_city_value!=null) {
                    editor2.putString("v_city", sp_city_value);
                    v_city = sp_city_value;
                }
                editor2.apply();
                editor2.commit();

                getAreaList2(sp_city_spinner.getSelectedItem().toString());

                for(int i=0; i< stateCityModelArrayList.size();i++ ){

                    if(stateCityModelArrayList.get(i).getCity().contains(v_city)){

                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        String state_value = stateCityModelArrayList.get(i).getState();
                        if(state_value!=null) {
                            editor.putString("v_state", state_value);
                        }
                        editor.apply();
                        editor.commit();

                    }

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



                    stateCityModelArrayList2.clear();

                    for (int k = 0; k < stateCityModelArrayList.size(); k++) {
                        if (stateCityModelArrayList.get(k).getCity().contains(s)) {
                            stateCityModelArrayList2.add(stateCityModelArrayList.get(k));
                        }
                    }
                    stateCityAdaptor = new StateCityAdaptor(LocationActivity.this, stateCityModelArrayList2);
                    rv_cityState.setAdapter(stateCityAdaptor);
                    stateCityAdaptor.notifyDataSetChanged();

                    if (stateCityModelArrayList2.size() > 0) {
                        searchFlag = true;
                    } else {
                        searchFlag = false;
                    }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        sp_Area_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(areaList.isEmpty() || areaList == null){

                }else {
                    Zone_Area = areaList.get(position);
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    if (Zone_Area != null) {
                        editor.putString("Zone_Area", Zone_Area);
                    }
                    editor.apply();
                    editor.commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBHelper db = new DBHelper(LocationActivity.this);
                //db.insertLocalityInfo(strCity, etArea.getText().toString());
                //db.insertLocalityInfo(v_state, v_city);
                for(int i=0; i<stateCityModelArrayList.size();i++){

                    if(stateCityModelArrayList.get(i).getCity().contains(v_city)){
                        strCity = stateCityModelArrayList.get(i).getState();
                    }
                }
                //strCity = spinner.getSelectedItem().toString();
                db.insertLocalityInfo(strCity, v_city);

                GateWay gateWay = new GateWay(LocationActivity.this);
                String City = gateWay.getCity();
                String Area = gateWay.getArea();

                // API call to Update selected city to server
                UpdateUserCity(v_city);

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                if(v_city!=null) {
                    editor.putString("v_city", v_city);
                    editor.putString("last_city", v_city);
                }
                editor.apply();
                editor.commit();



                if (City != null && Area != null && v_city != null) {
                    Intent intent = new Intent(LocationActivity.this, BaseActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    LocationActivity.this.finish();
                }else {
                    Toast.makeText(LocationActivity.this,"Please Select City",Toast.LENGTH_LONG).show();
                }
            }
        });

        rv_cityState.addOnItemTouchListener(new RecyclerTouchListener(this, rv_cityState, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if(searchFlag){
                    v_city = stateCityModelArrayList2.get(position).getCity();
                    v_state = stateCityModelArrayList2.get(position).getState();
                    Toast.makeText(LocationActivity.this,"City:"+v_city,Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    if(v_state!=null) {
                        editor.putString("v_state", v_state);
                    }
                    if(v_city!=null) {
                        editor.putString("v_city", v_city);
                    }
                    editor.apply();
                    editor.commit();
                }else {
                    v_city = stateCityModelArrayList.get(position).getCity();
                    v_state = stateCityModelArrayList.get(position).getState();
                    Toast.makeText(LocationActivity.this,"City:"+v_city,Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    if(v_state!=null) {
                        editor.putString("v_state", v_state);
                    }
                    if(v_city!=null) {
                        editor.putString("v_city", v_city);
                    }
                    editor.apply();
                    editor.commit();
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");

                etArea.setText(result);

                if (!etArea.getText().toString().equals("")) {
                    btnDone.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (strTag.equals("registration")) {
            if (etArea.getText().toString().equals("")) {
                Toast.makeText(LocationActivity.this, "Please select area..", Toast.LENGTH_SHORT).show();
            } else if (btnDone.getVisibility() == View.VISIBLE) {
                Toast.makeText(LocationActivity.this, "Please tap on DONE to continue", Toast.LENGTH_SHORT).show();
            }
        } else {
            finish();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getCity() { //TODO Server method here
        if (Connectivity.isConnected(LocationActivity.this)) {
            cityList.clear();

            final GateWay gateWay = new GateWay(LocationActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlStateConfiguration, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                  //  if (!response.isNull("posts")) {
                    Log.e("State_value:",""+response);
                        try {
                           //JSONArray jsonArray = response.getJSONArray("");
                           //JSONObject jsonObjectTest = jsonArray.getJSONObject(0);
                           // Log.d("posts",response.getString("image"));
                           //TODO here show location GIF image setup to glide
                           /*Glide.with(LocationActivity.this)
                                    .load(response.getString("image"))
                                    .thumbnail(Glide.with(LocationActivity.this).load(R.drawable.ic_location_on_black_24dp))
                                    .error(R.drawable.ic_app_transparent)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imageView);*/
                          imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_on_black_24dp));

                            JSONArray cityJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < cityJsonArray.length(); i++) {
                                JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                                cityList.add(jsonCityData.getString("city"));
                            }
                            CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, cityList, null, null, "location");
                            spinner.setAdapter(customSpinnerAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(LocationActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
           // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(LocationActivity.this).addToRequestQueue(request);
        }
    }
    /*cityList2*/
    private void getCity2() { //TODO Server method here
        if (Connectivity.isConnected(LocationActivity.this)) {
            cityList2.clear();

            stateCityModelArrayList.clear();

            final GateWay gateWay = new GateWay(LocationActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);
            stateCityModelArrayList = new ArrayList<>();
            /*JSONObject params = new JSONObject();
            try {
                //params.put("contact", gateWay.getContact());
                params.put("city", spinner.getSelectedItem().toString()); //sp_city_value
                //params.put("type", "Android");//true or false
                Log.e("userCityParam:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCityConfiguration, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("City_Reesp:",""+response);
                    //  if (!response.isNull("posts")) {
                    try {
                        //JSONArray jsonArray = response.getJSONArray("");
                        //JSONObject jsonObjectTest = jsonArray.getJSONObject(0);
                        // Log.d("posts",response.getString("image"));
                        //TODO here show location GIF image setup to glide
                           /*Glide.with(LocationActivity.this)
                                    .load(response.getString("image"))
                                    .thumbnail(Glide.with(LocationActivity.this).load(R.drawable.ic_location_on_black_24dp))
                                    .error(R.drawable.ic_app_transparent)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imageView);*/
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_on_black_24dp));

                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            StateCityModel model = new StateCityModel();
                            cityList2.add(jsonCityData.getString("city"));
                            model.setCity(jsonCityData.getString("city"));
                            model.setState(jsonCityData.getString("state"));
                            stateCityModelArrayList.add(model);
                        }
                        /*CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, cityList2, null, null, "location");
                        sp_city_spinner.setAdapter(customSpinnerAdapter);*/

                        //ArrayAdapter<String> adapterCity = new ArrayAdapter(LocationActivity.this,android.R.layout.simple_spinner_item,cityList2);
                        ArrayAdapter<String> adapterCity = new ArrayAdapter(LocationActivity.this,R.layout.layout_standard_spinner,cityList2);
                        sp_city_spinner.setAdapter(adapterCity);
                        /*for recycler*/
                        Log.e("state_Size:",""+stateCityModelArrayList.size());
                        stateCityAdaptor = new StateCityAdaptor(LocationActivity.this,stateCityModelArrayList);
                        rv_cityState.setAdapter(stateCityAdaptor);
                        stateCityAdaptor.notifyDataSetChanged();

                        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                        v_city = prefs.getString("v_city", "NA");

                                    if (v_city != null && v_city.length()>2) {
                                        int spinnerPosition = adapterCity.getPosition(v_city);
                                        //stateCityModelArrayList.contains(v_city);
                                        sp_city_spinner.setSelection(spinnerPosition);
                                    }/*else {
                                        //sp_city_spinner.setSelection(0);
                                    }*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(LocationActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            Log.e("cityReq:",""+request);
            VolleySingleton.getInstance(LocationActivity.this).addToRequestQueue(request);
        }
    }
    //Update User City  UpdateUserCity.php
    private void UpdateUserCity(String city) { //TODO Server method here
        if (Connectivity.isConnected(LocationActivity.this)) {

            GateWay gateWay = new GateWay(LocationActivity.this);

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("city", city);
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdateUserCity, params, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                     if (!response.isNull("status")) {
                    /*try {
                        JSONArray cityJsonArray = response.getJSONArray("posts");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    Log.e("UdateCityRes:",""+response);
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(LocationActivity.this);
                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(LocationActivity.this).addToRequestQueue(request);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(LocationActivity.this)) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, LocationActivity.this);*/

        }
    }

    private void getZoneList() { //TODO Server method here
        if (Connectivity.isConnected(LocationActivity.this)) {
            zoneList.clear();

            final GateWay gateWay = new GateWay(LocationActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCityConfiguration, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    try {
                        //JSONArray jsonArray = response.getJSONArray("");
                        //JSONObject jsonObjectTest = jsonArray.getJSONObject(0);
                        // Log.d("posts",response.getString("image"));
                        //TODO here show location GIF image setup to glide
                           /*Glide.with(LocationActivity.this)
                                    .load(response.getString("image"))
                                    .thumbnail(Glide.with(LocationActivity.this).load(R.drawable.ic_location_on_black_24dp))
                                    .error(R.drawable.ic_app_transparent)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imageView);*/
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_on_black_24dp));

                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            zoneList.add(jsonCityData.getString("city"));
                        }
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, cityList2, null, null, "location");
                        sp_city_spinner.setAdapter(customSpinnerAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(LocationActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(LocationActivity.this).addToRequestQueue(request);
        }
    }

    private void getAreaList() { //TODO Server method here
        if (Connectivity.isConnected(LocationActivity.this)) {
            areaList.clear();

            final GateWay gateWay = new GateWay(LocationActivity.this);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAreaConfiguration, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    Log.e("getCityRes:",""+response);
                    try {
                        //JSONArray jsonArray = response.getJSONArray("");
                        //JSONObject jsonObjectTest = jsonArray.getJSONObject(0);
                        // Log.d("posts",response.getString("image"));
                        //TODO here show location GIF image setup to glide
                           /*Glide.with(LocationActivity.this)
                                    .load(response.getString("image"))
                                    .thumbnail(Glide.with(LocationActivity.this).load(R.drawable.ic_location_on_black_24dp))
                                    .error(R.drawable.ic_app_transparent)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imageView);*/
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_on_black_24dp));

                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            areaList.add(jsonCityData.getString("area"));
                        }

                        /*CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, areaList, null, null, "location");
                        sp_Area_spinner.setAdapter(customSpinnerAdapter);*/

                        ArrayAdapter<String> adapter = new ArrayAdapter(LocationActivity.this,android.R.layout.simple_spinner_item,areaList);
                        sp_Area_spinner.setAdapter(adapter);

                        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                        Zone_Area = prefs.getString("Zone_Area", "");

                        if (Zone_Area != null) {
                            int spinnerPosition = adapter.getPosition(Zone_Area);
                            sp_Area_spinner.setSelection(spinnerPosition);
                        }else {
                            //sp_Area_spinner.setSelection(0);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Log.e("Parse Error:",""+error);
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(LocationActivity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(LocationActivity.this).addToRequestQueue(request);
        }
    }

    //urlgetAreaList

    public void getAreaList2(String city){

        areaList.clear();
        //String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/getAreaList.php?city="+city;  // Live Code at version 2.0.41
        String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/ActiveAreaConfiguration.php?city="+city;
        simpleProgressBar.setVisibility(View.VISIBLE);
        Log.d("targetUrlgetArea",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("AreaListRes",response);

                        try {
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        //ShopModel model = new ShopModel();
                                        //model.setShop_id(olderlist.getString("shop_id"));
                                        areaList.add(olderlist.getString("area"));
                                    }
                                    Log.e("areaListSize:",""+areaList.size());

                                    //ArrayAdapter<String> adapter = new ArrayAdapter(LocationActivity.this,android.R.layout.simple_spinner_item,areaList);
                                    ArrayAdapter<String> adapter = new ArrayAdapter(LocationActivity.this,R.layout.layout_standard_spinner,areaList);
                                    sp_Area_spinner.setAdapter(adapter);

                                    SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                                    Zone_Area = prefs.getString("Zone_Area", "");
                                    String last_city = prefs.getString("last_city","");

                                    //if(last_city.equalsIgnoreCase(v_city)) {

                                        if (Zone_Area != null && Zone_Area.length() >= 2) {
                                            int spinnerPosition = adapter.getPosition(Zone_Area);
                                            Log.e("spinnerPosition:",""+spinnerPosition);
                                            //sp_Area_spinner.setSelection(spinnerPosition);
                                            //String value = sp_Area_spinner.getSelectedItemPosition(spinnerPosition);
                                            if(spinnerPosition>=0) {
                                                sp_Area_spinner.setSelection(spinnerPosition);
                                            }
                                        }
                                    //}

                                }else {
                                    simpleProgressBar.setVisibility(View.INVISIBLE);
                                }
                            }else if(strStatus==false){
                                /*tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");*/
                                simpleProgressBar.setVisibility(View.INVISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressDialog.dismiss();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
        MySingleton.getInstance(this).addToRequestQueue(orderListRequest);
    }

    private void getbannerImages() { //TODO Server method here
        if (Connectivity.isConnected(LocationActivity.this)) { // Internet connection is not present, Ask user to connect to Internet

            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {

                params.put("activity_name", "LocationActivity");
                params.put("city", v_city);

                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlgetActivityBanners, params, new Response.Listener<JSONObject>() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("BannersRes",""+response.toString());

                    if (response.isNull("posts")) {
                        Toast.makeText(LocationActivity.this,"No Banner Found",Toast.LENGTH_LONG).show();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        // gateWay.progressDialogStop();
                        try {
                            JSONArray verificationCodeJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < verificationCodeJsonArray.length(); i++) {
                                JSONObject jSonVCodeData = verificationCodeJsonArray.getJSONObject(i);

                               String banner_url = jSonVCodeData.getString("banner_url");
                                Picasso.with(LocationActivity.this)
                                        .load(banner_url)
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(iv_banner);

                                Log.e("Res_banner_url:",""+banner_url);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {

        }
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
}
