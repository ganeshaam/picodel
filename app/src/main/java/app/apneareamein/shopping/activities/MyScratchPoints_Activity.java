package app.apneareamein.shopping.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class MyScratchPoints_Activity extends AppCompatActivity {

    String sum_value ="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_scratch_points_);
    }

    private void getScratch_amount() { //TODO Server method here
        if (Connectivity.isConnected(MyScratchPoints_Activity.this)) {
            GateWay gateWay = new GateWay(MyScratchPoints_Activity.this);

            String email = gateWay.getUserEmail();
            String user_contact = gateWay.getContact();


            JSONObject params = new JSONObject();
            try {

                params.put("email", email);
                params.put("user_contact", user_contact);
                params.put("order_id", "");
                params.put("order_amount", "");
                Log.d("scratch_get_sum", ";params: " + params.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.get_scratch_amount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("GET_SCRATCH_SUM", "onResponse: " + response.toString());

                        String result = response.getString("sum");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    Log.d("SCRATCHED", "onErr: " + error.toString());
                    GateWay gateWay = new GateWay(MyScratchPoints_Activity.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Toast.makeText(MyScratchPoints_Activity.this, "Please check Internet Connection", Toast.LENGTH_LONG).show();
        }
    }
}
