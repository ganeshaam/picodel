package app.apneareamein.shopping.sync;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

public class UserTracking {

    public static Context context;

    public UserTracking(Context c) {
        context = c;
    }

    public void user_tracking(String class_name, Context context) { //TODO Server method here

        GateWay gateWay = new GateWay(context);
        String strContact = gateWay.getContact();
        String strEmail = gateWay.getUserEmail();

        JSONObject params = new JSONObject();
        try {
            params.put("contact", strContact);
            params.put("email", strEmail);
            params.put("class_name", class_name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUserTracking, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.HIGH);
        AppController.getInstance().addToRequestQueue(request);
    }

    public void usernotification_tracking(String class_name, String title, String message, String img_url, String n_tag, String redirect_link, Context context) {
        if (Connectivity.isConnected(context)) {
            GateWay gateWay = new GateWay(context);
            String strContact = gateWay.getContact();
            String strEmail = gateWay.getUserEmail();

            JSONObject params = new JSONObject();
            try {
                params.put("contact", strContact);
                params.put("email", strEmail);
                params.put("notification_title", title);
                params.put("notification_message", message);
                params.put("notification_imgurl", img_url);
                params.put("notification_tag", n_tag);
                params.put("redirect_link", redirect_link);
                params.put("class_name", class_name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddUserNotifications, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().setPriority(Request.Priority.HIGH);
            AppController.getInstance().addToRequestQueue(request);
        }
    }
}
