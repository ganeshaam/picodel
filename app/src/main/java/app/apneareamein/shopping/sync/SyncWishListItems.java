package app.apneareamein.shopping.sync;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.utils.AppController;

public class SyncWishListItems {

    public static Context context;

    public SyncWishListItems(Context c) {
        context = c;
    }

    public void syncWishListItems(String strContact, String strEmail, final Context context) { //TODO Server method here
        /*final DBHelper db = new DBHelper(context);
        db.deleteOnlyWishListTable();*/

        JSONObject params = new JSONObject();
        try {
            params.put("contactNo", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetWishListItem, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (response.isNull("CartStatistics")) {
                    //db.deleteOnlyWishListTable();
                } else {
                    try {
                        JSONArray jsonArray = response.getJSONArray("CartStatistics");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String product_id = jsonObject.getString("product_id");

                            addWishListItemtoLocal(product_id, context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().setPriority(Request.Priority.HIGH);
        AppController.getInstance().addToRequestQueue(request);
    }

    private void addWishListItemtoLocal(String product_id, Context context) {
        DBHelper db = new DBHelper(context);

        HashMap AddToCartInfo;
        AddToCartInfo = db.getWishListDetails(product_id);
        String strId = (String) AddToCartInfo.get("pId");
        if (strId == null) {
            db.insertProductIntoWishList(product_id);
        }
    }
}
