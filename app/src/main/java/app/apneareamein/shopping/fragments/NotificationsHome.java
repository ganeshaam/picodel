package app.apneareamein.shopping.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.MyOrder;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsHome extends Fragment {

    private RecyclerView mRecyclerView;
    private RelativeLayout emptyNotificationLayout;
    private CoordinatorLayout notificationsMainLayout;
    private CustomAdapter customAdapter;
    private String userID;
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;
    private ProgressBar simpleProgressBar;
    android.app.AlertDialog progressDialog2;

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
        GateWay gateWay = new GateWay(getActivity());
       // gateWay.hide();
        simpleProgressBar.setVisibility(View.INVISIBLE);
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                mRecyclerView.setVisibility(View.GONE);
                emptyNotificationLayout.setVisibility(View.GONE);
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);
            } else {
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(BaseActivity.txtNoConnection);
                getNotifications();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);
            Activity activity = getActivity();
            if (isAdded() && activity != null) {
                dialog(status);
            }
        }
    }

    private void initializeViews() {
        homePageActivity = (BaseActivity) getActivity();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment__notifications_home, container, false);
        initializeViews();
        progressDialog2 = new SpotsDialog.Builder().setContext(getActivity()).build();
        myReceiver = new Network_Change_Receiver();

        emptyNotificationLayout = view.findViewById(R.id.emptyRelativeLayout);
        notificationsMainLayout = view.findViewById(R.id.notificationsMainLayout);
        simpleProgressBar = view.findViewById(R.id.simpleProgressBar);
        mRecyclerView = view.findViewById(R.id.notification_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    public class Movie {

        final String n_tag;
        final String title;
        final String message;
        final String img_url;
        final int n_id;

        public Movie(int n_id, String title, String message, String img_url, String n_tag) {
            this.n_id = n_id;
            this.title = title;
            this.message = message;
            this.img_url = img_url;
            this.n_tag = n_tag;
        }

        public String getN_tag() {
            return n_tag;
        }

        public String getTitle() {
            return title;
        }

        public String getMessage() {
            return message;
        }

        public String getImg_url() {
            return img_url;
        }

        public int getN_id() {
            return n_id;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        ((BaseActivity) getActivity()).changeTitle(getString(R.string.title_activity_notifications));

        if (Connectivity.isConnected(getActivity())) {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.SyncData();

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());*/
        }
        GateWay gateWay = new GateWay(getActivity());
        //gateWay.hide();
        //simpleProgressBar.setVisibility(View.INVISIBLE);
    }

    private void getNotifications() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            final GateWay gateWay = new GateWay(getActivity());
          //  gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);
            progressDialog2.show();
            customAdapter = new CustomAdapter();

            JSONObject params = new JSONObject();
            try {
                params.put("user_contact", gateWay.getContact());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetNotification, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (response.isNull("posts")) {
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                        progressDialog2.dismiss();
                        emptyNotificationLayout.setVisibility(View.VISIBLE);
                    } else {
                        emptyNotificationLayout.setVisibility(View.GONE);
                        try {
                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");

                            Log.e("Notification_RS:",""+mainClassificationJsonArray.toString());

                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = mainClassificationJsonArray.getJSONObject(i);
                                userID = jSonClassificationData.getString("user_id");

                                Movie1 movie = new Movie1(jSonClassificationData.getString("notification_id"), jSonClassificationData.getString("notification_tag"), jSonClassificationData.getString("title"),
                                        jSonClassificationData.getString("message"), jSonClassificationData.getString("image"), jSonClassificationData.getString("redirect_link"),
                                        jSonClassificationData.getString("created_date"),jSonClassificationData.getString("del_boy_imageurl"));
                                customAdapter.add(movie);
                            }
                            mRecyclerView.setAdapter(customAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDialog2.dismiss();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDialog2.dismiss();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error);
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            ((BaseActivity) getActivity()).Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    private class CustomAdapter extends RecyclerView.Adapter<ViewHolder> {

        Intent intent;
        String strFormattedDate;
        final List<Movie1> cardList = new ArrayList<>();

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_show_notifications, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
            final Movie1 movie = cardList.get(position);

            try {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
                Date d = df.parse(movie.getCreated_date());
                df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss a");
                strFormattedDate = df.format(d);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String strImage = movie.getResto_review_rdate();
            if (strImage.equals("")) {
                holder.imgOffer.setVisibility(View.GONE);
            } else {
                holder.imgOffer.setVisibility(View.VISIBLE);

                //TODO show offer images setup to gilde
                Glide.with(getActivity()).load(strImage)
                        .thumbnail(Glide.with(getActivity()).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.imgOffer);
            }
            String strNotificationTag = movie.getResto_review_name();
            String notificationTitle = movie.getResto_review_rating();
            String notificationMessage = movie.getResto_review_review();

            String del_boy_image = movie.getDel_boy_image();
            if (del_boy_image==null || del_boy_image.isEmpty()||del_boy_image.equals("")) {
                holder.imgdelboy.setVisibility(View.GONE);
            } else if(del_boy_image!=null){
                holder.imgdelboy.setVisibility(View.VISIBLE);

                //TODO show offer images setup to gilde
               /* Glide.with(getActivity()).load(del_boy_image)
                        .thumbnail(Glide.with(getActivity()).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.imgdelboy);*/

                Picasso.with(getActivity())
                        .load(movie.getDel_boy_image())
                        .into(holder.imgdelboy);
            }

            holder.imgRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on remove button", getActivity());*/
                    removeItem(position);
                }
            });

            switch (strNotificationTag) {
                case "orderConfirmed":
                    holder.notificationIcon.setImageResource(R.mipmap.ic_bag_shop);
                    break;
                case "orderDispatch":
                    holder.notificationIcon.setImageResource(R.mipmap.ic_delivery_truck);
                    break;
                case "offer":
                    holder.notificationIcon.setImageResource(R.mipmap.ic_offer_notification);
                    break;
                case "festival":
                    holder.notificationIcon.setImageResource(R.drawable.festival);
                    break;
                case "update":
                    holder.notificationIcon.setImageResource(R.drawable.ic_playstore);
                    break;
                default:
                    holder.notificationIcon.setImageResource(R.mipmap.ic_notification);
                    break;
            }

            holder.tvNotificationTitle.setText(notificationTitle);
            holder.tvNotificationMessage.setText(notificationMessage);
            holder.tvNotificationTime.setText(strFormattedDate);

            holder.boxRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String strTag = movie.getResto_review_name();
                    switch (strTag) {
                        case "orderConfirmed":
                            intent = new Intent(getActivity(), MyOrder.class);
                            startActivity(intent);
                        case "orderDispatch":
                            intent = new Intent(getActivity(), MyOrder.class);
                            startActivity(intent);
                            break;
                        case "update":
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=app.apneareamein.shopping&hl=en"));
                            startActivity(intent);
                            break;

                        case "browser":
                            if (movie.getRedirect_link().equals("")) {

                            } else {
                                intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(movie.getRedirect_link()));
                                startActivity(intent);
                            }
                            break;

                        case "activity":
                            if (movie.getRedirect_link().equals("")) {

                            } else {
                                try {
                                    Intent resultIntent = new Intent(getActivity(), Class.forName("app.apneareamein.shopping.activities" + movie.getRedirect_link()));
                                    startActivity(resultIntent);
                                } catch (ClassNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;

                        case "fragment":
                            if (movie.getRedirect_link().equals("")) {

                            } else {
                                try {
                                    Fragment frags = (Fragment) Class.forName("app.apneareamein.shopping.fragments." + movie.getRedirect_link()).newInstance();
                                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame, frags).commit();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return cardList.size();
        }

        public void removeItem(int position) {
            final Movie1 movie = cardList.get(position);
            cardList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, cardList.size());
            String strNotificationTag = movie.getResto_review_name();
            String strNotificationID = movie.getId();
            deActiveNotification(strNotificationTag, strNotificationID);
        }

        private void deActiveNotification(String notificationTag, String NotificationID) { //TODO Server method here
            if (Connectivity.isConnected(getActivity())) {
                final GateWay gateWay = new GateWay(getActivity());
                //gateWay.progressDialogStart();
                simpleProgressBar.setVisibility(View.VISIBLE);

                JSONObject params = new JSONObject();
                try {
                    params.put("user_id", userID);
                    params.put("notification_tag", notificationTag);
                    params.put("notification_id", NotificationID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeactiveNotification, params, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.isNull("posts")) {
                            //gateWay.progressDialogStop();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        } else {
                            if (cardList.size() > 0) {
                                emptyNotificationLayout.setVisibility(View.GONE);
                            } else {
                                emptyNotificationLayout.setVisibility(View.VISIBLE);
                            }
                            //gateWay.progressDialogStop();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);

                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                    }
                });
                AppController.getInstance().addToRequestQueue(request);
            } else {
                GateWay gateWay = new GateWay(getActivity());
                gateWay.displaySnackBar(notificationsMainLayout);
            }
        }

        public void add(Movie1 movie) {
            cardList.add(movie);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView tvNotificationTitle;
        final TextView tvNotificationMessage;
        final TextView tvNotificationTime;
        final ImageView imgOffer;
        final CircleImageView imgdelboy;
        final ImageView imgRemove;
        final ImageView notificationIcon;
        final RelativeLayout boxRelativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNotificationTitle = itemView.findViewById(R.id.txtHeadline);
            tvNotificationMessage = itemView.findViewById(R.id.txtMessage);
            tvNotificationTime = itemView.findViewById(R.id.txtNotificationTime);
            imgOffer = itemView.findViewById(R.id.imgOffer);
            imgdelboy = itemView.findViewById(R.id.imgdelboy);
            imgRemove = itemView.findViewById(R.id.imgRemove);
            notificationIcon = itemView.findViewById(R.id.imgNotificationIcon);
            boxRelativeLayout = itemView.findViewById(R.id.boxRelativeLayout);
            boxRelativeLayout.setTag(this);
            imgRemove.setTag(this);
        }
    }

    private class Movie1 {
        final String id;
        final String resto_review_name;
        final String resto_review_rating;
        final String resto_review_review;
        final String resto_review_rdate;
        final String redirect_link;
        final String created_date;
        final String del_boy_image;

        Movie1(String id, String resto_review_name, String resto_review_rating,
               String resto_review_review, String resto_review_rdate, String redirect_link, String created_date, String del_boy_image) {
            this.id = id;
            this.resto_review_name = resto_review_name;
            this.resto_review_rating = resto_review_rating;
            this.resto_review_review = resto_review_review;
            this.resto_review_rdate = resto_review_rdate;
            this.redirect_link = redirect_link;
            this.created_date = created_date;
            this.del_boy_image = del_boy_image;
        }


        public String getId() {
            return id;
        }

        public String getResto_review_name() {
            return resto_review_name;
        }

        public String getResto_review_rating() {
            return resto_review_rating;
        }

        public String getResto_review_review() {
            return resto_review_review;
        }

        public String getResto_review_rdate() {
            return resto_review_rdate;
        }

        public String getRedirect_link() {
            return redirect_link;
        }

        public String getCreated_date() {
            return created_date;
        }


        public String getDel_boy_image() {
            return del_boy_image;
        }
    }
}