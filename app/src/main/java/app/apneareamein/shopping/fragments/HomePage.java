package app.apneareamein.shopping.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.orhanobut.dialogplus.DialogPlus;
import com.squareup.picasso.Picasso;
import com.truizlop.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.truizlop.sectionedrecyclerview.SectionedSpanSizeLookup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.AddToCart;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.FeedbackActivity;
import app.apneareamein.shopping.activities.LocationActivity;
import app.apneareamein.shopping.activities.PromotionalPartner;
import app.apneareamein.shopping.activities.SearchProducts;
import app.apneareamein.shopping.activities.ShopCategoryActivity;
import app.apneareamein.shopping.activities.ShopListActivity;
import app.apneareamein.shopping.activities.SingleProductInformation;
import app.apneareamein.shopping.adapters.CustomSpinnerAdapter;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.model.CategoryModel;
import app.apneareamein.shopping.sync.SyncNormalCartItems;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.LoopingCirclePageIndicator;
import app.apneareamein.shopping.utils.LoopingPagerAdapter;
import app.apneareamein.shopping.utils.Movie;
import app.apneareamein.shopping.utils.MySingleton;
import app.apneareamein.shopping.utils.VerticalTextView;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class HomePage extends Fragment {

    AlertDialog alert;
    private String class_name = this.getClass().getSimpleName();
    public static final String MY_PREFS_NAME = "PICoDEL";
    private Context context;
    private Fragment fragment;
    private DialogPlus dialog;
    private Handler handler;
    private Runnable runnable;
    private HashMap offerCodeHashMap;
    private String status;
    private TextView tvShopByBrand;
    private VerticalTextView tvShopByPopCategory;
    private TextView tvUltimate;
    private TextView tvUltimate1;
    private TextView tvComboNote;
    private TextView tvComboOfferStart;
    private ImageView eventImage,grocery_img,fruits_veggies_img,img_chicken;
    private CardView card_feedback_tag,card_refer_tag,card_chicken_tag,card_oneRupee_tag;
    private ViewPager viewPager,viewPager2;
    private CustomAdapterForBrand customAdapterForBrand;
    private CustomAdapterForComboOffer customAdapterForComboOffer;
    private CustomAdapterForCategory customAdapterForCategory;
    private UltimateCardAdapter ultimateCardAdapter;
    private UltimateCardAdapter1 ultimateCardAdapter1;
    private PopCatAdapter popCatAdapter;
    private RecyclerView recyclerViewForBrands;
    private RecyclerView recyclerViewPopCat;
    private RecyclerView comboOfferRecyclerView;
    private RecyclerView recyclerViewForOfferLayout;
    private RecyclerView recyclerViewUltimate;
    private RecyclerView recyclerViewUltimate1;
    private RecyclerView recyclerViewForCategory;
    private CoordinatorLayout homeMainLayout;
    private LinearLayout eventLinearLayout;
    private LinearLayout comboofferLinearLayout;
    private LinearLayout LinShopByBrand;
    private LinearLayout LinOfferlayout;
    private ArrayList<String> cashBackBanner;
    private ArrayList<String> eventImgPath;
    private ArrayList<String> eventLink;
    private ArrayList<OfferCodeWiseProduct> ProductWiseComboOffer;
    private ArrayList<comboOfferWithOfferCode> FinalComboOffer;
    private ArrayList<String> productImage,productImage2;
    private ArrayList<String> event_array_url;
    private ArrayList<String> productImageCategory,productImageCategory2;
    private LinearLayout updateLayout;
    private RelativeLayout HomePageMainLayout;
    private String pId;
    private String shopId;
    private String strId;
    private Animation in;
    private Animation out;
    private int fadeCount;
    private Runnable mFadeOut = new Runnable() {
        @Override
        public void run() {
            if (fadeCount == 2) {
                out.setDuration(200);
            }
            tvComboNote.startAnimation(out);
        }
    };
    private Handler handler1;
    private ArrayList<productCodeWithProductList> FinalList;
    private ArrayList<productCodeWithProductList> FinalList1;
    private LinkedHashMap tempProductListHashMap = new LinkedHashMap();
    private ArrayList<CategoryList> tempProductWiseList = new ArrayList<>();
    private LinkedHashMap productListHashMap = new LinkedHashMap();
    private ArrayList<CategoryList> ProductWiseList = new ArrayList<>();
    private ArrayList<productCodeWithProductList1> FinalList2;
    private ArrayList<String> imgPath = new ArrayList<>();
    private ArrayList<String> catName = new ArrayList<>();
    LinkedHashMap productListHashMapfor_mr_products = new LinkedHashMap();
    LinkedHashMap productListHashMap1 = new LinkedHashMap();
    final ArrayList<UltimateProductList> productWiseList = new ArrayList<>();
    final ArrayList<UltimateProductList> productWiseList1 = new ArrayList<>();
    private LinearLayout layUltimate1;
    private LinearLayout layUltimate2;
    private CardView LinPopCat;
    private ViewGroup mFrameLayout;
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;
    private String event_status;
    private String cash_back_points;
    private String noti_count;
    private String version_status;
    EditText editCouponCode;
    String couponCode,fb_status="0",o_status="0",share_containt="",offer_order_id="",offer_status,offer_url="",Zone_Area="",v_state="",v_city="Pune",sessionMainCat="";
    DialogPlus couponCodeDialog;
    private int CartCount = 0;

    //initiat main cat
    private RecyclerView recyclerView;
    private CustomAdapter customAdapter;
    private LinkedHashMap linkedHashMap;
    private ArrayList<MainCatWiseInfo> ProductMainCatWise;
    //end main cat
    private ProgressBar simpleProgressBar;
    String cartUpdateFlag="false";

    //set up
    Timer timer;
    Handler timer_handler;
    private int currentPage = 0;
    private void initializeViews() {
        homePageActivity = (BaseActivity) getActivity();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //if user app goes into onPause() method and user come again to app
        // in that case dismiss the couponCodeDialog
        if (couponCodeDialog != null && couponCodeDialog.isShowing()) {
            couponCodeDialog.dismiss();
        }

        //if user app goes into onPause() method and user come again to app
        // in that case dismiss the alert
        if (alert != null && alert.isShowing()) {
            alert.dismiss();
        }
        getActivity().unregisterReceiver(myReceiver);
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                if (couponCodeDialog != null && couponCodeDialog.isShowing()) {
                    couponCodeDialog.dismiss();
                }
                if (alert != null && alert.isShowing()) {
                    alert.dismiss();
                }
                HomePageMainLayout.setVisibility(View.GONE);

                BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);
            } else {
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);

                HomePageMainLayout.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));

                slideDown(BaseActivity.txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            Activity activity = getActivity();
            if (isAdded() && activity != null) {
                dialog(status);
            }

        }
    }

    private void init(View view) {

        initializeViews();

        myReceiver = new Network_Change_Receiver();

        viewPager = view.findViewById(R.id.pager);
        viewPager2 = view.findViewById(R.id.pager2);
        mFrameLayout = view.findViewById(R.id.pagesIndiactor);
        eventImage = view.findViewById(R.id.eventImage);
        tvShopByBrand = view.findViewById(R.id.txtShopByBrand);
        tvShopByPopCategory = view.findViewById(R.id.txtShopByPopCategory);
        tvComboNote = view.findViewById(R.id.txtComboNote);
        tvComboOfferStart = view.findViewById(R.id.txtComboOfferStart);
        //grocery_img,fruits_veggies_img;
        grocery_img = view.findViewById(R.id.grocery_img);
        img_chicken = view.findViewById(R.id.img_chicken);
        fruits_veggies_img = view.findViewById(R.id.fruits_veggies_img);
        card_feedback_tag  = view.findViewById(R.id.card_feedback_tag);
        card_oneRupee_tag  = view.findViewById(R.id.card_oneRupee_tag);
        card_refer_tag  = view.findViewById(R.id.card_refer_tag);
        card_chicken_tag = view.findViewById(R.id.card_chicken_tag);
        simpleProgressBar = view.findViewById(R.id.simpleProgressBar);
        fadeCount = 0;

        /*handler1 = new Handler();
        in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(2000);

        out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(2000);*/
       /* out.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {
                fadeCount++;
                tvComboNote.startAnimation(in);
                handler1.postDelayed(mFadeOut, 2000);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });*/

        /*tvComboNote.startAnimation(in);
        handler1.postDelayed(mFadeOut, 3000);*/

        tvUltimate = view.findViewById(R.id.txtUltimate);
        tvUltimate1 = view.findViewById(R.id.txtUltimate1);

        homeMainLayout = view.findViewById(R.id.homeMainLayout);
        eventLinearLayout = view.findViewById(R.id.eventLinearLayout);
        comboofferLinearLayout = view.findViewById(R.id.comboofferLinearLayout);
        HomePageMainLayout = view.findViewById(R.id.HomePageMainLayout);
        LinShopByBrand = view.findViewById(R.id.LinShopByBrand);
        LinOfferlayout = view.findViewById(R.id.LinOfferlayout);

        Button btnUpdateApp = view.findViewById(R.id.btnUpdateApp);
        TextView tvNotNow = view.findViewById(R.id.txtNotNow);
        updateLayout = view.findViewById(R.id.updateLayout);
        layUltimate1 = view.findViewById(R.id.layoutUltimate1);
        layUltimate2 = view.findViewById(R.id.layoutUltimate2);

        recyclerViewForBrands = view.findViewById(R.id.recyclerViewForBrands);
        recyclerViewForBrands.setNestedScrollingEnabled(false);
        recyclerViewForBrands.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerViewForBrands.setLayoutManager(gridLayoutManager);

        recyclerViewPopCat = view.findViewById(R.id.recyclerViewPopCat);
        recyclerViewPopCat.setNestedScrollingEnabled(false);
        recyclerViewPopCat.setHasFixedSize(true);
        //recyclerViewPopCat.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
        recyclerViewPopCat.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));

        recyclerViewForOfferLayout = view.findViewById(R.id.recyclerViewForOfferLayout);
        recyclerViewForOfferLayout.setNestedScrollingEnabled(false);
        recyclerViewForOfferLayout.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewForOfferLayout.setLayoutManager(mLayoutManager);

        recyclerViewUltimate = view.findViewById(R.id.recyclerViewUltimate);
        recyclerViewUltimate.setNestedScrollingEnabled(false);
        recyclerViewUltimate.setHasFixedSize(true);
        LinearLayoutManager mLayoutManagerUltimate = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewUltimate.setLayoutManager(mLayoutManagerUltimate);

        recyclerViewUltimate1 = view.findViewById(R.id.recyclerViewUltimate1);
        recyclerViewUltimate1.setNestedScrollingEnabled(false);
        recyclerViewUltimate1.setHasFixedSize(true);
        LinearLayoutManager mLayoutManagerUltimate1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewUltimate1.setLayoutManager(mLayoutManagerUltimate1);

        comboOfferRecyclerView = view.findViewById(R.id.comboOfferRecyclerView);
        comboOfferRecyclerView.setHasFixedSize(true);
        comboOfferRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager mComboLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        comboOfferRecyclerView.setLayoutManager(mComboLayoutManager);

        recyclerViewForCategory = view.findViewById(R.id.recyclerViewForCategory);// Set up your RecyclerView with the SectionedRecyclerViewAdapter
        recyclerViewForCategory.setNestedScrollingEnabled(false);
        recyclerViewForCategory.setHasFixedSize(true);

        LinPopCat = view.findViewById(R.id.LinPopCat);

        btnUpdateApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Connectivity.isConnected(getActivity())) {
                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on update app button", getActivity());*/

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=app.apneareamein.shopping&hl=en"));
                    startActivity(intent);
                }
            }
        });

        tvNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on not now button", getActivity());*/
                updateLayout.setVisibility(View.GONE);
            }
        });

        //call main cat
        recyclerView =  view.findViewById(R.id.maincat_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        //LinearLayoutManager mLayoutManagerM = new LinearLayoutManager(getActivity());
        //recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        //recyclerView.setLayoutManager(mLayoutManagerM);

        // getCategoriesData();
        Log.e("Testing Log","Hello commite testing");

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);
        super.onSaveInstanceState(savedInstanceState);

        context = getActivity();

        init(view);

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");
        v_city = prefs.getString("v_city", "");
        v_state = prefs.getString("v_state", "");
        sessionMainCat = prefs.getString("sessionMainCat", "");

        if(v_city.isEmpty()||v_city == null){

            Intent bundle = new Intent(getActivity(), LocationActivity.class);
            bundle.putExtra("tag", "homepage");
            startActivity(bundle);
        }
        //requestFromServerMainCategory();
        getVolleryResStirng();

        fruits_veggies_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bundle = new Intent(getActivity(),BrowseByCategory.class);
                bundle.putExtra("product_SuperCat", "Fruits and Veggies");
                startActivity(bundle);
            }
        });

        grocery_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bundle2 = new Intent(getActivity(),BrowseByCategory.class);
                bundle2.putExtra("product_SuperCat", "Grocery");
                startActivity(bundle2);
            }
        });

        card_feedback_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent feedback = new Intent(getActivity(), FeedbackActivity.class);
                startActivity(feedback);

            }
        });

        card_oneRupee_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                String user_id = prefs.getString("user_id", "");
                String param="/"+user_id+"/"+offer_order_id;

                Uri webpage = Uri.parse(offer_url+param);
                Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                startActivity(myIntent);

            }
        });

        card_refer_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent feedback = new Intent(getActivity(), RefererPoints.class);
                startActivity(feedback);*/
                sendReferral(getActivity());

            }
        });

        card_chicken_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SearchProducts.class);
                intent.putExtra("tag", "next");
                intent.putExtra("selectedName", "Eggs Meat and Chicken");//categoryModelArrayList.get(position).getMaincategory()//Eggs
                //intent.putExtra("selectedName", "Non_Veg");
                startActivity(intent);
            }
        });

        //getCategoriesData(); // from row php
        getBrowseCategory(); // call from Yii framework
        getFeedDetails();
        SyncData();
        //homeCartUpdateDialog();
        //getUserCartCount();

        cartUpdateFlag = prefs.getString("cartUpdateFlag", "false");
        //visitCount = prefs.getString("visitCount", "true");//"No name defined" is the default value.
        if(cartUpdateFlag.equalsIgnoreCase("false")){
            try {
                homeCartUpdateDialog();
                //visitCounter();
            }catch (Exception e){
                e.getMessage();
            }
            SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("cartUpdateFlag", "true");
            editor.apply();
            editor.commit();
        }


        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {

                }catch (Exception e){

                }
            }
        },500);*/

        return view;
    }



    //this is for sync the cart count
    private int SyncData() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            GateWay gateWay = new GateWay(getActivity());

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("cartCountRes:",""+response.toString());

                        int value = response.getInt("normal_cart_count");

                        ((BaseActivity) getActivity()).updateAddToCartCount(value);
                        CartCount = response.getInt("normal_cart_count");

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
           // AppController.getInstance().setPriority(Request.Priority.LOW);
            AppController.getInstance().addToRequestQueue(request);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    //600000,
                    //DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getActivity()).add(request);
        }
        return CartCount;
    }

    private void openCartDialog2() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("You have items in Cart");
        builder.setMessage("You already have some previously \n" +
                "selected items in your Cart.\n" +
                "Pls check / clear before proceeding.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        updateStatusOfCartDialog("goto_cart");
                    }
                })
                .setNegativeButton("Clear Cart", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        EmptyCartAlertDialog();
                    }
                })
                .setNeutralButton("Remind Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        updateStatusOfCartDialog("remind_later");
                    }
                });
        alert = builder.create();
        alert.show();
    }

    public void openCartDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout_popup_default);

        Button btn_zero = dialog.findViewById(R.id.btn_zero);
        Button btn_one = dialog.findViewById(R.id.btn_one);
        Button btn_two = dialog.findViewById(R.id.btn_two);
        TextView tv_msg1 = dialog.findViewById(R.id.tv_msg1);
        TextView tv_msg2 = dialog.findViewById(R.id.tv_msg2);
        TextView tv_msg3 = dialog.findViewById(R.id.tv_msg3);


        btn_two.setText("OK");
        btn_one.setText("Later");
        btn_zero.setText("Clear Cart");

        tv_msg1.setText("\n You have items in Cart \n");

        tv_msg2.setText("You already have some previously \n" +
                "selected items in your Cart.\n\n" +
                "Pls check / clear before proceeding.");

        btn_zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        btn_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                updateStatusOfCartDialog("remind_later");
            }
        });
        btn_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    dialog.dismiss();
                    updateStatusOfCartDialog("goto_cart");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();

    }

    //show dialog when If he tries to select something in other category
    private void openSessionDialog(String message,String type)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sessoin);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        TextView tv_clearcart = dialog.findViewById(R.id.tv_clearcart);
        TextView tv_continue = dialog.findViewById(R.id.tv_continue);
        tv_message.setText(message);
        if(type.equalsIgnoreCase("promo")){
            tv_clearcart.setVisibility(View.GONE);
        }
        tv_clearcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EmptyCartAlertDialog();
            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void updateStatusOfCartDialog(String tag) {
        GateWay gateWay = new GateWay(getActivity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("contact", gateWay.getContact());
            jsonObject.put("tag", tag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdateStatusOfCartDialog, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("key").equals("goto_cart")) {
                        Intent intent = new Intent(getActivity(), AddToCart.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void EmptyCartAlertDialog2() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.app_name);
        builder.setMessage("Remove all the products from cart?")
                .setCancelable(false)
                .setPositiveButton("EMPTY CART", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Connectivity.isConnected(getActivity())) {
                            deleteAllProductFromCartItem();
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            GateWay gateWay = new GateWay(getActivity());
                            gateWay.displaySnackBar(homeMainLayout);
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void EmptyCartAlertDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout_popup_default2);

        Button btn_zero = dialog.findViewById(R.id.btn_zero);
        Button btn_one = dialog.findViewById(R.id.btn_one);
        btn_zero.setVisibility(View.GONE);
        Button btn_two = dialog.findViewById(R.id.btn_two);
        TextView tv_msg1 = dialog.findViewById(R.id.tv_msg1);
        TextView tv_msg2 = dialog.findViewById(R.id.tv_msg2);
        TextView tv_msg3 = dialog.findViewById(R.id.tv_msg3);


        btn_two.setText("EMPTY CART");
        btn_one.setText("NO");

        tv_msg1.setText("\n PICODEL Online Grocery Shopping \n");
        tv_msg2.setText("Remove all the products from cart?");

        btn_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    dialog.dismiss();
                    deleteAllProductFromCartItem();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();

    }

    private void deleteAllProductFromCartItem() {
        if (Connectivity.isConnected(getActivity())) {
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "delete_all_items");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        DBHelper db = new DBHelper(getActivity());
                        db.deleteOnlyCartTable();
                        db.NormalCartDeleteAll();
                        //after remove cart items from local and online db
                        //then sync online cart count again
                        SyncData();
                        //getUserCartCount();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    error.printStackTrace();

                    //GateWay gateWay = new GateWay(getActivity());
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    //from this method fetch all home page data
    private void requestFromServerMainCategory() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            customAdapterForBrand = new CustomAdapterForBrand(getActivity());
            popCatAdapter = new PopCatAdapter();
            customAdapterForComboOffer = new CustomAdapterForComboOffer(getActivity());

            productImage = new ArrayList<>();
            productImage2 = new ArrayList<>();
            event_array_url = new ArrayList<>();
            productImageCategory = new ArrayList<>();
            productImageCategory2 = new ArrayList<>();
            cashBackBanner = new ArrayList<>();
            eventImgPath = new ArrayList<>();
            eventLink = new ArrayList<>();
            offerCodeHashMap = new HashMap();
            ProductWiseComboOffer = new ArrayList<>();
            FinalComboOffer = new ArrayList<>();

            //header product footer
            tempProductListHashMap = new LinkedHashMap();
            tempProductWiseList = new ArrayList<>();
            FinalList2 = new ArrayList<>();
            imgPath = new ArrayList<>();
            catName = new ArrayList<>();
            //header product footer
            //mostly bought & recommended products
            FinalList = new ArrayList<>();
            FinalList1 = new ArrayList<>();
            //mostly bought & recommended products

            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
             Zone_Area = prefs.getString("Zone_Area", "");
            v_city = prefs.getString("v_city", "");
            v_state = prefs.getString("v_state", "");

            JSONObject params = new JSONObject();
            try {
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
                params.put("contact", gateWay.getContact());
                params.put("vcity", v_city);
                params.put("vstate", v_state);
                params.put("areaname", Zone_Area);
                params.put("os", "android");
                params.put("user_version_name", ApplicationUrlAndConstants.versionName);
                Log.e("HomePage11_parma:",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("deleteAllCartItem",""+params);
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlHomePage11, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.e("HomePageRes:",""+response.toString());

                        String coupon_code_status = response.getString("coupon_code_status");
                        String cart_dailog_status = response.getString("cart_dailog_status");
                        cash_back_points = response.getString("cash_back_points");
                        noti_count = response.getString("noti_count");
                        version_status = response.getString("version_status");

                        /*if (cart_dailog_status.equals("active")) {
                            //db.fetchAddToCartCount();
                            *//*final DBHelper db = new DBHelper(context);
                            int count = (int) db.fetchAddToCartCount();
                            //((BaseActivity) getActivity()).updateAddToCartCount(count);
                            if (count >= 0) {
                                openCartDialog();
                            }*//*
                            int count = (int) SyncData();
                            if (count >= 0) {
                                openCartDialog();
                            }
                        }*/
                       /* if (coupon_code_status.equals("active")) {
                            openCouponCodeDialog();
                        }*/
                        //String valid_shop = response.getString("valid_shop");
                        //String shop_type = response.getString("shop_type");
                      /*  try {
                            //this is for the condition wise visible and gone the navigation menus
                            switch (shop_type) {
                                case "Autowala":
                                    if (valid_shop.equals("yes")) {
                                        ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_Auto).setVisible(true);
                                        ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_GetAuto).setVisible(false);
                                       // ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_AddShop).setVisible(false);
                                    }
                                    break;
                                case "Fulfillment Partner":
                                    if (valid_shop.equals("yes")) {
                                        ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_Shopkeeper).setVisible(true);
                                        ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_GetAuto).setVisible(false);
                                       // ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_AddShop).setVisible(false);
                                    }
                                    break;
                                default:
                                    ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_Auto).setVisible(false);
                                    ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_Shopkeeper).setVisible(false);
                                    //this is for the user
                                    ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_GetAuto).setVisible(true);
                                   // ((BaseActivity) getActivity()).nav_Menu.findItem(R.id.nav_AddShop).setVisible(true);
                                    break;
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }*/

                        try {
                            if (noti_count.equals("0")) {
                                ((BaseActivity) getActivity()).tvNotification.setVisibility(View.INVISIBLE);
                                ((BaseActivity) getActivity()).imgNotify.setVisibility(View.GONE);
                            } else {
                                ((BaseActivity) getActivity()).tvNotification.setVisibility(View.VISIBLE);
                                ((BaseActivity) getActivity()).imgNotify.setVisibility(View.VISIBLE);
                                ((BaseActivity) getActivity()).tvNotification.setText(noti_count);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        try {
                            //  ((BaseActivity) getActivity()).tvCashBackPoints.setVisibility(View.VISIBLE);
                            //  ((BaseActivity) getActivity()).tvCashBackPoints.setText(cash_back_points);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        if (version_status.equals("update")) {
                            updateLayout.setVisibility(View.VISIBLE);
                            appUpdate();
                        } else {
                            updateLayout.setVisibility(View.GONE);
                        }

                        if (!response.isNull("event")) {
                            JSONArray jsonArray1 = response.getJSONArray("event");
                            for (int i = 0; i < jsonArray1.length(); i++) {
                                JSONObject jsonEventObject = (JSONObject) jsonArray1.get(i);
                                eventImgPath.add(jsonEventObject.getString("event_image"));
                                eventLink.add(jsonEventObject.getString("event_link"));
                                event_status = jsonEventObject.getString("event_status");
                            }
                        }

                        //Browser link here
                        if (event_status.contains("deactive")) {
                            eventLinearLayout.setVisibility(View.GONE);
                        } else {
                            eventLinearLayout.setVisibility(View.VISIBLE);

                            //TODO here event image setup to glide
                           /* Glide.with(context)
                                    .load(eventImgPath.get(0))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(eventImage);*/
                            Picasso.with(context)
                                    .load(eventImgPath.get(0))
                                    .into(eventImage);

                            eventLinearLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(eventLink.get(0)));
                                    startActivity(i);
                                }
                            });
                        }

                        if (!response.isNull("cashback_banner")) {
                            JSONArray jsonArray2 = response.getJSONArray("cashback_banner");
                            for (int i = 0; i < jsonArray2.length(); i++) {
                                JSONObject jsonCashbackBannerObject = (JSONObject) jsonArray2.get(i);
                                cashBackBanner.add(jsonCashbackBannerObject.getString("cashback"));
                                status = jsonCashbackBannerObject.getString("status");
                            }
                        }

                        //Displayed dialog here

                      /*  if (status.equals("true")) {
                        } else {
                            if (getActivity() != null) {
                                setCashBackImage();
                            }
                        }
*/
                        if (!response.isNull("front_banner")) {
                            JSONArray jsonArray4 = response.getJSONArray("front_banner");
                            for (int i = 0; i < jsonArray4.length(); i++) {
                                JSONObject jsonImageObject = (JSONObject) jsonArray4.get(i);
                                productImage.add(jsonImageObject.getString("front_search_banner")); //add to arrayList
                                productImageCategory.add(jsonImageObject.getString("front_search_category")); //add to arrayList
                            }
                            if (productImage.size() > 0) {
                                AnimatedSlideShow(); //set the banner images
                            }
                        } else {
                            viewPager.setVisibility(View.GONE);
                        }

                        //this is for popular category
                        if (!response.isNull("popularcategory")) {
                            LinPopCat.setVisibility(View.GONE);
                            tvShopByPopCategory.setText(response.getString("popularCategoryText"));
                            JSONArray jsonArray5 = response.getJSONArray("popularcategory");

                            Log.e("popularcat_pstatus:",""+jsonArray5.toString());

                            for (int i = 0; i < jsonArray5.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) jsonArray5.get(i);

                                tvShopByPopCategory.setVisibility(View.VISIBLE);

                                Movie movie = new Movie(jsonMainCatObject.getString("popularCategory"), jsonMainCatObject.getString("subcategory_image"));
                                Log.d("popularcategory",jsonMainCatObject.getString("subcategory_image"));
                                popCatAdapter.add(movie);
                            }
                            recyclerViewPopCat.setAdapter(popCatAdapter);
                        }

                        try {
                            if (!response.isNull("master_search")) {
                                JSONArray mainClassificationJsonArray1 = response.getJSONArray("master_search");
                                String title1 = response.getString("recommended_title");
                                layUltimate2.setVisibility(View.VISIBLE);
                                tvUltimate1.setText(title1);

                                Log.e("master_search_pstatus:",""+response.toString());
                                String title2 = response.getString("combo_offer");
                                Log.e("combo_offer:",""+title2);
                                for (int i = 0; i < mainClassificationJsonArray1.length(); i++) {
                                    JSONObject jSonClassificationData = mainClassificationJsonArray1.getJSONObject(i);

                                    productListHashMap1.put(jSonClassificationData.getString("code"), "");

                                    productWiseList1.add(new UltimateProductList(jSonClassificationData.getString("code"),
                                            jSonClassificationData.getString("product_id"),
                                            jSonClassificationData.getString("shop_id"),
                                            jSonClassificationData.getString("deliveryavailable"),
                                            jSonClassificationData.getString("product_name"),
                                            jSonClassificationData.getString("product_image"),
                                            jSonClassificationData.getString("product_mrp"),
                                            jSonClassificationData.getString("product_price"),
                                            jSonClassificationData.getString("product_discount"),
                                            jSonClassificationData.getString("product_size"),
                                            jSonClassificationData.getString("pName"),
                                            jSonClassificationData.getString("product_maincat"),//cart_pstatus
                                            jSonClassificationData.getString("product_hindi_name"),
                                            jSonClassificationData.getString("cart_pstatus")));//jSonClassificationData.getString("cart_pstatus")
                                }

                                for (Object o : productListHashMap1.keySet()) {
                                    String key = (String) o;
                                    productCodeWithProductList withProductCode1 = new productCodeWithProductList();
                                    withProductCode1.code = key;
                                    withProductCode1.productCodeWiseProducts = new ArrayList<>();
                                    for (UltimateProductList pp : productWiseList1) {
                                        if (pp.code.equals(key)) {
                                            withProductCode1.productCodeWiseProducts.add(pp);
                                        }
                                    }
                                    FinalList1.add(withProductCode1);
                                }
                                ultimateCardAdapter1 = new UltimateCardAdapter1(FinalList1);
                                recyclerViewUltimate1.setAdapter(ultimateCardAdapter1);
                            } else {
                                layUltimate2.setVisibility(View.GONE);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        if (!response.isNull("mostly_bought")) {
                            JSONArray mainClassificationJsonArray2 = response.getJSONArray("mostly_bought");
                            String title2 = response.getString("mostly_bought_title");
                            layUltimate1.setVisibility(View.VISIBLE);
                            tvUltimate.setText(title2);


                            Log.e("mostly_bought_pstatus:",""+mainClassificationJsonArray2.toString());


                            for (int i = 0; i < mainClassificationJsonArray2.length(); i++) {
                                JSONObject jSonClassificationData = mainClassificationJsonArray2.getJSONObject(i);

                                productListHashMapfor_mr_products.put(jSonClassificationData.getString("code"), "");

                                productWiseList.add(new UltimateProductList(jSonClassificationData.getString("code"),
                                        jSonClassificationData.getString("product_id"),
                                        jSonClassificationData.getString("shop_id"),
                                        jSonClassificationData.getString("deliveryavailable"),
                                        jSonClassificationData.getString("product_name"),
                                        jSonClassificationData.getString("product_image"),
                                        jSonClassificationData.getString("product_mrp"),
                                        jSonClassificationData.getString("product_price"),
                                        jSonClassificationData.getString("product_discount"),
                                        jSonClassificationData.getString("product_size"),
                                        jSonClassificationData.getString("pName"),
                                        jSonClassificationData.getString("product_maincat"), //cart_pstatus
                                        jSonClassificationData.getString("product_hindi_name"),
                                        jSonClassificationData.getString("cart_pstatus")));//jSonClassificationData.getString("cart_pstatus")
                            }

                            for (Object o : productListHashMapfor_mr_products.keySet()) {
                                String key = (String) o;
                                productCodeWithProductList withProductCode = new productCodeWithProductList();
                                withProductCode.code = key;
                                withProductCode.productCodeWiseProducts = new ArrayList<>();
                                for (UltimateProductList pp : productWiseList) {
                                    if (pp.code.equals(key)) {
                                        withProductCode.productCodeWiseProducts.add(pp);
                                    }
                                }
                                FinalList.add(withProductCode);
                            }
                            ultimateCardAdapter = new UltimateCardAdapter(FinalList);
                            recyclerViewUltimate.setAdapter(ultimateCardAdapter);
                        } else {
                            layUltimate1.setVisibility(View.GONE);
                        }

                        //this is for three products layout
                        if (response.isNull("productData1")) {
                            LinOfferlayout.setVisibility(View.GONE);
                        } else {
                            LinOfferlayout.setVisibility(View.VISIBLE);
                            JSONArray jsonArray6 = response.getJSONArray("productData1");
                            Log.e("ComboOfferList:",""+jsonArray6.toString());

                            for (int r = 0; r < jsonArray6.length(); r++) {
                                JSONObject jsonProductDataOneObject = (JSONObject) jsonArray6.get(r);

                                offerCodeHashMap.put(jsonProductDataOneObject.getString("selectedName1"), "");

                                ProductWiseComboOffer.add(new OfferCodeWiseProduct(jsonProductDataOneObject.getString("product_name1"), jsonProductDataOneObject.getString("product_image1"),
                                        jsonProductDataOneObject.getString("product_mrp1"), jsonProductDataOneObject.getString("product_price1"),
                                        jsonProductDataOneObject.getString("product_discount1"), jsonProductDataOneObject.getString("selectedName1"),
                                        jsonProductDataOneObject.getString("topProductsTextOne"), jsonProductDataOneObject.getString("product_name"),
                                        jsonProductDataOneObject.getString("product_maincat"),
                                        jsonProductDataOneObject.getString("product_brand"),
                                        jsonProductDataOneObject.getString("shop_id"),
                                        "0"));
                            }
                            for (Object o : offerCodeHashMap.keySet()) {
                                String key = (String) o;

                                comboOfferWithOfferCode withOfferCode = new comboOfferWithOfferCode();
                                withOfferCode.selectedName1 = key;
                                withOfferCode.offerCodeWiseProducts = new ArrayList<>();
                                for (OfferCodeWiseProduct pp : ProductWiseComboOffer) {
                                    if (pp.getOfferCategory().equals(key)) {
                                        withOfferCode.offerCodeWiseProducts.add(pp);
                                    }
                                }
                                FinalComboOffer.add(withOfferCode);
                            }
                            customAdapterForCategory = new CustomAdapterForCategory(getActivity(), FinalComboOffer);
                            recyclerViewForOfferLayout.setAdapter(customAdapterForCategory);
                        }

                        //this is for section header and footer
                        if (!response.isNull("posts")) {
                            Log.e("product_data_pstatus:",""+response.toString());
                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");

                            Log.e("post_pstatus:",""+mainClassificationJsonArray.toString());


                            JSONArray headerFooterJsonArray = response.getJSONArray("header_footer");

                            recyclerViewForCategory.setVisibility(View.VISIBLE);
                            for (int i = 0; i < headerFooterJsonArray.length(); i++) {
                                JSONObject jSonHeaderFooterData;
                                try {
                                    jSonHeaderFooterData = headerFooterJsonArray.getJSONObject(i);
                                    catName.add(jSonHeaderFooterData.getString("catName"));
                                    imgPath.add(jSonHeaderFooterData.getString("imagePath"));
                                    Log.e("Header_imag:",""+jSonHeaderFooterData.getString("imagePath"));
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {


                                JSONObject jSonClassificationData = mainClassificationJsonArray.getJSONObject(i);

                                tempProductListHashMap.put(jSonClassificationData.getString("code"), "");

                                tempProductWiseList.add(new CategoryList(jSonClassificationData.getString("code"),

                                        jSonClassificationData.getString("shop_id"),
                                        jSonClassificationData.getString("shop_name"),
                                        jSonClassificationData.getString("product_id"),
                                        jSonClassificationData.getString("product_name"),
                                        jSonClassificationData.getString("product_hindi_name"),
                                        jSonClassificationData.getString("product_image"),
                                        jSonClassificationData.getString("product_mrp"),
                                        jSonClassificationData.getString("product_price"),
                                        jSonClassificationData.getString("product_discount"),
                                        jSonClassificationData.getString("pName"),
                                        jSonClassificationData.getString("totalSize"),
                                        jSonClassificationData.getString("product_maincat"),
                                        jSonClassificationData.getString("product_brand"),
                                        jSonClassificationData.getString("type"),
                                        jSonClassificationData.getString("cart_pstatus")));//cart_pstatus
                            }
                            productListHashMap = tempProductListHashMap;
                            ProductWiseList = tempProductWiseList;

                            if (ProductWiseList.size() > 0) {
                                for (Object o : productListHashMap.keySet()) {
                                    String key = (String) o;
                                    productCodeWithProductList1 withProductCode = new productCodeWithProductList1();
                                    withProductCode.code = key;
                                    withProductCode.productCodeWiseProducts = new ArrayList<>();
                                    for (CategoryList pp : ProductWiseList) {
                                        if (pp.code.equals(key)) {
                                            withProductCode.productCodeWiseProducts.add(pp);
                                        }
                                    }
                                    FinalList2.add(withProductCode);
                                }
                                CountSectionAdapter adapter = new CountSectionAdapter(context, imgPath, catName, FinalList2);
                                recyclerViewForCategory.setAdapter(adapter);

                                GridLayoutManager layoutManager = new GridLayoutManager(context, 1, LinearLayoutManager.VERTICAL, false);
                                SectionedSpanSizeLookup lookup = new SectionedSpanSizeLookup(adapter, layoutManager);
                                layoutManager.setSpanSizeLookup(lookup);
                                recyclerViewForCategory.setLayoutManager(layoutManager);
                            }
                        } else {
                            recyclerViewForCategory.setVisibility(View.GONE);
                        }

                        //this is for combo offer
                        if (response.isNull("combo_offer")) {
                            comboofferLinearLayout.setVisibility(View.GONE);
                        } else {
                            comboofferLinearLayout.setVisibility(View.VISIBLE);
                            try {
                                JSONArray comboofferJsonArray = response.getJSONArray("combo_offer");

                                Log.e("combo_offer_pstatus:",""+comboofferJsonArray.toString());


                                String comboOfferHeader = response.getString("comboOfferHeader");
                                String comboOfferfade = response.getString("comboOfferFade");

                                if (comboOfferfade.equals("")) {
                                    tvComboNote.setVisibility(View.GONE);
                                } else {
                                    tvComboNote.setText(comboOfferfade);
                                }
                                tvComboOfferStart.setVisibility(View.VISIBLE);
                                tvComboOfferStart.setText(comboOfferHeader);



                                Log.e("ComboOfferList:",""+comboofferJsonArray.toString());

                                for (int i = 0; i < comboofferJsonArray.length(); i++) {
                                    JSONObject jSonClassificationData = comboofferJsonArray.getJSONObject(i);

                                    comboList movie = new comboList(jSonClassificationData.getString("offer_name"),
                                            jSonClassificationData.getString("total_price"),
                                            jSonClassificationData.getString("offer_price"),
                                            jSonClassificationData.getString("discount_in_percent"),
                                            jSonClassificationData.getString("combo_image"),
                                            jSonClassificationData.getString("shop_id"),
                                            jSonClassificationData.getString("product_maincat"),
                                            jSonClassificationData.getString("offer_end_date"),
                                            jSonClassificationData.getString("cart_pstatus"));//cart_pstatus
                                    customAdapterForComboOffer.add(movie);

                                }
                                comboOfferRecyclerView.setAdapter(customAdapterForComboOffer);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        //this is for brand
                        if (response.isNull("brand")) {
                            LinShopByBrand.setVisibility(View.GONE);
                        } else {
                            JSONArray jsonArray3 = response.getJSONArray("brand");
                            Log.d("Brandlist", "jsonArray3"+jsonArray3);
                            tvShopByBrand.setVisibility(View.VISIBLE);
                            tvShopByBrand.setText(response.getString("shopByBrandsText"));
                            for (int k = 0; k < jsonArray3.length(); k++) {
                                JSONObject jsonImageObject = (JSONObject) jsonArray3.get(k);
                                Movie movie = new Movie(jsonImageObject.getString("brand_images"),
                                        jsonImageObject.getString("brand_name"));
                                customAdapterForBrand.add(movie);
                            }

                            if (tvShopByBrand.getText().toString().length() > 0) {
                                recyclerViewForBrands.setAdapter(customAdapterForBrand);
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("HomepageEXP:",""+e.toString());
                        Log.e("HomepageEXP:",""+e.getMessage());
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    Log.e("EXP_HOME:",""+error.getMessage());
                    Log.e("EXP_HOME:",""+error.toString());
                    error.printStackTrace();

                    //GateWay gateWay = new GateWay(getActivity());
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            AppController.getInstance().setPriority(Request.Priority.HIGH);
            AppController.getInstance().addToRequestQueue(request);

            //getCategoriesData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        ((BaseActivity) getActivity()).toolbar.setVisibility(View.GONE);
        ((BaseActivity) getActivity()).search_bar.setVisibility(View.VISIBLE);

        BaseActivity.bottomNavigationView.getMenu().findItem(R.id.navigation_home).setCheckable(true);
        hideKeyboard(getActivity());

        if (Connectivity.isConnected(getActivity())) {
            GateWay gateWay = new GateWay(getActivity());

            /*SyncNormalCartItems UT1 = new SyncNormalCartItems(SyncNormalCartItems.context);
            UT1.syncNormalCartItems(gateWay.getContact(), gateWay.getUserEmail(), getActivity());*/

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());*/

            SyncData(); //this is from onResume() method
            //getUserCartCount();

        }

    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //from this method we open the cashback banner dialog
    private void setCashBackImage() {
        dialog = DialogPlus.newDialog(context)
                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_cashback_image))
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setPadding(20, 20, 20, 20)
                .create();
        dialog.show();
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        TextView txtCashBackMessage = (TextView) dialog.findViewById(R.id.txtCashBackMessage);
        ImageView imgCashBack = (ImageView) dialog.findViewById(R.id.imgCashBack);
        ImageView imgCancel = (ImageView) dialog.findViewById(R.id.imgCancel);

        Pattern p = Pattern.compile(ApplicationUrlAndConstants.URL_REGEX);
        Matcher m = p.matcher(cashBackBanner.get(0));//replace with string to compare
        if (m.find()) {
            imgCashBack.setVisibility(View.VISIBLE);
            txtCashBackMessage.setVisibility(View.GONE);
            btnOk.setVisibility(View.GONE);
            //TODO here cashback banner setup to glide
           /* Glide.with(context).load(cashBackBanner.get(0))
                    .thumbnail(Glide.with(context).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgCashBack);*/
            Picasso.with(context).load(cashBackBanner.get(0))
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(imgCashBack);


        } else {
            imgCashBack.setVisibility(View.GONE);
            txtCashBackMessage.setVisibility(View.VISIBLE);
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sentCashBackStatus();
                    dialog.dismiss();
                    Intent intent = new Intent(getActivity(), PromotionalPartner.class);
                    intent.putExtra("cash_back_points", cash_back_points);
                    startActivity(intent);
                }
            });
            txtCashBackMessage.setText(cashBackBanner.get(0));
        }

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (getActivity() != null) {
                    sentCashBackStatus();
                }
            }
        });
    }

    //from this method we update the status of cashback banner dialog
    private void sentCashBackStatus() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            GateWay gateWay = new GateWay(getActivity());

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCashBackTracking, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(homeMainLayout);
        }
    }

    private void openCouponCodeDialog() {
        try {
            couponCodeDialog = DialogPlus.newDialog(context)
                    .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_enter_coupons))
                    .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                    .setGravity(Gravity.CENTER)
                    .setCancelable(false)
                    .setPadding(10, 10, 10, 10)
                    .create();
            couponCodeDialog.show();

            editCouponCode = (EditText) couponCodeDialog.findViewById(R.id.editCouponCode);
            Button btnSubmit = (Button) couponCodeDialog.findViewById(R.id.btnSubmit);

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GateWay gateWay = new GateWay(getActivity());
                    couponCode = editCouponCode.getText().toString();
                    oneTimeCallMethod(gateWay.getContact(), couponCode);
                    hideKeyboard(getActivity());
                }
            });
            Button btnClose = (Button) couponCodeDialog.findViewById(R.id.btnClose);
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    couponCodeDialog.dismiss();
                    updateCouponCodeStatus();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCouponCodeStatus() {
        if (Connectivity.isConnected(getActivity())) {
            GateWay gateWay = new GateWay(getActivity());
            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("tag", "status_false");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCashBackTracking, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getString("posts").equals("true")) {
                            Intent intent = new Intent(getActivity(), PromotionalPartner.class);
                            intent.putExtra("cash_back_points", cash_back_points);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(homeMainLayout);
        }
    }

    //when user put coupon code and submit then this method call
    private void oneTimeCallMethod(String contact, String couponCode) {
        JSONObject params = new JSONObject();
        try {
            params.put("contact", contact);
            params.put("couponCode", couponCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlOneTimeCallMethod, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("posts").equals("true")) {
                        couponCodeDialog.dismiss();
                        Toast.makeText(getActivity(), "Successfully Applied!!!", Toast.LENGTH_LONG).show();
                        hideKeyboard(getActivity());
                    } else {
                        couponCodeDialog.dismiss();
                        Toast.makeText(getActivity(), response.getString("posts"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    //from this method we set sliding banner to adapter
    @SuppressLint("NewApi")
    private void AnimatedSlideShow() {
        try {
            //start----- this is for bannerint currentPage = 0;

            SlidingImages_Adapter slidingImages_adapter = new SlidingImages_Adapter(productImage,"other");
            viewPager.setAdapter(slidingImages_adapter);
            //viewPager.setCurrentItem(slidingImages_adapter.getCount()-1);
            LoopingCirclePageIndicator circlePageIndicator = new LoopingCirclePageIndicator(getActivity());
            circlePageIndicator.setViewPager(viewPager);
            mFrameLayout.addView(circlePageIndicator);

            //end----- this is for banner
            setupAutoPager(productImage.size());

            // for the new banners

            SlidingImages_Adapter slidingImages_adapter2 = new SlidingImages_Adapter(productImage2,"shop");
            viewPager2.setAdapter(slidingImages_adapter2);
            //viewPager2.setCurrentItem(slidingImages_adapter.getCount()-1);
            LoopingCirclePageIndicator circlePageIndicator2 = new LoopingCirclePageIndicator(getActivity());
            circlePageIndicator.setViewPager(viewPager2);
            mFrameLayout.addView(circlePageIndicator2);
            setupAutoPager2(productImage2.size());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //this is for mostly bought product section
    private class UltimateCardAdapter extends RecyclerView.Adapter<MainViewHolder> {

        int selectedPosition;
        ArrayList<productCodeWithProductList> mItems;

        UltimateCardAdapter(ArrayList<productCodeWithProductList> finalList) {
            mItems = finalList;
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_for_ultimate_products, parent, false);
            final MainViewHolder childViewHolder = new MainViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            int itemPosition = recyclerViewUltimate.getChildAdapterPosition(v);
                            productCodeWithProductList movie = mItems.get(itemPosition);
                            selectedPosition = movie.getPosition();
                            UltimateProductList tp = movie.productCodeWiseProducts.get(selectedPosition);

                           /* UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": UltimateCardAdapter view clicked on product: " + tp.getStr_product_name(), getActivity());*/

                            if (!tp.getStr_PName().equals("")) {
                                Intent intent = new Intent(getActivity(), SingleProductInformation.class);
                                intent.putExtra("product_name", tp.getStr_product_name());
                                intent.putExtra("shop_id", tp.getStr_ShopId());
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(getView());
                    }
                }
            });
            return childViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder holder, final int position) {
            final DBHelper db = new DBHelper(getActivity());

            ArrayList<String> sizesArrayList = new ArrayList<>();
            ArrayList<String> pricesArrayList = new ArrayList<>();
            ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

            productCodeWithProductList tp = mItems.get(position);
            UltimateProductList movie = tp.productCodeWiseProducts.get(0);

            /*HashMap AddToCartInfo = db.getCartDetails(movie.getStr_PId());
            String btnCheckStatus = (String) AddToCartInfo.get("new_pid");*/

            String server_cart_status = movie.getCart_pstatus();

            if (server_cart_status.equalsIgnoreCase("0")) {
                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
            } else {
                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
            }

            //TODO here mostly bought product image setup to glide
            /*Glide.with(getActivity()).load(movie.getStr_PImg())
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProduct);*/

            Picasso.with(getActivity()).load(movie.getStr_PImg())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgProduct);




            holder.tvProductName.setText(movie.getStr_PName());
            if (!movie.getStr_product_hindiname().equals("")) {
                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                holder.tvProductHindiName.setText(movie.getStr_product_hindiname());
            } else {
                holder.tvProductHindiName.setVisibility(View.GONE);
            }

            String dis = movie.getStr_PDiscount();
            holder.tvDiscount.setText(dis + "% OFF");
            String productMrp = movie.getStr_PMrp();
            String discountPrice = movie.getStr_Pprice();
            if (productMrp.equals(discountPrice)) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvMrp.setVisibility(View.GONE);
            } else {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.VISIBLE);
                holder.tvMrp.setVisibility(View.VISIBLE);
                holder.tvMrp.setBackgroundResource(R.drawable.dash);
            }
            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp1 = newProductList.productCodeWiseProducts.get(l);
                    sizesArrayList.add(tp1.getStr_PSize());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp3 = newProductList.productCodeWiseProducts.get(l);
                    DiscountpricesArrayList.add(tp3.getStr_Pprice());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp2 = newProductList.productCodeWiseProducts.get(l);
                    pricesArrayList.add(tp2.getStr_PMrp());

                    if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                        holder.LayoutSpinner.setVisibility(View.VISIBLE);
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), pricesArrayList, DiscountpricesArrayList, sizesArrayList, "homepage");
                        holder.spinner.setAdapter(customSpinnerAdapter);
                    } else {
                        holder.LayoutSpinner.setVisibility(View.INVISIBLE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            productCodeWithProductList tp = mItems.get(holder.getAdapterPosition());
                            UltimateProductList movie = tp.productCodeWiseProducts.get(pos);

                            tp.setPosition(pos); //set here position when user any wants to buy multiple size products

                            HashMap AddToCartInfo = db.getCartDetails(movie.getStr_PId());
                            String btnCheckStatus = (String) AddToCartInfo.get("new_pid");

                            String server_cart_status = movie.getCart_pstatus();//tp.productCodeWiseProducts.get(position).getCart_pstatus();
                            if (server_cart_status.equalsIgnoreCase("0")) {
                                movie.setStatus(false);
                                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                            } else {
                                movie.setStatus(true);
                                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                            }

                            //TODO here mostly bought product image setup to glide when user select different product variant from spinner
                            /*Glide.with(getActivity()).load(movie.getStr_PImg())
                                    .error(R.drawable.ic_app_transparent)
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(holder.imgProduct);*/

                            Picasso.with(getActivity()).load(movie.getStr_PImg())
                                    .placeholder(R.drawable.loading)
                                    .error(R.drawable.ic_app_transparent)
                                    .into(holder.imgProduct);


                            holder.tvProductName.setText(movie.getStr_PName());

                            if (!movie.getStr_product_hindiname().equals("")) {
                                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                                holder.tvProductHindiName.setText(movie.getStr_product_hindiname());
                            } else {
                                holder.tvProductHindiName.setVisibility(View.GONE);
                            }
                            String dis = movie.getStr_PDiscount();
                            holder.tvDiscount.setText(dis + "% OFF");
                            String productMrp = movie.getStr_PMrp();
                            String discountPrice = movie.getStr_Pprice();
                            if (productMrp.equals(discountPrice)) {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.tvDiscount.setVisibility(View.INVISIBLE);
                                holder.tvMrp.setVisibility(View.GONE);
                            } else {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.tvDiscount.setVisibility(View.VISIBLE);
                                holder.tvMrp.setVisibility(View.VISIBLE);
                                holder.tvMrp.setBackgroundResource(R.drawable.dash);
                            }
                            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            UltimateProductList forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": UltimateCardAdapter clicked on add to cart & product is: " + forAddToCart.getStr_PName(), getActivity());*/

                            shopId = forAddToCart.getStr_ShopId();
                            pId = forAddToCart.getStr_PId();
                            //call Popup to show veg 300 or grocery 500
                            String maincat_type = movie.productCodeWiseProducts.get(selectedPosition).getStr_product_maincat();

                            /*HashMap AddToCartInfo;
                            AddToCartInfo = db.getCartDetails(forAddToCart.getStr_PId());
                            strId = (String) AddToCartInfo.get("new_pid");*/
                            String cart_status = forAddToCart.getCart_pstatus();
                            if(cart_status.equalsIgnoreCase("1")){
                                Intent intent = new Intent(getActivity(), AddToCart.class);
                                startActivity(intent);
                            }

                            String toastMessg = forAddToCart.getStr_product_name();
                            addCartItemsToServer(pId, shopId,maincat_type,toastMessg);
                            forAddToCart.setStatus(false);

                            /* if (strId == null) {
                               //forAddToCart.setStatus(false);

                                db.insertCount(pId);

                                int count = (int) db.fetchAddToCartCount();
                                ((BaseActivity) getActivity()).updateAddToCartCount(count);

                                if (count >= 0) {
                                    holder.btnAddToCart.setText("Go To Cart");

                                } else {
                                    holder.btnAddToCart.setText("Add To Cart");
                                }
                                Log.e("CartCount:",""+count);
                            } else {
                                Intent intent = new Intent(getActivity(), AddToCart.class);
                                startActivity(intent);
                            } */
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }

                private void addCartItemsToServer(final String pId, String shopId, String maincat_type, final String toastMessg) { //TODO Server method here
                    GateWay gateWay = new GateWay(getActivity());

                    JSONObject params = new JSONObject();
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shopId);
                        params.put("areaname", Zone_Area);
                        params.put("v_city", v_city);
                        params.put("v_state", v_state);

                        params.put("selectedType", "");
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", gateWay.getContact());
                        params.put("email", gateWay.getUserEmail());

                        if(sessionMainCat.equalsIgnoreCase("Both")){
                            params.put("sessionMainCat", sessionMainCat);
                        }else {
                            params.put("sessionMainCat", maincat_type);
                        }

                        Log.e("param_addtoCart1:",""+params.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            Log.e("addCartItemsToServer1",""+response);
                            try {
                                String posts = response.getString("posts");
                                String promotional = response.getString("promotional");
                                String promotional_message = response.getString("promo_message");
                                if(promotional.equalsIgnoreCase("promotional")){
                                    openSessionDialog(promotional_message,"promo");

                                }else if (posts.equals("true")){
                                    if (strId == null) {
                                        //forAddToCart.setStatus(false);

                                        //db.insertCount(pId);
                                        try {


                                           /* int count2 = Integer.parseInt(response.getString("count"));
                                            int count = count2;*/
                                            int count = SyncData();//(int) db.fetchAddToCartCount();

                                            ((BaseActivity) getActivity()).updateAddToCartCount(count);

                                            if (count >= 0) {
                                                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));

                                            } else {
                                                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                                            }
                                            Log.e("CartCount:", "" + count);
                                        }catch (Exception e){
                                            e.getMessage();
                                        }
                                    } else {
                                        Intent intent = new Intent(getActivity(), AddToCart.class);
                                        startActivity(intent);
                                    }
                                    Log.d("addCartItemsToServer1",""+posts);
                                    Toast toast = Toast.makeText(getActivity(), toastMessg + " adding to cart", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    openSessionDialog(posts,"session");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            //GateWay gateWay = new GateWay(getActivity());
                            //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            showAlertDialog("Error",error.toString(),"OK");
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

    //this is for recommended product section
    private class UltimateCardAdapter1 extends RecyclerView.Adapter<MainViewHolder> {

        int selectedPosition;
        ArrayList<productCodeWithProductList> mItems;

        UltimateCardAdapter1(ArrayList<productCodeWithProductList> finalList) {
            mItems = finalList;
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_for_ultimate_products, parent, false);
            final MainViewHolder childViewHolder = new MainViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            int itemPosition = recyclerViewUltimate1.getChildAdapterPosition(v);
                            productCodeWithProductList movie = mItems.get(itemPosition);
                            selectedPosition = movie.getPosition();
                            UltimateProductList tp = movie.productCodeWiseProducts.get(selectedPosition);

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": UltimateCardAdapter1 view clicked on product: " + tp.getStr_product_name(), getActivity());*/

                            if (!tp.getStr_PName().equals("")) {
                                Intent intent = new Intent(getActivity(), SingleProductInformation.class);
                                intent.putExtra("product_name", tp.getStr_product_name());
                                intent.putExtra("shop_id", tp.getStr_ShopId());
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(getView());
                    }
                }
            });
            return childViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder holder, final int position) {
            final DBHelper db = new DBHelper(getActivity());

            ArrayList<String> sizesArrayList = new ArrayList<>();
            ArrayList<String> pricesArrayList = new ArrayList<>();
            ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

            productCodeWithProductList tp = mItems.get(position);
            UltimateProductList movie = tp.productCodeWiseProducts.get(0);

           /* HashMap AddToCartInfo = db.getCartDetails(movie.getStr_PId());
            String btnCheckStatus = (String) AddToCartInfo.get("new_pid");*/
            String server_cart_status = movie.getCart_pstatus();

            if (server_cart_status.equalsIgnoreCase("0")) {
                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
            } else {
                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
            }

            //TODO here recommended product image setup to glide
            /*Glide.with(getActivity()).load(movie.getStr_PImg())
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProduct);*/

            Picasso.with(getActivity()).load(movie.getStr_PImg())
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgProduct);


            holder.tvProductName.setText(movie.getStr_PName());
            if (!movie.getStr_product_hindiname().equals("")) {
                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                holder.tvProductHindiName.setText(movie.getStr_product_hindiname());
            } else {
                holder.tvProductHindiName.setVisibility(View.GONE);
            }
            String dis = movie.getStr_PDiscount();
            holder.tvDiscount.setText(dis + "% OFF");
            String productMrp = movie.getStr_PMrp();
            String discountPrice = movie.getStr_Pprice();
            if (productMrp.equals(discountPrice)) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvMrp.setVisibility(View.GONE);
            } else {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.VISIBLE);
                holder.tvMrp.setVisibility(View.VISIBLE);
                holder.tvMrp.setBackgroundResource(R.drawable.dash);
            }
            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp1 = newProductList.productCodeWiseProducts.get(l);
                    sizesArrayList.add(tp1.getStr_PSize());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp3 = newProductList.productCodeWiseProducts.get(l);
                    DiscountpricesArrayList.add(tp3.getStr_Pprice());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList newProductList = mItems.get(position);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    UltimateProductList tp2 = newProductList.productCodeWiseProducts.get(l);
                    pricesArrayList.add(tp2.getStr_PMrp());

                    if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                        holder.LayoutSpinner.setVisibility(View.VISIBLE);
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), pricesArrayList, DiscountpricesArrayList, sizesArrayList, "homepage");
                        holder.spinner.setAdapter(customSpinnerAdapter);
                    } else {
                        holder.LayoutSpinner.setVisibility(View.INVISIBLE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            productCodeWithProductList tp = mItems.get(holder.getAdapterPosition());
                            UltimateProductList movie = tp.productCodeWiseProducts.get(pos);

                            tp.setPosition(pos); //set here position when user any wants to buy multiple size products

                            /*HashMap AddToCartInfo = db.getCartDetails(movie.getStr_PId());
                            String btnCheckStatus = (String) AddToCartInfo.get("new_pid");*/

                            String server_cart_status = movie.getCart_pstatus();
                            if (server_cart_status.equalsIgnoreCase("0")) {
                                movie.setStatus(false);
                                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                            } else {
                                movie.setStatus(true);
                                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                            }

                            //TODO here recommended product image setup to glide when user select different product variant from spinner
                           /* Glide.with(getActivity()).load(movie.getStr_PImg())
                                    .error(R.drawable.ic_app_transparent)
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(holder.imgProduct);*/

                            Picasso.with(getActivity()).load(movie.getStr_PImg())
                                    .placeholder(R.drawable.loading)
                                    .error(R.drawable.ic_app_transparent)
                                    .into(holder.imgProduct);


                            holder.tvProductName.setText(movie.getStr_PName());
                            if (!movie.getStr_product_hindiname().equals("")) {
                                holder.tvProductHindiName.setVisibility(View.VISIBLE);
                                holder.tvProductHindiName.setText(movie.getStr_product_hindiname());
                            } else {
                                holder.tvProductHindiName.setVisibility(View.GONE);
                            }

                            String dis = movie.getStr_PDiscount();
                            holder.tvDiscount.setText(dis + "% OFF");
                            String productMrp = movie.getStr_PMrp();
                            String discountPrice = movie.getStr_Pprice();
                            if (productMrp.equals(discountPrice)) {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.tvDiscount.setVisibility(View.INVISIBLE);
                                holder.tvMrp.setVisibility(View.GONE);
                            } else {
                                holder.tvPrice.setVisibility(View.VISIBLE);
                                holder.tvDiscount.setVisibility(View.VISIBLE);
                                holder.tvMrp.setVisibility(View.VISIBLE);
                                holder.tvMrp.setBackgroundResource(R.drawable.dash);
                            }
                            holder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                            holder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            productCodeWithProductList movie = mItems.get(holder.getAdapterPosition());
                            selectedPosition = movie.getPosition();
                            UltimateProductList forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": UltimateCardAdapter1 clicked on add to cart & product is: " + forAddToCart.getStr_PName(), getActivity());*/

                            shopId = forAddToCart.getStr_ShopId();
                            pId = forAddToCart.getStr_PId();

                            //call Popup to show veg 300 or grocery 500
                            String maincat_type = movie.productCodeWiseProducts.get(selectedPosition).getStr_product_maincat();

                            /*HashMap AddToCartInfo;
                            AddToCartInfo = db.getCartDetails(forAddToCart.getStr_PId());
                            strId = (String) AddToCartInfo.get("new_pid");*/

                            String cart_status = forAddToCart.getCart_pstatus();
                            if(cart_status.equalsIgnoreCase("1")){
                                Intent intent = new Intent(getActivity(), AddToCart.class);
                                startActivity(intent);
                            }

                            String toastMessg = forAddToCart.getStr_product_name();
                            addCartItemsToServer(pId, shopId,maincat_type,toastMessg);



                            forAddToCart.setStatus(false);

                          /*  if (strId == null) {
                                forAddToCart.setStatus(false);
                                db.insertCount(pId);
                                int count = (int) db.fetchAddToCartCount();
                                ((BaseActivity) getActivity()).updateAddToCartCount(count);

                                if (count >= 0) {
                                    holder.btnAddToCart.setText("Go To Cart");

                                } else {
                                    holder.btnAddToCart.setText("Add To Cart");

                                }
                                Log.e("maincat_type:",""+maincat_type);
                                String toastMessg = forAddToCart.getStr_product_name();
                                addCartItemsToServer(pId, shopId,maincat_type,toastMessg);
                            } else {
                                Intent intent = new Intent(getActivity(), AddToCart.class);
                                startActivity(intent);
                            }*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }

                private void addCartItemsToServer(final String pId, String shopId,String maincat_type,final String toastMessg) { //TODO Server method here
                    GateWay gateWay = new GateWay(getActivity());

                    JSONObject params = new JSONObject();
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shopId);
                        params.put("areaname", Zone_Area);
                        params.put("v_city", v_city);
                        params.put("v_state", v_state);
                        params.put("selectedType", "");
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", gateWay.getContact());
                        params.put("email", gateWay.getUserEmail());

                        if(sessionMainCat.equalsIgnoreCase("Both")){
                            params.put("sessionMainCat", sessionMainCat);
                        }else {
                            params.put("sessionMainCat", maincat_type);
                        }
                        Log.e("param_addtoCart2:",""+params.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("addCartItemsToServer2",""+response);
                            try {
                                String posts = response.getString("posts");
                                String promotional = response.getString("promotional");
                                String promotional_message = response.getString("promo_message");
                                if(promotional.equalsIgnoreCase("promotional")){
                                    openSessionDialog(promotional_message,"promo");

                                } else if (posts.equals("true")){
                                    if (strId == null) {
                                        //forAddToCart.setStatus(false);

                                        // db.insertCount(pId);

                                        int count = SyncData();//(int) db.fetchAddToCartCount();
                                        ((BaseActivity) getActivity()).updateAddToCartCount(count);

                                        if (count >= 0) {
                                            holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                            holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));

                                        } else {
                                            holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                            holder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                                        }
                                        Log.e("CartCount:",""+count);
                                    } else {
                                        Intent intent = new Intent(getActivity(), AddToCart.class);
                                        startActivity(intent);
                                    }
                                    Log.d("addCartItemsToServer2",""+posts);
                                    Toast toast = Toast.makeText(getActivity(), toastMessg + " adding to cart", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    openSessionDialog(posts,"session");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            //GateWay gateWay = new GateWay(getActivity());
                            //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            showAlertDialog("Error",error.toString(),"OK");
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

    }

    private class MainViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgProduct;
        private TextView tvProductName;
        private TextView tvProductHindiName;
        private TextView tvMrp;
        private TextView tvDiscount;
        private TextView tvPrice;
        private TextView btnAddToCart;
        private Spinner spinner;
        private RelativeLayout LayoutSpinner;

        public MainViewHolder(View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvProductName = itemView.findViewById(R.id.txtProductName);
            tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
            tvMrp = itemView.findViewById(R.id.txtTotal);
            tvDiscount = itemView.findViewById(R.id.txtDiscount);
            tvPrice = itemView.findViewById(R.id.txtPrice);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            spinner = itemView.findViewById(R.id.spinner);
            LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
            btnAddToCart.setTag(this);

        }
    }

    //this is for combo offer section
    private class CustomAdapterForComboOffer extends RecyclerView.Adapter<CustomAdapterForComboOffer.ViewHolder> {

        final Context context;
        final List<comboList> cardList = new ArrayList<>();

        CustomAdapterForComboOffer(Activity activity) {
            super();
            this.context = activity;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_combo_offer, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            final int pos = holder.getAdapterPosition();
            final comboList movie = cardList.get(position);
            final String comboName = movie.getStr_combo_Offername();
            String totalPrice = movie.getStr_comboTotalprice();
            String offerPrice = movie.getStr_comboofferprice();
            String discount = movie.getStr_comboDiscountpercent();

            // code for decimal round off
            Log.e("RoundValue_Before:",""+discount);
            Double discount2 = roundTwoDecimals(Double.parseDouble(discount));
            Log.e("RoundValue_After:",""+discount2.toString());

            String imgUrl = "https://s.apneareamein.com/seller/assets/"+movie.getStr_offerComboimage();
            Log.d("imgUrl",imgUrl);
            //TODO here combo offer product image setup to glide
            /*Glide.with(context).load(imgUrl)
                    .thumbnail(Glide.with(context).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.comboImage);*/


            Picasso.with(context).load(imgUrl)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.comboImage);

            holder.tvPrice.setText("₹ " + offerPrice);
            holder.tvMrp.setText("₹ " + totalPrice);

            holder.tvDiscount.setText(discount2 + "% OFF");
            holder.tvSavingsInPercent.setText(discount2 + " %\nOFF");

            holder.tvDiscount.setText(discount + "% OFF");
            holder.tvSavingsInPercent.setText(discount + " %\nOFF");


            holder.tvComboName.setText(comboName);
            holder.tvMrp.setBackgroundResource(R.drawable.dash);


            if (cardList.size() > 1) {
                tvComboNote.setVisibility(View.VISIBLE);
            }

            final String endDateComboOffer = cardList.get(position).getStrOfferEnddate();

            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    //handler.postDelayed(this, 500);
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        Date futureDate = dateFormat.parse(endDateComboOffer);
                        Date currentDate = new Date();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String now = df.format(new Date());

                        if (currentDate.before(futureDate)) {
                            holder.timerLayout.setVisibility(View.VISIBLE);
                            int diff1 = futureDate.getMonth() - currentDate.getMonth();
                            int diff2 = futureDate.getDate() - currentDate.getDate();

                            if ((diff1 == 0) && (diff2 == 1)) {
                                long diff = futureDate.getTime() - currentDate.getTime();
                                long secondsInMilli = 1000;
                                long minutesInMilli = secondsInMilli * 60;
                                long hoursInMilli = minutesInMilli * 60;
                                long daysInMilli = hoursInMilli * 24;

                                long elapsedDays = diff / daysInMilli;
                                diff = diff % daysInMilli;

                                long elapsedHours = diff / hoursInMilli;
                                diff = diff % hoursInMilli;

                                long elapsedMinutes = diff / minutesInMilli;
                                diff = diff % minutesInMilli;

                                long elapsedSeconds = diff / secondsInMilli;

                                holder.tvTimerHours.setText("" + String.format("%02d", elapsedHours));
                                holder.tvMin.setText("" + String.format("%02d", elapsedMinutes));
                                holder.tvSec.setText("" + String.format("%02d", elapsedSeconds));
                            } else {
                                holder.timerLayout.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            comboofferLinearLayout.setVisibility(View.GONE);
                            holder.timerLayout.setVisibility(View.INVISIBLE);
                            handler.removeCallbacks(runnable);
                            handler.removeMessages(0);
                            cardList.remove(pos);
                            notifyItemRemoved(pos);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.postDelayed(runnable, 0);

            holder.mainRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                    UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": CustomAdapterForComboOffer clicked on: " + comboName, getActivity());*/

                    fragment = new ComboOffer();
                    FragmentManager comboManager = getActivity().getSupportFragmentManager();
                    boolean comboFragmentPopped = comboManager.popBackStackImmediate("addCombo", 0);
                    if (!comboFragmentPopped) { //fragment not in back stack, create it.
                        FragmentTransaction comboTransaction = comboManager.beginTransaction();
                        comboTransaction.replace(R.id.frame, fragment, "Combo");
                        comboTransaction.addToBackStack("addCombo");
                        comboTransaction.commit();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return cardList.size();
        }

        public void add(comboList movie) {
            cardList.add(movie);
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView comboImage;
            private TextView tvMrp;
            private TextView tvPrice;
            private TextView tvDiscount;
            private TextView tvComboName;
            private TextView tvSavingsInPercent;
            private TextView tvMin;
            private TextView tvTimerHours;
            private TextView tvSec;
            private RelativeLayout mainRelativeLayout;
            private LinearLayout timerLayout;

            ViewHolder(View itemView) {
                super(itemView);

                comboImage = itemView.findViewById(R.id.comboImage);
                tvMrp = itemView.findViewById(R.id.txtMrp1);
                tvPrice = itemView.findViewById(R.id.txtComboPrice1);
                tvDiscount = itemView.findViewById(R.id.txtDiscount1);
                tvComboName = itemView.findViewById(R.id.txtComboOfferName1);
                tvSavingsInPercent = itemView.findViewById(R.id.txtSavingsInPercent1);
                mainRelativeLayout = itemView.findViewById(R.id.mainRelativeLayout);
                tvTimerHours = itemView.findViewById(R.id.txtTimerHour);
                tvMin = itemView.findViewById(R.id.txtTimerMinute);
                tvSec = itemView.findViewById(R.id.txtTimerSecond);
                timerLayout = itemView.findViewById(R.id.timerLayout);
            }
        }
    }

    //this is for sliding banner section
    private class SlidingImages_Adapter extends PagerAdapter implements LoopingPagerAdapter{//

        ArrayList<String> imageUrlList;
        String flag;
        SlidingImages_Adapter(ArrayList<String> imageUrlList, String flag)    {
            this.imageUrlList = imageUrlList;
            this.flag = flag;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view ==  object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup viewGroup, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.sliding_image_homepage, viewGroup, false);

            ImageView imageView = view.findViewById(R.id.image);
            final int index = position % imageUrlList.size();

            //TODO here sliding banner setup to glide

            Picasso.with(getActivity())
                    .load(imageUrlList.get(index))
                    .placeholder(R.drawable.ic_app_transparent)
                    .error(R.drawable.ic_app_transparent)
                    //.resize(355,220)
                    //.fit()
                    .into(imageView);
            viewGroup.addView(view);

          /*  imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": " + productImageCategory.get(index) + " banner clicked", getActivity());
                   Intent intent = new Intent(context, SearchProducts.class);
                    intent.putExtra("tag", "next");
                    intent.putExtra("selectedName", productImageCategory.get(index));
                    startActivity(intent);
                }
            });*/

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(flag.equalsIgnoreCase("shop")){
                        Intent intent = new Intent(context, ShopCategoryActivity.class);
                        //intent.putExtra("tag", "next");
                        //intent.putExtra("selectedName", productImageCategory.get(index));
                        startActivity(intent);
                    }else {

                        if (event_array_url.get(position).isEmpty()) {
                            //Toast.makeText(BaseActivity.this,"No Event",Toast.LENGTH_LONG).show();
                            //productImageCategory.get(index)

                            Intent intent = new Intent(context, SearchProducts.class);
                            intent.putExtra("tag", "next");
                            intent.putExtra("selectedName", productImageCategory.get(index));
                            startActivity(intent);
                            Log.e("selectedSlide_Name", productImageCategory.get(index));

                        } else {

                            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                            String user_id = prefs.getString("user_id", "");
                            String param = "/" + user_id + "/" + productImageCategory.get(position);

                            Uri webpage = Uri.parse(event_array_url.get(position) + param);
                            Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                            startActivity(myIntent);
                            Log.e("Slider_image_url:", "" + webpage.toString());

                        }

                    }//close flag if

                }
            });
            return view;
        }

        @Override
        public int getCount() {
            //return    4;//Integer.MAX_VALUE;
            return imageUrlList.size();
        }

        @Override
        public int getRealCount() {
            return imageUrlList.size();
        }
    }

    private void setupAutoPager(final int list_size)
    {
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run()
            {
                viewPager.setCurrentItem(currentPage, true);
                //if(currentPage == Integer.MAX_VALUE)
                if(currentPage == list_size)
                {
                    currentPage = 0;
                }
                else
                {
                    ++currentPage ;
                }
            }
        };

        try {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(update);
                }
            }, 2000, 2000); //min 1000 max 3000
        }catch (Exception e){
            e.getMessage();
        }

      /* timer_handler = new Handler();
        timer_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.post(update);
            }
        },2000);*/
    }

    //second view pager
    private void setupAutoPager2(final int list_size)
    {
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run()
            {
                viewPager2.setCurrentItem(currentPage, true);
                //if(currentPage == Integer.MAX_VALUE)
                if(currentPage == list_size)
                {
                    currentPage = 0;
                }
                else
                {
                    ++currentPage ;
                }
            }
        };

        try {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(update);
                }
            }, 2000, 2000); //min 1000 max 3000
        }catch (Exception e){
            e.getMessage();
        }

      /* timer_handler = new Handler();
        timer_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.post(update);
            }
        },2000);*/
    }



    //this is for brand section
    private class CustomAdapterForBrand extends RecyclerView.Adapter<CustomAdapterForBrand.ViewHolder> {

        Context context;
        List<Movie> cardList = new ArrayList<>();

        CustomAdapterForBrand(Activity activity) {
            super();
            this.context = activity;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_brand_images, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Movie movie = cardList.get(position);


            Picasso.with(context).load(movie.getRedeemCategory())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgBrand);



            final String product_brand = movie.getRedeemCategoryCount();



            holder.imgBrand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": CustomAdapterForBrand clicked on: " + product_brand, getActivity());*/

                    Intent intent = new Intent(context, SearchProducts.class);
                    intent.putExtra("tag", "next");
                    intent.putExtra("selectedName", product_brand);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return cardList.size();
        }

        public void add(Movie movie) {
            cardList.add(movie);
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView imgBrand;

            public ViewHolder(View itemView) {
                super(itemView);

                imgBrand = itemView.findViewById(R.id.imgBrand);
            }
        }
    }

    private class comboOfferWithOfferCode {

        String selectedName1;
        ArrayList<OfferCodeWiseProduct> offerCodeWiseProducts;
    }

    private class OfferCodeWiseProduct {

        private final String offerProductName;
        private final String offerProductImage;
        private final String offerProductMrp;
        private final String offerProductPrice;
        private final String offerCategory;
        private final String offerTitle;
        private final String offerp_Name;
        private final String offerShop_Id;
        private final String offer_product_maincat;
        private final String product_brand;
        private final String cart_pstatus;
        private String offerProductDiscount;

        public OfferCodeWiseProduct(String product_name1, String product_image1, String product_mrp1,
                                    String product_price1, String product_discount1, String selectedName1,
                                    String topProductsTextOne, String offerp_Name, String offer_product_maincat, String product_brand, String offerShop_Id, String cart_pstatus) {
            this.offerProductName = product_name1;
            this.offerProductImage = product_image1;
            this.offerProductMrp = product_mrp1;
            this.offerProductPrice = product_price1;
            this.offerProductDiscount = product_discount1;
            this.offerCategory = selectedName1;
            this.offerTitle = topProductsTextOne;
            this.offerp_Name = offerp_Name;
            this.offer_product_maincat=offer_product_maincat;
            this.product_brand=product_brand;
            this.offerShop_Id = offerShop_Id;
            this.cart_pstatus = cart_pstatus;
        }

        public String getOfferProductName() {
            return offerProductName;
        }

        public String getOfferProductImage() {
            return offerProductImage;
        }

        public String getOfferProductMrp() {
            return offerProductMrp;
        }

        public String getOfferProductDiscount() {
            return offerProductDiscount;
        }

        public String setOfferProductDiscount(String offerProductDiscount) {
            this.offerProductDiscount = offerProductDiscount;
            return offerProductDiscount;
        }

        public String getOfferProductPrice() {
            return offerProductPrice;
        }

        public String getOfferCategory() {
            return offerCategory;
        }

        public String getOfferTitle() {
            return offerTitle;
        }

        public String getOfferp_Name() {
            return offerp_Name;
        }

        public String getOfferShop_Id() {
            return offerShop_Id;
        }

        public String getOffer_product_maincat() {
            return offer_product_maincat;
        }

        public String getCart_pstatus() {
            return cart_pstatus;
        }

        public String getProduct_brand() {
            return product_brand;
        }
    }

    //this is for three products layout section
    private class CustomAdapterForCategory extends RecyclerView.Adapter<CustomAdapterForCategory.MainViewHolder> {

        Context mContext;
        List<comboOfferWithOfferCode> cardList;

        CustomAdapterForCategory(Activity activity, ArrayList<comboOfferWithOfferCode> finalComboOffer) {
            this.mContext = activity;
            this.cardList = finalComboOffer;
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_three_product_view, parent, false);
            return new MainViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull MainViewHolder viewHolder, int position) {
            try {
                final int pos = viewHolder.getAdapterPosition();
                final comboOfferWithOfferCode movie = cardList.get(pos);


                final OfferCodeWiseProduct codeWiseProduct = movie.offerCodeWiseProducts.get(0);
                final OfferCodeWiseProduct codeWiseProduct1 = movie.offerCodeWiseProducts.get(1);
                 final OfferCodeWiseProduct codeWiseProduct2 = movie.offerCodeWiseProducts.get(2);



                //TODO here combo offer product images setup to glide
               /* Glide.with(mContext).load(codeWiseProduct.getOfferProductImage()).error(R.drawable.ic_app_transparent)
                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.imgProduct1);*/

                Picasso.with(mContext).load(codeWiseProduct.getOfferProductImage()).error(R.drawable.ic_app_transparent)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.ic_app_transparent)
                        .into(viewHolder.imgProduct1);

                //TODO here combo offer product images setup to glide
                /*Glide.with(mContext).load(codeWiseProduct1.getOfferProductImage()).error(R.drawable.ic_app_transparent)
                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.imgProduct2);*/


                Picasso.with(mContext).load(codeWiseProduct1.getOfferProductImage()).error(R.drawable.ic_app_transparent)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.ic_app_transparent)
                        .into(viewHolder.imgProduct2);



                //TODO here combo offer product images setup to glide
                /*Glide.with(mContext).load(codeWiseProduct2.getOfferProductImage()).error(R.drawable.ic_app_transparent)
                        .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                        .error(R.drawable.ic_app_transparent)
                        .fitCenter()
                        .crossFade()viewpa
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.imgProduct3);*/

                Picasso.with(mContext).load(codeWiseProduct2.getOfferProductImage()).error(R.drawable.ic_app_transparent)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.ic_app_transparent)
                        .into(viewHolder.imgProduct3);

                viewHolder.tvOfferName.setText(codeWiseProduct.getOfferTitle());
                viewHolder.tvOfferName.setText(codeWiseProduct1.getOfferTitle());
                viewHolder.tvProductName1.setText(codeWiseProduct.getOfferProductName());
                viewHolder.tvProductName2.setText(codeWiseProduct1.getOfferProductName());
                viewHolder.tvProductName3.setText(codeWiseProduct2.getOfferProductName());

                viewHolder.tv_brand1.setText(codeWiseProduct.getProduct_brand());
                viewHolder.tv_brand2.setText(codeWiseProduct1.getProduct_brand());
                viewHolder.tv_brand3.setText(codeWiseProduct2.getProduct_brand());

                viewHolder.tvViewAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*
                        UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": CustomAdapterForCategory ViewAll clicked on: " + movie.offerCodeWiseProducts.get(0).getOfferCategory(), getActivity());*/

                        Intent viewAll = new Intent(getActivity(), SearchProducts.class);
                        viewAll.putExtra("tag", "next");
                        viewAll.putExtra("selectedName", movie.offerCodeWiseProducts.get(0).getOfferCategory());
                        startActivity(viewAll);
                    }
                });

                viewHolder.lin_layout1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": CustomAdapterForCategory lin_layout1 clicked on product: " + codeWiseProduct.getOfferp_Name(), getActivity());*/

                        Intent intent = new Intent(getActivity(), SingleProductInformation.class);
                        intent.putExtra("product_name", codeWiseProduct.getOfferp_Name());
                        intent.putExtra("shop_id", codeWiseProduct.getOfferShop_Id());
                        startActivity(intent);
                    }
                });

                viewHolder.lin_layout2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": CustomAdapterForCategory lin_layout2 clicked on product: " + codeWiseProduct1.getOfferp_Name(), getActivity());*/

                        Intent intent = new Intent(getActivity(), SingleProductInformation.class);
                        intent.putExtra("product_name", codeWiseProduct1.getOfferp_Name());
                        intent.putExtra("shop_id", codeWiseProduct1.getOfferShop_Id());
                        startActivity(intent);
                    }
                });

                viewHolder.lin_layout3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": CustomAdapterForCategory lin_layout3 clicked on product: " + codeWiseProduct2.getOfferp_Name(), getActivity());*/

                        Intent intent = new Intent(getActivity(), SingleProductInformation.class);
                        intent.putExtra("product_name", codeWiseProduct2.getOfferp_Name());
                        intent.putExtra("shop_id", codeWiseProduct2.getOfferShop_Id());
                        startActivity(intent);
                    }
                });
                String dis1 = codeWiseProduct.getOfferProductDiscount();
                String discount1 = codeWiseProduct.setOfferProductDiscount(dis1);
                viewHolder.tvDiscountP1.setText(discount1 + "% OFF");
                String dis2 = codeWiseProduct1.getOfferProductDiscount();
                String discount2 = codeWiseProduct1.setOfferProductDiscount(dis2);
                viewHolder.tvDiscountP2.setText(discount2 + "% OFF");
                String dis3 = codeWiseProduct2.getOfferProductDiscount();
                String discount3 = codeWiseProduct2.setOfferProductDiscount(dis3);
                viewHolder.tvDiscountP3.setText(discount3 + "% OFF");

                String productMrp1 = codeWiseProduct.getOfferProductMrp();
                String discountPrice1 = codeWiseProduct.getOfferProductPrice();
                if (productMrp1.equals(discountPrice1)) {
                    viewHolder.tvProductP1.setVisibility(View.VISIBLE);
                    viewHolder.tvDiscountP1.setVisibility(View.GONE);
                    viewHolder.tvTotalP1.setVisibility(View.GONE);
                } else {
                    viewHolder.tvProductP1.setVisibility(View.VISIBLE);
                    viewHolder.tvDiscountP1.setVisibility(View.VISIBLE);
                    viewHolder.tvTotalP1.setVisibility(View.VISIBLE);
                    viewHolder.tvTotalP1.setBackgroundResource(R.drawable.dash);
                }
                viewHolder.tvTotalP1.setText("\u20B9" + " " + productMrp1 + "/-");
                viewHolder.tvProductP1.setText("\u20B9" + " " + discountPrice1 + "/-");

                String productMrp2 = codeWiseProduct1.getOfferProductMrp();
                String discountPrice2 = codeWiseProduct1.getOfferProductPrice();
                if (productMrp2.equals(discountPrice2)) {
                    viewHolder.tvProductP2.setVisibility(View.VISIBLE);
                    viewHolder.tvDiscountP2.setVisibility(View.GONE);
                    viewHolder.tvTotalP2.setVisibility(View.GONE);
                } else {
                    viewHolder.tvProductP2.setVisibility(View.VISIBLE);
                    viewHolder.tvDiscountP2.setVisibility(View.VISIBLE);
                    viewHolder.tvTotalP2.setVisibility(View.VISIBLE);
                    viewHolder.tvTotalP2.setBackgroundResource(R.drawable.dash);
                }
                viewHolder.tvTotalP2.setText("\u20B9" + " " + productMrp2 + "/-");
                viewHolder.tvProductP2.setText("\u20B9" + " " + discountPrice2 + "/-");

                String productMrp3 = codeWiseProduct2.getOfferProductMrp();
                String discountPrice3 = codeWiseProduct2.getOfferProductPrice();
                if (productMrp3.equals(discountPrice3)) {
                    viewHolder.tvProductP3.setVisibility(View.VISIBLE);
                    viewHolder.tvDiscountP3.setVisibility(View.GONE);
                    viewHolder.tvTotalP3.setVisibility(View.GONE);
                } else {
                    viewHolder.tvProductP3.setVisibility(View.VISIBLE);
                    viewHolder.tvDiscountP3.setVisibility(View.VISIBLE);
                    viewHolder.tvTotalP3.setVisibility(View.VISIBLE);
                    viewHolder.tvTotalP3.setBackgroundResource(R.drawable.dash);
                }
                viewHolder.tvTotalP3.setText("\u20B9" + " " + productMrp3 + "/-");
                viewHolder.tvProductP3.setText("\u20B9" + " " + discountPrice3 + "/-");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return cardList.size();
        }

        class MainViewHolder extends RecyclerView.ViewHolder {

            private TextView tvOfferName;
            private TextView tvProductName1;
            private TextView tvProductName2;
            private TextView tvProductName3;
            private TextView tv_brand1;
            private TextView tv_brand2;
            private TextView tv_brand3;
            private TextView tvProductP1;
            private TextView tvTotalP1;
            private TextView tvDiscountP1;
            private TextView tvProductP2;
            private TextView tvTotalP2;
            private TextView tvDiscountP2;
            private TextView tvProductP3;
            private TextView tvTotalP3;
            private TextView tvDiscountP3;
            private TextView tvViewAll;
            private ImageView imgProduct1;
            private ImageView imgProduct2;
            private ImageView imgProduct3;
            private LinearLayout lin_layout1;
            private LinearLayout lin_layout2;
            private LinearLayout lin_layout3;

            public MainViewHolder(View itemView) {
                super(itemView);

                tvOfferName = itemView.findViewById(R.id.txtOfferName);
                tvProductName1 = itemView.findViewById(R.id.txtProductName1);
                tvProductName2 = itemView.findViewById(R.id.txtProductName2);
                tvProductName3 = itemView.findViewById(R.id.txtProductName3);

                tv_brand1 = itemView.findViewById(R.id.tv_brand1);
                tv_brand2 = itemView.findViewById(R.id.tv_brand2);
                tv_brand3 = itemView.findViewById(R.id.tv_brand3);


                tvProductP1 = itemView.findViewById(R.id.txtProductP1);
                tvTotalP1 = itemView.findViewById(R.id.txtTotalP1);
                tvDiscountP1 = itemView.findViewById(R.id.txtDiscountP1);
                tvProductP2 = itemView.findViewById(R.id.txtProductP2);
                tvTotalP2 = itemView.findViewById(R.id.txtTotalP2);
                tvDiscountP2 = itemView.findViewById(R.id.txtDiscountP2);
                tvProductP3 = itemView.findViewById(R.id.txtProductP3);
                tvTotalP3 = itemView.findViewById(R.id.txtTotalP3);
                tvViewAll = itemView.findViewById(R.id.txtViewAll1);
                tvDiscountP3 = itemView.findViewById(R.id.txtDiscountP3);
                imgProduct1 = itemView.findViewById(R.id.imgProduct1);
                imgProduct2 = itemView.findViewById(R.id.imgProduct2);
                imgProduct3 = itemView.findViewById(R.id.imgProduct3);

                lin_layout1 = itemView.findViewById(R.id.lin_layout1);
                lin_layout2 = itemView.findViewById(R.id.lin_layout2);
                lin_layout3 = itemView.findViewById(R.id.lin_layout3);
            }
        }
    }

    private class comboList {

        private String str_combo_Offername;
        private String str_comboTotalprice;
        private String str_comboofferprice;
        private String str_comboDiscountpercent;
        private String str_offerComboimage;
        private String str_shop_id;
        private String str_product_maincat;
        private String strOfferEnddate;
        private String cart_pstatus;

        comboList(String combo_Offername, String comboTotalprice, String comboofferprice,
                  String comboDiscountpercent, String offerComboimage, String shop_id, String str_product_maincat,String offer_end_date,String cart_pstatus) {

            this.str_combo_Offername = combo_Offername;
            this.str_comboTotalprice = comboTotalprice;
            this.str_comboofferprice = comboofferprice;
            this.str_comboDiscountpercent = comboDiscountpercent;
            this.str_offerComboimage = offerComboimage;
            this.str_shop_id = shop_id;
            this.str_product_maincat = str_product_maincat;
            this.strOfferEnddate = offer_end_date;
            this.cart_pstatus = cart_pstatus;
        }

        public String getStrOfferEnddate() {
            return strOfferEnddate;
        }

        public String getStr_combo_Offername() {
            return str_combo_Offername;
        }

        public String getStr_comboTotalprice() {
            return str_comboTotalprice;
        }

        public String getStr_comboofferprice() {
            return str_comboofferprice;
        }

        public String getStr_comboDiscountpercent() {
            return str_comboDiscountpercent;
        }

        public String getStr_offerComboimage() {
            return str_offerComboimage;
        }

        public String getStr_shop_id() {
            return str_shop_id;
        }

        public String getStr_product_maincat() {
            return str_product_maincat;
        }

        public void setStr_product_maincat(String str_product_maincat) {
            this.str_product_maincat = str_product_maincat;
        }

        public String getCart_pstatus() {
            return cart_pstatus;
        }

        public void setCart_pstatus(String cart_pstatus) {
            this.cart_pstatus = cart_pstatus;
        }
    }

    private class UltimateProductList {

        final String str_PId;
        final String code;
        final String str_ShopId;
        final String str_DeliveryAvailable;
        final String str_PName;
        final String str_PImg;
        final String str_PMrp;
        final String str_Pprice;
        final String str_PDiscount;
        final String str_PSize;
        final String str_product_name;
        final String str_product_maincat;
        final String str_product_hindiname;
        final String cart_pstatus;

        boolean status = false;

        UltimateProductList(String code, String str_PId, String str_ShopId, String str_DeliveryAvailable,
                            String str_PName, String str_PImg, String str_PMrp, String str_Pprice,
                            String str_PDiscount, String str_PSize, String str_product_name, String str_product_maincat,String str_product_hindiname, String cart_pstatus) {

            this.code = code;
            this.str_PId = str_PId;
            this.str_ShopId = str_ShopId;
            this.str_DeliveryAvailable = str_DeliveryAvailable;
            this.str_PName = str_PName;
            this.str_PImg = str_PImg;
            this.str_PMrp = str_PMrp;
            this.str_Pprice = str_Pprice;
            this.str_PDiscount = str_PDiscount;
            this.str_PSize = str_PSize;
            this.str_product_name = str_product_name;
            this.str_product_maincat = str_product_maincat;
            this.str_product_hindiname = str_product_hindiname;
            this.cart_pstatus = cart_pstatus;
        }

        public String getStr_PId() {
            return str_PId;
        }

        public String getStr_ShopId() {
            return str_ShopId;
        }

        public String getStr_DeliveryAvailable() {
            return str_DeliveryAvailable;
        }

        public String getStr_PName() {
            return str_PName;
        }

        public String getStr_PImg() {
            return str_PImg;
        }

        public String getStr_PMrp() {
            return str_PMrp;
        }

        public String getStr_Pprice() {
            return str_Pprice;
        }

        public String getStr_PDiscount() {
            return str_PDiscount;
        }

        public String getStr_PSize() {
            return str_PSize;
        }

        public String getStr_product_name() {
            return str_product_name;
        }

        public String getStr_product_hindiname() {
            return str_product_hindiname;
        }

        public String getStr_product_maincat() {
            return str_product_maincat;
        }

        public boolean isStatus(boolean check) {
            return status;
        }

        public boolean setStatus(boolean status) {
            this.status = status;
            return status;
        }

        public String getCart_pstatus() {
            return cart_pstatus;
        }
    }

    public class productCodeWithProductList {

        private ArrayList<UltimateProductList> productCodeWiseProducts;

        String code;
        int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

    private class CategoryImage extends RecyclerView.ViewHolder {

        private ImageView catImg;

        private CategoryImage(View itemView) {
            super(itemView);

            catImg = itemView.findViewById(R.id.categoryImage);
        }

        void render(String text) {

            //TODO here section header banner setup to glide
           /* Glide.with(context)
                    .load(text)
                    .error(R.drawable.ic_app_transparent)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(catImg);*/

            Picasso.with(context)
                    .load(text)
                    .error(R.drawable.ic_app_transparent)
                    .into(catImg);
        }
    }

    private class CategoryViewAllProducts extends RecyclerView.ViewHolder {

        private RelativeLayout viewAllProducts;

        CategoryViewAllProducts(View itemView) {
            super(itemView);

            viewAllProducts = itemView.findViewById(R.id.viewAllProducts);
            viewAllProducts.setTag(this);
        }
    }

    private class CategoryViewHolder extends RecyclerView.ViewHolder {

        final ImageView img;
        final ImageView wishList;
        final TextView btnAddToCart;
        final TextView tvProductName;
        final TextView tvSellerName;
        final TextView tvMrp;
        final TextView tvDiscount;
        final TextView tvPrice;
        final TextView tvProductHindiName;
        final RelativeLayout linearLayoutProduct;
        final CardView cardView;
        final Spinner spinner;
        final RelativeLayout LayoutSpinner;
        final FrameLayout frameimgMsg;
        final View mView;

        CategoryViewHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.view);
            img = itemView.findViewById(R.id.imgProduct);
            wishList = itemView.findViewById(R.id.wishList);
            tvProductName = itemView.findViewById(R.id.txtProductName);
            tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
            tvSellerName = itemView.findViewById(R.id.txtSellerName);
            tvMrp = itemView.findViewById(R.id.txtTotal);
            tvDiscount = itemView.findViewById(R.id.txtDiscount);
            tvPrice = itemView.findViewById(R.id.txtPrice);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            linearLayoutProduct = itemView.findViewById(R.id.linearLayoutProduct);
            spinner = itemView.findViewById(R.id.spinner);
            LayoutSpinner = itemView.findViewById(R.id.LayoutSpinner);
            frameimgMsg = itemView.findViewById(R.id.frameimgMsg);
            this.mView = itemView;
            linearLayoutProduct.setTag(this);
            mView.setTag(this);
            wishList.setVisibility(View.GONE);
            btnAddToCart.setVisibility(View.VISIBLE);
        }
    }

    private class productCodeWithProductList1 {

        String code;
        int position;

        ArrayList<CategoryList> productCodeWiseProducts;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

    private class CategoryList {

        private String code;
        private String shop_id;
        private String shop_name;
        private String product_id;
        private String product_name;
        private String strHindiName;
        private String product_image;
        private String product_mrp;
        private String product_price;
        private String product_discount;
        private String pName;
        private String product_size;
        private String product_maincat;
        private String product_brand;
        private String item_type;
        private String cart_pstatus;
        private boolean status = false;

        CategoryList(String code, String shop_id, String shop_name, String product_id, String product_name, String strHindiName, String product_image, String product_mrp, String product_price, String product_discount, String pName, String product_size, String product_maincat, String product_brand, String item_type, String cart_pstatus) {
            this.code = code;
            this.shop_id = shop_id;
            this.shop_name = shop_name;
            this.product_id = product_id;
            this.product_name = product_name;
            this.strHindiName = strHindiName;
            this.product_image = product_image;
            this.product_mrp = product_mrp;
            this.product_price = product_price;
            this.product_discount = product_discount;
            this.pName = pName;
            this.product_size = product_size;
            this.product_maincat = product_maincat;
            this.product_brand = product_brand;
            this.item_type = item_type;
            this.cart_pstatus = cart_pstatus;
        }

        public String getCode() {
            return code;
        }

        public String getShop_id() {
            return shop_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getProduct_id() {
            return product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public String getStrHindiName() {
            return strHindiName;
        }

        public String getProduct_image() {
            return product_image;
        }

        public String getProduct_mrp() {
            return product_mrp;
        }

        public String getProduct_price() {
            return product_price;
        }

        public String getProduct_discount() {
            return product_discount;
        }

        public String setProduct_discount(String product_discount) {
            this.product_discount = product_discount;
            return product_discount;
        }

        public String getpName() {
            return pName;
        }

        public String getProduct_size() {
            return product_size;
        }

        public boolean isStatus(boolean check) {
            return status;
        }

        public boolean setStatus(boolean status) {
            this.status = status;
            return status;
        }

        public String getProduct_maincat() {
            return product_maincat;
        }

        public void setProduct_maincat(String product_maincat) {
            this.product_maincat = product_maincat;
        }

        public String getItem_type() {
            return item_type;
        }

        public String getProduct_brand() {
            return product_brand;
        }

        public void setProduct_brand(String product_brand) {
            this.product_brand = product_brand;
        }

        public String getCart_pstatus() {
            return cart_pstatus;
        }

        public void setCart_pstatus(String cart_pstatus) {
            this.cart_pstatus = cart_pstatus;
        }
    }

    //this is for section header and footer
    private class CountSectionAdapter extends SectionedRecyclerViewAdapter<CategoryImage,
            CategoryViewHolder,
            CategoryViewAllProducts> {

        int temp = 2;
        int selectedPosition;
        Context mContext;
        List<String> list;
        List<String> catName;
        ArrayList<productCodeWithProductList1> allProductItemsList;

        private CountSectionAdapter(Context context, ArrayList<String> imgPath, ArrayList<String> catName, ArrayList<productCodeWithProductList1> finalList2) {
            this.mContext = context;
            this.list = imgPath;
            this.catName = catName;
            this.allProductItemsList = finalList2;
        }

        @Override
        protected int getItemCountForSection(int section) {
            return 3;
        }

        @Override
        protected int getSectionCount() {
            return list.size();
        }

        @Override
        protected boolean hasFooterInSection(int section) {
            return true;
        }

        LayoutInflater getLayoutInflater() {
            return LayoutInflater.from(mContext);
        }

        @Override
        protected CategoryImage onCreateSectionHeaderViewHolder(ViewGroup parent, final int viewType) {
            View view = getLayoutInflater().inflate(R.layout.header_item, parent, false);

            return new CategoryImage(view);
        }

        @Override
        protected CategoryViewAllProducts onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.footer_item, parent, false);
            return new CategoryViewAllProducts(view);
        }
        @Override
        protected CategoryViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.card_view_for_search_products, parent, false);
            return new CategoryViewHolder(view);
        }

        @Override
        protected void onBindSectionHeaderViewHolder(CategoryImage holder, final int section) {

            holder.catImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connectivity.isConnected(getActivity())) {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": CountSectionAdapter category image clicked on: " + catName.get(section), getActivity());*/

                        Intent intent = new Intent(context, SearchProducts.class);
                        intent.putExtra("tag", "next");
                        intent.putExtra("selectedName", catName.get(section));
                        startActivity(intent);
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }
            });
            holder.render(list.get(section));
        }

        @Override
        protected void onBindSectionFooterViewHolder(CategoryViewAllProducts holder, final int section) {
            holder.viewAllProducts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Connectivity.isConnected(getActivity())) {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": CountSectionAdapter viewAllProducts clicked on: " + catName.get(section), getActivity());*/

                        Intent intent = new Intent(context, SearchProducts.class);
                        intent.putExtra("tag", "next");
                        intent.putExtra("selectedName", catName.get(section));
                        Log.e("viewAlldata:",""+catName.get(section));
                        startActivity(intent);
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }
            });
        }

        @Override
        protected void onBindItemViewHolder(final CategoryViewHolder viewHolder, int section, int position) {
            final DBHelper db = new DBHelper(context);
            int catPos = 0;
            String categoryName = catName.get(section);
            productCodeWithProductList1 movie1 = allProductItemsList.get(position);
            CategoryList tp1 = movie1.productCodeWiseProducts.get(0);
            String strCatName = tp1.getItem_type();
            if (categoryName.equals(strCatName)) {
                catPos = position;
            } else {
                position = temp;
                temp = position + 1;
                catPos = temp;
            }

            /*--------------------------------for default value------------------------------*/
            try {
                productCodeWithProductList1 movie = allProductItemsList.get(catPos);
                final CategoryList tp = movie.productCodeWiseProducts.get(0);
                if (tp.getStrHindiName().equals("")) {
                    viewHolder.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    viewHolder.tvProductHindiName.setText(" ( " + tp.getStrHindiName() + " ) ");
                }

                /*HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                String btnCheckStatus = (String) AddToCartInfo.get("new_pid");*/

                String server_cart_status =tp.getCart_pstatus();

                if (server_cart_status.equalsIgnoreCase("0")) {
                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                    viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                } else {
                    viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                    viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                }

                //TODO here section products images setup to glide
               /* Glide.with(context)
                        .load(tp.getProduct_image())
                        .error(R.drawable.ic_app_transparent)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.img);*/

                Picasso.with(context)
                        .load(tp.getProduct_image())
                        .error(R.drawable.ic_app_transparent)
                        .into(viewHolder.img);

                // viewHolder.tvSellerName.setText("By:" + " " + tp.getShop_name());
                viewHolder.tvSellerName.setText(tp.getProduct_brand());
                String size = tp.getProduct_size();
                viewHolder.tvProductName.setText(tp.getProduct_name());
                String dis = tp.getProduct_discount();
                String discount = tp.setProduct_discount(dis);
                viewHolder.tvDiscount.setText(discount+ "% OFF");//String.format("%.2f", discount)
                String productMrp = tp.getProduct_mrp();
                String discountPrice = tp.getProduct_price();

                if (productMrp.equals(discountPrice)) {
                    viewHolder.tvPrice.setVisibility(View.VISIBLE);
                    viewHolder.frameimgMsg.setVisibility(View.GONE);
                    viewHolder.tvMrp.setVisibility(View.GONE);
                } else {
                    viewHolder.tvPrice.setVisibility(View.VISIBLE);
                    viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                    viewHolder.tvMrp.setVisibility(View.VISIBLE);
                    viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                }
                viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (Connectivity.isConnected(getActivity())) {
                                    Intent intent = new Intent(getActivity(), SingleProductInformation.class);
                                    intent.putExtra("product_name", tp.getpName());
                                    intent.putExtra("shop_id", tp.getShop_id());
                                    startActivity(intent);
                                } else {
                                    GateWay gateWay = new GateWay(getActivity());
                                    gateWay.displaySnackBar(homeMainLayout);
                                }
                            }
                        });
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*--------------------------------for default value------------------------------*/
            ArrayList<String> sizesArrayList = new ArrayList<>();
            ArrayList<String> pricesArrayList = new ArrayList<>();
            ArrayList<String> DiscountpricesArrayList = new ArrayList<>();

            try {
                productCodeWithProductList1 newProductList = allProductItemsList.get(catPos);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    CategoryList tp = newProductList.productCodeWiseProducts.get(l);
                    sizesArrayList.add(tp.getProduct_size());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList1 newProductList = allProductItemsList.get(catPos);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    CategoryList tp = newProductList.productCodeWiseProducts.get(l);
                    DiscountpricesArrayList.add(tp.getProduct_price());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                productCodeWithProductList1 newProductList = allProductItemsList.get(catPos);
                selectedPosition = newProductList.getPosition();
                for (int l = 0; l < newProductList.productCodeWiseProducts.size(); l++) {
                    CategoryList tp = newProductList.productCodeWiseProducts.get(l);
                    pricesArrayList.add(tp.getProduct_mrp());

                    if (sizesArrayList.size() > 1 && pricesArrayList.size() > 1) {
                        viewHolder.LayoutSpinner.setVisibility(View.VISIBLE);
                        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), pricesArrayList, DiscountpricesArrayList, sizesArrayList, "");
                        viewHolder.spinner.setAdapter(customSpinnerAdapter);
                    } else {
                        viewHolder.LayoutSpinner.setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            final int finalCatPos = catPos;
            viewHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, final int pos, long l) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            productCodeWithProductList1 movie = allProductItemsList.get(finalCatPos);
                            CategoryList tp = movie.productCodeWiseProducts.get(pos);

                            movie.setPosition(pos); //set here position when user any wants to buy multiple size products

                            if (tp.getStrHindiName().equals("")) {
                                viewHolder.tvProductHindiName.setVisibility(View.GONE);
                            } else {
                                viewHolder.tvProductHindiName.setText(" ( " + tp.getStrHindiName() + " ) ");
                            }

                            /*HashMap AddToCartInfo = db.getCartDetails(tp.getProduct_id());
                            String btnCheckStatus = (String) AddToCartInfo.get("new_pid");*/

                            String server_cart_status = tp.getCart_pstatus();

                            if (server_cart_status.equalsIgnoreCase("0")) {
                                tp.setStatus(false);
                                viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                            } else {
                                tp.setStatus(true);
                                viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));
                            }

                            //TODO here section products images setup to glide when user select different product variant from spinner
                           /* Glide.with(getActivity())
                                    .load(tp.getProduct_image())
                                    .error(R.drawable.ic_app_transparent)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(viewHolder.img);*/
                            Picasso.with(getActivity())
                                    .load(tp.getProduct_image())
                                    .placeholder(R.drawable.loading)
                                    .error(R.drawable.ic_app_transparent)
                                    .into(viewHolder.img);

                            String size = tp.getProduct_size();
                            viewHolder.tvProductName.setText(tp.getProduct_name());
                            String dis = tp.getProduct_discount();
                            String discount = tp.setProduct_discount(dis);


                            viewHolder.tvDiscount.setText(discount + "% OFF");


                            viewHolder.tvDiscount.setText(discount + "% OFF");

                            String productMrp = tp.getProduct_mrp();
                            String discountPrice = tp.getProduct_price();
                            if (productMrp.equals(discountPrice)) {
                                viewHolder.tvPrice.setVisibility(View.VISIBLE);
                                viewHolder.frameimgMsg.setVisibility(View.GONE);
                                viewHolder.tvMrp.setVisibility(View.GONE);
                            } else {
                                viewHolder.tvPrice.setVisibility(View.VISIBLE);
                                viewHolder.frameimgMsg.setVisibility(View.VISIBLE);
                                viewHolder.tvMrp.setVisibility(View.VISIBLE);
                                viewHolder.tvMrp.setBackgroundResource(R.drawable.dash);
                            }
                            viewHolder.tvMrp.setText("\u20B9" + " " + productMrp + "/-");
                            viewHolder.tvPrice.setText("\u20B9" + " " + discountPrice + "/-");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            final int finalCatPos1 = catPos;
            viewHolder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        try {

                            productCodeWithProductList1 movie = allProductItemsList.get(finalCatPos1);
                            selectedPosition = movie.getPosition();
                            CategoryList forAddToCart = movie.productCodeWiseProducts.get(selectedPosition);

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": CountSectionAdapter clicked on add to cart button & product is: " + forAddToCart.getProduct_name(), getActivity());*/

                            shopId = forAddToCart.getShop_id();
                            pId = forAddToCart.getProduct_id();
                            //String type = movie.productCodeWiseProducts.get(selectedPosition).g
                            //call Popup to show veg 300 or grocery 500
                            String maincat_type = movie.productCodeWiseProducts.get(selectedPosition).getProduct_maincat();
                            /*HashMap AddToCartInfo;
                            AddToCartInfo = db.getCartDetails(pId);
                            strId = (String) AddToCartInfo.get("new_pid");*/

                            String cart_status = forAddToCart.getCart_pstatus();
                            if(cart_status.equalsIgnoreCase("1")){
                                Intent intent = new Intent(getActivity(), AddToCart.class);
                                startActivity(intent);
                            }

                            String toastMessg = forAddToCart.getpName();
                            addCartItemsToServer(pId, shopId,maincat_type,toastMessg);

                            forAddToCart.setStatus(false);
                        /*    if (strId == null) {
                                forAddToCart.setStatus(false);
                                db.insertCount(pId);
                                int count = (int) db.fetchAddToCartCount();
                                ((BaseActivity) getActivity()).updateAddToCartCount(count);

                                if (count >= 0) {
                                    viewHolder.btnAddToCart.setText("Go To Cart");

                                } else {
                                    viewHolder.btnAddToCart.setText("Add To Cart");

                                }

                                String toastMessg = forAddToCart.getpName();
                                addCartItemsToServer(pId, shopId,maincat_type,toastMessg);
                            } else {
                                Intent intent = new Intent(getActivity(), AddToCart.class);
                                startActivity(intent);
                            }*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }
                }

                private void addCartItemsToServer(final String pId, String shopId,String maincat_type,final String toastMessg) { //TODO Server method here
                    GateWay gateWay = new GateWay(getActivity());

                    JSONObject params = new JSONObject();
                    try {
                        params.put("product_id", pId);
                        params.put("shop_id", shopId);
                        params.put("areaname", Zone_Area);
                        params.put("v_city", v_city);
                        params.put("v_state", v_state);
                        params.put("selectedType", "");
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("Qty", 1);
                        params.put("contactNo", gateWay.getContact());
                        params.put("email", gateWay.getUserEmail());

                        if(sessionMainCat.equalsIgnoreCase("Both")){
                            params.put("sessionMainCat", sessionMainCat);
                        }else {
                            params.put("sessionMainCat", maincat_type);
                        }

                        Log.e("param_addtoCart3:",""+params.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("addCartItemsToServer3",""+response);
                            try {
                                String posts = response.getString("posts");
                                String promotional = response.getString("promotional");
                                String promotional_message = response.getString("promo_message");
                                if(promotional.equalsIgnoreCase("promotional")){
                                    openSessionDialog(promotional_message,"promo");
                                } else if (posts.equals("true")){
                                    if (strId == null) {
                                        //forAddToCart.setStatus(false);

                                        // db.insertCount(pId);

                                        int count =  SyncData();//(int) db.fetchAddToCartCount();
                                        ((BaseActivity) getActivity()).updateAddToCartCount(count);

                                        if (count >= 0) {
                                            viewHolder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                            viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_green));

                                        } else {
                                            viewHolder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                            viewHolder.btnAddToCart.setBackground(getResources().getDrawable(R.drawable.button_border));
                                        }
                                        Log.e("CartCount:",""+count);
                                    } else {
                                        Intent intent = new Intent(getActivity(), AddToCart.class);
                                        startActivity(intent);
                                    }
                                    Log.d("addCartItemsToServer3",""+posts);  Toast toast = Toast.makeText(getActivity(), toastMessg + " adding to cart", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();
                                }else {
                                    openSessionDialog(posts,"session");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            //GateWay gateWay = new GateWay(getActivity());
                            //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                            showAlertDialog("Error",error.toString(),"OK");
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });
        }
    }

    //this is for popular category section
    private class PopCatAdapter extends RecyclerView.Adapter<PopCatViewHolder> {

        private List<Movie> cardList = new ArrayList<>();

        PopCatAdapter() {
            super();
        }

        @NonNull
        @Override
        public PopCatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_popular_images, parent, false);
            return new PopCatViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull PopCatViewHolder holder, final int position) {
            final Movie movie = cardList.get(position);
            String imageName = movie.getRedeemCategoryCount();

            //TODO here popular category images setup to glide
           /* Glide.with(context).load(imageName)
                    .thumbnail(Glide.with(context).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgMainCat);*/
            Picasso.with(context).load(imageName)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgMainCat);

            holder.catName.setVisibility(View.VISIBLE);
            holder.catName.setText(movie.getRedeemCategory());
            holder.catName.setSelected(true);
            holder.catName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.catName.setSingleLine(true);

            holder.imgMainCat.setMinimumHeight(96);

            holder.imgMainCat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(getActivity())) {
                        Intent intent = new Intent(context, SearchProducts.class);
                        intent.putExtra("tag", "next");
                        intent.putExtra("selectedName", movie.getRedeemCategory());
                        startActivity(intent);
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(homeMainLayout);
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return cardList.size();
        }

        public void add(Movie movie) {
            cardList.add(movie);
        }
    }

    private class PopCatViewHolder extends RecyclerView.ViewHolder {

        final CircleImageView imgMainCat;
        final TextView catName;

        private PopCatViewHolder(View itemView) {
            super(itemView);
            imgMainCat = itemView.findViewById(R.id.imgBrand);
            catName = itemView.findViewById(R.id.catName);
        }
    }

    private void popupLimitbyCat(final String mainCat){
        String vegmsg="\n Fresh Vegetables & Fruits \n(Free Delivery above INR 300)\n\n ";
        String grocery_msg="\nGrocery products\n(Free Delivery above INR 500)";

        String MESSAGE="";
        if(mainCat.equals("Grocery")){
            MESSAGE=grocery_msg;
        }else {
            MESSAGE=vegmsg;
        }

        String Message = "\n What would you like to order today?" +
                MESSAGE+
                "\nExpected Delivery time within 3 hours.";
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(Message);
        builder1.setCancelable(true);
        builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(mainCat.equalsIgnoreCase("Fruits and Veggies")) {
                    dialog.dismiss();
                    Intent intentMainCategory = new Intent(getActivity(),BrowseByCategory.class);
                    //Bundle bundle = new Bundle();
                    //bundle.putString("product_cat", "Fruits and Veggies");
                    intentMainCategory.putExtra("product_SuperCat", "Fruits and Veggies");
                    startActivity(intentMainCategory);
                    // fragment.setArguments(bundle);
                    // fragment_replace();
                }else  if(mainCat.equalsIgnoreCase("Grocery")) {
                    dialog.dismiss();
                    Intent intentMainCategory = new Intent(getActivity(),BrowseByCategory.class);
                    //Bundle bundle2 = new Bundle();
                    //bundle2.putString("product_cat", "Grocery");
                    intentMainCategory.putExtra("product_SuperCat", "Grocery");
                    startActivity(intentMainCategory);
                    //fragment.setArguments(bundle2);
                    // fragment_replace();
                }
            }
        });
               /*  builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                         fragment = new DataSegregationLevelThree();
                        Bundle bundle = new Bundle();
                        bundle.putString("product_cat", "Fruits and Veggies");
                        bundle.putString("product_mainCat", "Grocery");
                        fragment.setArguments(bundle);
                        fragment_replace();
                    }
                }); */
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void fragment_replace() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate("addDataSegregationLevelThree", 0);
        if (!fragmentPopped) { //fragment not in back stack, create it.
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment, "DataSegregationLevelThree")
                    .addToBackStack("addDataSegregationLevelThree")
                    .commit();
        }
    }

    double roundTwoDecimals(double d)
    {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        Log.e("value_decimal:",""+Double.valueOf(twoDForm.format(d)));
        return Double.valueOf(twoDForm.format(d));

    }

    //main cat
    private void getCategoriesData() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            ProductMainCatWise = new ArrayList<>();
            linkedHashMap = new LinkedHashMap();
            customAdapter = new CustomAdapter();

            JSONObject params = new JSONObject();
            try {
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlBrowseByMainCategory, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if (response.isNull("posts")) {
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        Log.e("broseByMCate_Response:",""+response.toString());

                        try {
                            JSONArray mainCatArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);

                                linkedHashMap.put(jsonMainCatObject.getString("maincategory"), "");
                                ProductMainCatWise.add(new MainCatWiseInfo(jsonMainCatObject.getString("maincategory"),
                                        jsonMainCatObject.getString("product_cat_image")));

                                //if(i==0) {
                                if(jsonMainCatObject.getString("maincategory").equalsIgnoreCase("Fruits and Veggies")) {
                                    Log.e("Fruits_Veggies:",""+jsonMainCatObject.getString("product_cat_image"));
                                    Picasso.with(context)
                                            .load(jsonMainCatObject.getString("product_cat_image"))
                                            .into(fruits_veggies_img);
                                }
                                //if(i==1){
                                if(jsonMainCatObject.getString("maincategory").equalsIgnoreCase("Grocery")){
                                    Log.e("Grocery:",""+jsonMainCatObject.getString("product_cat_image"));
                                    Picasso.with(context)
                                            .load(jsonMainCatObject.getString("product_cat_image"))
                                            .into(grocery_img);
                                }
                                if(jsonMainCatObject.getString("maincategory").equalsIgnoreCase("Eggs Meat and Chicken")){//Eggs Meat and Chicken
                                    card_chicken_tag.setVisibility(View.VISIBLE);
                                    //img_chicken
                                    Log.e("Eggs_Chicken_Url:",""+jsonMainCatObject.getString("product_cat_image"));
                                    Picasso.with(context)
                                            .load(jsonMainCatObject.getString("product_cat_image"))
                                            .into(img_chicken);
                                }
                            }
                            if (linkedHashMap.size() > 0) {
                                for (Object o : linkedHashMap.keySet()) {
                                    String key = (String) o;
                                    InfoWithMainCat infowithMainCat = new InfoWithMainCat();
                                    infowithMainCat.mainCat = key;
                                    infowithMainCat.mainCatWiseInfos = new ArrayList<>();
                                    for (MainCatWiseInfo pp : ProductMainCatWise) {
                                        if (pp.getProduct_maincat().equals(key)) {
                                            infowithMainCat.mainCatWiseInfos.add(pp);
                                        }
                                    }
                                    customAdapter.add(infowithMainCat);
                                }
                                recyclerView.setAdapter(customAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    //GateWay gateWay = new GateWay(getActivity());
                    //gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            //DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
            request.setRetryPolicy(new DefaultRetryPolicy(
                    //DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5,
                    600000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getActivity()).add(request);
        }
    }
    public class MainCatWiseInfo {

        private String product_maincat;
        private String product_cat;
        private String product_cat_image;

        MainCatWiseInfo(String product_maincat, String product_cat, String product_cat_image) {
            this.product_maincat = product_maincat;
            this.product_cat = product_cat;
            this.product_cat_image = product_cat_image;
        }

        public MainCatWiseInfo(String product_maincat, String product_cat_image) {
            this.product_maincat = product_maincat;
            this.product_cat_image = product_cat_image;
        }

        public String getProduct_maincat() {
            return product_maincat;
        }

        public String getProduct_cat() {
            return product_cat;
        }

        public String getProduct_cat_image() {
            return product_cat_image;
        }

    }
    private class InfoWithMainCat {
        String mainCat;
        ArrayList<MainCatWiseInfo> mainCatWiseInfos;
    }
    private class ChildViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCat;
        private ImageView imgProductCat;
        private LinearLayout boxLinearLayout;

        ChildViewHolder(View itemView) {
            super(itemView);
            tvCat = itemView.findViewById(R.id.txtCat);
            imgProductCat = itemView.findViewById(R.id.productCatImg);
            boxLinearLayout = itemView.findViewById(R.id.boxLinearLayout);
            boxLinearLayout.setTag(this);
        }
    }
    private class ChildAdapter extends RecyclerView.Adapter<ChildViewHolder> {

        private List<MainCatWiseInfo> mChildItems;

        ChildAdapter(ArrayList<MainCatWiseInfo> mainCatWiseInfos) {
            this.mChildItems = mainCatWiseInfos;
        }

        @NonNull
        @Override
        public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inner_data_segregration_card_halfview, parent, false);//inner_data_segregration_card_view
            return new ChildViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ChildViewHolder holder, int position) {
            final MainCatWiseInfo childMovie = mChildItems.get(position);

            //TODO here we show category images under the main category setup to glide
           /* Glide.with(getActivity()).load(childMovie.getProduct_cat_image())
                    .thumbnail(Glide.with(getActivity()).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProductCat);*/

            Picasso.with(getActivity()).load(childMovie.getProduct_cat_image())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgProductCat);

            holder.tvCat.setText(childMovie.getProduct_cat());

            holder.boxLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*
                    UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on view: " + childMovie.getProduct_maincat() + " & sub cat: " + holder.tvCat.getText().toString(), getActivity());*/

                   /* SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(getActivity());
                    String sessioinCat = sharedPreferencesUtils.getCategory();
                    //chk session
                    if(sessioinCat.equalsIgnoreCase("Fruits and Veggies")) {
                        fragment = new BrowseByCategory();
                        Bundle bundle = new Bundle();
                        //bundle.putString("product_cat", holder.tvCat.getText().toString());
                        bundle.putString("product_SuperCat", childMovie.getProduct_maincat());
                        fragment.setArguments(bundle);
                        fragment_replace();
                    }else  if(sessioinCat.equalsIgnoreCase("Grocery")) {
                        fragment = new BrowseByCategory();
                        Bundle bundle = new Bundle();
                        //bundle.putString("product_cat", holder.tvCat.getText().toString());
                        bundle.putString("product_SuperCat", childMovie.getProduct_maincat());
                        fragment.setArguments(bundle);
                        fragment_replace();
                    }else if(sessioinCat.equalsIgnoreCase("nocat")){
                        fragment = new BrowseByCategory();
                        Bundle bundle = new Bundle();
                        //bundle.putString("product_cat", holder.tvCat.getText().toString());
                        bundle.putString("product_SuperCat", childMovie.getProduct_maincat());
                        fragment.setArguments(bundle);
                        fragment_replace();
                    }else {
                        Toast.makeText(getActivity(),"Place your order or remove items from Cart",Toast.LENGTH_SHORT).show();
                    }*/
                    Intent bundle = new Intent(getActivity(),BrowseByCategory.class);
                    //Bundle bundle = new Bundle();
                    //bundle.putString("product_cat", holder.tvCat.getText().toString());
                    bundle.putExtra("product_SuperCat", childMovie.getProduct_maincat());
                    startActivity(bundle);
                    //fragment.setArguments(bundle);
                    //fragment_replace();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mChildItems.size();
        }
    }
    private class OuterViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMainCat;
        private RecyclerView innerRecyclerView;

        OuterViewHolder(View itemView) {
            super(itemView);
            tvMainCat = itemView.findViewById(R.id.txtMainCat);
            innerRecyclerView = itemView.findViewById(R.id.inner_dataSegregation_recycler_view);
            innerRecyclerView.setHasFixedSize(true);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
            innerRecyclerView.setLayoutManager(gridLayoutManager);
        }
    }
    public class CustomAdapter extends RecyclerView.Adapter<OuterViewHolder> {

        ArrayList<InfoWithMainCat> mItems = new ArrayList<>();
        ChildAdapter childAdapter;

        CustomAdapter() {
        }

        @NonNull
        @Override
        public OuterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.outer_data_segregration_two_card_halfview, parent, false);
            return new OuterViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull OuterViewHolder holder, int position) {
            InfoWithMainCat movie = mItems.get(position);
            try {
                for (int k = 0; k < mItems.size(); k++) {
                    for (int j = k; j < movie.mainCatWiseInfos.size(); j++) {
                        MainCatWiseInfo tp = movie.mainCatWiseInfos.get(j);
                        holder.tvMainCat.setText(tp.getProduct_maincat());

                        Log.e("subCatLog:",""+tp.getProduct_maincat().toString());

                        /*childAdapter = new ChildAdapter(movie.mainCatWiseInfos);
                        holder.innerRecyclerView.setAdapter(childAdapter);*/
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(InfoWithMainCat innerMovie) {
            mItems.add(innerMovie);
        }
    }

    public  void appUpdate(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_app_update);
        dialog.setCancelable(false);
        Button button = dialog.findViewById(R.id.btn_update_playstore);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=app.apneareamein.shopping&hl=en"));
                startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    // Get Order Details and feedback stutus
    private void getFeedDetails() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());

            String strContact = gateWay.getContact();

            JSONObject params = new JSONObject();
            try {
                params.put("contact_no", strContact);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlmyLastOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.e("LastOrderRes:",""+response);
                        event_array_url.clear();
                        if (response.isNull("posts")) {

                        } else {
                            JSONArray mainOrderArray = response.getJSONArray("posts");
                            JSONArray event_array = response.getJSONArray("event_array");
                            //Toast.makeText(mcontext,"Review Successfully",Toast.LENGTH_LONG).show();
                            for (int i = 0; i < mainOrderArray.length(); i++) {
                                JSONObject jSonMyOrderData = mainOrderArray.getJSONObject(i);
                                fb_status = jSonMyOrderData.getString("fb_status");
                                o_status = jSonMyOrderData.getString("o_status");
                                String r_status = jSonMyOrderData.getString("refer_status");
                                String r_color = jSonMyOrderData.getString("refer_color");
                                share_containt =jSonMyOrderData.getString("share_containt");
                                offer_order_id =jSonMyOrderData.getString("offer_order_id");
                                offer_status =jSonMyOrderData.getString("offer_status");
                                offer_url =jSonMyOrderData.getString("offer_url");
                                //o_status

                                if(r_status.equalsIgnoreCase("1")){
                                    card_refer_tag.setVisibility(View.VISIBLE);
                                }else {
                                    card_refer_tag.setVisibility(View.GONE);
                                }

                                if (offer_status.equalsIgnoreCase("active")) {

                                    card_oneRupee_tag.setVisibility(View.VISIBLE);
                                }else {
                                    card_oneRupee_tag.setVisibility(View.GONE);
                                }


                                try{
                                    card_refer_tag.setCardBackgroundColor(Color.parseColor(r_color));
                                }catch (Exception e){
                                    e.getMessage();
                                }

                                if(fb_status.equalsIgnoreCase("New Feedback")&& o_status.equalsIgnoreCase("Delivered")){
                                    card_feedback_tag.setVisibility(View.VISIBLE);
                                    //card_refer_tag.setVisibility(View.VISIBLE);
                                }else {
                                    card_feedback_tag.setVisibility(View.GONE);
                                    //card_refer_tag.setVisibility(View.GONE);
                                }
                            }

                            for(int j =0 ;j<event_array.length();j++){
                                JSONObject jsonObject = event_array.getJSONObject(j);
                                event_array_url.add(jsonObject.getString("event_link"));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                   // GateWay gateWay = new GateWay(getActivity());
                   // gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                    showAlertDialog("Error",error.toString(),"OK");
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getActivity()).add(request);
        }
    }

    public void sendReferral(Context context) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getInvitationMessage());
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "PICODEL");
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, "PICODEL REFARAL"));
        card_feedback_tag.setVisibility(View.GONE);
       /* Intent i = new Intent("com.android.vending.INSTALL_REFERRER");
        //Set Package name
        i.setPackage("app.apneareamein.shopping");
        //referrer is a composition of the parameter of the campaing
        i.putExtra("referrer", "OkPICODEL99");
        sendBroadcast(i);*/
    }

    private String getInvitationMessage(){
        //String SUB = "Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 2 hours...\nDownload PICODEL App and enjoy lot more unique features !";
        String SUB =  share_containt;//  "Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 2 hours...\nDownload PICODEL App and enjoy lot more unique features !";
        String playStoreLink = SUB+"\n "+"https://play.google.com/store/apps/details?id=app.apneareamein.shopping&referrer=";
        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String user_id = prefs.getString("user_id", "");
        Log.e("user_id",""+user_id);
        return playStoreLink + "PIC"+user_id;//utm_source=
    }

    // Getting response by volley StringRequest pure response
    private void getVolleryResStirng(){

        customAdapterForBrand = new CustomAdapterForBrand(getActivity());
        popCatAdapter = new PopCatAdapter();
        customAdapterForComboOffer = new CustomAdapterForComboOffer(getActivity());

        productImage = new ArrayList<>();
        event_array_url = new ArrayList<>();
        productImageCategory = new ArrayList<>();
        cashBackBanner = new ArrayList<>();
        eventImgPath = new ArrayList<>();
        eventLink = new ArrayList<>();
        offerCodeHashMap = new HashMap();
        ProductWiseComboOffer = new ArrayList<>();
        FinalComboOffer = new ArrayList<>();

        //header product footer
        tempProductListHashMap = new LinkedHashMap();
        tempProductWiseList = new ArrayList<>();
        FinalList2 = new ArrayList<>();
        imgPath = new ArrayList<>();
        catName = new ArrayList<>();
        //header product footer
        //mostly bought & recommended products
        FinalList = new ArrayList<>();
        FinalList1 = new ArrayList<>();
        //mostly bought & recommended products


        final GateWay gateWay = new GateWay(getActivity());
        //gateWay.progressDialogStart();
        simpleProgressBar.setVisibility(View.VISIBLE);

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");
        v_city = prefs.getString("v_city", "");
        v_state = prefs.getString("v_state", "");

        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST,ApplicationUrlAndConstants.urlHomePage11, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ResHompage2:",""+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    cash_back_points = jsonObject.getString("cash_back_points");
                    noti_count = jsonObject.getString("noti_count");
                    version_status = jsonObject.getString("version_status");
                    String product_city = jsonObject.getString("product_city");
                    Log.e("product_city2",""+product_city);


                // Code for the Notification Count
                try {
                    if (noti_count.equals("0")) {
                        ((BaseActivity) getActivity()).tvNotification.setVisibility(View.INVISIBLE);
                        ((BaseActivity) getActivity()).imgNotify.setVisibility(View.GONE);
                    } else {
                        ((BaseActivity) getActivity()).tvNotification.setVisibility(View.VISIBLE);
                        ((BaseActivity) getActivity()).imgNotify.setVisibility(View.VISIBLE);
                        ((BaseActivity) getActivity()).tvNotification.setText(noti_count);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


                // Code for the Update Dailog7
                if (version_status.equals("update")) {
                    updateLayout.setVisibility(View.VISIBLE);
                    appUpdate();
                } else {
                    updateLayout.setVisibility(View.GONE);
                }

                //Code for the Event Array response

                if (!jsonObject.isNull("event")) {
                    JSONArray jsonArray1 = jsonObject.getJSONArray("event");
                    for (int i = 0; i < jsonArray1.length(); i++) {
                        JSONObject jsonEventObject = (JSONObject) jsonArray1.get(i);
                        eventImgPath.add(jsonEventObject.getString("event_image"));
                        eventLink.add(jsonEventObject.getString("event_link"));
                        event_status = jsonEventObject.getString("event_status");
                    }
                  }

                  if (!jsonObject.isNull("front_banner")) {
                        JSONArray jsonArray4 = jsonObject.getJSONArray("front_banner");
                        for (int i = 0; i < jsonArray4.length(); i++) {
                            JSONObject jsonImageObject = (JSONObject) jsonArray4.get(i);
                            productImage.add(jsonImageObject.getString("front_search_banner")); //add to arrayList
                            productImageCategory.add(jsonImageObject.getString("front_search_category")); //add to arrayList
                        }
                        if (productImage.size() > 0) {
                            AnimatedSlideShow(); //set the banner images
                        }
                    } else {
                        viewPager.setVisibility(View.GONE);
                    }

                    // for the shop category wise sliders
                    JSONArray jsonArray_4 = jsonObject.getJSONArray("front_banner2");
                    Log.e("new_Slider2",""+jsonArray_4);
                    productImage2 = new ArrayList<>();
                    productImageCategory2 = new ArrayList<>();

                    if (!jsonObject.isNull("front_banner2")) {
                        JSONArray jsonArray_slider = jsonObject.getJSONArray("front_banner2");
                        for (int i = 0; i < jsonArray_slider.length(); i++) {
                            JSONObject jsonImageObject = (JSONObject) jsonArray_slider.get(i);
                            Log.e("value_slider",jsonImageObject.getString("slider_image"));
                            productImage2.add(jsonImageObject.getString("slider_image")); //add to arrayList
                            productImageCategory2.add(jsonImageObject.getString("description")); //add to arrayList
                        }
                       if (productImage2.size() > 0) {
                            AnimatedSlideShow(); //set the banner images
                        }
                    } else {
                        viewPager.setVisibility(View.GONE);
                    }

                    // this for the most bought items
                    if (!jsonObject.isNull("mostly_bought")) {
                        JSONArray mainClassificationJsonArray2 = jsonObject.getJSONArray("mostly_bought");
                        String title2 = jsonObject.getString("mostly_bought_title");
                        layUltimate1.setVisibility(View.VISIBLE);
                        tvUltimate.setText(title2);


                        Log.e("mostly_bought_pstatus:",""+mainClassificationJsonArray2.toString());


                        for (int i = 0; i < mainClassificationJsonArray2.length(); i++) {
                            JSONObject jSonClassificationData = mainClassificationJsonArray2.getJSONObject(i);

                            productListHashMapfor_mr_products.put(jSonClassificationData.getString("code"), "");

                            productWiseList.add(new UltimateProductList(jSonClassificationData.getString("code"),
                                    jSonClassificationData.getString("product_id"),
                                    jSonClassificationData.getString("shop_id"),
                                    jSonClassificationData.getString("deliveryavailable"),
                                    jSonClassificationData.getString("product_name"),
                                    jSonClassificationData.getString("product_image"),
                                    jSonClassificationData.getString("product_mrp"),
                                    jSonClassificationData.getString("product_price"),
                                    jSonClassificationData.getString("product_discount"),
                                    jSonClassificationData.getString("product_size"),
                                    jSonClassificationData.getString("pName"),
                                    jSonClassificationData.getString("product_maincat"), //cart_pstatus
                                    jSonClassificationData.getString("product_hindi_name"),
                                    jSonClassificationData.getString("cart_pstatus")));//jSonClassificationData.getString("cart_pstatus")
                        }

                        for (Object o : productListHashMapfor_mr_products.keySet()) {
                            String key = (String) o;
                            productCodeWithProductList withProductCode = new productCodeWithProductList();
                            withProductCode.code = key;
                            withProductCode.productCodeWiseProducts = new ArrayList<>();
                            for (UltimateProductList pp : productWiseList) {
                                if (pp.code.equals(key)) {
                                    withProductCode.productCodeWiseProducts.add(pp);
                                }
                            }
                            FinalList.add(withProductCode);
                        }
                        ultimateCardAdapter = new UltimateCardAdapter(FinalList);
                        recyclerViewUltimate.setAdapter(ultimateCardAdapter);
                    } else {
                        layUltimate1.setVisibility(View.GONE);
                    }


                    //this is for section header and footer
                    if (!jsonObject.isNull("posts")) {
                        Log.e("product_data_pstatus:",""+jsonObject.toString());
                        JSONArray mainClassificationJsonArray = jsonObject.getJSONArray("posts");

                        Log.e("post_pstatus:",""+mainClassificationJsonArray.toString());


                        JSONArray headerFooterJsonArray = jsonObject.getJSONArray("header_footer");

                        recyclerViewForCategory.setVisibility(View.VISIBLE);
                        for (int i = 0; i < headerFooterJsonArray.length(); i++) {
                            JSONObject jSonHeaderFooterData;
                            try {
                                jSonHeaderFooterData = headerFooterJsonArray.getJSONObject(i);
                                catName.add(jSonHeaderFooterData.getString("catName"));
                                imgPath.add(jSonHeaderFooterData.getString("imagePath"));
                                Log.e("Header_imag:",""+jSonHeaderFooterData.getString("imagePath"));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                        for (int i = 0; i < mainClassificationJsonArray.length(); i++) {


                            JSONObject jSonClassificationData = mainClassificationJsonArray.getJSONObject(i);

                            tempProductListHashMap.put(jSonClassificationData.getString("code"), "");

                            tempProductWiseList.add(new CategoryList(jSonClassificationData.getString("code"),

                                    jSonClassificationData.getString("shop_id"),
                                    jSonClassificationData.getString("shop_name"),
                                    jSonClassificationData.getString("product_id"),
                                    jSonClassificationData.getString("product_name"),
                                    jSonClassificationData.getString("product_hindi_name"),
                                    jSonClassificationData.getString("product_image"),
                                    jSonClassificationData.getString("product_mrp"),
                                    jSonClassificationData.getString("product_price"),
                                    jSonClassificationData.getString("product_discount"),
                                    jSonClassificationData.getString("pName"),
                                    jSonClassificationData.getString("totalSize"),
                                    jSonClassificationData.getString("product_maincat"),
                                    jSonClassificationData.getString("product_brand"),
                                    jSonClassificationData.getString("type"),
                                    jSonClassificationData.getString("cart_pstatus")));//cart_pstatus
                        }
                        productListHashMap = tempProductListHashMap;
                        ProductWiseList = tempProductWiseList;

                        if (ProductWiseList.size() > 0) {
                            for (Object o : productListHashMap.keySet()) {
                                String key = (String) o;
                                productCodeWithProductList1 withProductCode = new productCodeWithProductList1();
                                withProductCode.code = key;
                                withProductCode.productCodeWiseProducts = new ArrayList<>();
                                for (CategoryList pp : ProductWiseList) {
                                    if (pp.code.equals(key)) {
                                        withProductCode.productCodeWiseProducts.add(pp);
                                    }
                                }
                                FinalList2.add(withProductCode);
                            }
                            CountSectionAdapter adapter = new CountSectionAdapter(context, imgPath, catName, FinalList2);
                            recyclerViewForCategory.setAdapter(adapter);

                            GridLayoutManager layoutManager = new GridLayoutManager(context, 1, LinearLayoutManager.VERTICAL, false);
                            SectionedSpanSizeLookup lookup = new SectionedSpanSizeLookup(adapter, layoutManager);
                            layoutManager.setSpanSizeLookup(lookup);
                            recyclerViewForCategory.setLayoutManager(layoutManager);
                        }
                    } else {
                        recyclerViewForCategory.setVisibility(View.GONE);
                    }



                    //Browser link here
                    if (event_status.contains("deactive")) {
                        eventLinearLayout.setVisibility(View.GONE);
                    } else {
                        eventLinearLayout.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(eventImgPath.get(0))
                                .into(eventImage);

                        eventLinearLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(eventLink.get(0)));
                                startActivity(i);
                            }
                        });
                    }

                    if (!jsonObject.isNull("cashback_banner")) {
                        JSONArray jsonArray2 = jsonObject.getJSONArray("cashback_banner");
                        for (int i = 0; i < jsonArray2.length(); i++) {
                            JSONObject jsonCashbackBannerObject = (JSONObject) jsonArray2.get(i);
                            cashBackBanner.add(jsonCashbackBannerObject.getString("cashback"));
                            status = jsonCashbackBannerObject.getString("status");
                        }
                    }


                    //this is for popular category
                    if (!jsonObject.isNull("popularcategory")) {
                        LinPopCat.setVisibility(View.GONE);
                        tvShopByPopCategory.setText(jsonObject.getString("popularCategoryText"));
                        JSONArray jsonArray5 = jsonObject.getJSONArray("popularcategory");

                        Log.e("popularcat_pstatus:",""+jsonArray5.toString());

                        for (int i = 0; i < jsonArray5.length(); i++) {
                            JSONObject jsonMainCatObject = (JSONObject) jsonArray5.get(i);

                            tvShopByPopCategory.setVisibility(View.VISIBLE);

                            Movie movie = new Movie(jsonMainCatObject.getString("popularCategory"), jsonMainCatObject.getString("subcategory_image"));
                            Log.d("popularcategory",jsonMainCatObject.getString("subcategory_image"));
                            popCatAdapter.add(movie);
                        }
                        recyclerViewPopCat.setAdapter(popCatAdapter);
                    }

                    /*try {
                        if (!jsonObject.isNull("master_search")) {
                            JSONArray mainClassificationJsonArray1 = jsonObject.getJSONArray("master_search");
                            String title1 = jsonObject.getString("recommended_title");
                            layUltimate2.setVisibility(View.VISIBLE);
                            tvUltimate1.setText(title1);

                            Log.e("master_search_pstatus:",""+response.toString());
                            String title2 = jsonObject.getString("combo_offer");
                            Log.e("combo_offer:",""+title2);
                            for (int i = 0; i < mainClassificationJsonArray1.length(); i++) {
                                JSONObject jSonClassificationData = mainClassificationJsonArray1.getJSONObject(i);

                                productListHashMap1.put(jSonClassificationData.getString("code"), "");

                                productWiseList1.add(new UltimateProductList(jSonClassificationData.getString("code"),
                                        jSonClassificationData.getString("product_id"),
                                        jSonClassificationData.getString("shop_id"),
                                        jSonClassificationData.getString("deliveryavailable"),
                                        jSonClassificationData.getString("product_name"),
                                        jSonClassificationData.getString("product_image"),
                                        jSonClassificationData.getString("product_mrp"),
                                        jSonClassificationData.getString("product_price"),
                                        jSonClassificationData.getString("product_discount"),
                                        jSonClassificationData.getString("product_size"),
                                        jSonClassificationData.getString("pName"),
                                        jSonClassificationData.getString("product_maincat"),//cart_pstatus
                                        jSonClassificationData.getString("product_hindi_name"),
                                        jSonClassificationData.getString("cart_pstatus")));//jSonClassificationData.getString("cart_pstatus")
                            }

                            for (Object o : productListHashMap1.keySet()) {
                                String key = (String) o;
                                productCodeWithProductList withProductCode1 = new productCodeWithProductList();
                                withProductCode1.code = key;
                                withProductCode1.productCodeWiseProducts = new ArrayList<>();
                                for (UltimateProductList pp : productWiseList1) {
                                    if (pp.code.equals(key)) {
                                        withProductCode1.productCodeWiseProducts.add(pp);
                                    }
                                }
                                FinalList1.add(withProductCode1);
                            }
                            ultimateCardAdapter1 = new UltimateCardAdapter1(FinalList1);
                            recyclerViewUltimate1.setAdapter(ultimateCardAdapter1);
                        } else {
                            layUltimate2.setVisibility(View.GONE);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }*/


                    //this is for three products layout
                    if (jsonObject.isNull("productData1")) {
                        LinOfferlayout.setVisibility(View.GONE);
                    } else {
                        LinOfferlayout.setVisibility(View.VISIBLE);
                        JSONArray jsonArray6 = jsonObject.getJSONArray("productData1");
                        Log.e("ComboOfferList:",""+jsonArray6.toString());

                        for (int r = 0; r < jsonArray6.length(); r++) {
                            JSONObject jsonProductDataOneObject = (JSONObject) jsonArray6.get(r);

                            offerCodeHashMap.put(jsonProductDataOneObject.getString("selectedName1"), "");

                            ProductWiseComboOffer.add(new OfferCodeWiseProduct(jsonProductDataOneObject.getString("product_name1"), jsonProductDataOneObject.getString("product_image1"),
                                    jsonProductDataOneObject.getString("product_mrp1"), jsonProductDataOneObject.getString("product_price1"),
                                    jsonProductDataOneObject.getString("product_discount1"), jsonProductDataOneObject.getString("selectedName1"),
                                    jsonProductDataOneObject.getString("topProductsTextOne"), jsonProductDataOneObject.getString("product_name"),
                                    jsonProductDataOneObject.getString("product_maincat"),
                                    jsonProductDataOneObject.getString("product_brand"),
                                    jsonProductDataOneObject.getString("shop_id"),
                                    "0"));
                        }
                        for (Object o : offerCodeHashMap.keySet()) {
                            String key = (String) o;

                            comboOfferWithOfferCode withOfferCode = new comboOfferWithOfferCode();
                            withOfferCode.selectedName1 = key;
                            withOfferCode.offerCodeWiseProducts = new ArrayList<>();
                            for (OfferCodeWiseProduct pp : ProductWiseComboOffer) {
                                if (pp.getOfferCategory().equals(key)) {
                                    withOfferCode.offerCodeWiseProducts.add(pp);
                                }
                            }
                            FinalComboOffer.add(withOfferCode);
                        }
                        customAdapterForCategory = new CustomAdapterForCategory(getActivity(), FinalComboOffer);
                        recyclerViewForOfferLayout.setAdapter(customAdapterForCategory);
                    }



                    //this is for combo offer
                    if (jsonObject.isNull("combo_offer")) {
                        comboofferLinearLayout.setVisibility(View.GONE);
                    } else {
                        comboofferLinearLayout.setVisibility(View.VISIBLE);
                        try {
                            JSONArray comboofferJsonArray = jsonObject.getJSONArray("combo_offer");

                            Log.e("combo_offer_pstatus:",""+comboofferJsonArray.toString());


                            String comboOfferHeader = jsonObject.getString("comboOfferHeader");
                            String comboOfferfade = jsonObject.getString("comboOfferFade");

                            if (comboOfferfade.equals("")) {
                                tvComboNote.setVisibility(View.GONE);
                            } else {
                                tvComboNote.setText(comboOfferfade);
                            }
                            tvComboOfferStart.setVisibility(View.VISIBLE);
                            tvComboOfferStart.setText(comboOfferHeader);



                            Log.e("ComboOfferList:",""+comboofferJsonArray.toString());

                            for (int i = 0; i < comboofferJsonArray.length(); i++) {
                                JSONObject jSonClassificationData = comboofferJsonArray.getJSONObject(i);

                                comboList movie = new comboList(jSonClassificationData.getString("offer_name"),
                                        jSonClassificationData.getString("total_price"),
                                        jSonClassificationData.getString("offer_price"),
                                        jSonClassificationData.getString("discount_in_percent"),
                                        jSonClassificationData.getString("combo_image"),
                                        jSonClassificationData.getString("shop_id"),
                                        jSonClassificationData.getString("product_maincat"),
                                        jSonClassificationData.getString("offer_end_date"),
                                        jSonClassificationData.getString("cart_pstatus"));//cart_pstatus
                                customAdapterForComboOffer.add(movie);

                            }
                            comboOfferRecyclerView.setAdapter(customAdapterForComboOffer);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    // THis is for brand list items
                    //this is for brand
                    if (jsonObject.isNull("brand")) {
                        LinShopByBrand.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray3 = jsonObject.getJSONArray("brand");
                        Log.d("Brandlist", "jsonArray3"+jsonArray3);
                        tvShopByBrand.setVisibility(View.VISIBLE);
                        tvShopByBrand.setText(jsonObject.getString("shopByBrandsText"));
                        for (int k = 0; k < jsonArray3.length(); k++) {
                            JSONObject jsonImageObject = (JSONObject) jsonArray3.get(k);
                            Movie movie = new Movie(jsonImageObject.getString("brand_images"),
                                    jsonImageObject.getString("brand_name"));
                            customAdapterForBrand.add(movie);
                        }

                        if (tvShopByBrand.getText().toString().length() > 0) {
                            recyclerViewForBrands.setAdapter(customAdapterForBrand);
                        }
                    }


                } catch (JSONException e) { // Close Json main response
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("ExpHompage:",""+error);

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
                params.put("contact", gateWay.getContact());
                params.put("vcity", v_city);
                params.put("vstate", v_state);
                params.put("areaname", Zone_Area);
                params.put("os", "android");
                params.put("user_version_name", ApplicationUrlAndConstants.versionName);
                //params.put("user_version_name", "2.0.44");
                Log.e("HomePage11_parma:",""+params.toString());
                Log.e("Volley_parma2:",""+params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);

    }


    private void showAlertDialog(String title, String message, String buttonText) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setIcon(R.drawable.app_icon_new); //Setting Dialog icon Image
        alertDialog.setTitle(title);  // Setting Dialog Title
        alertDialog.setMessage(message); // Setting Dialog Message
        alertDialog.setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { // Setting OK Button
                //context.finish();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.show(); // Showing Alert Message
        TextView messageText = dialog.findViewById(android.R.id.message);
        assert messageText != null;
        messageText.setGravity(Gravity.CENTER);
        dialog.show();
    }

    //Get Cart count from yii api
    public void getUserCartCount() {

        GateWay gateWay = new GateWay(getActivity());
        String urlCartlist = "";
        //String urlCartlist = "ApplicationUrlAndConstants.userTermsCondition";

        try {
            urlCartlist = ApplicationUrlAndConstants.url_getCartCount
            + "?contact=" + gateWay.getContact()+"email="+ URLEncoder.encode(gateWay.getUserEmail(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Log.d("userCartCount", urlCartlist);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlCartlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("GetCartCountRes", response);


                            int value = Integer.parseInt(jsonObject.getString("normal_cart_count"));

                            ((BaseActivity) getActivity()).updateAddToCartCount(value);
                            CartCount = Integer.parseInt(jsonObject.getString("normal_cart_count"));
                           /* boolean strStatus = jsonObject.getBoolean("status");
                            String user_terms = jsonObject.getString("user_terms");
                            String user_remark = jsonObject.getString("user_remark");*/

                            /*if ((strStatus == true)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() > 0) {

                                    //cartListAdapter.notifyDataSetChanged();
                                } else {
                                    //tv_network_con.setText("No Records Found");
                                }
                            } else if (strStatus == false) {

                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        MySingleton.getInstance(getActivity()).addToRequestQueue(requestCartList);

    }

    //urldrawer_options from yii api
    public void getBrowseCategory() {

        GateWay gateWay = new GateWay(getActivity());
        String urlbrowoseCategory = "";
        //String urlCartlist = "ApplicationUrlAndConstants.userTermsCondition";

        try {
            urlbrowoseCategory = ApplicationUrlAndConstants.url_getbrowoseCategory
                    + "?contact=" + gateWay.getContact()+"&city="+ URLEncoder.encode(v_city,"UTF-8")+"&area="+ URLEncoder.encode(Zone_Area,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Log.d("urlbrowoseCategory", urlbrowoseCategory);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlbrowoseCategory,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("browoseCategoryRes", response);

                            simpleProgressBar.setVisibility(View.INVISIBLE);

                                JSONArray mainCatArray = jsonObject.getJSONArray("posts");
                                for (int i = 0; i < mainCatArray.length(); i++) {
                                    JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);

                                    /*linkedHashMap.put(jsonMainCatObject.getString("maincategory"), "");
                                    ProductMainCatWise.add(new MainCatWiseInfo(jsonMainCatObject.getString("maincategory"),
                                            jsonMainCatObject.getString("product_cat_image")));*/

                                    //if(i==0) {
                                    if(jsonMainCatObject.getString("maincategory").equalsIgnoreCase("Fruits and Veggies")) {
                                        Log.e("Fruits_Veggies:",""+jsonMainCatObject.getString("product_cat_image"));
                                        Picasso.with(context)
                                                .load(jsonMainCatObject.getString("product_cat_image"))
                                                .into(fruits_veggies_img);
                                    }
                                    //if(i==1){
                                    if(jsonMainCatObject.getString("maincategory").equalsIgnoreCase("Grocery")){
                                        Log.e("Grocery:",""+jsonMainCatObject.getString("product_cat_image"));
                                        Picasso.with(context)
                                                .load(jsonMainCatObject.getString("product_cat_image"))
                                                .into(grocery_img);
                                    }
                                    if(jsonMainCatObject.getString("maincategory").equalsIgnoreCase("Eggs Meat and Chicken")){//Eggs Meat and Chicken
                                        card_chicken_tag.setVisibility(View.VISIBLE);
                                        //img_chicken
                                        Log.e("Eggs_Chicken_Url:",""+jsonMainCatObject.getString("product_cat_image"));
                                        Picasso.with(context)
                                                .load(jsonMainCatObject.getString("product_cat_image"))
                                                .into(img_chicken);
                                    }
                                }
                                /*if (linkedHashMap.size() > 0) {
                                    for (Object o : linkedHashMap.keySet()) {
                                        String key = (String) o;
                                        InfoWithMainCat infowithMainCat = new InfoWithMainCat();
                                        infowithMainCat.mainCat = key;
                                        infowithMainCat.mainCatWiseInfos = new ArrayList<>();
                                        for (MainCatWiseInfo pp : ProductMainCatWise) {
                                            if (pp.getProduct_maincat().equals(key)) {
                                                infowithMainCat.mainCatWiseInfos.add(pp);
                                            }
                                        }
                                        customAdapter.add(infowithMainCat);
                                    }
                                    recyclerView.setAdapter(customAdapter);
                                }*/


                        } catch (JSONException e) {
                            e.printStackTrace();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
        MySingleton.getInstance(getActivity()).addToRequestQueue(requestCartList);

    }

    //Method to show the Tersms and Condition
    public void homeCartUpdateDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dailog_cart_update_price, null);

        Button btn_submitreson = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);

        TextView tv_home_message = view.findViewById(R.id.tv_home_message);

       // tv_home_message.setText(home_page_message);

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Log.e("v_city_value",""+v_city);
                updateCartPrices();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    private void updateCartPrices() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();


            JSONObject params = new JSONObject();
            try {
                params.put("city", v_city);
                params.put("contactNo", gateWay.getContact());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUpdate_User_Cart_Pirces, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if (response.isNull("posts")) {

                    } else {

                        Log.e("updateResponse:",""+response.toString());

                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    600000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getActivity()).add(request);
        }
    }
}