package app.apneareamein.shopping.fragments;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.AddToCart;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.sync.SyncNormalCartItems;
import app.apneareamein.shopping.sync.SyncWishListItems;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;


public class ComboOffer extends Fragment {

    private Context context;
    private ArrayList<String> addToCartArrayList = new ArrayList<>();
    private final ArrayList<String> offerCode = new ArrayList<>();
    private Handler handler;
    private Runnable runnable;
    private String strContact, strOCode, strId;
    private RecyclerView mRecyclerView;
    private CoordinatorLayout comboOfferMainLayout;
    private CustomAdapterComboOffer customAdapterComboOffer;
    private final HashMap offerCodeHashMap = new HashMap();// will contain all
    private ArrayList<OfferCodeWiseProduct> ProductWiseComboOffer;
    private ArrayList<comboOfferWithOfferCode> FinalComboOffer;
    private DialogPlus dialogPlus;
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
        GateWay gateWay = new GateWay(getActivity());
        gateWay.hide();

    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                mRecyclerView.setVisibility(View.GONE);
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);
            } else {
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(BaseActivity.txtNoConnection);
                getDataOfComboOffersFromServer();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);

            Activity activity = getActivity();
            if (isAdded() && activity != null) {
                dialog(status);
            }
        }
    }

    private void initializeViews() {
        homePageActivity = (BaseActivity) getActivity();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_combo_offer, container, false);

        context = getActivity();

        initializeViews();

        myReceiver = new Network_Change_Receiver();

        comboOfferMainLayout = view.findViewById(R.id.comboOfferMainLayout);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    private void getDataOfComboOffersFromServer() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            ProductWiseComboOffer = new ArrayList<>();
            FinalComboOffer = new ArrayList<>();

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlComboOffer,   null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (response.isNull("posts")) {

                    } else {
                        try {
                            JSONArray comboOfferJsonArray = response.getJSONArray("posts");

                            for (int i = 0; i < comboOfferJsonArray.length(); i++) {
                                JSONObject jSonComboOffer = comboOfferJsonArray.getJSONObject(i);

                                offerCodeHashMap.put(jSonComboOffer.getString("offer_code"), "");
                                offerCode.add(jSonComboOffer.getString("offer_code"));
                                ProductWiseComboOffer.add(new OfferCodeWiseProduct(jSonComboOffer.getString("offer_code"), jSonComboOffer.getString("combo_image"),
                                        jSonComboOffer.getString("product_image"), jSonComboOffer.getString("offer_name"),
                                        jSonComboOffer.getString("offer_end_date"), jSonComboOffer.getString("total_price"),
                                        jSonComboOffer.getString("offer_price"), jSonComboOffer.getString("discount_in_rupess"),
                                        jSonComboOffer.getString("discount_in_percent"), jSonComboOffer.getString("product_id"),
                                        jSonComboOffer.getString("product_price"), jSonComboOffer.getString("product_name"),
                                        jSonComboOffer.getString("shop_name"), jSonComboOffer.getString("available_product_quantity"),
                                        jSonComboOffer.getString("offer_description")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    for (Object o : offerCodeHashMap.keySet()) {
                        String key = (String) o;

                        comboOfferWithOfferCode withOfferCode = new comboOfferWithOfferCode();
                        withOfferCode.offerCode = key;
                        withOfferCode.offerCodeWiseProducts = new ArrayList<>();
                        for (OfferCodeWiseProduct pp : ProductWiseComboOffer) {
                            if (pp.getOffer_code().equals(key)) {
                                withOfferCode.offerCodeWiseProducts.add(pp);
                            }
                        }
                        FinalComboOffer.add(withOfferCode);
                    }

                    customAdapterComboOffer = new CustomAdapterComboOffer(FinalComboOffer);
                    mRecyclerView.setAdapter(customAdapterComboOffer);
                    callBackPressed();
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            ((BaseActivity) getActivity()).Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(getActivity())) {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.SyncData();
            SyncNormalCartItems UT1 = new SyncNormalCartItems(SyncNormalCartItems.context);
            UT1.syncNormalCartItems(gateWay.getContact(), gateWay.getUserEmail(), getActivity());
            GateWay gateWay1 = new GateWay(getActivity());
            SyncWishListItems UT2 = new SyncWishListItems(SyncWishListItems.context);
            UT2.syncWishListItems(gateWay1.getContact(), gateWay1.getUserEmail(), getActivity());

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());*/
        }

        GateWay gateWay = new GateWay(getActivity());
        gateWay.hide();

        ((BaseActivity) getActivity()).changeTitle(getString(R.string.title_activity_combo_offer));
    }

    private void callBackPressed() {
        DBHelper db = new DBHelper(context);
        addToCartArrayList = new ArrayList<>();  //this is for addToCart code
        for (int j = 0; j < offerCode.size(); j++) {
            HashMap info = new HashMap();
            try {
                info = db.getCartDetails(offerCode.get(j));
            } catch (Exception e) {
                e.printStackTrace();
            }
            String strProductId = (String) info.get("new_pid");
            addToCartArrayList.add(strProductId);
        }
    }

    public class CustomAdapterComboOffer extends RecyclerView.Adapter<ViewHolder1> {

        ArrayList<comboOfferWithOfferCode> mItems;

        CustomAdapterComboOffer(ArrayList<comboOfferWithOfferCode> dataList) {
            this.mItems = dataList;
        }

        @NonNull
        @Override
        public ViewHolder1 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_combo_offer, parent, false);
            return new ViewHolder1(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder1 holder, final int position) {
            final DBHelper db = new DBHelper(context);

            final int pos = holder.getAdapterPosition();
            final comboOfferWithOfferCode movie = mItems.get(pos);
            final OfferCodeWiseProduct tp = movie.offerCodeWiseProducts.get(0);

            String strOCode1 = movie.offerCodeWiseProducts.get(0).getOffer_code();
            boolean btnCheckStatus = addToCartArrayList.contains(strOCode1);

            if (btnCheckStatus) {
                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
            } else {
                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
            }

            String QTY = tp.getStrAvailable_Qty();

            if (QTY.equals("0")) {
                holder.btnAddToCart.setVisibility(View.GONE);
                holder.imgOutOfStock.setVisibility(View.VISIBLE);

                //TODO here out of stock GIF image setup to glide
                /*Glide.with(getActivity()).load(R.drawable.outofstock)
                        .error(R.drawable.icerror_outofstock)
                        .into(holder.imgOutOfStock);*/
                Picasso.with(getActivity()).load(R.drawable.outofstock)
                        .error(R.drawable.icerror_outofstock)
                        .into(holder.imgOutOfStock);
            } else {
                holder.btnAddToCart.setVisibility(View.VISIBLE);
                holder.imgOutOfStock.setVisibility(View.GONE);
            }

            //TODO here combo offer product image setup to glide
            /*Glide.with(ComboOffer.this).load("http://s.apneareamein.com/seller/assets/" + tp.getCombo_image())
                    .thumbnail(Glide.with(ComboOffer.this).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.comboOfferImage);*/
            Picasso.with(getActivity()).load("http://s.apneareamein.com/seller/assets/" + tp.getCombo_image())
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.comboOfferImage);

            holder.tvEndDate.setText(tp.getOffer_end_date());
            holder.tvOfferName.setText(tp.getOffer_name());
            holder.tvTotalSaving.setText("₹ " + tp.getDiscount_in_rupess());
            holder.tvComboPrice.setText("₹ " + tp.getOffer_price());
            holder.tvOfferPrice.setText("₹  " + tp.getTotal_price());
            holder.tvOfferDesc.setText(tp.getStrComboOfferDesc());
            holder.tvSavingInPercent.setText(tp.getDiscount_in_percent() + " % OFF");
            holder.tvSavingInPercent1.setText(tp.getDiscount_in_percent() + " %\nOFF");
            holder.tvShopName.setText("By:- " + tp.getShop_name());

            holder.comboOfferImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        dialogPlus = DialogPlus.newDialog(getActivity())
                                .setContentHolder(new ViewHolder(R.layout.image_pop_up))
                                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .create();

                        ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                        //TODO here when user clicks on product image setup to glide
                       /* Glide.with(ComboOffer.this).load("http://s.apneareamein.com/seller/assets/" + tp.getCombo_image())
                                .thumbnail(Glide.with(ComboOffer.this).load(R.drawable.loading))
                                .error(R.drawable.ic_app_transparent)
                                .fitCenter()
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(alertProductImage1);*/
                        Picasso.with(getActivity()).load("http://s.apneareamein.com/seller/assets/" + tp.getCombo_image())
                                .error(R.drawable.ic_app_transparent)
                                .into(alertProductImage1);

                        dialogPlus.show();
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(comboOfferMainLayout);
                    }
                }
            });

            final String endDateComboOffer = tp.getOffer_end_date();

            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(this, 1000);
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        Date futureDate = dateFormat.parse(endDateComboOffer);
                        Date currentDate = new Date();

                        if (currentDate.before(futureDate)) {
                            holder.timerLayout.setVisibility(View.VISIBLE);
                            int diff1 = futureDate.getMonth() - currentDate.getMonth();
                            int diff2 = futureDate.getDate() - currentDate.getDate();

                            if ((diff1 == 0) && (diff2 == 1)) {
                                long diff = futureDate.getTime() - currentDate.getTime();
                                long secondsInMilli = 1000;
                                long minutesInMilli = secondsInMilli * 60;
                                long hoursInMilli = minutesInMilli * 60;
                                long daysInMilli = hoursInMilli * 24;

                                long elapsedDays = diff / daysInMilli;
                                diff = diff % daysInMilli;

                                long elapsedHours = diff / hoursInMilli;
                                diff = diff % hoursInMilli;

                                long elapsedMinutes = diff / minutesInMilli;
                                diff = diff % minutesInMilli;

                                long elapsedSeconds = diff / secondsInMilli;

                                holder.tvTimerHours.setText("" + String.format("%02d", elapsedHours));
                                holder.tvMin.setText("" + String.format("%02d", elapsedMinutes));
                                holder.tvSec.setText("" + String.format("%02d", elapsedSeconds));
                            } else {
                                holder.timerLayout.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            holder.timerLayout.setVisibility(View.INVISIBLE);
                            handler.removeCallbacks(runnable);
                            handler.removeMessages(0);
                            mItems.remove(pos);
                            notifyItemRemoved(pos);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.postDelayed(runnable, 0);

            holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        HashMap AddToCartInfo;
                        strOCode = movie.offerCodeWiseProducts.get(0).getOffer_code();
                        addToCartArrayList.add(strOCode);
                        AddToCartInfo = db.getCartDetails(strOCode);

                        strId = (String) AddToCartInfo.get("new_pid");

                        if (strId == null) {
                            //db.insertCount(strOCode);
                            int count = (int) db.fetchAddToCartCount();
                            ((BaseActivity) getActivity()).updateAddToCartCount(count);

                            if (count >= 0) {
                                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                            } else {
                                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                            }
                            addComboOfferCartItem();
                        } else {
                            Intent intent = new Intent(getActivity(), AddToCart.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(comboOfferMainLayout);
                    }
                }

                private void addComboOfferCartItem() { //TODO Server method here
                    GateWay gateWay = new GateWay(getActivity());
                    strContact = gateWay.getContact();

                    JSONObject params = new JSONObject();
                    try {
                        params.put("offer_code", strOCode); //offercode
                        params.put("versionCode", ApplicationUrlAndConstants.versionName);
                        params.put("contactNo", strContact);
                        params.put("email", gateWay.getUserEmail());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddCartComboOffer, params, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            ArrayList<String> comboMsgList = new ArrayList<>();
                            try {
                                JSONArray mainJsonArray = response.getJSONArray("posts");
                                for (int i = 0; i < mainJsonArray.length(); i++) {
                                    JSONObject jSonData = mainJsonArray.getJSONObject(i);
                                    comboMsgList.add(jSonData.getString("msg"));
                                }
                                Toast.makeText(getActivity(), comboMsgList.get(0), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                            GateWay gateWay = new GateWay(getActivity());
                            gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                        }
                    });
                    AppController.getInstance().addToRequestQueue(request);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(comboOfferWithOfferCode movie) {
            mItems.add(movie);
        }
    }

    private class ViewHolder1 extends RecyclerView.ViewHolder {

        final TextView tvEndDate;
        final TextView tvOfferName;
        final TextView tvShopName;
        final TextView tvOfferPrice;
        final TextView tvOfferDesc;
        final TextView tvComboPrice;
        final TextView tvTotalSaving;
        final TextView tvSavingInPercent;
        final TextView tvSavingInPercent1;
        final TextView tvTimerHours;
        final TextView tvMin;
        final TextView tvSec;
        final ImageView comboOfferImage;
        final ImageView imgOutOfStock;
        final LinearLayout timerLayout;
        final Button btnAddToCart;

        public ViewHolder1(View itemView) {
            super(itemView);

            tvEndDate = itemView.findViewById(R.id.txtEndDate);
            tvOfferName = itemView.findViewById(R.id.txtOfferName);
            tvShopName = itemView.findViewById(R.id.txtShopName);
            tvOfferPrice = itemView.findViewById(R.id.txtOfferPrice);
            tvOfferDesc = itemView.findViewById(R.id.txtOfferDesc);
            tvComboPrice = itemView.findViewById(R.id.txtComboPrice);
            tvTotalSaving = itemView.findViewById(R.id.txtTotalSaving);
            tvSavingInPercent = itemView.findViewById(R.id.txtSavingsInPercent);
            tvSavingInPercent1 = itemView.findViewById(R.id.txtSavingsInPercent1);
            tvTimerHours = itemView.findViewById(R.id.txtTimerHour);
            tvMin = itemView.findViewById(R.id.txtTimerMinute);
            tvSec = itemView.findViewById(R.id.txtTimerSecond);
            comboOfferImage = itemView.findViewById(R.id.comboOfferImage);
            timerLayout = itemView.findViewById(R.id.timerLayout);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
            btnAddToCart.setTag(this);
        }
    }

    private class comboOfferWithOfferCode {

        String offerCode;
        ArrayList<OfferCodeWiseProduct> offerCodeWiseProducts;
    }

    private class OfferCodeWiseProduct {
        final String offer_code;
        final String combo_image;
        String product_image;
        final String offer_name;
        final String offer_end_date;
        final String total_price;
        final String offer_price;
        final String discount_in_rupess;
        final String discount_in_percent;
        String product_id;
        String product_price;
        String product_name;
        String shop_name;
        final String strAvailable_Qty;
        final String strComboOfferDesc;

        public String getStrAvailable_Qty() {
            return strAvailable_Qty;
        }

        public OfferCodeWiseProduct(String offer_code, String combo_image, String product_image, String offer_name,
                                    String offer_end_date, String total_price, String offer_price, String discount_in_rupess,
                                    String discount_in_percent, String product_id, String product_price,
                                    String product_name, String shop_name, String available_qty, String strComboOfferDesc) {
            this.offer_code = offer_code;
            this.combo_image = combo_image;
            this.product_image = product_image;
            this.offer_name = offer_name;
            this.offer_end_date = offer_end_date;
            this.total_price = total_price;
            this.offer_price = offer_price;
            this.discount_in_rupess = discount_in_rupess;
            this.discount_in_percent = discount_in_percent;
            this.product_id = product_id;
            this.product_price = product_price;
            this.product_name = product_name;
            this.shop_name = shop_name;
            this.strAvailable_Qty = available_qty;
            this.strComboOfferDesc = strComboOfferDesc;
        }

        public String getCombo_image() {
            return combo_image;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getOffer_code() {
            return offer_code;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public String getOffer_end_date() {
            return offer_end_date;
        }

        public String getTotal_price() {
            return total_price;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public String getDiscount_in_rupess() {
            return discount_in_rupess;
        }

        public String getDiscount_in_percent() {
            return discount_in_percent;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_price() {
            return product_price;
        }

        public void setProduct_price(String product_price) {
            this.product_price = product_price;
        }

        public String getStrComboOfferDesc() {
            return strComboOfferDesc;
        }
    }
}
