package app.apneareamein.shopping.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.EditAddress;
import app.apneareamein.shopping.activities.Order_Details_StepperActivity;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.services.EasyOrderDetails;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class MyAddress extends AppCompatActivity {

    boolean defaultValue = false;
    private Bundle bundle;
    private String tag, finalPrice;
    private RecyclerView mRecyclerView;
    private CardAdapter cardAdapter;
    private CoordinatorLayout myaddressedMainLayout;
    private RelativeLayout relativeLayoutaddresses, relativeLayoutEmptyAddress;
    private final String class_name = this.getClass().getSimpleName();
    Intent i = null;
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;
    TextView tvTitle;

    @Override
    public void onPause() {
        super.onPause();
       unregisterReceiver(myReceiver);
        /*GateWay gateWay = new GateWay(getActivity());
        gateWay.hide();*/
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                relativeLayoutaddresses.setVisibility(View.GONE);
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);
            } else {
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(BaseActivity.txtNoConnection);
                RequestforAddresses();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    private void initializeViews() {
       // homePageActivity = (BaseActivity) getActivity();
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }*/


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_address);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_my_address);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      //  getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        myReceiver = new Network_Change_Receiver();

        try {

            bundle = getIntent().getExtras();
            if (bundle != null) {
                tag = bundle.getString("tag");
                finalPrice = bundle.getString("finalPrice");
            }
        }catch (Exception e){
            e.getMessage();
        }

        Button btnAddAddress = findViewById(R.id.btnAddAddress);
        Button btnAddAddress1 = findViewById(R.id.btnAddAddress1);
        myaddressedMainLayout = findViewById(R.id.myaddressedMainLayout);
        relativeLayoutEmptyAddress = findViewById(R.id.relativeLayoutEmptyAddress);
        relativeLayoutaddresses = findViewById(R.id.relativeLayoutaddresses);
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        relativeLayoutaddresses.setVisibility(View.GONE);
        relativeLayoutEmptyAddress.setVisibility(View.GONE);

        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tag) {
                    case "address":
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "address");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;

                    case "easy_order_address":
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "easy_order_address");
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;

                    case "resto":
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "resto");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;

                    default:
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "simple");
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;
                }
            }
        });

        btnAddAddress1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tag) {
                    case "address":
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "address");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;

                    case "easy_order_address":
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "easy_order_address");
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;

                    case "resto":
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "resto");
                            bundle.putString("finalPrice", finalPrice);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;

                    default:
                        if (Connectivity.isConnected(MyAddress.this)) {
                            i = new Intent(MyAddress.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("type", "add");
                            bundle.putString("tag", "simple");
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            GateWay gateWay = new GateWay(MyAddress.this);
                            gateWay.displaySnackBar(myaddressedMainLayout);
                        }
                        break;
                }
            }
        });

       // return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        hideKeyboard(MyAddress.this);
        if (Connectivity.isConnected(MyAddress.this)) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, MyAddress.this);*/
        }
        /*GateWay gateWay = new GateWay(MyAddress.this);
        gateWay.hide();*/
    }

    private void RequestforAddresses() { //TODO Server method here
        if (Connectivity.isConnected(MyAddress.this)) {
            final GateWay gateWay = new GateWay(MyAddress.this);
            gateWay.progressDialogStart();

            cardAdapter = new CardAdapter();

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "fetch");
                Log.e("ParamFetchAdd",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddNewAddress, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        Log.e("FetchAdd:",""+res);
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");

                        if (status.equals("success")) {
                            JSONArray addressArray = jsonObject.getJSONArray("user_data");

                            for (int i = 0; i < addressArray.length(); i++) {
                                JSONObject json_event_list = addressArray.getJSONObject(i);
                                addressList e = new addressList(json_event_list.getString("id"),
                                        json_event_list.getString("name"),
                                        json_event_list.getString("contact"),
                                        json_event_list.getString("address"),
                                        json_event_list.getString("address1"),
                                        json_event_list.getString("address2"),
                                        json_event_list.getString("address3"),
                                        json_event_list.getString("address4"),
                                        json_event_list.getString("address5"),
                                        json_event_list.getString("area"));
                                cardAdapter.add(e);
                            }
                            relativeLayoutaddresses.setVisibility(View.VISIBLE);
                            relativeLayoutEmptyAddress.setVisibility(View.GONE);
                            mRecyclerView.setAdapter(cardAdapter);
                        } else {
                            relativeLayoutaddresses.setVisibility(View.GONE);
                            relativeLayoutEmptyAddress.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(MyAddress.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            //((BaseActivity) MyAddress.this).Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    private void deleteAddress(String id) {
        if (Connectivity.isConnected(MyAddress.this)) {
            GateWay gateWay = new GateWay(MyAddress.this);

            JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("id", id);
                params.put("tag", "delete");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddNewAddress, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("message");

                        if (status.equals("fail")) {
                            Toast.makeText(MyAddress.this, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MyAddress.this, msg, Toast.LENGTH_SHORT).show();
                            RequestforAddresses();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(MyAddress.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(MyAddress.this);
            gateWay.displaySnackBar(myaddressedMainLayout);
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<ViewHolder> {

        final ArrayList<addressList> mItems = new ArrayList<>();

        public CardAdapter() {
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
            final addressList ad = mItems.get(position);

            try {
                /*if (ad.getAddress_type().equals("default")) {
                    viewHolder.viewLine.setVisibility(View.VISIBLE);
                    viewHolder.txtDefault.setVisibility(View.VISIBLE);
                    viewHolder.txtDefault.setText("To be Delivered");
                } else {
                    viewHolder.txtDefault.setVisibility(View.GONE);
                    viewHolder.viewLine.setVisibility(View.GONE);
                }*/
                viewHolder.tvName.setText(ad.getStr_name());
                viewHolder.tvContact.setText(ad.getStr_contact());
                viewHolder.tvAddress.setText(ad.getAddress1()+" "+ad.getAddress2()+" "+ad.getAddress3()+" "+ad.getAddress4()+" "+ad.getAddress5()+" "+ad.area);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //defaultValue = ad.getAddress_type().equals("default");

                    String address_id = ad.getStr_id();

                    switch (tag) {
                        case "address":
                            if (Connectivity.isConnected(MyAddress.this)) {
                                Intent i = new Intent(MyAddress.this, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "address");
                                bundle.putString("finalPrice", finalPrice);
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(MyAddress.this);
                                gateWay.displaySnackBar(myaddressedMainLayout);
                            }
                            break;

                        case "easy_order_address":
                            if (Connectivity.isConnected(MyAddress.this)) {
                                Intent i = new Intent(MyAddress.this, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "easy_order_address");
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(MyAddress.this);
                                gateWay.displaySnackBar(myaddressedMainLayout);
                            }
                            break;

                        case "resto":
                            if (Connectivity.isConnected(MyAddress.this)) {
                                Intent i = new Intent(MyAddress.this, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "resto");
                                bundle.putString("finalPrice", finalPrice);
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(MyAddress.this);
                                gateWay.displaySnackBar(myaddressedMainLayout);
                            }
                            break;

                        default:
                            if (Connectivity.isConnected(MyAddress.this)) {
                                Intent i = new Intent(MyAddress.this, EditAddress.class);
                                bundle = new Bundle();
                                bundle.putString("id", address_id);
                                bundle.putString("type", "edit");
                                bundle.putString("tag", "simple");
                                bundle.putString("finalPrice", "");
                                //bundle.putBoolean("defaultValue", defaultValue);
                                i.putExtras(bundle);
                                startActivity(i);
                            } else {
                                GateWay gateWay = new GateWay(MyAddress.this);
                                gateWay.displaySnackBar(myaddressedMainLayout);
                            }
                            break;
                    }
                }
            });

            viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String address_id = ad.getStr_id();

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on remove To be Delivered address", MyAddress.this);*/

                    deleteAlertDialog(address_id);
                    /*if (ad.getAddress_type().equals("default")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MyAddress.this);
                        builder.setTitle("To be Delivered address");
                        builder.setMessage("Are you sure, you want to delete To be Delivered address?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (Connectivity.isConnected(MyAddress.this)) {
                                            dialog.dismiss();

                                            deleteAddress(address_id);
                                        } else {
                                            dialog.dismiss();
                                        }
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {

                    }*/
                }
            });

            viewHolder.btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tag == null) {
                    } else if (tag.equals("address")) {
                        addressList movie = mItems.get(viewHolder.getAdapterPosition());
                        String id = movie.getStr_id();

                        Intent intent = new Intent(MyAddress.this, Order_Details_StepperActivity.class);
                        intent.putExtra("finalPrice", finalPrice);
                        intent.putExtra("address_id", id);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        MyAddress.this.finish();
                    } else if (tag.equals("easy_order_address")) {
                        addressList movie = mItems.get(viewHolder.getAdapterPosition());
                        String id = movie.getStr_id();

                        Intent intent = new Intent(MyAddress.this, EasyOrderDetails.class);
                        intent.putExtra("address_id", id);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        MyAddress.this.finish();
                    }
                }
            });
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_view_for_address, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(addressList e) {
            mItems.add(e);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvName;
        final TextView tvContact;
        final TextView tvAddress;
        final TextView txtDefault;
        Button btnEdit, btnDelete, btnConfirm;
        View viewLine;

        public ViewHolder(View itemView) {
            super(itemView);

            viewLine = itemView.findViewById(R.id.viewLine);
            tvName = itemView.findViewById(R.id.txtName);
            tvContact = itemView.findViewById(R.id.txtContact);
            tvAddress = itemView.findViewById(R.id.txtAddress);
            txtDefault = itemView.findViewById(R.id.txtDefault);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnConfirm = itemView.findViewById(R.id.btnConfirm);
            btnEdit.setTag(this);
            btnDelete.setTag(this);
            btnConfirm.setTag(this);
        }
    }

    private void deleteAlertDialog(final String address_id) { //this is calling from HomePage to check the combo offers available or not
        AlertDialog.Builder builder = new AlertDialog.Builder(MyAddress.this);
        builder.setMessage("Are you sure you want to delete this address")
                .setTitle("Remove Address")
                .setCancelable(true)
                .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": clicked on remove address", MyAddress.this);*/

                        deleteAddress(address_id);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();
    }

    private class addressList {

        final String str_id;
        final String str_name;
        final String str_contact;
        final String str_address;
        final String address1;
        final String address2;
        final String address3;
        final String address4;
        final String address5;
        final String area;

        addressList(String str_id, String str_name, String str_contact, String str_address,String address1,String address2,String address3,String address4,String address5,String area) {//,
            this.str_id = str_id;
            this.str_name = str_name;
            this.str_contact = str_contact;
            this.str_address = str_address;
            this.address1 = address1;
            this.address2 = address2;
            this.address3 = address3;
            this.address4 = address4;
            this.address5 = address5;
            this.area = area;
        }

        public String getStr_id() {
            return str_id;
        }

        public String getStr_name() {
            return str_name;
        }

        public String getStr_contact() {
            return str_contact;
        }

        public String getStr_address() {
            return str_address;
        }

        public String getAddress1() {
            return address1;
        }
        public String getAddress2() {
            return address2;
        }
        public String getAddress3() {
            return address3;
        }
        public String getAddress4() {
            return address4;
        }
        public String getAddress5() {
            return address5;
        }
        public String getArea() {
            return area;
        }
    }




    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
