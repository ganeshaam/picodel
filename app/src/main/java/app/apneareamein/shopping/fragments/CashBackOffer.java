package app.apneareamein.shopping.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.GetCoupons;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.Movie;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class CashBackOffer extends Fragment {

    private CashBackCategory cashBackCategory;
    private RecyclerView redeemCategoryRecyclerView;
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
        GateWay gateWay = new GateWay(getActivity());
        gateWay.hide();

    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                redeemCategoryRecyclerView.setVisibility(View.GONE);
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);
            } else {
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);
                redeemCategoryRecyclerView.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(BaseActivity.txtNoConnection);
                getCashBackCategoryFromServer();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            Activity activity = getActivity();
            if (isAdded() && activity != null) {
                dialog(status);
            }
        }
    }


    private void initializeViews() {
        homePageActivity = (BaseActivity) getActivity();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment__cash_back_offer, container, false);
        initializeViews();
        myReceiver = new Network_Change_Receiver();

        TextView tvRedeemMessage = view.findViewById(R.id.txtRedeemMessage);

        redeemCategoryRecyclerView = view.findViewById(R.id.redeemCategoryRecyclerView);
        redeemCategoryRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManagerForRedeemCategory = new LinearLayoutManager(getActivity());
        redeemCategoryRecyclerView.setLayoutManager(mLayoutManagerForRedeemCategory);

        String message = this.getArguments().getString("message");
        tvRedeemMessage.setText(message);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        ((BaseActivity) getActivity()).changeTitle(getString(R.string.title_activity_cash_back_offer));
        GateWay gateWay = new GateWay(getActivity());
        gateWay.hide();

        if (Connectivity.isConnected(getActivity())) {
            gateWay.SyncData();

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());*/
        }
    }

    private void getCashBackCategoryFromServer() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            cashBackCategory = new CashBackCategory();

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, ApplicationUrlAndConstants.urlUserCashBackCategory,  null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("redeemCategory")) {
                            gateWay.progressDialogStop();
                            Toast.makeText(getActivity(), "There are no cashback offers.", Toast.LENGTH_LONG).show();
                        } else {
                            JSONArray redeemCategoryJsonArray = response.getJSONArray("redeemCategory");
                            for (int i = 0; i < redeemCategoryJsonArray.length(); i++) {
                                JSONObject jSonRedeemCategory = redeemCategoryJsonArray.getJSONObject(i);

                                Movie movie = new Movie(jSonRedeemCategory.getString("category"), jSonRedeemCategory.getString("cnt"));
                                cashBackCategory.add(movie);
                            }
                            redeemCategoryRecyclerView.setAdapter(cashBackCategory);
                        }
                        gateWay.progressDialogStop();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    private class CashBackCategory extends RecyclerView.Adapter<CashBackCategory.ViewHolder> {

        final List<Movie> mItems = new ArrayList<>();
        Context context;

        public CashBackCategory() {
            super();
        }

        @Override
        public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_redeem_offer, viewGroup, false);
            final ViewHolder viewHolder = new ViewHolder(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String redeemCategory = viewHolder.tvRedeemCategory.getText().toString();

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on: " + redeemCategory, getActivity());*/

                    Intent intent = new Intent(getActivity(), GetCoupons.class);
                    intent.putExtra("cashBackCategory", redeemCategory);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, int position) {
            Movie movie = mItems.get(position);
            viewHolder.tvRedeemCategory.setText(movie.getRedeemCategory());
            viewHolder.tvRedeemCategoryCount.setText("( " + movie.getRedeemCategoryCount() + " )");
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(Movie movie) {
            mItems.add(movie);
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            final CardView redeemCategoryCardView;
            final TextView tvRedeemCategory;
            final TextView tvRedeemCategoryCount;

            public ViewHolder(View itemView) {
                super(itemView);
                redeemCategoryCardView = itemView.findViewById(R.id.redeemCategoryCardView);
                tvRedeemCategory = itemView.findViewById(R.id.txtRedeemCategory);
                tvRedeemCategoryCount = itemView.findViewById(R.id.txtRedeemCategoryCount);
                redeemCategoryCardView.setTag(this);
            }
        }
    }

}
