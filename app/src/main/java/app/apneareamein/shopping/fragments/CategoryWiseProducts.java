package app.apneareamein.shopping.fragments;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.MasterSearch;
import app.apneareamein.shopping.activities.SearchProducts;
import app.apneareamein.shopping.activities.WishList;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.sync.SyncNormalCartItems;
import app.apneareamein.shopping.sync.SyncWishListItems;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.VolleySingleton;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;


public class CategoryWiseProducts extends Fragment {

    private Context context;
    private String strImageNumber, strDiscountName, strCity, strArea;
    private static TextView tvWishList;
    private ExpandableListView expandableListView;
    private ExpandableAdapter expListAdapter;
    private final HashMap keyValueCategoryMap = new HashMap();
    private final Map<String, List<String>> childCollection = new LinkedHashMap<>();
    private final List<String> productSubCategory = new ArrayList<>();
    private ArrayList<String> subSubCategory = new ArrayList<>();
    private int WishListCount, CartCount;
    private final String class_name = this.getClass().getSimpleName();

    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
        GateWay gateWay = new GateWay(getActivity());
        gateWay.hide();

    }

    private void initializeViews() {
        homePageActivity = (BaseActivity) getActivity();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                expandableListView.setVisibility(View.GONE);
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);
            } else {
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);
                expandableListView.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(BaseActivity.txtNoConnection);
                getSubCategories();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);

            Activity activity = getActivity();
            if (isAdded() && activity != null) {
                dialog(status);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category_wise_products, container, false);
        super.onSaveInstanceState(savedInstanceState);
        initializeViews();

        context = getActivity();

        myReceiver = new Network_Change_Receiver();

        expandableListView = view.findViewById(R.id.expList);
        Bundle bundle = getArguments();
        strImageNumber = bundle.getString("image_number");
        strDiscountName = bundle.getString("discountName");
        GateWay gateWay = new GateWay(getActivity());
        strCity = gateWay.getCity();
        strArea = gateWay.getArea();


        //start----- this is for expandable listView
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousItem = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem)
                    expandableListView.collapseGroup(previousItem);
                previousItem = groupPosition;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (parent.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroup(groupPosition);
                }
                String strNameNavigation = (String) expListAdapter.getChild(groupPosition, childPosition);

                /*UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on: " + strNameNavigation, getActivity());*/

                Intent catIntent = new Intent(getActivity(), SearchProducts.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("tag", "next");
                bundle1.putString("selectedName", strNameNavigation);
                catIntent.putExtras(bundle1);
                startActivity(catIntent);
                return true;
            }
        });
        //end----- this is for expandable listView

        setHasOptionsMenu(true);
        return view;
    }

    private void getSubCategories() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            productSubCategory.clear();

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("image_number", strImageNumber);
                params.put("discountRange", strDiscountName);
                params.put("appVersionName", ApplicationUrlAndConstants.versionName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetSubCat, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray jsonArray = new JSONArray("");

                        JSONObject value = jsonArray.getJSONObject(0);
                        JSONObject value1 = jsonArray.getJSONObject(1);
                        if (value.isNull("subcat")) {
                            Toast.makeText(getActivity(), "There are no products available for this category!", Toast.LENGTH_LONG).show();
                        } else {
                            JSONArray mainCat = value.getJSONArray("subcat");
                            for (int i = 0; i < mainCat.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCat.get(i);
                                productSubCategory.add(jsonMainCatObject.getString("product_subcat")); //add to arraylist
                            }

                            JSONArray mainProductCat = value1.getJSONArray("sub_subcat");
                            for (int j = 0; j < mainProductCat.length(); j++) {
                                JSONObject jsonProductCatObject = (JSONObject) mainProductCat.get(j);
                                JSONArray mainProductCatArray = jsonProductCatObject.getJSONArray(productSubCategory.get(j));
                                subSubCategory = new ArrayList<>();

                                for (int g = 0; g < mainProductCatArray.length(); g++) {
                                    JSONObject jsonProductCatObjectArray = (JSONObject) mainProductCatArray.get(g);
                                    subSubCategory.add(jsonProductCatObjectArray.getString("product_sub_subcat")); //add to arraylist
                                }
                                Collections.sort(subSubCategory);
                                keyValueCategoryMap.put(productSubCategory.get(j), subSubCategory);
                            }
                            preparationForCollection(); //this is for set the product category wise product sub category
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        }
    }

    private void preparationForCollection() {
        Set tpSet = keyValueCategoryMap.keySet();
        ArrayList<String> tpArrayList = new ArrayList<>();
        for (String temp : productSubCategory) {
            if (tpSet.contains(temp)) {
                tpArrayList = (ArrayList<String>) keyValueCategoryMap.get(temp);
            }
            childCollection.put(temp, tpArrayList);
        }
        try {
            expListAdapter = new ExpandableAdapter(getActivity(), productSubCategory, childCollection);
            expandableListView.setAdapter(expListAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        ((BaseActivity) getActivity()).changeTitle("Categories");
        GateWay gateWay = new GateWay(getActivity());
        gateWay.hide();

        if (Connectivity.isConnected(getActivity())) {
            GateWay gateWay1 = new GateWay(getActivity());
            SyncNormalCartItems UT1 = new SyncNormalCartItems(SyncNormalCartItems.context);
            UT1.syncNormalCartItems(gateWay1.getContact(), gateWay1.getUserEmail(), getActivity());

            GateWay gateWay2 = new GateWay(getActivity());
            SyncWishListItems UT2 = new SyncWishListItems(SyncWishListItems.context);
            UT2.syncWishListItems(gateWay2.getContact(), gateWay2.getUserEmail(), getActivity());

            SyncData();

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());*/
        }
    }

    private void SyncData() { //TODO Server Method here
        GateWay gateWay = new GateWay(getActivity());
        String strContact = gateWay.getContact();
        String strEmail = gateWay.getUserEmail();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JSONObject params = new JSONObject();
        try {
            params.put("contact", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    WishListCount = response.getInt("wishlist_count");
                    CartCount = response.getInt("normal_cart_count");

                    ((BaseActivity) getActivity()).updateAddToCartCount(CartCount);

                    if (WishListCount >= 0) {
                        updateWishListCount(WishListCount);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private class ExpandableAdapter extends BaseExpandableListAdapter {

        final List<String> mListDataGroup;
        final Activity mContext;
        String groupText;
        GroupViewHolder groupViewHolder;
        final Map<String, List<String>> childCollection;

        public ExpandableAdapter(Activity activity, List<String> productSubCategory, Map<String, List<String>> childCollection) {
            this.mContext = activity;
            this.mListDataGroup = productSubCategory;
            this.childCollection = childCollection;
        }

        @Override
        public int getGroupCount() {
            return mListDataGroup.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            try {
                return childCollection.get(mListDataGroup.get(groupPosition)).size();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 1;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return mListDataGroup.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childCollection.get(mListDataGroup.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            groupText = (String) getGroup(groupPosition);

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.segregation_group, null);
                groupViewHolder = new GroupViewHolder();
                groupViewHolder.mGroupText = convertView.findViewById(R.id.txtParent);
                groupViewHolder.indicator = convertView.findViewById(R.id.circleIndicator);

                convertView.setTag(groupViewHolder);
            } else {
                groupViewHolder = (GroupViewHolder) convertView.getTag();
            }
            String[] res = groupText.split("@");
            groupViewHolder.mGroupText.setText(res[1]);

            if (groupPosition != -1) {
                int imageResourceId = isExpanded ? R.drawable.ic_minus : R.drawable.ic_plus;
                groupViewHolder.indicator.setImageResource(imageResourceId);
                groupViewHolder.indicator.setVisibility(View.VISIBLE);
            } else {
                groupViewHolder.indicator.setVisibility(View.INVISIBLE);
            }
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            try {
                String laptop = (String) getChild(groupPosition, childPosition);
                LayoutInflater inflater = mContext.getLayoutInflater();

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.segregation_child, null);
                }
                TextView item = convertView.findViewById(R.id.txtChild);
                item.setText(laptop);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }

    public final class GroupViewHolder {
        TextView mGroupText;
        ImageView indicator;
    }

    private void updateWishListCount(final int count) {
        if (tvWishList == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0)
                    tvWishList.setVisibility(View.INVISIBLE);
                else {
                    tvWishList.setVisibility(View.VISIBLE);
                    tvWishList.setText(Integer.toString(count));
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_local_search_product, menu);
        MenuItem item = menu.findItem(R.id.action_login);
        item.setVisible(false);
        MenuItem item2 = menu.findItem(R.id.search);
        item2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(getActivity(), MasterSearch.class));
                return false;
            }
        });
        final DBHelper db = new DBHelper(context);
        MenuItem item1 = menu.findItem(R.id.action_wish_list);
        MenuItemCompat.setActionView(item1, R.layout.wish_list_notification_icon);
        RelativeLayout notifyCount1 = (RelativeLayout) MenuItemCompat.getActionView(item1);

        tvWishList = notifyCount1.findViewById(R.id.cartNumber);

        if (WishListCount == 0) {
            tvWishList.setVisibility(View.INVISIBLE);
        }
        notifyCount1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked wishList icon on toolbar", getActivity());*/

                if (WishListCount == 0) {
                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.wishListAlertDialog();
                } else {
                    Intent intent = new Intent(getActivity(), WishList.class);
                    startActivity(intent);
                }
            }
        });
        notifyCount1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast toast = Toast.makeText(getActivity(), "WishList", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.RIGHT, 0, 110);
                toast.show();
                return true;
            }
        });
        int WishListCnt = (int) db.fetchWishListCount();
        updateWishListCount(WishListCnt);

    }

}
