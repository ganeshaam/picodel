package app.apneareamein.shopping.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orhanobut.dialogplus.DialogPlus;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.MasterSearch;
import app.apneareamein.shopping.activities.SingleProductInformation;
import app.apneareamein.shopping.activities.SubProductListActivity;
import app.apneareamein.shopping.activities.WishList;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.model.InnerMovie;
import app.apneareamein.shopping.sync.SyncNormalCartItems;
import app.apneareamein.shopping.sync.SyncWishListItems;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.Movie;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class DataSegregationLevelThree extends AppCompatActivity {

    private Context context;
    private String strCity, strArea, strName, strProductCat, selectedName, product_mainCat,productSuperCat, shopId, pId, strId, strContact, strEmail,
            dialog_strName, dialog_strContact, dialog_strEmail, dialog_strPincode, dialog_strAddress, dialog_strQty, dialog_strRemark;
    private TextView tvWishList, tvEssential, tvMessage, tvYes, tvNo, dialog_tvTitle;
    private EditText dialogedit_name, dialogedit_contact, dialogedit_email, dialogedit_pincode, dialogedit_address, dialogedit_qty, dialogedit_remark;
    private AlertDialog alertDialog;
    private CoordinatorLayout segregationThreeMainLayout;
    private RecyclerView recyclerView, productsRecyclerView;
    private CustomAdapter customAdapter;
    private CardAdapter cardAdapter;
    public InnerMovie innerMovie;
    private Movie movie;
    private ArrayList<String> addToCartArrayList = new ArrayList<>();
    private ArrayList<String> productId = new ArrayList<>();
    public static final Set<String> uniqueWishList = new TreeSet<>();
    private DialogPlus dialog;
    private Button dialog_btnSubmit;
    private DialogPlus dialogPlus;
    private int WishListCount = 0, CartCount = 0;
    private String strCheckQty,Zone_Area="";
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    //private MenuItem item1;
    private BaseActivity homePageActivity;
    TextView tvTitle;
    private LinearLayout Main_Layout_NoInternet;
    private TextView txtNoConnection;
    private ProgressBar simpleProgressBar;
    public static final String MY_PREFS_NAME = "PICoDEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_data_segregation_level_three);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_browseby_categoty);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = DataSegregationLevelThree.this;

        tvEssential = findViewById(R.id.txtEssential);
        segregationThreeMainLayout = findViewById(R.id.segregationThreeMainLayout);
        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        txtNoConnection = findViewById(R.id.txtNoConnection);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

        GateWay gateWay = new GateWay(context);
        strCity = gateWay.getCity();
        strArea = gateWay.getArea();
        strName = gateWay.getUserName();
        strContact = gateWay.getContact();
        strEmail = gateWay.getUserEmail();

        Intent bundle = getIntent();
        strProductCat = bundle.getStringExtra("product_cat");
        selectedName = strProductCat;
        product_mainCat = bundle.getStringExtra("product_mainCat");
        productSuperCat = bundle.getStringExtra("productSuperCat");

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Zone_Area = prefs.getString("Zone_Area", "");

        tvTitle.setText(selectedName);

        recyclerView = findViewById(R.id.inner_dataSegregation_level_three_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
        recyclerView.setLayoutManager(gridLayoutManager);

        productsRecyclerView = findViewById(R.id.essential_products_recycler_view);
        productsRecyclerView.setHasFixedSize(true);
        productsRecyclerView.setNestedScrollingEnabled(false);
        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(context, 2);
        productsRecyclerView.setLayoutManager(gridLayoutManager1);

        myReceiver = new Network_Change_Receiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(myReceiver);
       // GateWay gateWay = new GateWay(context);
        //gateWay.hide();
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
              //  item1.setVisible(false);
                recyclerView.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
              //  item1.setVisible(true);
                Main_Layout_NoInternet.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
                getShopSubCategoriesData();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    //sub categories
    private void getShopSubCategoriesData() { //TODO Server method here
        if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(context);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            customAdapter = new CustomAdapter();

            JSONObject params = new JSONObject();
            try {
                params.put("city", strCity);
                params.put("area", strArea);
                params.put("product_cat", strProductCat);
                params.put("product_maincat", product_mainCat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("ShopSubCategorie",""+params);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlShopSubCatResult, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.d("ShopSubCategorieRes",""+response);

                    ArrayList<String> product_subCat = new ArrayList<>();
                    ArrayList<String> product_subCatImage = new ArrayList<>();
                    ArrayList<String> title = new ArrayList<>();

                    if (!response.isNull("posts")) {
                        try {
                            JSONArray mainCatArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);

                                product_subCat.add(jsonMainCatObject.getString("product_subcat"));
                                product_subCatImage.add(jsonMainCatObject.getString("product_subcat_image"));
                                title.add(jsonMainCatObject.getString("title"));
                            }
                            if (product_subCat.size() > 0) {
                                for (int i = 0; i < product_subCat.size(); i++) {
                                    innerMovie = new InnerMovie(product_subCat.get(i), product_subCatImage.get(i));
                                    customAdapter.add(innerMovie);
                                }
                                recyclerView.setAdapter(customAdapter);
                                tvEssential.setText(title.get(0));
                                getEssentialProducts();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //gateWay.progressDialogStop();
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(context);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
          Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }

    private void getEssentialProducts() { //TODO Server method here
        if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
            productId = new ArrayList<>();
            cardAdapter = new CardAdapter();

            Log.d("abhi", "getEssentialProducts product_mainCat: " + product_mainCat);
            Log.d("abhi", "getEssentialProducts strProductCat: " + strProductCat);
            JSONObject params = new JSONObject();
            try {
                params.put("product_maincat", product_mainCat);
                params.put("product_cat", strProductCat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlEssentialProducts, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (!response.isNull("posts")) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                                productId.add(jsonObject.getString("product_id"));

                                movie = new Movie(jsonObject.getString("shop_id"), jsonObject.getString("product_image"), jsonObject.getString("product_image1"),
                                        jsonObject.getString("product_image2"), jsonObject.getString("product_image3"), jsonObject.getString("product_image4"),
                                        jsonObject.getString("product_id"), jsonObject.getString("product_name"), jsonObject.getString("product_brand"),
                                        jsonObject.getString("shop_name"), jsonObject.getString("product_mrp"), jsonObject.getString("product_discount"),
                                        jsonObject.getString("product_price"), jsonObject.getString("type"), jsonObject.getString("available_product_quantity"),
                                        jsonObject.getString("hindi_name"), jsonObject.getString("product_size"), jsonObject.getString("p_name"));
                                cardAdapter.add(movie);
                            }
                            if (productId.size() > 0) {
                                tvEssential.setVisibility(View.VISIBLE);
                                productsRecyclerView.setAdapter(cardAdapter);
                            }
                            callBackPressed();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            Main_Layout_NoInternet.setVisibility(View.VISIBLE);
        }
    }


    private class CustomAdapter extends RecyclerView.Adapter<ViewHolder1> {

        private ArrayList<InnerMovie> mItems = new ArrayList<>();

        @NonNull
        @Override
        public ViewHolder1 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inner_data_segregration_level_three_card_view, parent, false);
            return new ViewHolder1(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder1 holder, final int position) {
            final InnerMovie movie = mItems.get(position);

            //TODO here show sub category image under the product category setup to gilde
           /* Glide.with(DataSegregationLevelThree.this).load(movie.getProduct_subCatImage())
                    .thumbnail(Glide.with(DataSegregationLevelThree.this).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProductSubCat);*/

                  Picasso.with(context).load(movie.getProduct_subCatImage())
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgProductSubCat);
           String imgUrl = movie.getProduct_subCatImage();
            if (imgUrl.isEmpty()) {
                holder.imgProductSubCat.setImageResource(R.drawable.loading);
            } else {
                Picasso.with(context)
                        .load(imgUrl)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.ic_app_transparent)
                        .into(holder.imgProductSubCat);
            }
            holder.tvSubCat.setText(movie.getProduct_subCat());

            holder.boxLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on: " + product_mainCat + " & sub cat: " + movie.getProduct_subCat(), context);*/

                     //Intent intent = new Intent(context, DataSegregationFourTabLayout.class);
                    //Intent intent = new Intent(context, ProductListTabLayouts.class);
                    Intent intent = new Intent(context, SubProductListActivity.class);
                    intent.putExtra("product_cat", movie.getProduct_subCat());
                    intent.putExtra("product_mainCat", product_mainCat);

                    intent.putExtra("shopStrProductCat", strProductCat);
                    intent.putExtra("shopProductMainCat", product_mainCat);

                    intent.putExtra("position", position);

                    intent.putExtra("productSuperCat", productSuperCat);
                    startActivity(intent);

                    Log.e("ClickFOURtab:",""+productSuperCat+":"+position+":"+product_mainCat+":"+strProductCat+":"+movie.getProduct_subCat());

                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(InnerMovie innerMovie) {
            mItems.add(innerMovie);
        }
    }

    private class ViewHolder1 extends RecyclerView.ViewHolder {

        private TextView tvSubCat;
        private CircleImageView imgProductSubCat;
        private LinearLayout boxLinearLayout;

        ViewHolder1(View itemView) {
            super(itemView);
            tvSubCat = itemView.findViewById(R.id.txtSubCat);
            imgProductSubCat = itemView.findViewById(R.id.productSubCatImg);
            boxLinearLayout = itemView.findViewById(R.id.boxLinearLayout);
            boxLinearLayout.setTag(this);
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<MainViewHolder> {

        private String selectCondition;
        private ArrayList<Movie> mItems = new ArrayList<>();

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_grid_layout, parent, false);
            final MainViewHolder childViewHolder = new MainViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
                            int itemPosition = recyclerView.getChildAdapterPosition(v);
                            movie = mItems.get(itemPosition);

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on view & product is: " + movie.getP_Name() + "" + movie.getProduct_size(), context);*/

                            if (movie.getProductName().equals("")) {

                            } else {
                                Intent intent = new Intent(context, SingleProductInformation.class);
                                intent.putExtra("product_name", movie.getP_Name());
                                intent.putExtra("shop_id", movie.getShop_id());
                                startActivity(intent);
                            }
                        } else {
                          //  GateWay gateWay = new GateWay(context);
                            //gateWay.displaySnackBar(getView());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return childViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder holder, final int position) {
            final Movie movie = mItems.get(position);
            final DBHelper db = new DBHelper(context);

            boolean btnCheckStatus = addToCartArrayList.contains(movie.getId());
            if (btnCheckStatus) {
                holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
            } else {
                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
            }

            boolean wishListCheckStatus = uniqueWishList.contains(movie.getId());

            if (wishListCheckStatus) {
                holder.imgWishList.setVisibility(View.GONE);
                holder.imgWishListSelected.setVisibility(View.VISIBLE);
            } else {
                holder.imgWishList.setVisibility(View.VISIBLE);
                holder.imgWishListSelected.setVisibility(View.GONE);
            }

            switch (movie.getSelect_type()) {
                case "Size":
                    holder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                    holder.linearLayoutSelectType.setVisibility(View.GONE);
                    holder.linearLayoutSelectSize.setVisibility(View.VISIBLE);
                    break;
                case "Condition":
                    holder.linearLayoutSelectCondition.setVisibility(View.VISIBLE);
                    holder.linearLayoutSelectType.setVisibility(View.VISIBLE);
                    holder.linearLayoutSelectSize.setVisibility(View.GONE);
                    break;
                default:
                    holder.linearLayoutSelectCondition.setVisibility(View.GONE);
                    holder.linearLayoutSelectType.setVisibility(View.GONE);
                    holder.linearLayoutSelectSize.setVisibility(View.GONE);
                    break;
            }

            //TODO here product image setup to glide
            /*Glide.with(DataSegregationLevelThree.this).load("http://simg.picodel.com/" + movie.getImage())
                    .thumbnail(Glide.with(DataSegregationLevelThree.this).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .fitCenter()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.img);*/
            Picasso.with(context).load("https://simg.picodel.com/" + movie.getImage())
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.img);

            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
                        dialogPlus = DialogPlus.newDialog(context)
                                .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.image_pop_up))
                                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .create();

                        ImageView alertProductImage1 = (ImageView) dialogPlus.findViewById(R.id.productImage1);

                        //TODO here when user clicks on product image setup to glide
                       /* Glide.with(DataSegregationLevelThree.this).load("http://simg.picodel.com/" + movie.getImage())
                                .thumbnail(Glide.with(DataSegregationLevelThree.this).load(R.drawable.loading))
                                .error(R.drawable.ic_app_transparent)
                                .fitCenter()
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(alertProductImage1);*/
                        Picasso.with(context).load("https://simg.picodel.com/" + movie.getImage())
                                .error(R.drawable.ic_app_transparent)
                                .into(alertProductImage1);

                        dialogPlus.show();
                    } else {
                        GateWay gateWay = new GateWay(context);
                        gateWay.displaySnackBar(segregationThreeMainLayout);
                    }
                }
            });

            holder.tvProductName.setText(movie.getProductName());
            holder.tvSellerName.setText("By:" + " " + movie.getSellerName());
            String dis = movie.getDiscount();
            String discount = movie.setDiscount(dis);
            holder.tvDiscount.setText(discount + "% off");
            String productMrp = movie.getMrp();
            String discountPrice = movie.getPrice();
            if (productMrp.equals(discountPrice)) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.GONE);
                holder.tvMrp.setVisibility(View.GONE);
            } else {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setVisibility(View.VISIBLE);
                holder.tvMrp.setVisibility(View.VISIBLE);
                holder.tvMrp.setBackgroundResource(R.drawable.dash);
            }
            holder.tvMrp.setText(productMrp);
            holder.tvPrice.setText("\u20B9" + " " + discountPrice);

            String QTY = movie.getStrAvailableQuantity();
            if (QTY.equals("0")) {
                holder.btnAddToCart.setVisibility(View.GONE);
                holder.imgOutOfStock.setVisibility(View.VISIBLE);
                holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));

                //TODO here out of stock GIF image setup to glide
                Picasso.with(context)
                        .load(R.drawable.outofstock)
                        .error(R.drawable.icerror_outofstock)
                        .into(holder.imgOutOfStock);
            } else {
                holder.btnAddToCart.setVisibility(View.VISIBLE);
                holder.imgOutOfStock.setVisibility(View.GONE);
            }

            if (movie.getStrHindiName().equals("")) {
                holder.tvProductHindiName.setVisibility(View.GONE);
            } else {
                holder.tvProductHindiName.setText(movie.getStrHindiName());
            }

            holder.tvRipeType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = holder.tvRipeType.getText().toString();
                    holder.tvProductName.setText(selectCondition + " " + movie.getProductName());
                }
            });

            holder.tvRawType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = holder.tvRawType.getText().toString();
                    holder.tvProductName.setText(selectCondition + " " + movie.getProductName());
                }
            });

            holder.tvSmallSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = holder.tvSmallSize.getText().toString();
                    holder.tvProductName.setText(selectCondition + " " + movie.getProductName());
                }
            });

            holder.tvMediumSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = holder.tvMediumSize.getText().toString();
                    holder.tvProductName.setText(selectCondition + " " + movie.getProductName());
                }
            });

            holder.tvLargeSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectCondition = holder.tvLargeSize.getText().toString();
                    holder.tvProductName.setText(selectCondition + " " + movie.getProductName());
                }
            });

            holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
                        try {
                            shopId = movie.getShop_id();
                            HashMap AddToCartInfo;
                            pId = movie.getId();
                            addToCartArrayList.add(pId);
                            AddToCartInfo = db.getCartDetails(pId);
                            strId = (String) AddToCartInfo.get("new_pid");

                            /*UserTracking UT = new UserTracking(UserTracking.context);
                            UT.user_tracking(class_name + ": clicked on add to cart button & product is: " + movie.getProductName() + " " + movie.getProduct_size(), context);*/

                            if (strId == null) { //TODO Server method here
                                movie.setStatus(false);
                                JSONObject params = new JSONObject();
                                try {
                                    params.put("product_id", movie.getId());
                                    params.put("shop_id", shopId);
                                    params.put("size", movie.getProduct_size());
                                    params.put("qty", 1);
                                    params.put("contactNo", strContact);
                                    params.put("email", strEmail);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCheckQtyProductWise, params, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (response.isNull("posts")) {

                                        } else {
                                            try {
                                                strCheckQty = response.getString("posts");
                                                if (strCheckQty.equals("true")) {
                                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                                    alertDialogBuilder.setMessage("You cannot add more than 1000 gm of this product.");
                                                    alertDialogBuilder.setPositiveButton("OK",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int arg1) {
                                                                    dialogInterface.dismiss();
                                                                }
                                                            });
                                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                                    alertDialog.show();
                                                } else {
                                                    db.insertCount(pId);
                                                    int cnt = (int) db.fetchAddToCartCount();
                                                    ((BaseActivity) context).updateAddToCartCount(cnt);

                                                    if (cnt >= 0) {
                                                        movie.setStatus(true);
                                                        if (movie.isStatus(true)) {
                                                            holder.btnAddToCart.setText(getResources().getString(R.string.go_to_cart));
                                                        }
                                                    } else {
                                                        movie.setStatus(false);
                                                        if (movie.isStatus(false)) {
                                                            holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                                        }
                                                    }
                                                    Toast.makeText(context, "Adding product to cart.", Toast.LENGTH_SHORT).show();
                                                    addCartItemsToServer();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();

                                        GateWay gateWay = new GateWay(context);
                                        gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(request);
                            } else {

                                /*UserTracking UT1 = new UserTracking(UserTracking.context);
                                UT1.user_tracking(class_name + ": clicked on remove cart button & product is: " + movie.getProductName() + " " + movie.getProduct_size(), context);*/

                                holder.btnAddToCart.setText("Go To Cart");
                                deleteCartItemDialog(pId);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        GateWay gateWay = new GateWay(context);
                        gateWay.displaySnackBar(segregationThreeMainLayout);
                    }
                }

                private void deleteCartItemDialog(final String pId) {

                    LayoutInflater layoutInflater = LayoutInflater.from(context);
                    View review = layoutInflater.inflate(R.layout.same_shop_cart, null);
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setView(review);

                    tvMessage = review.findViewById(R.id.txtMessage);
                    tvYes = review.findViewById(R.id.btnYes);
                    tvNo = review.findViewById(R.id.btnNo);
                    tvMessage.setText("Product is " + movie.getProductName() + ". Do you want remove or Go To Cart.");
                    // set dialog message
                    alertDialogBuilder.setCancelable(false);
                    // create alert dialog
                    alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();

                    tvYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            deleteNormalProductFromCartItem(pId);

                        }
                    });

                    tvNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();

                        }
                    });
                }

                private void deleteNormalProductFromCartItem(String p_id) { //TODO Server method here
                    final GateWay gateWay = new GateWay(context);
                    //gateWay.progressDialogStart();
                    simpleProgressBar.setVisibility(View.VISIBLE);

                    final DBHelper db = new DBHelper(context);

                    if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", p_id);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                            params.put("tag", "delete_one_item");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteCartResult, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {

                                } else {
                                    db.deleteProductItem(pId);
                                    holder.btnAddToCart.setText(getResources().getString(R.string.add_to_cart));
                                }
                                int count = (int) db.fetchAddToCartCount();
                                ((BaseActivity) context).updateAddToCartCount(count);
                                //gateWay.progressDialogStop();
                                simpleProgressBar.setVisibility(View.INVISIBLE);
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //gateWay.progressDialogStop();
                                simpleProgressBar.setVisibility(View.INVISIBLE);

                                error.printStackTrace();

                                GateWay gateWay = new GateWay(context);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                        gateWay.displaySnackBar(segregationThreeMainLayout);
                    }
                }

                private void addCartItemsToServer() { //TODO Server method here
                    final GateWay gateWay = new GateWay(context);

                    if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("areaname", Zone_Area);
                            params.put("selectedType", selectCondition);
                            params.put("versionCode", ApplicationUrlAndConstants.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAdd_UpdateCartDetails, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(context);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                        gateWay.displaySnackBar(segregationThreeMainLayout);
                    }
                }
            });

            holder.imgWishList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        shopId = movie.getShop_id();
                        HashMap wishListHashMap;
                        String pId = movie.getId();

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": clicked on add wishlist button & product is: " + movie.getProductName() + " " + movie.getProduct_size(), context);*/

                        uniqueWishList.add(pId); //here productId add in the arrayList for img wishList
                        wishListHashMap = db.getWishListDetails(pId);
                        strId = (String) wishListHashMap.get("pId");

                        if (strId == null) {
                            db.insertProductIntoWishList(pId);
                            int cnt = (int) db.fetchWishListCount();
                            updateWishListCount(cnt);

                            HashMap infoChangeButtonName;
                            String pIdForButtonChange = movie.getId();
                            infoChangeButtonName = db.getWishListDetails(pIdForButtonChange);

                            List<String> listProductNameAlreadyHave = new ArrayList<>(infoChangeButtonName.values());
                            boolean val = listProductNameAlreadyHave.contains(pId);

                            if (val) {
                                holder.imgWishList.setVisibility(View.GONE);
                                holder.imgWishListSelected.setVisibility(View.VISIBLE);
                            } else {
                                holder.imgWishList.setVisibility(View.VISIBLE);
                            }
                            addWishListItemsToServer();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void addWishListItemsToServer() { //TODO Server method here
                    pId = movie.getId();
                    shopId = movie.getShop_id();

                    if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
                        JSONObject params = new JSONObject();
                        try {
                            params.put("product_id", pId);
                            params.put("shop_id", shopId);
                            params.put("versionCode", ApplicationUrlAndConstants.versionName);
                            params.put("Qty", 1);
                            params.put("contactNo", strContact);
                            params.put("email", strEmail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlAddMyWishListItems, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(context);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                        GateWay gateWay = new GateWay(context);
                        gateWay.displaySnackBar(segregationThreeMainLayout);
                    }
                }
            });

            holder.imgWishListSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        pId = movie.getId();

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": clicked on remove wishlist button & product is: " + movie.getProductName() + " " + movie.getProduct_size(), context);*/


                        int deleteProductItem = db.deleteWishListProductItem(pId);
                        int cnt = (int) db.fetchWishListCount();
                        updateWishListCount(cnt);
                        uniqueWishList.remove(pId);
                        holder.imgWishList.setVisibility(View.VISIBLE);
                        holder.imgWishListSelected.setVisibility(View.GONE);
                        deleteoneProductFromWishlist(pId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void deleteoneProductFromWishlist(String pid) { //TODO Server method here
                    if (Connectivity.isConnected(context)) { // Internet connection is not present, Ask user to connect to Internet
                        JSONObject params = new JSONObject();
                        try {
                            params.put("contactNo", strContact);
                            params.put("product_id", pid);
                            params.put("tag", "delete_one_item");
                            params.put("email", strEmail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlDeleteMyWishListItems, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (response.isNull("posts")) {

                                } else {
                                    db.deleteWishListProductItem(pId);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();

                                GateWay gateWay = new GateWay(context);
                                gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                            }
                        });
                        AppController.getInstance().addToRequestQueue(request);
                    } else {
                        GateWay gateWay = new GateWay(context);
                        gateWay.displaySnackBar(segregationThreeMainLayout);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(Movie movie) {
            mItems.add(movie);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        //((BaseActivity) context).changeTitle(selectedName);

        if (Connectivity.isConnected(context)) {
            GateWay gateWay = new GateWay(context);
            SyncNormalCartItems UT1 = new SyncNormalCartItems(SyncNormalCartItems.context);
            UT1.syncNormalCartItems(gateWay.getContact(), gateWay.getUserEmail(), context);
            GateWay gateWay1 = new GateWay(context);
            SyncWishListItems UT2 = new SyncWishListItems(SyncWishListItems.context);
            UT2.syncWishListItems(gateWay1.getContact(), gateWay1.getUserEmail(), context);
            SyncData();

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, context);*/
        }

      //  GateWay gateWay = new GateWay(context);
       // gateWay.hide();
    }

    private void SyncData() { //TODO Server method here
        JSONObject params = new JSONObject();
        try {
            params.put("contact", strContact);
            params.put("email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    WishListCount = response.getInt("wishlist_count");
                    CartCount = response.getInt("normal_cart_count");

                   // ((BaseActivity) context).updateAddToCartCount(CartCount);

                    if (WishListCount >= 0) {
                        updateWishListCount(WishListCount);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callBackPressed();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void callBackPressed() {
        DBHelper db = new DBHelper(context);
        addToCartArrayList = new ArrayList<>();  //this is for addToCart code
        for (int j = 0; j < productId.size(); j++) {
            HashMap info = new HashMap();
            try {
                info = db.getCartDetails(productId.get(j));
            } catch (Exception e) {
                e.printStackTrace();
            }
            String strProductId = (String) info.get("new_pid");
            addToCartArrayList.add(strProductId);
        }

        for (int j = 0; j < productId.size(); j++) { //this is for wishList code
            HashMap info = new HashMap();
            try {
                info = db.getWishListDetails(productId.get(j));
            } catch (Exception e) {
                e.printStackTrace();
            }
            String strProductIdWishList = (String) info.get("pId");
            if (strProductIdWishList != null && !(strProductIdWishList.equals(""))) {
                uniqueWishList.add(strProductIdWishList);
            }
        }
    }

    private class MainViewHolder extends RecyclerView.ViewHolder {

        private ImageView img;
        private TextView tvProductName;
        private TextView tvSellerName;
        private TextView tvMrp;
        private TextView tvDiscount;
        private TextView tvPrice;
        private TextView tvRipeType;
        private TextView tvRawType;
        private TextView tvSmallSize;
        private TextView tvMediumSize;
        private TextView tvLargeSize;
        private TextView tvProductHindiName;
        private TextView btnAddToCart;
        private ImageView imgWishList;
        private ImageView imgWishListSelected;
        private ImageView imgOutOfStock;
        private LinearLayout linearLayoutSelectCondition;
        private LinearLayout linearLayoutSelectType;
        private LinearLayout linearLayoutSelectSize;

        public MainViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imgProduct);
            tvProductName = itemView.findViewById(R.id.txtProductName);
            tvProductHindiName = itemView.findViewById(R.id.txtProductHindiName);
            tvSellerName = itemView.findViewById(R.id.txtSellerName);
            tvMrp = itemView.findViewById(R.id.txtTotal);
            tvDiscount = itemView.findViewById(R.id.txtDiscount);
            tvPrice = itemView.findViewById(R.id.txtPrice);
            tvRipeType = itemView.findViewById(R.id.txtRipeType);
            tvRawType = itemView.findViewById(R.id.txtRawType);
            tvSmallSize = itemView.findViewById(R.id.txtSmallSize);
            tvMediumSize = itemView.findViewById(R.id.txtMediumSize);
            tvLargeSize = itemView.findViewById(R.id.txtLargeSize);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            imgWishList = itemView.findViewById(R.id.wishList);
            imgWishListSelected = itemView.findViewById(R.id.wishList1);
            linearLayoutSelectCondition = itemView.findViewById(R.id.selectCondition);
            linearLayoutSelectType = itemView.findViewById(R.id.linearLayoutSelectType);
            linearLayoutSelectSize = itemView.findViewById(R.id.linearLayoutSelectSize);
            imgOutOfStock = itemView.findViewById(R.id.imgOutOfStock);
            btnAddToCart.setTag(this);
            imgWishList.setTag(this);
            imgWishListSelected.setTag(this);
        }
    }



    private void updateWishListCount(final int count) {
        if (tvWishList == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0)
                    tvWishList.setVisibility(View.INVISIBLE);
                else {
                    tvWishList.setVisibility(View.VISIBLE);
                    tvWishList.setText(Integer.toString(count));
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        final DBHelper db = new DBHelper(context);
        getMenuInflater().inflate(R.menu.menu_local_search_product, menu);
        super.onCreateOptionsMenu(menu);

        MenuItem item = menu.findItem(R.id.action_login);
        item.setVisible(false);

        MenuItem item2 = menu.findItem(R.id.search);
        item2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(context, MasterSearch.class));
                return false;
            }
        });

        MenuItem item1 = menu.findItem(R.id.action_wish_list);
        MenuItemCompat.setActionView(item1, R.layout.wish_list_notification_icon);
        RelativeLayout notifyCount1 = (RelativeLayout) MenuItemCompat.getActionView(item1);

        tvWishList = notifyCount1.findViewById(R.id.cartNumber);
        notifyCount1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int cnt = (int) db.fetchWishListCount();
                if (cnt == 0) {
                    GateWay gateWay = new GateWay(context);
                    gateWay.wishListAlertDialog();
                } else {
                    Intent intent = new Intent(context, WishList.class);
                    startActivity(intent);
                }
            }
        });

        int wishListCount = (int) db.fetchWishListCount();
        if (wishListCount == 0) {
            tvWishList.setVisibility(View.INVISIBLE);
        } else {
            tvWishList.setVisibility(View.VISIBLE);
            tvWishList.setText("" + wishListCount);
        }

        notifyCount1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast toast = Toast.makeText(context, "WishList", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.RIGHT, 0, 110);
                toast.show();
                return true;
            }
        });
        return true;
    }
}
