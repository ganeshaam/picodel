package app.apneareamein.shopping.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.ContinueShopingActivity;
import app.apneareamein.shopping.activities.DeliveryAddress;
import app.apneareamein.shopping.activities.Order_Details_StepperActivity;
import app.apneareamein.shopping.activities.PaymentActivityFreecharge;
import app.apneareamein.shopping.adapters.PaymentModeAdaptor;
import app.apneareamein.shopping.adapters.ShopListAdapter;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.local_storage.DBHelper;
import app.apneareamein.shopping.model.PaymentModeModel;
import app.apneareamein.shopping.model.ShopModel;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.MySingleton;
import app.apneareamein.shopping.utils.SharedPreferencesUtils;
import app.apneareamein.shopping.utils.VolleySingleton;
import dmax.dialog.SpotsDialog;
//import app.apneareamein.shopping.utils.SharedPreferencesUtils;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;


//import dmax.dialog.SpotsDialog;
public class Order_Details extends Fragment implements View.OnClickListener, Order_Details_StepperActivity.CheckBoxStateCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener{

    private final String class_name = this.getClass().getSimpleName();
    private DialogPlus dialog, dialogPlus;
    private RecyclerView coupon_recycler_view, viewcart_items_recycler_view,rv_paymentMode;
    private Bundle bundle;
    private CardAdapter dialog_cardAdapter;
    private CouponsAdapter couponsAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private List<String> childArrayList;
    private final HashMap keyValueCategoryMap = new HashMap();
    private final Map<String, List<String>> childCollection = new LinkedHashMap<>();
    private ArrayList<String> mainScheduledDateValues = new ArrayList<>();
    private ArrayList<String> mainScheduledTimeValues = new ArrayList<>();
    private final List<Normal_Cart_Item> mItems = new ArrayList<>();
    ArrayList<UserInfo> arr_user_info;// = new ArrayList<>();
    private AlertDialog alertDialog;
    android.app.AlertDialog progressDialog2;
    private String strEmail, strContact, cart_count, finalPrice;
    private String strAddress_Id,strAddress_addresId,address1,address2,address3,address4,address5,address,area;;
    private String final_price;
    private String strDynamicPriceMsg;
    private String strResponse;
    private String strInitialTotalAmount = null;
    private String strShippingAmount = null;
    private String strSavingAmount = null;
    private String strFinalAmount = null;
    private String strScratchCardAmount = null;
    private EditText etSpecialRemark;
    private double double_coupon_min_amt, double_final_amt;
    private String strCouponSavingAmt;
    private String coupon_code="";
    private String cashback_points;
    private String strNotifyMessage;
    private TextView tvPrice;
    private TextView tvDiscount;
    private TextView tvShippingCharge;
    private TextView txtpaymnentmsg;
    private TextView tvTotal;
    private TextView tvCouponDiscount;
    private TextView tvDiscountAmt,tvScratchCardAmount;
    private TextView tvCouponMessage;
    private TextView tvAddress;
    //private TextView txtDefault;
    private TextView tvUserName;
    private TextView tvUserContact;
    private TextView tvItems;
    private TextView tvDialogTitle;
    private TextView tvPriceCond;
    private LinearLayout priceLinearLayout,coordinatorLayout;
    //private RelativeLayout coordinatorLayout;
    private CheckBox chkCashBackTerms,cb_cash,cb_paytm,cb_googlepay,cb_phonepe,cb_swipe;
    private String strName;
    private boolean chkStatus;
    private String SelectedDeliveryMethod;
    private String paymentType = "";
    private Spinner date_spinner;
    private Spinner coupondate_spinner;
    private Spinner time_spinner;
    private Spinner coupontime_spinner;
    private String scheduleSelectedDate;
    private String scheduleSelectedTime;
    private EditText editCouponCode;
    private EditText editTRN;
    private EditText edittrnAmt;
    private String couponCode;
    LinearLayout TRNLayout;
    LinearLayout datenTimeLayout,list_check_box;
    //String URL = "http://192.168.43.196/";
    private LinearLayout deliveryfeesLayout;
    private LinearLayout applyCouponLayout,layout_condition;
    LinearLayout delivery_optionsDisplayLayout;
    LinearLayout couponLayout;
    public RecyclerView rv_nearbyshoplist;

     /*Curretn Location Objects*/
    int position=0;
    String  latitude,longitude,c_latitude="0.0",c_longitude="0.0";
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager locationManager,mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private RelativeLayout relativeLayoutEmptyCart;

    Button btnShopNow;
    SharedPreferencesUtils sharedPreferencesUtils;
    ArrayList<String> payment_opt = new ArrayList<>();
    private ProgressBar simpleProgressBar;
    private ProgressDialog progressDoalog;
      /*Payment Mode List*/
    PaymentModeAdaptor paymentModeAdaptor;
    LinearLayoutManager layoutManager;
    ArrayList <PaymentModeModel> paymentModeList;
    ArrayList <String> payment_types;
    String coupn_status = "deactive",user_terms="NA",v_city="",nearbyshop_id="00",product_maincat="",order_id="12689";
    public static final String MY_PREFS_NAME = "PICoDEL";

    ShopListAdapter shopListAdapter;
    ArrayList<ShopModel> orderArrayList;
    private Spinner sp_shopList;
    ArrayList<String> arrayshoplist;
    Button btnPlaceOrder,btnPlaceOrder2;

    RadioGroup radioGroup_online;
    RadioButton radio_online,radio_pod,selectedRadioButton;

    /*FreechargeIntegration*/

    public String Freecharge_checksum = "";
    private  WebView wb_freecharge;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        sharedPreferencesUtils = new SharedPreferencesUtils(getActivity());
        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        //user_terms = prefs.getString("user_terms", "");
        //nearbyshopid = prefs.getString("nearbyshopid", "");
        product_maincat = prefs.getString("product_maincat", "");
        v_city = prefs.getString("v_city", "");

        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Its loading....");
        progressDoalog.setTitle("Please wait while come to response");

        coordinatorLayout = view.findViewById(R.id.coordinatorLayout);
        Button tvChangeAddress = view.findViewById(R.id.txtChangeAddress);
        tvAddress = view.findViewById(R.id.txtAddress);
        //txtDefault = view.findViewById(R.id.txtDefault);
        tvUserName = view.findViewById(R.id.txtUserName);
        tvUserContact = view.findViewById(R.id.txtContact);
        tvItems = view.findViewById(R.id.txtItems);
        etSpecialRemark = view.findViewById(R.id.editSpecialRemark);
        tvPrice = view.findViewById(R.id.txtPrice);
        tvDiscount = view.findViewById(R.id.txtDiscount);
        tvScratchCardAmount = view.findViewById(R.id.txtScratchCardAmount);
        tvShippingCharge = view.findViewById(R.id.txtShippingCharge);
        txtpaymnentmsg = view.findViewById(R.id.txtpaymnentmsg);
        tvTotal = view.findViewById(R.id.txtTotal);
        tvDiscountAmt = view.findViewById(R.id.txtDiscountAmt);
        tvCouponDiscount = view.findViewById(R.id.txtCouponDiscount);
        editCouponCode = view.findViewById(R.id.editCouponCode);
        editTRN = view.findViewById(R.id.editTRN);
        edittrnAmt = view.findViewById(R.id.edittrnAmt);
        Button btnAddCouponCode = view.findViewById(R.id.btnAddCouponCode);
        Button btnSkipCouponCode = view.findViewById(R.id.btnSkipCouponCode);
        Button btnAddTRN = view.findViewById(R.id.btnAddTRN);
        btnPlaceOrder = view.findViewById(R.id.btnPlaceOrder);
        btnPlaceOrder2 = view.findViewById(R.id.btnPlaceOrder2);
        expListView = view.findViewById(R.id.lvExp);

        date_spinner = view.findViewById(R.id.date_spinner);
        coupondate_spinner = view.findViewById(R.id.coupondate_spinner);
        time_spinner = view.findViewById(R.id.time_spinner);
        coupontime_spinner = view.findViewById(R.id.coupontime_spinner);
        relativeLayoutEmptyCart = view.findViewById(R.id.relativeLayoutEmptyCart);
        /* btnShopNow = view.findViewById(R.id.btnShopNow);*/

        simpleProgressBar = view.findViewById(R.id.simpleProgressBar);
        layout_condition = view.findViewById(R.id.layout_condition);

        LinearLayout addressDisplayLayout = view.findViewById(R.id.addressDisplayLayout);
        LinearLayout ll_shoplist = view.findViewById(R.id.ll_shoplist);
        deliveryfeesLayout = view.findViewById(R.id.deliveryfeesLayout);
        applyCouponLayout = view.findViewById(R.id.applyCouponLayout);
        LinearLayout itemsDisplayLayout = view.findViewById(R.id.itemsDisplayLayout);
        delivery_optionsDisplayLayout = view.findViewById(R.id.delivery_optionsDisplayLayout);
        TRNLayout = view.findViewById(R.id.TRNLayout);
        datenTimeLayout = view.findViewById(R.id.datenTimeLayout);
        couponLayout = view.findViewById(R.id.couponLayout);
        priceLinearLayout = view.findViewById(R.id.priceLinearLayout);
        list_check_box = view.findViewById(R.id.list_check_box);

        LinearLayout finalDisplayLayout = view.findViewById(R.id.FinalDisplayLayout);
        sp_shopList = view.findViewById(R.id.sp_shopList);
        wb_freecharge = view.findViewById(R.id.wb_freecharge);
        WebSettings webSettings = wb_freecharge.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wb_freecharge.getSettings().setJavaScriptEnabled(true);
        wb_freecharge.getSettings().setLoadsImagesAutomatically(true);
        wb_freecharge.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        radioGroup_online = view.findViewById(R.id.radioGroup_online);
        radio_online = view.findViewById(R.id.radio_online);
        radio_pod = view.findViewById(R.id.radio_pod);


        GateWay gateWay = new GateWay(getActivity());
        strEmail = gateWay.getUserEmail();
        strContact = gateWay.getContact();
        strName = gateWay.getUserName();



        //coordinatorLayout_next = view.findViewById(R.id.coordinatorLayout_next);
        // checkbox init
        arrayshoplist = new ArrayList<>();
        payment_types = new ArrayList<>();
        orderArrayList = new ArrayList<>();
        rv_paymentMode = view.findViewById(R.id.rv_paymentMode);
        rv_paymentMode.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        paymentModeList = new ArrayList<>();

        rv_paymentMode.setLayoutManager(layoutManager);
        rv_paymentMode.setItemAnimator(new DefaultItemAnimator());
        paymentModeAdaptor = new PaymentModeAdaptor(getActivity(),paymentModeList);
        rv_paymentMode.setAdapter(paymentModeAdaptor);


        rv_nearbyshoplist = view.findViewById(R.id.rv_nearbyshoplist);
        rv_nearbyshoplist.setHasFixedSize(true);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
        rv_nearbyshoplist.setLayoutManager(layoutManager1);
        shopListAdapter = new ShopListAdapter(orderArrayList,getActivity());
        shopListAdapter.notifyDataSetChanged();




        radioGroup_online.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch(i) {
                    case R.id.radio_online:

                        if(payment_types.contains("Freecharge")){
                            payment_types.remove("Freecharge");
                        }else {
                            payment_types.add("Freecharge");
                        }
                        rv_paymentMode.setVisibility(View.GONE);
                        btnPlaceOrder.setText("Pay Now");
                        //num=1;
                        //Toast.makeText(getApplicationContext()," Car",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.radio_pod:
                        rv_paymentMode.setVisibility(View.GONE);
                        btnPlaceOrder.setText("Place Order");
                        if(payment_types.contains("Cash/any wallet/UPI ie PhonePe/Gpay/Paytm/Freecharge")){
                            payment_types.remove("Cash/any wallet/UPI ie PhonePe/Gpay/Paytm/Freecharge");
                        }else {
                            payment_types.add("Cash/any wallet/UPI ie PhonePe/Gpay/Paytm/Freecharge");
                        }
                        //Cash/any wallet/UPI ie PhonePe/Gpay/Paytm/Freecharge
                        //num=2;
                        //Toast.makeText(getApplicationContext()," Bike",Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });






        //getshoplist();

        paymentModeList();
        payment_types.add("Cash");
        rv_paymentMode.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_paymentMode,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        Toast.makeText(getActivity(),paymentModeList.get(position).getPaymentType(),Toast.LENGTH_SHORT).show();
                        if(payment_types.contains(paymentModeList.get(position).getPaymentType())){
                            payment_types.remove(paymentModeList.get(position).getPaymentType());
                        }else {
                            payment_types.add(paymentModeList.get(position).getPaymentType());
                        }
                        Log.e("Final_payment_option",""+payment_types.toString());

                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        //wb_freecharge.setWebViewClient(new WebViewClient());
        wb_freecharge.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view , String url){
                //view.loadUrl("javascript:clickFunction()");
                Log.e("ClickEvent:",""+url);
            }
        });

        //GET SHOPID FROM THE NEAR BY SHOPLIST

        //GET SHOP ID From the selected shop by user
        sp_shopList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String value = sp_shopList.getSelectedItem().toString();

                for(int i=0; i<orderArrayList.size();i++){

                    if(orderArrayList.get(i).getShop_name().contains(value)){

                        nearbyshop_id = orderArrayList.get(i).getShop_id();
                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("nearbyshopid", nearbyshop_id);
                        editor.putString("product_maincat", product_maincat);
                        editor.apply();
                        editor.commit();

                        Toast.makeText(getActivity(),"ShopID:"+nearbyshop_id+"Name"+orderArrayList.get(i).getShop_name(),Toast.LENGTH_LONG).show();

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rv_nearbyshoplist.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rv_nearbyshoplist, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               /* nearbyshopid = orderArrayList.get(position).getShop_id();
                Toast.makeText(getActivity(),"Selected ShopId:"+orderArrayList.get(position).getShop_name(),Toast.LENGTH_LONG).show();
                SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("nearbyshopid", nearbyshopid);
                editor.apply();
                editor.commit();*/
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        tvChangeAddress.setOnClickListener(this);
        tvItems.setOnClickListener(this);
        btnPlaceOrder.setOnClickListener(this);
        btnPlaceOrder2.setOnClickListener(this);
        btnAddCouponCode.setOnClickListener(this);
        btnSkipCouponCode.setOnClickListener(this);
        btnAddTRN.setOnClickListener(this);
        txtpaymnentmsg.setOnClickListener(this);
        tvPriceCond = view.findViewById(R.id.txtPriceCond);
        final CheckBox chkAccept = view.findViewById(R.id.chkAcceptTerms);

        progressDialog2 = new SpotsDialog.Builder().setContext(getActivity()).build();

        chkAccept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chkStatus = b;
            }
        });



        layout_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tvPriceCond.getVisibility()==View.VISIBLE){
                    tvPriceCond.setVisibility(View.GONE);
                }else {
                    tvPriceCond.setVisibility(View.VISIBLE);
                }
            }
        });

        //here we get all values from bundle
        bundle = getArguments();
        finalPrice = bundle.getString("finalPrice");

        //strAddress_Id = sharedPreferencesUtils.getCategory();
        strAddress_Id = bundle.getString("address_id");
        String strTag = bundle.getString("tag");

        switch (strTag) {
            case "address_view":
                addressDisplayLayout.setVisibility(View.VISIBLE);
                break;//shop_view
            case "shop_view":
                ll_shoplist.setVisibility(View.VISIBLE);
                break;
            case "item_view":
                itemsDisplayLayout.setVisibility(View.VISIBLE);
                break;
            case "final_order":
                finalDisplayLayout.setVisibility(View.VISIBLE);
                break;
            case "options_view":
                // delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);


                //openCupunDialog();  Dialog to Ask for Cupon Code
                getCoupons();
                /*if(coupn_status.equalsIgnoreCase("active")) {
                    openCupunDialog();

                }else {
                    couponLayout.setVisibility(View.GONE);
                    getDeliveryOptions();
                    delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                }*/
                /*couponLayout.setVisibility(View.GONE);
                getDeliveryOptions();
                delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);*/
                break;
        }

        ///Log.d("gotoNextStep",strAddress_Id);
       /* if (strAddress_Id != null && strAddress_Id.equals("address_id")) {
            //position=0;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    strAddress_Id = sharedPreferencesUtils.getCategory();
                    Log.d("getCategoryA",strAddress_Id);
                    //position=0;
                    fetchUserAddress(strAddress_Id);
                }
            },300);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    strAddress_Id = sharedPreferencesUtils.getCategory();
                    Log.d("getCategoryB",strAddress_Id);
                    //position=0;
                    fetchUserAddress(strAddress_Id);
                }
            },300);

        }*/

        btnPlaceOrder2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PaymentActivityFreecharge.class);
                intent.putExtra("strFinalAmount", strFinalAmount);
                intent.putExtra("order_id", "12689");//categoryModelArrayList.get(position).getMaincategory()//Eggs
                //intent.putExtra("selectedName", "Non_Veg");
                startActivity(intent);
                getActivity().finish();
            }
        });

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousItem = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem)
                    expListView.collapseGroup(previousItem);
                previousItem = groupPosition;
                if (date_spinner.getVisibility() == View.VISIBLE) {
                    date_spinner.setVisibility(View.GONE);
                    list_check_box.setVisibility(View.GONE);
                }
                if (time_spinner.getVisibility() == View.VISIBLE) {
                    time_spinner.setVisibility(View.GONE);
                    list_check_box.setVisibility(View.GONE);
                }

                if (priceLinearLayout.getVisibility() == View.VISIBLE) {
                    priceLinearLayout.setVisibility(View.GONE);
                    list_check_box.setVisibility(View.GONE);
                }
                paymentType = listDataHeader.get(groupPosition);

                /*UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + " selected " + paymentType, getActivity());*/
            }
        });

        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int i) {
                if (date_spinner.getVisibility() == View.VISIBLE) {
                    date_spinner.setVisibility(View.GONE);
                    list_check_box.setVisibility(View.GONE);
                }
                if (time_spinner.getVisibility() == View.VISIBLE) {
                    time_spinner.setVisibility(View.GONE);
                    list_check_box.setVisibility(View.GONE);
                }
                if (priceLinearLayout.getVisibility() == View.VISIBLE) {
                    priceLinearLayout.setVisibility(View.GONE);
                    list_check_box.setVisibility(View.GONE);
                }
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view,
                                        int groupPosition, int childPosition, long id) {
                if (parent.isGroupExpanded(groupPosition)) {
                    expListView.collapseGroup(groupPosition);
                }
                return false;
            }
        });

        chkCashBackTerms = view.findViewById(R.id.chkCashBackTerms);
        if (!chkCashBackTerms.isChecked()) {
            chkCashBackTerms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on cashback terms", getActivity());*/

                    dialogPlus = DialogPlus.newDialog(getActivity())
                            .setContentHolder(new ViewHolder(R.layout.dialog_cashback_terms))
                            .setContentHeight(ViewGroup .LayoutParams.WRAP_CONTENT)
                            .setGravity(Gravity.CENTER)
                            .setHeader(R.layout.terms_header)
                            .setPadding(20, 20, 20, 20)
                            .setFooter(R.layout.terms_footer)
                            .setCancelable(true)
                            .create();
                    dialogPlus.show();

                    WebView terms = (WebView) dialogPlus.findViewById(R.id.webview);
                    terms.setVisibility(View.VISIBLE);
                    TextView tvAcceptTerms = (TextView) dialogPlus.findViewById(R.id.txtAcceptTerms);
                    terms.loadUrl(ApplicationUrlAndConstants.cash_back);
                    tvAcceptTerms.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogPlus.dismiss();
                            chkCashBackTerms.setFocusable(true);
                            chkCashBackTerms.setClickable(false);
                        }
                    });
                }
            });
        }

        if (Connectivity.isConnected(getActivity())) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());*/

        }

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        checkLocation(); //check whether location service is enable or not in your  phone

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }

     /*   btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BaseActivity.class);
                intent.putExtra("tag", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });*/
        return view;
    }

    private void preparationForCollection() {

        Set tpSet = keyValueCategoryMap.keySet();
        ArrayList<String> tpArrayList = new ArrayList<>();
        for (String temp : listDataHeader) {
            if (tpSet.contains(temp)) {
                tpArrayList = (ArrayList<String>) keyValueCategoryMap.get(temp);
            }
            childCollection.put(temp, tpArrayList);
        }
        try {
            ExpandableListAdapter listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, childCollection);
            expListView.setAdapter(listAdapter);

            expListView.expandGroup(0);
            /*for (int i = 0; i < childCollection.size(); i++) {
                expListView.expandGroup(i);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //TODO Server method here String strcart_count,String strPincode
  /*  private void getDeliveryOptions() {
        if (Connectivity.isConnected(getActivity())) {
            String strPincode = arr_user_info.get(0).getStrPincode();
            cart_count = arr_user_info.get(0).getStrCount();

            listDataHeader = new ArrayList<>();

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("pin_code", strPincode);
                params.put("my_cart_count", cart_count);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, ApplicationUrlAndConstants.urlOrderDetails, params, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        Log.d("OrderDetails", "getDeliveryOptions response:" + response.toString());
                        JSONObject value = response.getJSONObject(0);
                        JSONObject value1 = response.getJSONObject(1);

                        JSONArray mainCat = value.getJSONArray("parent");
                        for (int i = 0; i < mainCat.length(); i++) {
                            JSONObject jsonMainCatObject = mainCat.getJSONObject(i);
                            listDataHeader.add(jsonMainCatObject.getString("key_text")); //add to arraylist
                        }

                        JSONArray mainProductCat = value1.getJSONArray("child");

                        for (int j = 0; j < mainProductCat.length(); j++) {
                            JSONObject jsonProductCatObject = mainProductCat.getJSONObject(j);
                            JSONArray mainProductCatArray = jsonProductCatObject.getJSONArray(listDataHeader.get(j));

                            childArrayList = new ArrayList<>();

                            for (int g = 0; g < mainProductCatArray.length(); g++) {

                                JSONObject jsonProductCatObjectArray = mainProductCatArray.getJSONObject(g);

                                childArrayList.add(jsonProductCatObjectArray.getString("child_text")); //add to arraylist
                            }
                            Collections.sort(childArrayList);

                            keyValueCategoryMap.put(listDataHeader.get(j), childArrayList);

                        }
                        gateWay.progressDialogStop();
                        preparationForCollection();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }*/

    private void getDeliveryOptions() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            //String strPincode = arr_user_info.get(0).getStrPincode();
            //cart_count = arr_user_info.get(0).getStrCount();

            listDataHeader = new ArrayList<>();

            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("pin_code", "411028");
                //params.put("my_cart_count", cart_count);//1
                params.put("my_cart_count", "1");//1
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, ApplicationUrlAndConstants.urlOrderDetails, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        //JSONArray jsonObject = response.getJSONArray("");
                        Log.e("OrderDetails", "getDeliveryOptions response:" + response.toString());
                        JSONObject value = response.getJSONObject(0);
                        JSONObject value1 = response.getJSONObject(1);

                        JSONArray mainCat = value.getJSONArray("parent");
                        for (int i = 0; i < mainCat.length(); i++) {
                            JSONObject jsonMainCatObject = mainCat.getJSONObject(i);
                            listDataHeader.add(jsonMainCatObject.getString("key_text")); //add to arraylist
                        }

                        JSONArray mainProductCat = value1.getJSONArray("child");

                        for (int j = 0; j < mainProductCat.length(); j++) {
                            JSONObject jsonProductCatObject = mainProductCat.getJSONObject(j);
                            JSONArray mainProductCatArray = jsonProductCatObject.getJSONArray(listDataHeader.get(j));

                            childArrayList = new ArrayList<>();

                            for (int g = 0; g < mainProductCatArray.length(); g++) {

                                JSONObject jsonProductCatObjectArray = mainProductCatArray.getJSONObject(g);

                                childArrayList.add(jsonProductCatObjectArray.getString("child_text")); //add to arraylist
                            }
                            Collections.sort(childArrayList);

                            keyValueCategoryMap.put(listDataHeader.get(j), childArrayList);

                        }
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                        preparationForCollection();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
            // VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        }
    }

    private void fetchUserAddress(String strAddress_Id) { //TODO  server method here
        if (Connectivity.isConnected(getActivity())) {
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            arr_user_info = new ArrayList<>();
            //arr_user_info.clear();
            JSONObject params = new JSONObject();
            try {
                params.put("id", strAddress_Id);//
                params.put("tag", "checkout");
                params.put("contact", strContact);
                params.put("email", strEmail);
                params.put("tag_for_Cart_Count", "normal_cart");
                Log.e("fetchAddress_params",params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetuserdata, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        Log.d("fetchAddress",res);
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");

                        if (status.equals("success")) {
                            JSONArray addressArray = jsonObject.getJSONArray("user_data");

                            for (int i = 0; i < addressArray.length(); i++) {
                                JSONObject json_event_list = addressArray.getJSONObject(i);
                                UserInfo ui = new UserInfo(json_event_list.getString("id"),
                                        json_event_list.getString("name"),
                                        json_event_list.getString("address"), json_event_list.getString("contact"),
                                        json_event_list.getString("pincode"), json_event_list.getString("count"),
                                        json_event_list.getString("cashback_points"), json_event_list.getString("dynamic_price_msg"),
                                        json_event_list.getString("msg"), json_event_list.getString("address1"),
                                        json_event_list.getString("address2"), json_event_list.getString("address3"),
                                        json_event_list.getString("address4"), json_event_list.getString("address4"),
                                        json_event_list.getString("area"),json_event_list.getString("city"),
                                        json_event_list.getString("state"),json_event_list.getString("latitude"),
                                        json_event_list.getString("longitude"));
                                arr_user_info.add(ui);
                                latitude = json_event_list.getString("latitude");
                                longitude = json_event_list.getString("longitude");
                            }
                            //setDefaultAddress(arr_user_info.get(0).getAddress_id());//this is for old user when they have no default address set

                            tvUserName.setText(arr_user_info.get(position).getStrName());
                            getshoplist();
                            tvAddress.setText(arr_user_info.get(position).getAddress1()+" "+arr_user_info.get(position).getAddress2()+" "+arr_user_info.get(position).getAddress3()+" "+arr_user_info.get(position).getAddress4()+" "+arr_user_info.get(position).getArea());
                            //address1=arr_user_info.get(position).getAddress1();
                            //Log.d("testAddress1",address1);
                            tvUserContact.setText(arr_user_info.get(position).getStrContact());
                            cart_count = arr_user_info.get(position).getStrCount();
                            cashback_points = arr_user_info.get(position).getStrCB_Points();
                            strDynamicPriceMsg = arr_user_info.get(position).getStrDynamic_price_msg();
                            tvPriceCond.setText(strDynamicPriceMsg);

                            if (!cashback_points.equals(0) || !cashback_points.equals("") || !cashback_points.equals(null)) {
                                Order_Details_StepperActivity.LinCB_Points.setVisibility(View.VISIBLE);
                                String[] cb_res = cashback_points.split("@");
                                Order_Details_StepperActivity.tvCBPoints.setText(cb_res[0]);
                                Order_Details_StepperActivity.tvPoints.setText(cb_res[1]);
                            }

                            if (cart_count.equals(1)) {
                                tvItems.setText("View " + cart_count + " item in cart");
                            } else {
                                tvItems.setText("View " + cart_count + " items in cart");
                            }
                            //getDeliveryOptions();
                        } else {
                         /*   Intent intent = new Intent(getActivity(), EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("tag", "address");
                            bundle.putString("type", "add");
                            bundle.putString("finalPrice", finalPrice);
                            intent.putExtras(bundle);
                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            //getActivity().finish();*/
                            Intent intent = new Intent(getActivity(), DeliveryAddress.class);
                            String tag_address = "address";
                            intent.putExtra("tag", tag_address);
                            intent.putExtra("finalPrice", finalPrice);
                            //startActivity(intent);
                            startActivityForResult(intent, 1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }


    //from this method we can set current address of user as a default address
    private void setDefaultAddress(String address_id) { //TODO Server Method here
        GateWay gateWay = new GateWay(getActivity());

        JSONObject params = new JSONObject();
        try {
            params.put("contact", gateWay.getContact());
            params.put("address_id", address_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlSetDefaultAddress, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("posts").equals("true")) {
                        Toast.makeText(getActivity(), "Set default address successfully", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public Boolean getTheState() {
        return chkStatus;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                // position = data.getIntExtra("addressPosition",0);
                String sstrAddress_Id = data.getStringExtra("address_id_test");

                Log.d("addressPosition",sstrAddress_Id+" "+""+position+" "+strAddress_Id);
                // arr_user_info.clear();
         /*       new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        strAddress_Id = sharedPreferencesUtils.getCategory();
                        Log.d("getCategoryC",strAddress_Id);
                        Log.d("onActivityResult",strAddress_Id);
                        //position=0;
                        fetchUserAddress(strAddress_Id);
                    }
                },500);*/
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                strAddress_Id = sharedPreferencesUtils.getCategory();
                Log.d("addressPositionR",+position+" "+strAddress_Id);
                //position=0;
                if (strAddress_Id != null && strAddress_Id.equals("address_id")) {
                    //position=0;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fetchUserAddress(strAddress_Id);
                        }
                    },300);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fetchUserAddress(strAddress_Id);
                        }
                    },300);

                }
            }
        },300);


    }

    //Model Class
    private class UserInfo {

        String address_id;
        String strName;
        String strAddress;
        String strContact;
        String strPincode;
        String strCount;
        String strCB_Points;
        String strDynamic_price_msg;
        String strSwipeCardMsg;
        String address1;
        String address2;
        String address3;
        String address4;
        String address5;
        String area;
        String city;
        String state;
        String user_lat;
        String user_long;

      /*  public UserInfo(String address_id, String strName, String strAddress, String strContact, String strPincode, String strCount, String strCB_Points, String strDynamic_price_msg, String strSwipeCardMsg) {
            this.address_id = address_id;
            this.strName = strName;
            this.strAddress = strAddress;
            this.strContact = strContact;
            this.strPincode = strPincode;
            this.strCount = strCount;
            this.strCB_Points = strCB_Points;
            this.strDynamic_price_msg = strDynamic_price_msg;
            this.strSwipeCardMsg = strSwipeCardMsg;
        }*/

        UserInfo(String address_id, String name, String address, String contact,
                 String strPincode, String strCount, String strCB_Points,
                 String strDynamic_price_msg, String strSwipeCardMsg,String address1,String address2,String address3,String address4,String address5,String area,String city,String state, String user_lat, String user_long) {
            this.address_id = address_id;
            this.strName = name;
            this.strAddress = address;
            this.strContact = contact;
            this.strPincode = strPincode;
            this.strCount = strCount;
            this.strCB_Points = strCB_Points;
            this.strDynamic_price_msg = strDynamic_price_msg;
            this.strSwipeCardMsg = strSwipeCardMsg;
            this.address1 = address1;
            this.address2 = address2;
            this.address3 = address3;
            this.address4 = address4;
            this.address5 = address5;
            this.area = area;
            this.city = city;
            this.state = state;
            this.user_lat = user_lat;
            this.user_long = user_long;
        }

        public String getAddress_id() {
            return address_id;
        }

        public String getStrName() {
            return strName;
        }

        public String getStrAddress() {
            return strAddress;
        }

        public String getStrContact() {
            return strContact;
        }

        public String getStrPincode() {
            return strPincode;
        }

        public String getStrCount() {
            return strCount;
        }

        public String getStrCB_Points() {
            return strCB_Points;
        }

        public String getStrDynamic_price_msg() {
            return strDynamic_price_msg;
        }

        public String getStrSwipeCardMsg() {
            return strSwipeCardMsg;
        }

        public String getAddress1() {
            return address1;
        }

        public String getAddress2() {
            return address2;
        }

        public String getAddress3() {
            return address3;
        }

        public String getAddress4() {
            return address4;
        }

        public String getAddress5() {
            return address5;
        }

        public String getArea() {
            return area;
        }

        public String getCity() {
            return city;
        }

        public String getState() {
            return state;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public void setStrName(String strName) {
            this.strName = strName;
        }

        public void setStrAddress(String strAddress) {
            this.strAddress = strAddress;
        }

        public void setStrContact(String strContact) {
            this.strContact = strContact;
        }

        public void setStrPincode(String strPincode) {
            this.strPincode = strPincode;
        }

        public void setStrCount(String strCount) {
            this.strCount = strCount;
        }

        public void setStrCB_Points(String strCB_Points) {
            this.strCB_Points = strCB_Points;
        }

        public void setStrDynamic_price_msg(String strDynamic_price_msg) {
            this.strDynamic_price_msg = strDynamic_price_msg;
        }

        public void setStrSwipeCardMsg(String strSwipeCardMsg) {
            this.strSwipeCardMsg = strSwipeCardMsg;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public void setAddress3(String address3) {
            this.address3 = address3;
        }

        public void setAddress4(String address4) {
            this.address4 = address4;
        }

        public void setAddress5(String address5) {
            this.address5 = address5;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getUser_lat() {
            return user_lat;
        }

        public void setUser_lat(String user_lat) {
            this.user_lat = user_lat;
        }

        public String getUser_long() {
            return user_long;
        }

        public void setUser_long(String user_long) {
            this.user_long = user_long;
        }



        @Override
        public String toString() {
            return "UserInfo{" +
                    "address_id='" + address_id + '\'' +
                    ", strName='" + strName + '\'' +
                    ", strAddress='" + strAddress + '\'' +
                    ", strContact='" + strContact + '\'' +
                    ", strPincode='" + strPincode + '\'' +
                    ", strCount='" + strCount + '\'' +
                    ", strCB_Points='" + strCB_Points + '\'' +
                    ", strDynamic_price_msg='" + strDynamic_price_msg + '\'' +
                    ", strSwipeCardMsg='" + strSwipeCardMsg + '\'' +
                    ", address1='" + address1 + '\'' +
                    ", address2='" + address2 + '\'' +
                    ", address3='" + address3 + '\'' +
                    ", address4='" + address4 + '\'' +
                    ", address5='" + address5 + '\'' +
                    ", area='" + area + '\'' +
                    ", city='" + city + '\'' +
                    ", state='" + state + '\'' +
                    ", user_lat='" + user_lat + '\'' +
                    ", user_long='" + user_long + '\'' +
                    '}';
        }
    }

    private void fetch_cart_item() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            mItems.clear();

            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);


            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetMyCartItems, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.isNull("posts")) {
                        } else {
                            JSONArray mainClassificationJsonArray = response.getJSONArray("posts");
                            final_price = response.getString("Final_Price");
                            for (int i = 0; i < mainClassificationJsonArray.length(); i++) {
                                JSONObject jsonCart = mainClassificationJsonArray.getJSONObject(i);

                                Normal_Cart_Item c = new Normal_Cart_Item(jsonCart.getString("normal_cart_shop_id"),
                                        jsonCart.getString("product_id"), jsonCart.getString("product_name"),
                                        jsonCart.getString("normal_shop_name"), jsonCart.getDouble("product_price"),
                                        jsonCart.getString("product_qty"), jsonCart.getString("product_image"),
                                        jsonCart.getString("type"), jsonCart.getString("available_product_quantity"),
                                        jsonCart.getString("hindi_name"), jsonCart.getString("p_name"));
                                dialog_cardAdapter.add(c);
                            }
                            viewcart_items_recycler_view.setAdapter(dialog_cardAdapter);
                            double finalValue = Math.round(Double.parseDouble(final_price) * 100.0) / 100.0;
                            tvDialogTitle.setText("â‚¹ " + finalValue);
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    public static class Normal_Cart_Item {
        final String str_normal_cart_shopId;
        final String str_product_Id;
        final String str_productName;
        final String str_shopName;
        final String str_productImg;
        final String strtype;
        final String str_AvailableProductQuantity;
        final String strHindi_Name;
        final double str_price;
        final String p_Name;
        String str_qty;

        Normal_Cart_Item(String normal_cart_shopId, String productId, String productName, String shopName,
                         double price, String qty, String productImg, String type,
                         String AvailableProductQuantity, String hindiname, String p_Name) {
            this.str_normal_cart_shopId = normal_cart_shopId;
            this.str_product_Id = productId;
            this.str_productName = productName;
            this.str_shopName = shopName;
            this.str_price = price;
            this.str_qty = qty;
            this.str_productImg = productImg;
            this.strtype = type;
            this.str_AvailableProductQuantity = AvailableProductQuantity;
            this.strHindi_Name = hindiname;
            this.p_Name = p_Name;
        }

        public String getP_Name() {
            return p_Name;
        }

        public String getStrHindi_Name() {
            return strHindi_Name;
        }

        public String getStr_normal_cart_shopId() {
            return str_normal_cart_shopId;
        }

        public String getStr_product_Id() {
            return str_product_Id;
        }

        public String getStr_productName() {
            return str_productName;
        }

        public String getStr_shopName() {
            return str_shopName;
        }

        public double getStr_price() {
            return str_price;
        }

        public String getStr_qty() {
            return str_qty;
        }

        public void setStr_qty(String str_qty) {
            this.str_qty = str_qty;
        }

        public String getStr_productImg() {
            return str_productImg;
        }

        public String getStrtype() {
            return strtype;
        }

        public String getStr_AvailableProductQuantity() {
            return str_AvailableProductQuantity;
        }
    }

    private class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

        double totalAmt;
        DialogPlus dialog;

        public CardAdapter() {
            super();
        }

        @NonNull
        @Override
        public CardAdapter.ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_for_add_to_cart, viewGroup, false);
            return new CardAdapter.NormalProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull CardAdapter.ViewHolder viewHolder, int position) {
            final Normal_Cart_Item cart_item = mItems.get(position);

            if (cart_item != null) {
                final CardAdapter.NormalProductViewHolder holder1 = (CardAdapter.NormalProductViewHolder) viewHolder;
                String strType = cart_item.getStrtype();

                if (strType.equals("combo")) {
                    //TODO here show combo offer product images setup to gilde
                   /* Glide.with(Order_Details.this).load("http://s.apneareamein.com/seller/assets/" + cart_item.getStr_productImg())
                            .thumbnail(Glide.with(Order_Details.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/
                    Picasso.with(getActivity()).load("https://s.apneareamein.com/seller/assets/" + cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.removeItem.setVisibility(View.GONE);
                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                } else {
                    //TODO here show product images setup to gilde
                   /* Glide.with(Order_Details.this).load(cart_item.getStr_productImg())
                            .thumbnail(Glide.with(Order_Details.this).load(R.drawable.loading))
                            .error(R.drawable.ic_app_transparent)
                            .fitCenter()
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder1.productImg);*/

                    Picasso.with(getActivity()).load(cart_item.getStr_productImg())
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.ic_app_transparent)
                            .into(holder1.productImg);

                    holder1.removeItem.setVisibility(View.GONE);
                    holder1.tvMovetoWishlist.setVisibility(View.GONE);
                }

                holder1.tvProductName.setText(cart_item.getStr_productName());
                holder1.tvNormalSellerName.setText("By: " + cart_item.getStr_shopName());
                holder1.tvNormalPrice.setText("" + cart_item.getStr_price());
                holder1.etQuantity.setText(String.valueOf(cart_item.getStr_qty()));

                if (cart_item.getStrHindi_Name().equals("")) {
                    holder1.tvProductHindiName.setVisibility(View.GONE);
                } else {
                    holder1.tvProductHindiName.setText(" ( " + cart_item.getStrHindi_Name() + " )");
                }


                try {
                    int qty = Integer.parseInt(cart_item.getStr_qty());
                    double productPrice = cart_item.getStr_price();
                    if (qty > 1) {
                        double total_price = qty * productPrice;
                        double finalValue = Math.round(total_price * 100.0) / 100.0;
                        holder1.tvProductTotal.setText("" + finalValue);
                    } else {
                        holder1.tvProductTotal.setText("" + cart_item.getStr_price());
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();

        }

        public void add(Normal_Cart_Item cart_item) {
            mItems.add(cart_item);
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(View itemView) {
                super(itemView);
            }
        }

        class NormalProductViewHolder extends CardAdapter.ViewHolder {

            final TextView tvProductName;
            final TextView tvNormalSellerName;
            final TextView tvNormalPrice;
            final TextView tvProductTotal;
            final TextView tvProductHindiName;
            final TextView tvMovetoWishlist;
            final ImageView removeItem;
            final ImageView productImg;
            final EditText etQuantity;

            NormalProductViewHolder(View v) {
                super(v);
                tvProductName = v.findViewById(R.id.txtProductName);
                tvProductHindiName = v.findViewById(R.id.txtProductHindiName);
                tvNormalSellerName = v.findViewById(R.id.txtNormalSellerName);
                tvNormalPrice = v.findViewById(R.id.txtNormalPrice);
                removeItem = v.findViewById(R.id.removeItem);
                etQuantity = v.findViewById(R.id.editQuantity);
                productImg = v.findViewById(R.id.productImage);
                tvProductTotal = v.findViewById(R.id.txtProductTotal);
                tvMovetoWishlist = v.findViewById(R.id.txtMovetoWishlist);
                removeItem.setTag(this);
                tvMovetoWishlist.setTag(this);
            }
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private final Context _context;
        private final List<String> _listDataHeader; // header titles
        private final LayoutInflater inflater;
        private RadioButton selected = null;
        final Map<String, List<String>> _listDataChild;

        ExpandableListAdapter(Context context, List<String> listDataHeader,
                              Map<String, List<String>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
            inflater = LayoutInflater.from(context);


        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final String childText = (String) getChild(groupPosition, childPosition);
            final ExpandableListAdapter.Holder holder;


            if (convertView == null) {
                holder = new ExpandableListAdapter.Holder();
                convertView = inflater.inflate(R.layout.delivery_options_list_item, null);
                holder.radioButton = convertView.findViewById(R.id.lblListItem);
                convertView.setTag(holder);
            } else {
                holder = (ExpandableListAdapter.Holder) convertView.getTag();
            }

            holder.radioButton.setText(childText);

            holder.radioButton.setChecked(false);

            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectedDeliveryMethod = childText;

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + " selected " + SelectedDeliveryMethod, getActivity());*/

                    if (childText.contains("Schedule Delivery")) {
                        if (priceLinearLayout.getVisibility() == View.VISIBLE) {
                            priceLinearLayout.setVisibility(View.GONE);
                            list_check_box.setVisibility(View.GONE);
                        }
                        if (Connectivity.isConnected(getActivity())) {
                            getNewScheduledDates("date_slot", "", "");
                        } else {
                            GateWay gateWay = new GateWay(getActivity());
                            gateWay.displaySnackBar(coordinatorLayout);
                        }
                    } else {
                        date_spinner.setVisibility(View.GONE);
                        time_spinner.setVisibility(View.GONE);
                        priceLinearLayout.setVisibility(View.GONE);
                        list_check_box.setVisibility(View.GONE);

                        getFinalPriceFromServer(childText);
                    }

                    if (selected != null) {
                        selected.setChecked(false);
                    }
                    holder.radioButton.setChecked(true);
                    selected = holder.radioButton;
                }
            });

            return convertView;
        }

        class Holder {
            RadioButton radioButton;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();

        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            final String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.delivery_options_list_group, null);
            }

            TextView lblListHeader = convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    private void getNewScheduledDates(final String tag, String date, final String selectedDeliveryMethod) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            mainScheduledDateValues = new ArrayList<>();
            //mainScheduledDateValues.add(0, "Please Select Date");
            mainScheduledTimeValues = new ArrayList<>();
            //mainScheduledTimeValues.add(0, "Please Select Time");

            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("tag", tag);
                params.put("date", date);
                Log.e("new Schedule Date Slot",""+params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlNewScheduleDates, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("OrderDetailsTimeslot", "onResponse: " + response.toString());

                    try {
                        try {
                            if (!response.isNull("posts1")) {
                                time_spinner.setVisibility(View.VISIBLE);

                                JSONArray jsonArray = response.getJSONArray("posts1");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledTimeValues.add(jsonObject.getString("time"));
                                }
                                setValuesToTimeSpinner(selectedDeliveryMethod);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (!response.isNull("posts2")) {
                                date_spinner.setVisibility(View.VISIBLE);

                                JSONArray jsonArray = response.getJSONArray("posts2");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledDateValues.add(jsonObject.getString("date"));
                                }
                                setValuesToDateSpinner();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void getNewScheduledDatesForCoupon(String tag) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            mainScheduledDateValues = new ArrayList<>();
            mainScheduledDateValues.add(0, "Please Select Date");
            mainScheduledTimeValues = new ArrayList<>();
            mainScheduledTimeValues.add(0, "Please Select Time");

            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("couponCode", coupon_code);
                params.put("tag", tag);
                Log.e("DateforCouponcode", "params: " + params.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlgetNewScheduledDatesForCoupon, params, new Response.Listener<JSONObject>() {
                // JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL+"getNewScheduledDatesForCoupon.php", params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("OrderDetails", "onResponse: " + response.toString());

                    try {
                        try {
                            if (!response.isNull("posts1")) {
                                time_spinner.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = response.getJSONArray("posts1");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledTimeValues.add(jsonObject.getString("time"));
                                }
                                setValuesToTimeSpinnerForCoupon();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (!response.isNull("posts2")) {
                                date_spinner.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = response.getJSONArray("posts2");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    mainScheduledDateValues.add(jsonObject.getString("date"));
                                }
                                setValuesToDateSpinnerForCoupon();
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void setValuesToDateSpinnerForCoupon() {
        try {
            if (mainScheduledDateValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledDateValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                coupondate_spinner.setAdapter(gameKindArray);

                coupondate_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        scheduleSelectedDate = (String) coupondate_spinner.getSelectedItem();

                        if (!scheduleSelectedDate.equals("Please Select Date")) {
                            getNewScheduledDatesForCoupon("time_slot");
                        } else {
                            time_spinner.setVisibility(View.GONE);
                            priceLinearLayout.setVisibility(View.GONE);
                            list_check_box.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValuesToTimeSpinnerForCoupon() {
        try {
            if (mainScheduledTimeValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledTimeValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                coupontime_spinner.setAdapter(gameKindArray);

                coupontime_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        scheduleSelectedTime = (String) coupontime_spinner.getSelectedItem();
                        if (!scheduleSelectedTime.equals("Please Select Time")) {
                            priceLinearLayout.setVisibility(View.VISIBLE);
                            list_check_box.setVisibility(View.VISIBLE);
                            tvPrice.setText("\u20B9 " + strInitialTotalAmount);
                            tvShippingCharge.setText("\u20B9 " + strShippingAmount);
                            tvDiscount.setText("\u20B9 " + strSavingAmount);
                            tvTotal.setText("\u20B9 " + strFinalAmount);

                            applyCouponLayout.setVisibility(View.GONE);
                            deliveryfeesLayout.setVisibility(View.GONE);
                            // paymentType="Schedule Delivery";
                        } else {
                            priceLinearLayout.setVisibility(View.GONE);
                            list_check_box.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValuesToDateSpinner() {
        try {
            if (mainScheduledDateValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledDateValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                date_spinner.setAdapter(gameKindArray);

                date_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        scheduleSelectedDate = (String) date_spinner.getSelectedItem();

                        if (!scheduleSelectedDate.equals("Please Select Date")) {
                            getNewScheduledDates("time_slot", scheduleSelectedDate, SelectedDeliveryMethod);
                        } else {
                            time_spinner.setVisibility(View.GONE);
                            priceLinearLayout.setVisibility(View.GONE);
                            list_check_box.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValuesToTimeSpinner(final String selectedDeliveryMethod) {
        try {
            if (mainScheduledTimeValues.size() > 0) {
                ArrayAdapter<String> gameKindArray = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mainScheduledTimeValues);
                gameKindArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                time_spinner.setAdapter(gameKindArray);

                time_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int positionF, long id) {
                        scheduleSelectedTime = (String) time_spinner.getSelectedItem();
                        if (!scheduleSelectedTime.equals("Please Select Time")) {

                            getFinalPriceFromServer(selectedDeliveryMethod);
                        } else {
                            priceLinearLayout.setVisibility(View.GONE);
                            list_check_box.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFinalPriceFromServer(final String deliveryMethod) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            //simpleProgressBar.setVisibility(View.VISIBLE);
            progressDoalog.show();
            JSONObject params = new JSONObject();
            try {
                params.put("deliveryMethod", deliveryMethod);
                params.put("scheduleSelectedTime", time_spinner.getSelectedItem().toString());
                params.put("coupon_code", coupon_code);
                params.put("email", strEmail);
                params.put("contact", strContact);
                params.put("city", v_city);
                Log.e("param_getFinalPrice:",""+params.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
          String targetUrl = ApplicationUrlAndConstants.urlGetPriceDetails+"?deliveryMethod="+ URLEncoder.encode(deliveryMethod)+"&coupon_code="+coupon_code+"&email="+strEmail+"&contact="+strContact+"&scheduleSelectedTime="+scheduleSelectedTime+"&city="+URLEncoder.encode(v_city);
           JsonArrayRequest request = new JsonArrayRequest(targetUrl, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        progressDoalog.dismiss();
                        //JSONArray jsonObject = response.getJSONArray("");
                        Log.e("OrderDetails", "getDeliveryOptions response:" + response.toString());
                        //JSONObject value = response.getJSONObject(0);

                        // JSONArray jsonArray = response.getJSONArray("");
                      /*  if (tvCouponDiscount.getVisibility() == View.VISIBLE) {
                            Animation anim = new AlphaAnimation(0.0f, 1.0f);
                            anim.setDuration(100);
                            anim.setStartOffset(20);
                            anim.setRepeatMode(Animation.REVERSE);
                            anim.setRepeatCount(Animation.INFINITE);
                            tvCouponDiscount.startAnimation(anim);
                        }*/
                        JSONObject value = response.getJSONObject(0);

                        JSONArray seller = value.getJSONArray("posts");
                        for (int i = 0; i < seller.length(); i++) {
                            JSONObject jsonSellerObject = (JSONObject) seller.get(i);

                            strInitialTotalAmount = jsonSellerObject.getString("initialTotalAmount");
                            strShippingAmount = jsonSellerObject.getString("shippingAmount");
                            strSavingAmount = jsonSellerObject.getString("savingAmount");
                            strScratchCardAmount= jsonSellerObject.getString("scratch_card_amount");
                            strFinalAmount = jsonSellerObject.getString("finalAmount");
                            strResponse = jsonSellerObject.getString("response");
                            String remainCB_Points = jsonSellerObject.getString("user_cash_back_points");
                            Order_Details_StepperActivity.tvCBPoints.setText(remainCB_Points);
                            double_final_amt = Double.parseDouble(strFinalAmount);
                            if (jsonSellerObject.isNull("coupon_min_amt")) {
                            } else {
                                double_coupon_min_amt = Double.parseDouble(jsonSellerObject.getString("coupon_min_amt"));
                            }
                            if (jsonSellerObject.isNull("couponSavingAmt")) {
                            } else {
                                strCouponSavingAmt = jsonSellerObject.getString("couponSavingAmt");
                            }
                            if (jsonSellerObject.isNull("message")) {
                            } else {
                                strNotifyMessage = jsonSellerObject.getString("message");
                            }
                        }
                        if (strInitialTotalAmount.equals("0") || strInitialTotalAmount.equals("null")) {
                            priceLinearLayout.setVisibility(View.GONE);
                            list_check_box.setVisibility(View.GONE);
                            if (!(getActivity()).isFinishing()) {
                                errorAlertDialog();
                            }

                        } else {
                            priceLinearLayout.setVisibility(View.VISIBLE);
                            list_check_box.setVisibility(View.VISIBLE);
                            tvPrice.setText("\u20B9 " + strInitialTotalAmount);
                            tvShippingCharge.setText("\u20B9 " + strShippingAmount);
                            tvDiscount.setText("\u20B9 " + strSavingAmount);
                            tvScratchCardAmount.setText("\u20B9 " + strScratchCardAmount);
                            tvTotal.setText("\u20B9 " + strFinalAmount);

                            /*try {
                                Freecharge_checksum =    generateChecksum("","");
                                Log.e("checksum11",""+Freecharge_checksum);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                            //getFreeChargePaymentGateway();
                            //getWebviewFreeCharge();

                            int couponSavingAmount = Integer.parseInt(strCouponSavingAmt);
                            if (couponSavingAmount != 0) {
                                if (strResponse.equals("true")) {
                                    tvDiscountAmt.setVisibility(View.VISIBLE);
                                    tvCouponDiscount.clearAnimation();
                                    tvCouponDiscount.setVisibility(View.GONE);
                                    tvDiscountAmt.setText("\u20B9 " + strCouponSavingAmt);
                                    //  Toast.makeText(getActivity(), strNotifyMessage, Toast.LENGTH_LONG).show();
                                }
                            } else {

                                if (strResponse.equals("null")) {
                                    tvDiscountAmt.setVisibility(View.GONE);
                                    tvCouponDiscount.setVisibility(View.VISIBLE);
                                } else {
                                    coupon_code = "";
                                    tvDiscountAmt.setVisibility(View.GONE);
                                    tvCouponDiscount.setVisibility(View.VISIBLE);
                                    // Toast.makeText(getActivity(), strNotifyMessage, Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDoalog.dismiss();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //simpleProgressBar.setVisibility(View.INVISIBLE);
                    progressDoalog.dismiss();

                    ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            //AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void errorAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Oops! Seems your order got stuck due to some Technical Error.Please call/sms on 7219090909 for confirmation.Thanks - PICODEL.com")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DBHelper dbHelper = new DBHelper(getActivity());
                        dbHelper.deleteOnlyCartTable();
                        deleteCartItemsAtProblematicCondition();
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteCartItemsAtProblematicCondition() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strName);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlProblematicDeleteCondition, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    //Place Order
    //send lat and long
    private void finalOrder(String deliveryMethod, ArrayList<String> optionArray) { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            progressDialog2.show();
            btnPlaceOrder.setVisibility(View.GONE);

            JSONObject params = new JSONObject();
            try {
                String contact = tvUserContact.getText().toString();
                String userName = tvUserName.getText().toString();
                String specialRemark = etSpecialRemark.getText().toString();

                params.put("name", userName);
                params.put("email", strEmail);
                params.put("contactNo", strContact);
                params.put("contact", contact);
                params.put("address_id", arr_user_info.get(position).address_id);
                Log.d("address_id",""+arr_user_info.get(position).address_id+" "+arr_user_info.get(position).getStrAddress());
                params.put("address", arr_user_info.get(position).getStrAddress());
                params.put("address1", arr_user_info.get(position).getAddress1());//arr_user_info.get(position).getAddress1());
                params.put("address2", arr_user_info.get(position).getAddress2());
                params.put("address3", arr_user_info.get(position).getAddress3());
                params.put("address4", arr_user_info.get(position).getAddress4());
                params.put("address5", arr_user_info.get(position).getAddress5());
                params.put("area", arr_user_info.get(position).getArea());
                params.put("city", arr_user_info.get(position).getCity());
                params.put("v_city", v_city);
                params.put("state", arr_user_info.get(position).getState());
                params.put("totalAmt", strFinalAmount);
                params.put("paymentType", paymentType);
                params.put("deliveryType", deliveryMethod);
                params.put("time", scheduleSelectedTime);
                params.put("pDate", scheduleSelectedDate);
                params.put("coupon_code", coupon_code);
                params.put("coupon_save_amt", strCouponSavingAmt);
                params.put("special_remark", specialRemark);
                params.put("scratchCardAmount",strScratchCardAmount);
                params.put("payment_option",optionArray.toString());
                params.put("order_latitude",c_latitude);
                params.put("order_longitude",c_longitude);
                params.put("latitude", arr_user_info.get(position).getUser_lat());
                params.put("longitude", arr_user_info.get(position).getUser_long());
                params.put("user_terms",user_terms);
                SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                nearbyshop_id = prefs.getString("nearbyshopid", "");
                params.put("nearbyshopid",nearbyshop_id);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("PlaceFinalOrderParam", ""+params);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCashOnDelivery, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("OrderPlaceFinalResponse", ""+response);
                    DBHelper dbHelper = new DBHelper(getActivity());
                    dbHelper.deleteOnlyCartTable();
                    //btnPlaceOrder.setVisibility(View.VISIBLE);
                    try {
                        if (response.isNull("posts")) {

                        } else {
                            JSONArray array = response.getJSONArray("posts");
                            JSONObject jsonObject = array.getJSONObject(0);
                            order_id = jsonObject.getString("orderId");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                    if(radio_online.isChecked()){
                        sendNotification();
                        Intent intent = new Intent(getActivity(), PaymentActivityFreecharge.class);
                        intent.putExtra("strFinalAmount", strFinalAmount);
                        intent.putExtra("order_id", order_id);//categoryModelArrayList.get(position).getMaincategory()//Eggs
                        //intent.putExtra("selectedName", "Non_Veg");
                        startActivity(intent);
                        getActivity().finish();
                    }else {
                        sendNotification();
                    }

                    // orderPlacedAlertDialog();
                    //gateWay.progressDialogStop();
                    progressDialog2.dismiss();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    //gateWay.progressDialogStop();
                    progressDialog2.dismiss();
                   ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtChangeAddress:
                if (Connectivity.isConnected(getActivity())) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + " clicked on change address button", getActivity());*/

                    //Intent intent = new Intent(getActivity(), BaseActivity.class);
                    Intent intent = new Intent(getActivity(), DeliveryAddress.class);
                    String tag_address = "address";
                    intent.putExtra("tag", tag_address);
                    intent.putExtra("finalPrice", finalPrice);
                    //startActivity(intent);
                    startActivityForResult(intent, 1);
                } else {
                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.displaySnackBar(coordinatorLayout);
                }
                break;

            case R.id.txtItems:
                if (Connectivity.isConnected(getActivity())) {

                    /*UserTracking UT3 = new UserTracking(UserTracking.context);
                    UT3.user_tracking(class_name + ": clicked on view cart item button", getActivity());*/

                    dialog = DialogPlus.newDialog(getActivity())
                            .setContentHolder(new ViewHolder(R.layout.dialog_viewcart_items))
                            .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                            .setGravity(Gravity.BOTTOM)
                            .create();

                    TextView tvViewCartItemsTitle = (TextView) dialog.findViewById(R.id.txtTitle);
                    tvDialogTitle = (TextView) dialog.findViewById(R.id.txtDialogTotal);

                    if (arr_user_info.get(0).getStrCount().equals(1)) {
                        tvViewCartItemsTitle.setText(cart_count = arr_user_info.get(0).getStrCount() + " " + "Item");
                    } else {
                        tvViewCartItemsTitle.setText(cart_count = arr_user_info.get(0).getStrCount() + " " + "Items");
                    }

                    ImageView closedDialog1 = (ImageView) dialog.findViewById(R.id.imgClosedDialog);
                    closedDialog1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    viewcart_items_recycler_view = (RecyclerView) dialog.findViewById(R.id.viewcart_items_recycler_view);
                    viewcart_items_recycler_view.setHasFixedSize(true);
                    LinearLayoutManager mLayoutManagerForviewcart_items = new LinearLayoutManager(getActivity());
                    viewcart_items_recycler_view.setLayoutManager(mLayoutManagerForviewcart_items);
                    dialog_cardAdapter = new CardAdapter();
                    dialog.show();
                    fetch_cart_item();
                } else {
                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.displaySnackBar(coordinatorLayout);
                }
                break;

         /*   case R.id.txtCouponDiscount:
                if (Connectivity.isConnected(getActivity())) {
                    UserTracking UT2 = new UserTracking(UserTracking.context);
                    UT2.user_tracking(class_name + ": clicked on apply coupon button", getActivity());

                    dialog = DialogPlus.newDialog(getActivity())
                            .setContentHolder(new ViewHolder(R.layout.dialog_coupons))
                            .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                            .setGravity(Gravity.BOTTOM)
                            .create();

                    tvCouponMessage = (TextView) dialog.findViewById(R.id.txtCouponMessage);
                    ImageView closedDialog = (ImageView) dialog.findViewById(R.id.imgClosedDialog);
                    closedDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    coupon_recycler_view = (RecyclerView) dialog.findViewById(R.id.coupon_recycler_view);
                    coupon_recycler_view.setHasFixedSize(true);
                    LinearLayoutManager mLayoutManagerForCoupons = new LinearLayoutManager(getActivity());
                    coupon_recycler_view.setLayoutManager(mLayoutManagerForCoupons);
                    couponsAdapter = new CouponsAdapter();
                    coupon_recycler_view.setAdapter(couponsAdapter);
                    getCoupons();
                    dialog.show();
                } else {
                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.displaySnackBar(coordinatorLayout);
                }
                break;*/

            case R.id.btnPlaceOrder:
                if (Connectivity.isConnected(getActivity())) {

                    /*UserTracking UT1 = new UserTracking(UserTracking.context);
                    UT1.user_tracking(class_name + " clicked on place order button", getActivity());*/
                }
                StopOrder();
                break;
            case R.id.btnAddCouponCode:

                coupon_code = editCouponCode.getText().toString();
                Log.d("OrderDetails", ":coupon_code " + coupon_code);
                couponLayout.setVisibility(View.GONE);
                delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                if (!coupon_code.equalsIgnoreCase("")) {
                    sendCouponCode();
                }
                break;
            case R.id. btnSkipCouponCode:
                couponLayout.setVisibility(View.GONE);
                delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);

                break;
            case R.id.btnAddTRN:
                String paid_amt= edittrnAmt.getText().toString();
                String TRN_No= editTRN.getText().toString();
                if(!TRN_No.equalsIgnoreCase("")&& !paid_amt.equalsIgnoreCase("")){

                    sendTRNAmt(paid_amt,TRN_No);
                }else{
                    Toast.makeText(getActivity(), "Please enter TRN No and Paid Amount", Toast.LENGTH_SHORT).show();
                }
                getNewScheduledDatesForCoupon("date_slot");
                break;
            case R.id.txtpaymnentmsg:
                datenTimeLayout.setVisibility(View.VISIBLE);
                TRNLayout.setVisibility(View.VISIBLE);
                Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("https://www.ftcash.com/9552544539"));
                startActivity(browserIntent);
                break;

        }
    }

    /*private void getCouponCodePayment() {
        if (Connectivity.isConnected(getActivity())) {

            listDataHeader = new ArrayList<>();

            final GateWay gateWay = new GateWay(getActivity());
             gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("couponCode", coupon_code);
                // params.put("my_cart_count", cart_count);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("OrderDetails", ":params " + params.toString());

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, URL + "couponKeyText.php", params, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d("OrderDetails", ":response " + response.toString());

                    try {
                        JSONObject value = response.getJSONObject(0);
                        JSONObject value1 = response.getJSONObject(1);

                        JSONArray mainCat = value.getJSONArray("parent");
                        for (int i = 0; i < mainCat.length(); i++) {
                            JSONObject jsonMainCatObject = mainCat.getJSONObject(i);
                            listDataHeader.add(jsonMainCatObject.getString("Key")); //add to arraylist
                        }

                        JSONArray mainProductCat = value1.getJSONArray("child");

                        for (int j = 0; j < mainProductCat.length(); j++) {
                            JSONObject jsonProductCatObject = mainProductCat.getJSONObject(j);
                            JSONArray mainProductCatArray = jsonProductCatObject.getJSONArray(listDataHeader.get(j));

                            childArrayList = new ArrayList<>();

                            for (int g = 0; g < mainProductCatArray.length(); g++) {

                                JSONObject jsonProductCatObjectArray = mainProductCatArray.getJSONObject(g);

                                childArrayList.add(jsonProductCatObjectArray.getString("child_text")); //add to arraylist
                            }
                            Collections.sort(childArrayList);

                            keyValueCategoryMap.put(listDataHeader.get(j), childArrayList);

                        }
                        gateWay.progressDialogStop();
                        preparationForCollection();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Log.d("getCouponCodePayment", ":error " + error.toString());

                      gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }*/

    private void sendTRNAmt(String paid_amt, String TRN_No) {

        final GateWay gateWay = new GateWay(getActivity());
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            //  gateWay.progressDialogStart();
            JSONObject params = new JSONObject();
            try {
                params.put("name", strName);
                params.put("contact", strContact);
                params.put("email", strEmail);
                params.put("transId", TRN_No);
                params.put("amt", paid_amt);
                Log.d("OrderDetails", ":params " + params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlverifyTRN, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(getActivity(), "Order will be confirmed after verification of Advance Payment", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //  gateWay.progressDialogStop();
                    Log.d("OrderDetails", "sendCouponCode:error " + error.toString());
                    error.printStackTrace();


                    ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void sendCouponCode() {

        final GateWay gateWay = new GateWay(getActivity());
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);
            JSONObject params = new JSONObject();
            try {
                params.put("couponCode", coupon_code);
                params.put("totalPrice", finalPrice);
                Log.d("OrderDetails", ":params " + params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlsendcouponCode, params, new Response.Listener<JSONObject>() {
                //  JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL+"sendcouponCode.php", params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    Log.d("OrderDetails", "response " + response.toString());


                    try {
                        if (response.isNull("posts")) {

                        } else {
                            JSONArray array = response.getJSONArray("posts");
                            JSONObject jsonObject = array.getJSONObject(0);
                            String message = jsonObject.getString("message");
                            if(message.equalsIgnoreCase("Success")) {
                                strFinalAmount = jsonObject.getString("total");
                                strSavingAmount = jsonObject.getString("discount");
                                strInitialTotalAmount = jsonObject.getString("Price");
                                couponCode = jsonObject.getString("couponCode");

                                if (couponCode.equalsIgnoreCase("AAMBEN1500")) {
                                    txtpaymnentmsg.setVisibility(View.VISIBLE);
                                } else {
                                    datenTimeLayout.setVisibility(View.VISIBLE);
                                    getNewScheduledDatesForCoupon("date_slot");
                                }
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } catch (Exception e) {
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    Log.d("OrderDetails", "sendCouponCode:error " + error.toString());
                    error.printStackTrace();


                   ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void StopOrder() {
        final GateWay gateWay = new GateWay(getActivity());
        if (Connectivity.isConnected(getActivity())) {
            try {
                //ArrayList<String> optArray = payment_opt_checkbox();
                ArrayList<String> optArray = payment_types;
                Log.e("optArray:",""+optArray.toString());
                if(optArray.isEmpty()) {
                    Toast.makeText(getActivity(),"Please select Payment Options",Toast.LENGTH_LONG).show();
                }else {
                    String order_lat = "0.0";
                    order_lat = arr_user_info.get(position).getUser_lat();
                    String order_long = "0.0";
                    order_long =  arr_user_info.get(position).getUser_long();
                    if (coupon_code.equalsIgnoreCase("")) {

                         if(c_latitude.length()<4 && c_longitude.length()<4 && order_lat.length()<4 &&order_long.length()<4){
                             LocationAlertDialog();
                         }else {
                             finalOrder(SelectedDeliveryMethod,optArray);
                             /*Toast.makeText(getActivity(),"Yes Get Location",Toast.LENGTH_LONG).show();
                             Log.e("Location_Order_P1",""+c_latitude+""+c_longitude+""+c_latitude.length());
                             Log.e("Location_Order_P1",""+order_lat+""+order_long);*/

                         }
                    } else {
                        if(c_latitude.length()<4 && c_longitude.length()< 4 && order_lat.length() <4 && order_long.length()<4 ){
                            LocationAlertDialog();
                        }else {
                            finalOrder("Schedule Delivery",optArray);
                            /*Log.e("Location_Order_P",""+c_latitude+""+c_longitude);
                            Log.e("Location_Order_P",""+order_lat+""+order_long);
                            Toast.makeText(getActivity(),"Yes Get Location",Toast.LENGTH_LONG).show();*/
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }
      /*  if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", strContact);
                params.put("email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlStopOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            if (Connectivity.isConnected(getActivity())) {
                                try {
                                    if(coupon_code.equalsIgnoreCase("")){
                                        finalOrder(SelectedDeliveryMethod);
                                    }else{
                                        finalOrder("Schedule Delivery");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                gateWay.displaySnackBar(coordinatorLayout);
                            }
                        } else {

                            JSONArray array = response.getJSONArray("posts");
                            JSONObject jsonObject = array.getJSONObject(0);

                            String reason = jsonObject.getString("reason");
                            String image = jsonObject.getString("image");

                            dialog = DialogPlus.newDialog(getActivity())
                                    .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_stop_order))
                                    .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                    .setGravity(Gravity.CENTER)
                                    .setCancelable(false)
                                    .setPadding(20, 20, 20, 20)
                                    .create();
                            dialog.show();
                            ImageView imgStopOrder = (ImageView) dialog.findViewById(R.id.imgCashBack);
                            RelativeLayout stop_orderLayout = (RelativeLayout) dialog.findViewById(R.id.stop_orderLayout);
                            TextView tvMsg = (TextView) dialog.findViewById(R.id.txtMessage);
                            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                            if (reason.equals("")) {
                                tvMsg.setVisibility(View.GONE);
                            } else {
                                tvMsg.setVisibility(View.VISIBLE);
                                tvMsg.setText(reason);
                            }
                            if (image.equals("")) {
                                stop_orderLayout.setVisibility(View.GONE);
                            } else {
                                stop_orderLayout.setVisibility(View.VISIBLE);

                                //TODO here out of stock GIF image setup to glide
                                Glide.with(getActivity())
                                        .load(image)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .error(R.drawable.ic_app_transparent)
                                        .into(imgStopOrder);
                            }

                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(coordinatorLayout);
        }*/
    }

    private class CouponsAdapter extends RecyclerView.Adapter<MainViewHolder> {

        final ArrayList<InnerMovie> innerMovies = new ArrayList<>();

        CouponsAdapter() {
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_coupons, parent, false);
            return new MainViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final MainViewHolder holder, int position) {
            InnerMovie innerMovie = innerMovies.get(position);

            holder.tvSellerName.setText("By:" + " " + innerMovie.getShop_name());
            holder.tvCouponCode.setText(innerMovie.getCoupon_code());
            holder.tvCouponInfo.setText(innerMovie.getOffer_amt() + innerMovie.getAmt_in() + " off on minimum amount of on " + "\u20B9 " + innerMovie.getMin_amt());
            holder.tvEndDate.setText(innerMovie.getCoupon_enddate());

            holder.tvCouponCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
                        //coupon_code = holder.tvCouponCode.getText().toString();
                        if (couponCode.equalsIgnoreCase(""))
                            getFinalPriceFromServer(SelectedDeliveryMethod);
                        dialog.dismiss();
                    } else {
                        GateWay gateWay = new GateWay(getActivity());
                        gateWay.displaySnackBar(coordinatorLayout);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return innerMovies.size();
        }

        public void add(InnerMovie innerMovie) {
            innerMovies.add(innerMovie);
        }

    }

    private class MainViewHolder extends RecyclerView.ViewHolder {

        final TextView tvSellerName;
        final TextView tvCouponCode;
        final TextView tvCouponInfo;
        final TextView tvEndDate;

        public MainViewHolder(View contentView) {
            super(contentView);

            tvSellerName = contentView.findViewById(R.id.txtSellerName);
            tvCouponCode = contentView.findViewById(R.id.txtCouponCode);
            tvCouponInfo = contentView.findViewById(R.id.txtCouponInfo);
            tvEndDate = contentView.findViewById(R.id.txtEndDate);
        }
    }

    private class InnerMovie {

        final String shop_name;
        final String coupon_code;
        final String min_amt;
        final String offer_amt;
        final String amt_in;
        final String coupon_enddate;

        InnerMovie(String shop_name, String coupon_code, String min_amt, String offer_amt, String amt_in, String coupon_enddate) {
            this.shop_name = shop_name;
            this.coupon_code = coupon_code;
            this.min_amt = min_amt;
            this.offer_amt = offer_amt;
            this.amt_in = amt_in;
            this.coupon_enddate = coupon_enddate;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public String getMin_amt() {
            return min_amt;
        }

        public String getOffer_amt() {
            return offer_amt;
        }

        public String getAmt_in() {
            return amt_in;
        }

        public String getCoupon_enddate() {
            return coupon_enddate;
        }
    }

    private void getCoupons() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(getActivity());
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlCouponsDetails, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.e("couponsRes:",""+response.toString());
                        if (response.isNull("posts")) {
                            //coupon_recycler_view.setVisibility(View.GONE);
                            //tvCouponMessage.setVisibility(View.VISIBLE);
                        } else {
                            JSONArray couponJsonArray = response.getJSONArray("posts");
                            for (int i = 0; i < couponJsonArray.length(); i++) {
                                //active_status
                                ////deactive

                                JSONObject jsonObjectCoupons = couponJsonArray.getJSONObject(i);
                                coupn_status = jsonObjectCoupons.getString("active_status");
                                if(coupn_status.equalsIgnoreCase("active")) {
                                    openCupunDialog();

                                }else {
                                    couponLayout.setVisibility(View.GONE);
                                    getDeliveryOptions();
                                    delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                                }
                                //deactive
                                /*InnerMovie innerMovie = new InnerMovie(
                                        jsonObjectCoupons.getString("shop_name"),
                                        jsonObjectCoupons.getString("coupon_code"),
                                        jsonObjectCoupons.getString("min_amt"), jsonObjectCoupons.getString("offer_amt"),
                                        jsonObjectCoupons.getString("amt_in"), jsonObjectCoupons.getString("coupon_enddate"));*/
                                //couponsAdapter.add(innerMovie);
                            }
                            //coupon_recycler_view.setAdapter(couponsAdapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            dialog.dismiss();
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }

    private void orderPlacedAlertDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View review = layoutInflater.inflate(R.layout.order_placed_successfully, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(review); // set alert_dialog_verification_code.xml to shopReviewDialog builder
        alertDialogBuilder // set dialog message
                .setCancelable(false)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        alertDialog = alertDialogBuilder.create(); // create alert dialog
        alertDialog.show(); // show it

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(Color.parseColor("#03A9F4"));
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                UserTracking UT = new UserTracking(UserTracking.context);
                UT.user_tracking(class_name + ": clicked on order place dialog button", getActivity());*/

                if (alertDialog != null && alertDialog.isShowing()) { //this condition is for window leak error at runtime
                    alertDialog.dismiss();

                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    intent.putExtra("tag", "");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                    //coordinatorLayout_next.setVisibility(View.GONE);
                    // relativeLayoutEmptyCart.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void sendNotification() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) { // Internet connection is not present, Ask user to connect to Internet
            JSONObject params = new JSONObject();
            progressDialog2.show();
            try {
                params.put("contactNo", strContact);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlOrderConfirmationNotification,
                    params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressDialog2.dismiss();
                                Log.d("sendNotification",""+response);

                                btnPlaceOrder.setVisibility(View.VISIBLE);

                                DBHelper db = new DBHelper(getActivity());
                                db.NormalCartDeleteAll();
                                DBHelper dbHelper = new DBHelper(getActivity());
                                dbHelper.deleteOnlyCartTable();

                                //Clear sessionMainCat of Type Both

                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("sessionMainCat", "NA");
                                editor.apply();
                                editor.commit();



                                //orderPlacedAlertDialog();
                                if(radio_online.isChecked()){

                                }else {
                                    Intent intent = new Intent(getActivity(), ContinueShopingActivity.class);
                                    intent.putExtra("tag", "");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    progressDialog2.dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(getActivity());
            gateWay.displaySnackBar(coordinatorLayout);
        }
    }
    //Location
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            Log.d("Location",mLocation.getLatitude()+" "+mLocation.getLongitude());
            c_latitude = String.valueOf(mLocation.getLatitude());
            c_longitude = String.valueOf(mLocation.getLongitude());
            //mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            //  Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //  Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }
/*
    @Override
    public void onLocationChanged(Location location) {

    }*/

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
     /*   mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);*/
        mLocationRequest = LocationRequest.create();//.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(getActivity(), "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            //locationTv.setText("Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
            Log.e("Location:", "" + location.getLatitude() + "" + location.getLongitude());
            c_latitude = String.valueOf(location.getLatitude());
            c_longitude = String.valueOf(location.getLongitude());
        }

        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.d("UpdatedLocation",msg);
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        //mLongitudeTextView.setText(String.valueOf(location.getLongitude() ));
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        //LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }



    public void openCupunDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout_popup_default2);

        Button btn_zero = dialog.findViewById(R.id.btn_zero);
        Button btn_one = dialog.findViewById(R.id.btn_one);
        btn_one.setVisibility(View.GONE);
        Button btn_two = dialog.findViewById(R.id.btn_two);
        TextView tv_msg1 = dialog.findViewById(R.id.tv_msg1);
        TextView tv_msg2 = dialog.findViewById(R.id.tv_msg2);
        TextView tv_msg3 = dialog.findViewById(R.id.tv_msg3);


        btn_two.setText("YES");
        btn_zero.setText("NO");

        //tv_msg1.setText("\nPlease Confirm");
        tv_msg1.setVisibility(View.GONE);
        tv_msg2.setText("Do you have Coupon Code?");
        tv_msg3.setVisibility(View.GONE);

        btn_zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                couponLayout.setVisibility(View.GONE);
                getDeliveryOptions();
                delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);

            }
        });

        btn_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    dialog.dismiss();

                    couponLayout.setVisibility(View.VISIBLE);
                    //delivery_optionsDisplayLayout.setVisibility(View.VISIBLE);
                    getDeliveryOptions();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();

    }



    private void showAlertDialog(String title, String message, String buttonText) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setIcon(R.drawable.app_icon_new); //Setting Dialog icon Image
        alertDialog.setTitle(title);  // Setting Dialog Title
        alertDialog.setMessage(message); // Setting Dialog Message
        alertDialog.setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { // Setting OK Button
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.show(); // Showing Alert Message
        TextView messageText = dialog.findViewById(android.R.id.message);
        assert messageText != null;
        messageText.setGravity(Gravity.CENTER);
        dialog.show();
    }

    public void ErrorHandlingMethod(VolleyError error) {
        if (error instanceof ServerError) {
            String title = "Server Error";
            String message = "Sorry for the inconvenience, the web server is not responding. Please try again after some time.";
            String buttonText = "Ok";

            showAlertDialog(title, message, buttonText);
        } else if (error instanceof NoConnectionError) {
            String title = "No Connection Error";
            String message = "Communication Error! Please try again after some time.";
            String buttonText = "Ok";

            showAlertDialog(title, message, buttonText);
        } else if (error instanceof TimeoutError) {
            String title = "Timeout Error";
            String message = "Connection TimeOut! Please check your internet connection.";
            String buttonText = "Ok";

            showAlertDialog(title, message, buttonText);
        } else if (error instanceof ParseError) {
            String title = "Parse Error";
            String message = "Parsing error! Please try again after some time.";
            String buttonText = "Ok";

            showAlertDialog(title, message, buttonText);
        }
    }

    //Get paymentmode Api method
    public void paymentModeList(){

        if(Connectivity.isConnected(getActivity())){

            //progressDialog.show();

            String urlAcceptOrder = ApplicationUrlAndConstants.paymentmode;

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                          //  progressDialog.dismiss();
                            try {
                                Log.e("PaymentModeRES:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONArray jsonArray =  jsonObject.getJSONArray("data");

                                    for (int i=0; i<jsonArray.length();i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        PaymentModeModel paymentMode_model = new PaymentModeModel();
                                        paymentMode_model.setPaymentType(jsonObject1.getString("payment_type"));
                                        paymentMode_model.setPayment_details(" ");
                                        paymentModeList.add(paymentMode_model);
                                    }
                                    paymentModeAdaptor.notifyDataSetChanged();

                                    //  Toast.makeText(mContext,"Payment List Successfully fetched",Toast.LENGTH_SHORT).show();
                                }else if(status=false){
                                    Toast.makeText(getActivity(),"Sorry. No PaymentMode Found.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(getActivity(),"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void getshoplist(){
        orderArrayList.clear();
        arrayshoplist.clear();
        ShopModel model0 = new ShopModel();
        model0.setShop_id("00");
        model0.setSellername("Any Seller");
        model0.setShop_name("Any Shop");
        model0.setArea("Any Area");
        orderArrayList.add(model0);
        arrayshoplist.add("Any Shop");

        String targetUrlShopList = null;
        try {
            targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/nearbyShopList.php?latitude="+latitude+"&longitude="+longitude+"&sessionMainCat="+ URLEncoder.encode(product_maincat,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+orderId+"&shopListType="+shopListType;

        Log.d("targetUrlShopList",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ShopListRes",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        //progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            boolean s_status = jsonObject.getBoolean("s_status");

                            if(s_status==false){

                                Log.e("ShopSize",""+arrayshoplist.size());
                                ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,arrayshoplist);
                                sp_shopList.setAdapter(adapter);
                            }

                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        ShopModel model = new ShopModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setContact(olderlist.getString("contact"));
                                        model.setEmail(olderlist.getString("email"));
                                        model.setAdmin_type(olderlist.getString("admin_type"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setRecharge_amount(olderlist.getString("recharge_amount"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_category(olderlist.getString("shop_category"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setPreferredDeliveryAreaKm(olderlist.getString("preferred_delivery_area_km"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        orderArrayList.add(model);
                                        arrayshoplist.add(olderlist.getString("shop_name"));
                                    }
                                    Log.e("ShopListSize:",""+arrayshoplist.size());
                                    /*shopListAdapter = new ShopListAdapter(orderArrayList,getActivity());
                                    rv_nearbyshoplist.setAdapter(shopListAdapter);
                                    shopListAdapter.notifyDataSetChanged();*/
                                }else {
                                    ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,arrayshoplist);
                                    sp_shopList.setAdapter(adapter);
                                }
                            }else if(strStatus=false){
                                /*tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");*/
                                ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,arrayshoplist);
                                sp_shopList.setAdapter(adapter);
                            }



                            ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,arrayshoplist);
                            sp_shopList.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(getActivity()).addToRequestQueue(orderListRequest);

    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }


    // Generate Valid Checksum working fine
    public String generateChecksum(String jsonString, String merchantKey)
            throws Exception {
        //merchantKey = "95205790-466c-42b9-8daa-b83c19bdef79"; // sandbox key
        merchantKey = "23c9668b-6ea4-4622-8c0b-957291f0b6ce"; // production key
        String jsonString2 ="";
        JSONObject  jsonObject = new JSONObject();
        try {
            //jsonObject.put("amount", "211.65");
            jsonObject.put("amount", "1.00");//strFinalAmount //1.21
            jsonObject.put("channel", "ANDROID");
            jsonObject.put("currency", "INR");
            jsonObject.put("customNote", "hello_freecharge");
            jsonObject.put("customerName", "pramod");
            //jsonObject.put("customerName",strName);
            //jsonObject.put("email", "panderinath.aam@gmail.com");
            jsonObject.put("email", strEmail);
            jsonObject.put("furl", "https://www.google.com");
            jsonObject.put("merchantId", "5YkbjlyQBKjHq1");//
            jsonObject.put("merchantTxnId", "picodel_orderid_12621");
            //jsonObject.put("mobile", "7276201058");
            jsonObject.put("mobile", strContact);
            jsonObject.put("os", "ubuntu-14.04");
            jsonObject.put("productInfo", "auth");
            jsonObject.put("surl", "https://picodel.com/");
            jsonString2 = jsonObject.toString();
            jsonString2 = jsonString2.replace("\\","");
            Log.e("JsonStringparam:",""+jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        MessageDigest md;
        String plainText = jsonString2.concat(merchantKey);
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(); //
        }
        md.update(plainText.getBytes(Charset.defaultCharset()));
        byte[] mdbytes = md.digest();
// convert the byte to hex format method 1
        StringBuffer checksum = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
            checksum.append(Integer.toString((mdbytes[i] & 0xff) +
                    0x100, 16).substring(1));
        }
        return checksum.toString();
    }

    // Getting response by volley StringRequest pure response
    private void getFreeChargePaymentGateway(){

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST,ApplicationUrlAndConstants.urlgetFreechargePayment, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ResPaymentGateway:",""+response);
                wb_freecharge.loadData(response, "text/html", "UTF-8");
                String url = "http://www.example.com";
                String postData = "username=my_username&password=my_password";
                //webview.postUrl(url,EncodingUtils.getBytes(postData, "BASE64"));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("ExpFreeCharge:",""+error);

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> jsonObject = new HashMap<String, String>();
                //jsonObject.put("amount", "211.65");
                jsonObject.put("amount", strFinalAmount);
                jsonObject.put("channel", "ANDROID");
                jsonObject.put("currency", "INR");
                jsonObject.put("customNote", "hello_freecharge");
                jsonObject.put("customerName", "pramod");
                //jsonObject.put("customerName",strName);
                //jsonObject.put("email", "panderinath.aam@gmail.com");
                jsonObject.put("email", strEmail);
                jsonObject.put("furl", "https://picodel.com/");
                jsonObject.put("merchantId", "5YkbjlyQBKjHq1");//
                jsonObject.put("merchantTxnId", "picodel2020");
                //jsonObject.put("mobile", "7276201058");
                jsonObject.put("mobile", strContact);
                jsonObject.put("os", "ubuntu-14.04");
                jsonObject.put("productInfo", "auth");
                jsonObject.put("surl", "https://picodel.com/");
                jsonObject.put("checksum", Freecharge_checksum);
                Log.e("Volley_parma2:",""+jsonObject.toString());

                return jsonObject;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    // Getting Webview with paramters in android

    private void getWebviewFreeCharge(){
        //String url = "https://checkout-sandbox.freecharge.in/api/v1/co/pay/init";
        String url = "https://checkout.freecharge.in/api/v1/co/pay/init";
        try {  //strFinalAmount
            String postData = "amount=" + "1.00" + "&channel=ANDROID" + "&currency=INR"
                    + "&customNote=" + URLEncoder.encode("hello_freecharge", "UTF-8")
                    +"&customerName="+ URLEncoder.encode("pramod", "UTF-8")
                    +"&email="+strEmail     //+ URLEncoder.encode(strEmail, "UTF-8")
                    +"&furl=https://www.google.com" //+ URLEncoder.encode("https://picodel.com/", "UTF-8")
                    +"&merchantId="+ URLEncoder.encode("5YkbjlyQBKjHq1", "UTF-8")
                    +"&merchantTxnId="+ URLEncoder.encode("picodel_orderid_12621", "UTF-8")
                    +"&mobile="+ URLEncoder.encode(strContact, "UTF-8")
                    +"&os="+ URLEncoder.encode("ubuntu-14.04", "UTF-8")
                    +"&productInfo="+ URLEncoder.encode("auth", "UTF-8")
                    +"&surl=https://picodel.com/" //URLEncoder.encode("https://picodel.com/", "UTF-8")
                    +"&checksum="+Freecharge_checksum;
            wb_freecharge.postUrl(url, postData.getBytes());
            Log.e("WebviewUrl:",""+url+postData);

        }catch (Exception e){
            e.printStackTrace();
            Log.e("WebviewExp",""+e.getMessage());
            Log.e("WebviewExp",""+e);
        }


    }

    public String generateTxnChecksum(String jsonString, String merchantKey)
            throws Exception {
        //merchantKey = "95205790-466c-42b9-8daa-b83c19bdef79"; // sandbox key
        merchantKey = "23c9668b-6ea4-4622-8c0b-957291f0b6ce"; // production key
        String jsonString2 ="";
        JSONObject  jsonObject = new JSONObject();
        try {

            jsonObject.put("merchantId", "5YkbjlyQBKjHq1");//
            jsonObject.put("merchantTxnId", "picodel_orderid_12621");
            jsonObject.put("txnType", "CUSTOMER_PAYMENT");

            Log.e("JsonStringparam:",""+jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        MessageDigest md;
        String plainText = jsonString2.concat(merchantKey);
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(); //
        }
        md.update(plainText.getBytes(Charset.defaultCharset()));
        byte[] mdbytes = md.digest();
// convert the byte to hex format method 1
        StringBuffer checksum = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
            checksum.append(Integer.toString((mdbytes[i] & 0xff) +
                    0x100, 16).substring(1));
        }
        return checksum.toString();
    }

    private void LocationAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Location Alert");
        builder.setMessage("Dear User,\n" +
                "To provide you smooth experience in Order Delivery, your location is required.Thanks.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        getActivity().finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
