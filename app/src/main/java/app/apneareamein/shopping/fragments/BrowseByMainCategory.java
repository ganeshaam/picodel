package app.apneareamein.shopping.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.MasterSearch;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class BrowseByMainCategory extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CustomAdapter customAdapter;
    private LinkedHashMap linkedHashMap;
    private ArrayList<MainCatWiseInfo> ProductMainCatWise;
    private Fragment fragment;
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    //private BaseActivity homePageActivity;
    Context mContext;
    TextView tvTitle;
    private LinearLayout Main_Layout_NoInternet;
    private TextView txtNoConnection,tvWishList;
    private ProgressBar simpleProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //View rootView = inflater.inflate(R.layout.fragment_browse_maincat, container, false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_browse_maincat);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_browseby_categoty);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = BrowseByMainCategory.this;
        //initializeViews();

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        txtNoConnection = findViewById(R.id.txtNoConnection);
        //setHasOptionsMenu(true);

        //ProductMainCatWise = new ArrayList<>();
       // linkedHashMap = new LinkedHashMap();
        //customAdapter = new CustomAdapter();
        myReceiver = new Network_Change_Receiver();
        //setHasOptionsMenu(true);
        recyclerView = findViewById(R.id.outer_dataSegregation_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        simpleProgressBar = findViewById(R.id.simpleProgressBar);

    }

    @Override
    public void onPause() {
        super.onPause();
        mContext.unregisterReceiver(myReceiver);
        GateWay gateWay = new GateWay(mContext);
        gateWay.hide();
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                recyclerView.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);
                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
                getCategoriesData();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

   /* private class Network_Change_Receiver extends BroadcastReceiver {
        public Network_Change_Receiver() {
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            Activity activity = mContext;
            if (isAdded() && activity != null) {
                dialog(status);
            }
        }
    }*/
   private class Network_Change_Receiver extends BroadcastReceiver {

       public Network_Change_Receiver() {
       }

       @Override
       public void onReceive(Context context, Intent intent) {
           String status = getConnectivityStatusString(context);
           dialog(status);
       }
   }
/*
    private void initializeViews() {
        homePageActivity = (BaseActivity) mContext;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }*/


    private void getCategoriesData() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(mContext);
            //gateWay.progressDialogStart();
            simpleProgressBar.setVisibility(View.VISIBLE);

            //ProductMainCatWise.clear();
            ProductMainCatWise = new ArrayList<>();

            linkedHashMap = new LinkedHashMap();
            customAdapter = new CustomAdapter();

            JSONObject params = new JSONObject();
            try {
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlBrowseByMainCategory, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if (response.isNull("posts")) {
                        //gateWay.progressDialogStop();
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                    } else {

                        Log.e("broseByCate_Response:",""+response.toString());

                        try {
                            JSONArray mainCatArray = response.getJSONArray("posts");
                            for (int i = 0; i < mainCatArray.length(); i++) {
                                JSONObject jsonMainCatObject = (JSONObject) mainCatArray.get(i);

                                linkedHashMap.put(jsonMainCatObject.getString("maincategory"), "");
                                ProductMainCatWise.add(new MainCatWiseInfo(jsonMainCatObject.getString("maincategory"),
                                        jsonMainCatObject.getString("product_cat_image")));
                            }
                            if (linkedHashMap.size() > 0) {
                                for (Object o : linkedHashMap.keySet()) {
                                    String key = (String) o;
                                    InfoWithMainCat infowithMainCat = new InfoWithMainCat();
                                    infowithMainCat.mainCat = key;
                                    infowithMainCat.mainCatWiseInfos = new ArrayList<>();
                                    for (MainCatWiseInfo pp : ProductMainCatWise) {
                                        if (pp.getProduct_maincat().equals(key)) {
                                            infowithMainCat.mainCatWiseInfos.add(pp);
                                        }
                                    }
                                    customAdapter.add(infowithMainCat);
                                }
                                recyclerView.setAdapter(customAdapter);
                               // customAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    simpleProgressBar.setVisibility(View.INVISIBLE);

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    public class MainCatWiseInfo {

        private String product_maincat;
        private String product_cat;
        private String product_cat_image;

        MainCatWiseInfo(String product_maincat, String product_cat, String product_cat_image) {
            this.product_maincat = product_maincat;
            this.product_cat = product_cat;
            this.product_cat_image = product_cat_image;
        }

        public MainCatWiseInfo(String product_maincat, String product_cat_image) {
            this.product_maincat = product_maincat;
            this.product_cat_image = product_cat_image;
        }

        public String getProduct_maincat() {
            return product_maincat;
        }

        public String getProduct_cat() {
            return product_cat;
        }

        public String getProduct_cat_image() {
            return product_cat_image;
        }

    }

    private class InfoWithMainCat {
        String mainCat;
        ArrayList<MainCatWiseInfo> mainCatWiseInfos;
    }

    public class CustomAdapter extends RecyclerView.Adapter<OuterViewHolder> {

        ArrayList<InfoWithMainCat> mItems = new ArrayList<>();
        ChildAdapter childAdapter;

        CustomAdapter() {
        }


        @NonNull
        @Override
        public OuterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.outer_data_segregration_two_card_view, parent, false);
            return new OuterViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull OuterViewHolder holder, int position) {
            InfoWithMainCat movie = mItems.get(position);
            try {
                for (int k = 0; k < mItems.size(); k++) {
                    for (int j = k; j < movie.mainCatWiseInfos.size(); j++) {
                        MainCatWiseInfo tp = movie.mainCatWiseInfos.get(j);
                       // holder.tvMainCat.setText(tp.getProduct_maincat());

                        Log.e("subCatLog:",""+tp.getProduct_maincat().toString());

                        childAdapter = new ChildAdapter(movie.mainCatWiseInfos);
                        holder.innerRecyclerView.setAdapter(childAdapter);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void add(InfoWithMainCat innerMovie) {
            mItems.add(innerMovie);
        }
    }

    private class OuterViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMainCat;
        private RecyclerView innerRecyclerView;

        OuterViewHolder(View itemView) {
            super(itemView);
            tvMainCat = itemView.findViewById(R.id.txtMainCat);
            innerRecyclerView = itemView.findViewById(R.id.inner_dataSegregation_recycler_view);
            innerRecyclerView.setHasFixedSize(true);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 1);
            innerRecyclerView.setLayoutManager(gridLayoutManager);
        }
    }

    private class ChildAdapter extends RecyclerView.Adapter<ChildViewHolder> {

        private List<MainCatWiseInfo> mChildItems;

        ChildAdapter(ArrayList<MainCatWiseInfo> mainCatWiseInfos) {
            this.mChildItems = mainCatWiseInfos;
        }

        @NonNull
        @Override
        public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inner_data_segregration_card_fullview, parent, false);//inner_data_segregration_card_view
            return new ChildViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ChildViewHolder holder, int position) {
            final MainCatWiseInfo childMovie = mChildItems.get(position);

            //TODO here we show category images under the main category setup to glide
            /*Glide.with(mContext).load(childMovie.getProduct_cat_image())
                    .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProductCat);*/

            Picasso.with(mContext).load(childMovie.getProduct_cat_image())
                    .error(R.drawable.ic_app_transparent)
                    .into(holder.imgProductCat);



           // holder.tvCat.setText(childMovie.getProduct_cat());
            holder.tvCat.setVisibility(View.GONE);

            holder.boxLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*UserTracking UT = new UserTracking(UserTracking.context);
                    UT.user_tracking(class_name + ": clicked on view: " + childMovie.getProduct_maincat() + " & sub cat: " + holder.tvCat.getText().toString(), mContext);*/

                    Intent intentBrowseByCategory= new Intent(BrowseByMainCategory.this,BrowseByCategory.class);
                    intentBrowseByCategory.putExtra("product_SuperCat", childMovie.getProduct_maincat());
                    startActivity(intentBrowseByCategory);

                   /*  fragment = new BrowseByCategory();
                    Bundle bundle = new Bundle();
                    //bundle.putString("product_cat", holder.tvCat.getText().toString());
                    bundle.putString("product_SuperCat", childMovie.getProduct_maincat());
                    fragment.setArguments(bundle);
                    fragment_replace();

                  /*  SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
                    String sessioinCat = sharedPreferencesUtils.getCategory();
                    //chk session
                     if(sessioinCat.equalsIgnoreCase("Fruits and Veggies")) {
                         fragment = new BrowseByCategory();
                         Bundle bundle = new Bundle();
                         //bundle.putString("product_cat", holder.tvCat.getText().toString());
                         bundle.putString("product_SuperCat", childMovie.getProduct_maincat());
                         fragment.setArguments(bundle);
                         fragment_replace();
                     }else  if(sessioinCat.equalsIgnoreCase("Grocery")) {
                         fragment = new BrowseByCategory();
                         Bundle bundle = new Bundle();
                         //bundle.putString("product_cat", holder.tvCat.getText().toString());
                         bundle.putString("product_SuperCat", childMovie.getProduct_maincat());
                         fragment.setArguments(bundle);
                         fragment_replace();
                     }else if(sessioinCat.equalsIgnoreCase("nocat")){
                         fragment = new BrowseByCategory();
                         Bundle bundle = new Bundle();
                         //bundle.putString("product_cat", holder.tvCat.getText().toString());
                         bundle.putString("product_SuperCat", childMovie.getProduct_maincat());
                         fragment.setArguments(bundle);
                         fragment_replace();
                     }else {
                         Toast.makeText(mContext,"Place your order or remove items from Cart",Toast.LENGTH_SHORT).show();
                     }*/
                }
            });
        }

        @Override
        public int getItemCount() {
            return mChildItems.size();
        }
    }

 /*   private void fragment_replace() {
        FragmentManager fragmentManager =getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate("addDataSegregationLevelThree", 0);
        if (!fragmentPopped) { //fragment not in back stack, create it.
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment, "DataSegregationLevelThree")
                    .addToBackStack("addDataSegregationLevelThree")
                    .commit();
        }
    }*/

    private class ChildViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCat;
        private ImageView imgProductCat;
        private LinearLayout boxLinearLayout;

        ChildViewHolder(View itemView) {
            super(itemView);
            tvCat = itemView.findViewById(R.id.txtCat);
            imgProductCat = itemView.findViewById(R.id.productCatImg);
            boxLinearLayout = itemView.findViewById(R.id.boxLinearLayout);
            boxLinearLayout.setTag(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mContext.registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        //((BaseActivity) mContext).changeTitle(getString(R.string.title_activity_browseby_categoty));
        GateWay gateWay = new GateWay(mContext);
        gateWay.hide();

        if (Connectivity.isConnected(mContext)) {
          //  gateWay.SyncData();

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, mContext);*/
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onOptionsItemSelected(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_wishlist, menu);
        super.onCreateOptionsMenu(menu);

        MenuItem item = menu.findItem(R.id.action_empty_wishlist);
        item.setVisible(false);

        MenuItem item2 = menu.findItem(R.id.search);
        item2.setVisible(true);
        item2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(mContext, MasterSearch.class));
                return false;
            }
        });
        return true;
    }

}