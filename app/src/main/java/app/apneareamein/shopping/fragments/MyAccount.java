package app.apneareamein.shopping.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.MasterSearch;
import app.apneareamein.shopping.activities.MyOrder;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class MyAccount extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private Fragment fragment;
    private String strName;
    private String strEmail;
    private String strContact;
    private TextView tvUserName, tvEmail, tvContact, tvNoRedeemPoints,txtNoConnection;
    private RelativeLayout redeemOfferLayout;
    private RelativeLayout noRedeemPointsLayout;
    private CoordinatorLayout myAccountMainLayout;
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;
    
    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                myAccountMainLayout.setVisibility(View.GONE);
                //BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                myAccountMainLayout.setVisibility(View.VISIBLE);
                //BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);

                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);

               // checkRedeemOffersAtServer();

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

  /*  private void initializeViews() {
        homePageActivity = (BaseActivity) mContext;
    }*/

  /*  @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }addressLayout
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment__my_account);

        //initializeViews();

        mContext = MyAccount.this;
        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_my_account);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myReceiver = new Network_Change_Receiver();

        GateWay gateWay = new GateWay(mContext);
        strContact = gateWay.getContact();
        strName = gateWay.getUserName();
        strEmail = gateWay.getUserEmail();

        myAccountMainLayout = findViewById(R.id.myAccountMainLayout);
        RelativeLayout orderLayout = findViewById(R.id.OrderLayout);
        RelativeLayout addressLayout = findViewById(R.id.addressLayout);
        redeemOfferLayout = findViewById(R.id.redeemOfferLayout);
        noRedeemPointsLayout = findViewById(R.id.noRedeemPointsLayout);
        tvUserName = findViewById(R.id.txtUserName);
        tvEmail = findViewById(R.id.txtEmail);
        tvContact = findViewById(R.id.txtContact);
        tvNoRedeemPoints = findViewById(R.id.txtNoRedeemPoints);
        txtNoConnection  =findViewById(R.id.txtNoConnection);
        
        tvUserName.setText(strName);
        tvEmail.setText(strEmail);
        tvContact.setText(strContact);

        orderLayout.setOnClickListener(this);
        addressLayout.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.OrderLayout:

                /*UserTracking UT1 = new UserTracking(UserTracking.context);
                UT1.user_tracking(class_name + ": clicked on order layout", mContext);*/

                Intent i = new Intent(mContext, MyOrder.class);
                startActivity(i);
                break;

            case R.id.addressLayout:

                /*UserTracking UT2 = new UserTracking(UserTracking.context);
                UT2.user_tracking(class_name + ": clicked on address layout", mContext);*/

                Intent add = new Intent(mContext, MyAddress.class);
                Bundle bundle2 = new Bundle();
               // bundle2.putString("type", "add");
                String tag = "simple";
                bundle2.putString("tag", tag);
                add.putExtras(bundle2);
                startActivity(add);

               /*fragment = new MyAddress();
                Bundle bundle = new Bundle();
                String tag = "simple";
                bundle.putString("tag", tag);
                fragment.setArguments(bundle);
                FragmentManager myAddressManager = getSupportFragmentManager();
                boolean myAddressFragmentPopped = myAddressManager.popBackStackImmediate("addMyAddress", 0);
                if (!myAddressFragmentPopped) { //fragment not in back stack, create it.
                    FragmentTransaction myAddressTransaction = myAddressManager.beginTransaction();
                    myAddressTransaction.replace(R.id.frame, fragment, "MyAddress");
                    myAddressTransaction.addToBackStack("addMyAddress");
                    myAddressTransaction.commit();
                }*/

                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
   /*     (mContext).changeTitle(getString(R.string.title_my_account));

        mContext.registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        BaseActivity.bottomNavigationView.getMenu().findItem(R.id.navigation_profile).setCheckable(true);
        tvUserName.setText(strName);
        tvEmail.setText(strEmail);
        tvContact.setText(strContact);
*/
      /*    if (Connectivity.isConnected(mContext)) {
            GateWay gateWay = new GateWay(mContext);
            gateWay.SyncData();

            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, mContext);
        }*/

        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        hideKeyboard(MyAccount.this);
        if (Connectivity.isConnected(mContext)) {

            /*UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, mContext);*/
        }
    }
    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @Override
    public void onPause() {
        super.onPause();
        mContext.unregisterReceiver(myReceiver);
    }

    private void checkRedeemOffersAtServer() { //TODO Server method here
        if (Connectivity.isConnected(mContext)) { // Internet connection is not present, Ask user to connect to Internet
            JSONObject params = new JSONObject();
            try {
                String email = tvEmail.getText().toString();
                String contact = tvContact.getText().toString();
                params.put("email", email);
                params.put("contact", contact);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUserRedeemOffer, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String status = response.getString("status");
                        final String message = response.getString("message");

                        if (status.equals("deactive")) {
                            redeemOfferLayout.setVisibility(View.GONE);
                            noRedeemPointsLayout.setVisibility(View.VISIBLE);
                            tvNoRedeemPoints.setText(message);
                        } else {
                            redeemOfferLayout.setVisibility(View.VISIBLE);
                            noRedeemPointsLayout.setVisibility(View.GONE);
                            redeemOfferLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fragment = new CashBackOffer();
                                    Bundle args = new Bundle();
                                    args.putString("message", message);
                                    fragment.setArguments(args);
                                    FragmentManager CashBackOfferManager =  getSupportFragmentManager();
                                    boolean myAddressFragmentPopped = CashBackOfferManager.popBackStackImmediate("addCashBackOffer", 0);
                                    if (!myAddressFragmentPopped) { //fragment not in back stack, create it.
                                        FragmentTransaction myAddressTransaction = CashBackOfferManager.beginTransaction();
                                        myAddressTransaction.replace(R.id.frame, fragment, "CashBackOffer");
                                        myAddressTransaction.addToBackStack("addCashBackOffer");
                                        myAddressTransaction.commit();
                                    }
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(mContext);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_my_wishlist, menu);
       //item = menu.findItem(R.id.action_empty_wishlist);
        MenuItem search = menu.findItem(R.id.search);

        search.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(MyAccount.this, MasterSearch.class));
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
