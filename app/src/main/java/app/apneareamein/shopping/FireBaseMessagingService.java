package app.apneareamein.shopping;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.MyOrder;
import app.apneareamein.shopping.activities.OfferScreenActivity;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.NotificationUtils;

public class FireBaseMessagingService extends FirebaseMessagingService {

    private final String class_name = this.getClass().getSimpleName();
    Bitmap bitmap;
    private NotificationUtils notificationUtils;

    private String n_title;
    private String n_message;
    private String n_imgurl = null;
    private String n_tag = null;
    private String p_tag = null;
    private String p_cat = null;
    private String n_class = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

        }
        Log.d("remote",""+remoteMessage.getData().toString());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");
            String flag = remoteMessage.getData().get("flag");
            String offerflag = remoteMessage.getData().get("offerstatus");

            Log.e("NotificationFlag:",""+flag);

            if(flag.equals("yes")) {
                //imageUri will contain URL of the image to be displayed with Notification
                String imageUri = remoteMessage.getData().get("image");
                //To get a Bitmap image from the URL received
                bitmap = getBitmapfromUrl(imageUri);
                sendNotification(title,body,bitmap,offerflag);
                //sendNotification(title,body,bitmap,flag);
            }else {
                sendNotification(title,body,offerflag);
            }

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

            /*try {
                JSONObject json = new JSONObject(remoteMessage.getData().get("body"));
                //String json2 =  remoteMessage.getData().toString();
                Log.d("remoteMessage",""+remoteMessage.getData().get("body"));
                  *//* String title = json.getString("title");
                   String body = json.getString("body");*//*
                handleDataMessage(json);
                //handleDataMessage(json2);  //Error In this message while the Image Notification
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        }
    }
    private void handleNow() {
        Log.d("handleNow", "Short lived task is done.");
    }

    private void sendNotification(String title, String messageBody, Bitmap image, String offerflag) {

        Intent intent;
        if(offerflag.equals("yes")){
            intent = new Intent(this, OfferScreenActivity.class);

        }else{
            intent = new Intent(this, BaseActivity.class);
        }

        //Intent intent = new Intent(this, BaseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);

        //set sound
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Log.e("intentNofity:","SEND NOTIFY");
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_firebase)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(image))
                        .setColor(getResources().getColor(R.color.colorAccent))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "PICODEL Merchant",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendNotification(String title, String messageBody,String offerflag) {
        Intent intent;
        if(offerflag.equals("yes")){
             intent = new Intent(this, OfferScreenActivity.class);
        }else{
             intent = new Intent(this, BaseActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_firebase)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "PICODEL",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void scheduleJob() {
        // [START dispatch_job]
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
                .build();
        WorkManager.getInstance().beginWith(work).enqueue();
        // [END dispatch_job]
    }

    private void handleDataMessage(JSONObject json) {
        try {
            Class aClass = null;
            Intent resultIntent = null;

            JSONObject data = json.getJSONObject("data");

            n_title = data.getString("title");
            n_message = data.getString("message");
            if (!data.isNull("image")) {
                n_imgurl = data.getString("image");
            }
            n_tag = data.getString("tag");
            if (!data.isNull("p_cat")) {
                p_cat = data.getString("p_cat");
            }
            if (!data.isNull("p_tag")) {
                p_tag = data.getString("p_tag");
            }
            n_class = data.getString("classname");
            String n_summary_text = data.getString("summary_text");
            String savetoDB = data.getString("save");

            if (savetoDB.equals("yes")) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    public void run() {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.usernotification_tracking(class_name + ": Api Notification", n_title, n_message, n_imgurl, n_tag, n_class, FireBaseMessagingService.this);*/
                    }
                });
            } else {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    public void run() {

                        /*UserTracking UT = new UserTracking(UserTracking.context);
                        UT.user_tracking(class_name + ": Api Notification", FireBaseMessagingService.this);*/
                    }
                });
            }

            if (p_tag == null && p_cat == null) {
                p_tag = "";
                p_cat = "";
            }

            if (n_summary_text == null) {
                n_summary_text = "";
            }

            if (savetoDB == null) {
                savetoDB = "";
            }

            if (n_imgurl == null) {
                n_imgurl = "";
            }

            switch (n_tag) {
                case "orderConfirmed":
                    aClass = MyOrder.class;
                    break;

                case "orderDispatch":
                    aClass = MyOrder.class;
                    break;

                case "activity":
                    try {
                        aClass = Class.forName("app.apneareamein.shopping.activities." + n_class);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;

                case "update":
                    aClass = BaseActivity.class;
                    break;

                default:
                    aClass = BaseActivity.class;
                    break;
            }

            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {  // app is in foreground, broadcast the push message
                switch (n_tag) {
                    case "update":
                        resultIntent = new Intent(getApplicationContext(), aClass);
                        resultIntent.putExtra("tag", "update");
                        break;

                    case "offer":
                        break;

                    case "festival":
                        break;

                    case "other":
                        resultIntent = new Intent(getApplicationContext(), aClass);
                        resultIntent.putExtra("tag", "notification_Home");
                        break;

                    case "browser":
                        resultIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(n_class));
                        break;

                    default:
                        if (!p_cat.equals("")) {
                            resultIntent = new Intent(getApplicationContext(), aClass);
                            resultIntent.putExtra("tag", p_tag);
                            resultIntent.putExtra("selectedName", p_cat);
                        } else {
                            resultIntent = new Intent(getApplicationContext(), aClass);
                        }
                        break;
                }
                LocalBroadcastManager.getInstance(this).sendBroadcast(resultIntent);
                // check for image attachment
                if (TextUtils.isEmpty(n_imgurl)) {
                    showNotificationMessage(getApplicationContext(), n_title, n_message, resultIntent, n_summary_text);
                } else {
                    showNotificationMessageWithBigImage(getApplicationContext(), n_title, n_message, resultIntent, n_imgurl, n_summary_text); // image is present, show notification with image
                }
            } else {  // app is in background, show the notification in notification tray
                switch (n_tag) {
                    case "update":
                        resultIntent = new Intent(getApplicationContext(), aClass);
                        resultIntent.putExtra("tag", "update");
                        break;

                    case "offer":
                        break;

                    case "festival":
                        break;

                    case "other":
                        resultIntent = new Intent(getApplicationContext(), aClass);
                        resultIntent.putExtra("tag", "notification_Home");
                        break;

                    case "browser":
                        resultIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(n_class));
                        break;

                    default:
                        if (!p_cat.equals("")) {
                            resultIntent = new Intent(getApplicationContext(), aClass);
                            resultIntent.putExtra("tag", p_tag);
                            resultIntent.putExtra("selectedName", p_cat);
                        } else {
                            resultIntent = new Intent(getApplicationContext(), aClass);
                        }
                        break;
                }
                // check for image attachment
                if (TextUtils.isEmpty(n_imgurl)) {
                    showNotificationMessage(getApplicationContext(), n_title, n_message, resultIntent, n_summary_text);
                } else {
                    showNotificationMessageWithBigImage(getApplicationContext(), n_title, n_message, resultIntent, n_imgurl, n_summary_text); // image is present, show notification with image
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, Intent intent, String summary_text) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent, null, summary_text);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, Intent intent, String imageUrl, String summary_text) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent, imageUrl, summary_text);
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}


/*
*> Task :app:signingReport
Variant: debug
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------
Variant: release
Config: none
----------
Variant: releaseUnitTest
Config: none
----------
Variant: debugUnitTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------
Variant: debugAndroidTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: B1:90:23:FC:DA:0A:78:09:1B:C2:A1:E0:97:BA:E3:87
SHA1: 50:7F:6D:77:A3:08:C9:49:49:BC:6C:8C:8B:20:A7:33:8E:35:15:D7
SHA-256: 0D:50:BB:61:EB:E9:50:7F:57:AF:7C:77:91:49:F7:1C:8D:2C:67:13:BA:86:7C:A0:30:96:0D:5A:8D:C4:0C:2C
Valid until: Wednesday, 5 May, 2049
----------

BUILD SUCCESSFUL in 1s
1 actionable task: 1 executed
PM 05:21:58: Task execution finished 'signingReport'.

* */
