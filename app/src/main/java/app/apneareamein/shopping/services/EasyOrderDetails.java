package app.apneareamein.shopping.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.activities.EditAddress;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;
import app.apneareamein.shopping.utils.Movie;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class EasyOrderDetails extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private Bundle bundle;
    private String storeSelectedDate;
    private String storeSelectedTime;
    private String strProductList;
    private String tag;
    private String deliveryMethod;
    private String strAddress_Id;
    private TextView tvAddress;
    //private TextView txtDefault;
    private TextView tvUserName;
    private TextView tvUserContact;
    private TextView tvHeaderDeliveryOption;
    private TextView tvWrongPincode;
    private RadioButton rbScheduleDelivery;
    private CoordinatorLayout easyOrderMainLayout;
    private Spinner scheduleDeliveryDateSpinner, scheduleDeliveryTimeSpinner;
    private Button btnPlaceOrder;
    private ArrayList<String> storeMonthDatesArrayList = new ArrayList<>();
    private ArrayList<String> storeNextTimeArrayList = new ArrayList<>();
    private ArrayList<String> storeCurrentTimeArrayList = new ArrayList<>();
    private final ArrayList<Movie> arr_user_info = new ArrayList<>();
    private LinearLayout layoutDeliveryOptions;
    private LinearLayout Main_Layout_NoInternet;
    private ScrollView scrollView;
    private TextView txtNoConnection;
    private BroadcastReceiver myReceiver;

    @Override
    protected void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            dialog(status);
        }
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                btnPlaceOrder.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                txtNoConnection.setText("No connection");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(txtNoConnection);
            } else {
                Main_Layout_NoInternet.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                txtNoConnection.setText("Back online");
                txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(txtNoConnection);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easy_order_details);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_order_details);
        setSupportActionBar(toolbar);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            strProductList = bundle.getString("strProductList");
            tag = bundle.getString("tag");
            strAddress_Id = bundle.getString("address_id");
        }
        myReceiver = new Network_Change_Receiver();

        easyOrderMainLayout = findViewById(R.id.easyOrderMainLayout);
        TextView tvChangeAddress = findViewById(R.id.txtChangeAddress);
        tvAddress = findViewById(R.id.txtAddress);
        //txtDefault = findViewById(R.id.txtDefault);
        tvUserName = findViewById(R.id.txtUserName);
        tvUserContact = findViewById(R.id.txtContact);
        scheduleDeliveryDateSpinner = findViewById(R.id.scheduleDeliveryDateSpinner);
        scheduleDeliveryTimeSpinner = findViewById(R.id.scheduleDeliveryTimeSpinner);
        btnPlaceOrder = findViewById(R.id.btnPlacedOrder);
        tvWrongPincode = findViewById(R.id.txtWrongPincode);
        tvHeaderDeliveryOption = findViewById(R.id.txtHeaderDeliveryOption);
        layoutDeliveryOptions = findViewById(R.id.layoutDeliveryOptions);
        rbScheduleDelivery = findViewById(R.id.scheduleDeliveryRadioButton);
        txtNoConnection = findViewById(R.id.txtNoConnection);

        btnPlaceOrder.setOnClickListener(this);
        tvChangeAddress.setOnClickListener(this);
        rbScheduleDelivery.setOnCheckedChangeListener(this);

        Main_Layout_NoInternet = findViewById(R.id.no_internet_layout);
        scrollView = findViewById(R.id.scroll);

        if (strAddress_Id != null && strAddress_Id.equals("address_id")) {
            fetchuserAddress(strAddress_Id);
        } else {
            fetchuserAddress("");
        }
    }

    private void setValuesToQuickOrderSpinner() {
        try {
            if (storeMonthDatesArrayList.size() > 0) {
                ArrayAdapter<String> adapter1 = new ArrayAdapter<>(EasyOrderDetails.this, android.R.layout.simple_spinner_item, storeMonthDatesArrayList);
                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                scheduleDeliveryDateSpinner.setAdapter(adapter1);

                scheduleDeliveryDateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        SimpleDateFormat storeDateFormat = new SimpleDateFormat("EEEE,dd-MM-yyyy");
                        String storeFormattedDate = storeDateFormat.format(new Date());
                        storeSelectedDate = (String) scheduleDeliveryDateSpinner.getSelectedItem();
                        if (!storeSelectedDate.equals("Please Select Date")) {
                            scheduleDeliveryTimeSpinner.setVisibility(View.VISIBLE);
                            if (storeSelectedDate.equals(storeFormattedDate)) {
                                ArrayAdapter<String> adapter2 = new ArrayAdapter<>(EasyOrderDetails.this, android.R.layout.simple_spinner_item, storeCurrentTimeArrayList);
                                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                scheduleDeliveryTimeSpinner.setAdapter(adapter2);
                            } else {
                                ArrayAdapter<String> adapter3 = new ArrayAdapter<>(EasyOrderDetails.this, android.R.layout.simple_spinner_item, storeNextTimeArrayList);
                                adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                scheduleDeliveryTimeSpinner.setAdapter(adapter3);
                            }
                        } else {
                            scheduleDeliveryTimeSpinner.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                scheduleDeliveryTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            Toast.makeText(EasyOrderDetails.this, "Please Select Time", Toast.LENGTH_SHORT).show();
                        } else {
                            storeSelectedTime = (String) scheduleDeliveryTimeSpinner.getSelectedItem();
                            btnPlaceOrder.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlacedOrder:
                quickOrder();
                break;

            case R.id.txtChangeAddress:
                Intent intent = new Intent(EasyOrderDetails.this, BaseActivity.class);
                String tag_address = "easy_order_address";
                intent.putExtra("tag", tag_address);
                startActivity(intent);
                break;
        }
    }

    private void quickOrder() { //TODO Server method here
        if (Connectivity.isConnected(EasyOrderDetails.this)) { // Internet connection is not present, Ask user to connect to Internet
            final GateWay gateWay = new GateWay(EasyOrderDetails.this);
            gateWay.progressDialogStart();

            String userName = tvUserName.getText().toString();
            String contact = tvUserContact.getText().toString();
            String address = tvAddress.getText().toString();

            JSONObject params = new JSONObject();
            try {
                if (deliveryMethod.equals("Scheduled Delivery")) {
                    deliveryMethod = "Schedule";
                    storeSelectedDate = (String) scheduleDeliveryDateSpinner.getSelectedItem();
                    storeSelectedTime = (String) scheduleDeliveryTimeSpinner.getSelectedItem();
                }
                params.put("name", userName);
                params.put("email", gateWay.getUserEmail());
                params.put("contactNo", gateWay.getContact());
                params.put("contact", contact);
                params.put("address", address);
                params.put("deliveryDate", storeSelectedDate);
                params.put("deliveryTime", storeSelectedTime);
                params.put("strProductList", strProductList);
                params.put("deliveryType", deliveryMethod);
                params.put("tag", tag);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlQuickOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String status = response.getString("posts");
                        if (status.equals("true")) {
                            quickOrderSuccessfulDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(EasyOrderDetails.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(this);
            gateWay.displaySnackBar(easyOrderMainLayout);
        }
    }

    private void quickOrderSuccessfulDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Easy Order has been placed successfully. \nKeep Shopping.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        Intent intent = new Intent(EasyOrderDetails.this, BaseActivity.class);
                        intent.putExtra("tag", "");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void fetchuserAddress(String id) {
        if (Connectivity.isConnected(EasyOrderDetails.this)) {
            final GateWay gateWay = new GateWay(EasyOrderDetails.this);
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("id", strAddress_Id);
                params.put("tag", "checkout");
                params.put("contact", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetuserdata, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String res = response.toString();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");

                        if (status.equals("success")) {
                            easyOrderMainLayout.setVisibility(View.VISIBLE);
                            JSONArray addressArray = jsonObject.getJSONArray("user_data");

                            for (int i = 0; i < addressArray.length(); i++) {
                                JSONObject json_event_list = addressArray.getJSONObject(i);
                                Movie ui = new Movie(json_event_list.getString("id"), json_event_list.getString("name"),
                                        json_event_list.getString("address"), json_event_list.getString("contact"), json_event_list.getString("pin_code"), json_event_list.getString("count"));
                                arr_user_info.add(ui);
                            }
                            //setDefaultAddress(arr_user_info.get(0).getAddress_id());//this is for old user when they have no default address set

                            tvUserName.setText(arr_user_info.get(0).getStrName());
                            /*if (arr_user_info.get(0).getAddress_type().equals("default")) {
                                txtDefault.setText("To be Delivered");
                            }*/
                            tvAddress.setText(arr_user_info.get(0).getStrAddress());
                            tvUserContact.setText(arr_user_info.get(0).getStrContact());
                            getOrderDetails();
                        } else {
                            Intent intent = new Intent(EasyOrderDetails.this, EditAddress.class);
                            bundle = new Bundle();
                            bundle.putString("tag", "easy_order_address");
                            bundle.putString("type", "add");
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            EasyOrderDetails.this.finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    gateWay.progressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    //from this method we can set current address of user as a default address
    private void setDefaultAddress(String address_id) { //TODO Server Method here
        GateWay gateWay = new GateWay(EasyOrderDetails.this);

        JSONObject params = new JSONObject();
        try {
            params.put("contact", gateWay.getContact());
            params.put("address_id", address_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlSetDefaultAddress, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("posts").equals("true")) {
                        Toast.makeText(EasyOrderDetails.this, "Set default address successfully", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void getOrderDetails() { //TODO Server method here
        if (Connectivity.isConnected(EasyOrderDetails.this)) {
            storeMonthDatesArrayList = new ArrayList<>();
            storeNextTimeArrayList = new ArrayList<>();
            storeCurrentTimeArrayList = new ArrayList<>();
            storeMonthDatesArrayList.add(0, "Please Select Date");
            storeCurrentTimeArrayList.add(0, "Please Select Time");
            storeNextTimeArrayList.add(0, "Please Select Time");

            String strPincode = arr_user_info.get(0).getStrPincode();

            JSONObject params = new JSONObject();
            try {
                params.put("pin_code", strPincode);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlEasyOrderDeliveryOptions, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response.isNull("posts")) {
                        layoutDeliveryOptions.setVisibility(View.GONE);
                        tvHeaderDeliveryOption.setVisibility(View.GONE);
                        tvWrongPincode.setVisibility(View.VISIBLE);
                    } else {
                        layoutDeliveryOptions.setVisibility(View.VISIBLE);
                        tvHeaderDeliveryOption.setVisibility(View.VISIBLE);
                        tvWrongPincode.setVisibility(View.GONE);
                        try {
                            JSONArray restaurantJsonArray = response.getJSONArray("posts");

                            String schedule = restaurantJsonArray.getString(0);
                            String flexible = restaurantJsonArray.getString(1);
                            rbScheduleDelivery.setText(schedule);

                            JSONArray jsonCurrent = response.getJSONArray("current");

                            for (int i = 0; i < jsonCurrent.length(); i++) {
                                JSONObject jsonObject = jsonCurrent.getJSONObject(i);

                                JSONArray value1 = jsonObject.getJSONArray("months_range");
                                JSONArray value2 = jsonObject.getJSONArray("current_time_range");
                                JSONArray value3 = jsonObject.getJSONArray("next_time_range");

                                if (value1.isNull(0) && value3.isNull(2)) {
                                } else {
                                    for (int j = 0; j < value1.length(); j++) {
                                        JSONObject jsonMonthRangeObject = (JSONObject) value1.get(j);
                                        storeMonthDatesArrayList.add(jsonMonthRangeObject.getString("month_dates"));
                                    }

                                    for (int k = 0; k < value2.length(); k++) {
                                        JSONObject jsonCurrentTimeObject = (JSONObject) value2.get(k);
                                        storeCurrentTimeArrayList.add(jsonCurrentTimeObject.getString("currentTime"));
                                    }

                                    for (int l = 0; l < value3.length(); l++) {
                                        JSONObject jsonNextTimeObject = (JSONObject) value3.get(l);
                                        storeNextTimeArrayList.add(jsonNextTimeObject.getString("nextTime"));
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }


            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    GateWay gateWay = new GateWay(EasyOrderDetails.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            GateWay gateWay = new GateWay(this);
            gateWay.displaySnackBar(easyOrderMainLayout);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (buttonView.getId() == R.id.scheduleDeliveryRadioButton) {
                setValuesToQuickOrderSpinner();
                if (Connectivity.isConnected(EasyOrderDetails.this)) {
                    deliveryMethod = rbScheduleDelivery.getText().toString();
                    scheduleDeliveryDateSpinner.setVisibility(View.VISIBLE);
                    scheduleDeliveryTimeSpinner.setVisibility(View.VISIBLE);
                    btnPlaceOrder.setVisibility(View.GONE);
                } else {
                    GateWay gateWay = new GateWay(EasyOrderDetails.this);
                    gateWay.displaySnackBar(easyOrderMainLayout);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (Connectivity.isConnected(EasyOrderDetails.this)) {
            String class_name = this.getClass().getSimpleName();
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, EasyOrderDetails.this);
        }
    }
}
