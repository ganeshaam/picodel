package app.apneareamein.shopping.services;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.orhanobut.dialogplus.DialogPlus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.FilePath;
import app.apneareamein.shopping.utils.GateWay;

public class EasyOrder extends AppCompatActivity implements View.OnClickListener {

    ImageView productImage;
    String userChoosenTask, encodedString;
    private static final int PICK_FILE_REQUEST = 1;
    int REQUEST_CAMERA_ONE = 2;
    int SELECT_FILE_ONE = 3;
    private static final String TAG = EasyOrder.class.getSimpleName();
    private String selectedFilePath;
    private EditText etProductList;
    private CoordinatorLayout EasyOrderMainLayout;
    private DialogPlus dialog;
    private TextView tv_file_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easy_order);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        TextView tvTitle = findViewById(R.id.txtTitle);
        tvTitle.setText(R.string.title_activity_easy_order);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btnQuickOrder = findViewById(R.id.btnQuickOrder);
        etProductList = findViewById(R.id.editProductList);
        EasyOrderMainLayout = findViewById(R.id.EasyOrderMainLayout);
        btnQuickOrder.setOnClickListener(this);

        tv_file_name = findViewById(R.id.tv_file_name);

        Button btnAudioUpload = findViewById(R.id.btnAudioUpload);
        btnAudioUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userChoosenTask = "audio";
                requestStoragePermission();
            }
        });

        productImage = findViewById(R.id.productImage);
        Button btnImageUpload = findViewById(R.id.btnImageUpload);
        btnImageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(EasyOrder.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    requestStoragePermission();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    requestStoragePermission();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Requesting multiple permissions (storage and location) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private void requestStoragePermission() {
        // Requesting READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE & CAMERA using Dexter library
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                            switch (userChoosenTask) {
                                case "Take Photo":
                                    cameraIntent();
                                    break;
                                case "audio":
                                    showFileChooser();
                                    break;
                                default:
                                    galleryIntent();
                                    break;
                            }
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EasyOrder.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setCancelable(false);
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA_ONE);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE_ONE);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("audio/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnQuickOrder:
                if (Connectivity.isConnected(EasyOrder.this)) {
                    StopOrder();
                } else {
                    GateWay gateWay = new GateWay(EasyOrder.this);
                    gateWay.displaySnackBar(EasyOrderMainLayout);
                }
                break;
        }
    }

    private void StopOrder() { //TODO Server Method here
        final GateWay gateWay = new GateWay(EasyOrder.this);
        gateWay.progressDialogStart();

        if (Connectivity.isConnected(EasyOrder.this)) { // Internet connection is not present, Ask user to connect to Internet
            JSONObject params = new JSONObject();
            try {
                params.put("contactNo", gateWay.getContact());
                params.put("email", gateWay.getUserEmail());
                params.put("tag", "easy_order");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlStopOrder, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.isNull("posts")) {
                            String strTempList = etProductList.getText().toString().trim();
                            if (Connectivity.isConnected(EasyOrder.this)) {
                                if (strTempList.equals("") || TextUtils.isEmpty(strTempList)) {
                                    Toast.makeText(EasyOrder.this, "please enter product names, if you have to proceed", Toast.LENGTH_LONG).show();
                                } else {
                                    Intent intent = new Intent(EasyOrder.this, EasyOrderDetails.class);
                                    intent.putExtra("strProductList", strTempList);
                                    intent.putExtra("tag", "SMS");
                                    startActivity(intent);
                                }
                            } else {
                                GateWay gateWay = new GateWay(EasyOrder.this);
                                gateWay.displaySnackBar(EasyOrderMainLayout);
                            }
                        } else {
                            String strTempList = etProductList.getText().toString().trim();
                            if (strTempList.equals("") || TextUtils.isEmpty(strTempList)) {
                                Toast.makeText(EasyOrder.this, "please enter product names, if you have to proceed", Toast.LENGTH_LONG).show();
                            } else {
                                JSONArray array = response.getJSONArray("posts");
                                JSONObject jsonObject = array.getJSONObject(0);

                                String reason = jsonObject.getString("reason");
                                String image = jsonObject.getString("image");

                                dialog = DialogPlus.newDialog(EasyOrder.this)
                                        .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(R.layout.dialog_stop_order))
                                        .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                                        .setGravity(Gravity.CENTER)
                                        .setCancelable(false)
                                        .setPadding(20, 20, 20, 20)
                                        .create();
                                dialog.show();
                                ImageView imgStopOrder = (ImageView) dialog.findViewById(R.id.imgCashBack);
                                RelativeLayout stop_orderLayout = (RelativeLayout) dialog.findViewById(R.id.stop_orderLayout);
                                TextView tvMsg = (TextView) dialog.findViewById(R.id.txtMessage);
                                Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                                if (reason.equals("")) {
                                    tvMsg.setVisibility(View.GONE);
                                } else {
                                    tvMsg.setVisibility(View.VISIBLE);
                                    tvMsg.setText(reason);
                                }
                                if (image.equals("")) {
                                    stop_orderLayout.setVisibility(View.GONE);
                                } else {
                                    stop_orderLayout.setVisibility(View.VISIBLE);

                                    //TODO show image setup to glide
                                    Glide.with(EasyOrder.this)
                                            .load(image)
                                            .thumbnail(Glide.with(EasyOrder.this).load(R.drawable.loading))
                                            .error(R.drawable.ic_app_transparent)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(imgStopOrder);
                                }

                                btnOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    gateWay.progressDialogStop();

                    GateWay gateWay = new GateWay(EasyOrder.this);
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        } else {
            gateWay.displaySnackBar(EasyOrderMainLayout);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Connectivity.isConnected(EasyOrder.this)) {
            String class_name = this.getClass().getSimpleName();
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, EasyOrder.this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FILE_REQUEST) {
                if (data == null) {
                    //no data present
                    return;
                }

                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(this, selectedFileUri);

                if (selectedFilePath != null && !selectedFilePath.equals("")) {
                    // Get file from file name
                    File file = new File(selectedFilePath);

                    // Get length of file in bytes
                    long fileSizeInBytes = file.length();

                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;

                    // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    double fileSizeInMB = fileSizeInKB / 1024.0;

                    double res = Double.parseDouble(String.format("%.2f", fileSizeInMB));
                    if (res <= 1.00f) {
                        tv_file_name.setVisibility(View.VISIBLE);
                        tv_file_name.setText(selectedFilePath);

                        if (selectedFilePath != null) {
                            final GateWay gateWay = new GateWay(EasyOrder.this);
                            gateWay.progressDialogStart();

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    //creating new thread to handle Http Operations
                                    uploadFile(selectedFilePath, gateWay);
                                }
                            }).start();
                        } else {
                            Toast.makeText(EasyOrder.this, "Please choose a File First", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(EasyOrder.this, "Please choose a File below 1MB", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == SELECT_FILE_ONE) {
                Uri uri = data.getData();
                onSelectFromGalleryResult(uri);
            } else if (requestCode == REQUEST_CAMERA_ONE) {
                onCaptureImageResult(data);
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        productImage.setVisibility(View.VISIBLE);
        productImage.setImageBitmap(thumbnail);

        byte[] b = bytes.toByteArray();
        encodedString = Base64.encodeToString(b, Base64.DEFAULT);

        uploadToServer();
    }

    private void onSelectFromGalleryResult(Uri data) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data);

            productImage.setVisibility(View.VISIBLE);
            productImage.setImageBitmap(bitmap);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();

            encodedString = Base64.encodeToString(b, Base64.DEFAULT);

            uploadToServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void uploadToServer() {
        final GateWay gateWay = new GateWay(EasyOrder.this);
        gateWay.progressDialogStart();

        JSONObject params = new JSONObject();
        try {
            params.put("contactNo", gateWay.getContact());
            params.put("image_file", encodedString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlUploadImageFile, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String file_name = response.getString("posts");
                    sendToNextStep(file_name, "image", gateWay);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                gateWay.progressDialogStop();

                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    //android upload file to server
    public int uploadFile(final String selectedFilePath, final GateWay gateWay) {
        int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);

        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length - 1];

        if (!selectedFile.isFile()) {
            gateWay.progressDialogStop();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv_file_name.setVisibility(View.VISIBLE);
                    tv_file_name.setText("Source File Doesn't Exist: " + selectedFilePath);
                }
            });
            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL(ApplicationUrlAndConstants.urlUploadAudioFile);
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploaded_file", selectedFilePath);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0) {
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();

                //response code of 200 indicates the server status OK
                if (serverResponseCode == 200) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            sendToNextStep(fileName, "audio", gateWay);
                        }
                    });
                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "File Not Found", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            return serverResponseCode;
        }
    }

    private void sendToNextStep(final String fileName, final String tag, GateWay gateWay) {
        gateWay.progressDialogStop();

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        if (tag.equals("image")) {
            builder.setTitle("Image file uploaded successfully");
        } else {
            builder.setTitle("Audio file uploaded successfully");
        }
        builder.setMessage("Please goto next step and select the Date & Time")
                .setCancelable(false)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(EasyOrder.this, EasyOrderDetails.class);
                        intent.putExtra("strProductList", fileName);
                        intent.putExtra("tag", tag);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
