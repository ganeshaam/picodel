package app.apneareamein.shopping.services.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.BaseActivity;
import app.apneareamein.shopping.infiniteviewpager.InfinitePagerAdapter;
import app.apneareamein.shopping.interfaces.ApplicationUrlAndConstants;
import app.apneareamein.shopping.services.EasyOrder;
import app.apneareamein.shopping.sync.UserTracking;
import app.apneareamein.shopping.utils.AppController;
import app.apneareamein.shopping.utils.Connectivity;
import app.apneareamein.shopping.utils.GateWay;

import static app.apneareamein.shopping.utils.GateWay.getConnectivityStatusString;
import static app.apneareamein.shopping.utils.GateWay.slideDown;
import static app.apneareamein.shopping.utils.GateWay.slideUp;

public class Services extends Fragment {

    private Context context;
    private ServicesDetailsAdapter servicesDetailsAdapter;
    private Intent intent;
    private ViewPager viewPager;
    private int currentPage = 0;
    private String lotMoreThanShopping;
    public static ArrayList<String> productImage = new ArrayList<>();
    public static ArrayList<String> services = new ArrayList<>();
    private ArrayList<String> servicesTag = new ArrayList<>();
    private ArrayList<String> servicesDetails = new ArrayList<>();
    private ArrayList<String> exclusiveImgPath;
    private ArrayList<String> exclusiveShopCategory;
    private TextView tvLotMoreThanShopping;
    private RecyclerView servicesRecyclerView;
    private RelativeLayout slidingLayout;
    private int CartCount;
    private final String class_name = this.getClass().getSimpleName();
    private BroadcastReceiver myReceiver;
    private BaseActivity homePageActivity;
    private RelativeLayout servicesMainLayout;

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
    }

    public void dialog(String status) {
        try {
            if (status.equals("No")) {
                servicesMainLayout.setVisibility(View.GONE);
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("No connection");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.red));
                slideUp(BaseActivity.txtNoConnection);
            } else {
                BaseActivity.Main_Layout_NoInternet.setVisibility(View.GONE);
                servicesMainLayout.setVisibility(View.VISIBLE);

                BaseActivity.txtNoConnection.setText("Back online");
                BaseActivity.txtNoConnection.setBackgroundColor(getResources().getColor(R.color.green));
                slideDown(BaseActivity.txtNoConnection);
                requestFromServerMainCategory();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class Network_Change_Receiver extends BroadcastReceiver {

        public Network_Change_Receiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String status = getConnectivityStatusString(context);

            Activity activity = getActivity();
            if (isAdded() && activity != null) {
                dialog(status);
            }
        }
    }

    private void initializeViews() {
        homePageActivity = (BaseActivity) getActivity();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            homePageActivity = (BaseActivity) context;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_services, container, false);

        context = getActivity();

        initializeViews();

        myReceiver = new Network_Change_Receiver();

        this.getArguments().getString("type");

        viewPager = rootView.findViewById(R.id.pager);
        tvLotMoreThanShopping = rootView.findViewById(R.id.txtLotMoreThanShopping);
        servicesRecyclerView = rootView.findViewById(R.id.servicesRecyclerView);
        servicesRecyclerView.setNestedScrollingEnabled(false);
        servicesRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        servicesRecyclerView.setLayoutManager(mLayoutManager);
        servicesMainLayout = rootView.findViewById(R.id.ServicesMainLayout);

        slidingLayout = rootView.findViewById(R.id.slidingLayout);

        return rootView;
    }

    private class ServicesDetailsAdapter extends RecyclerView.Adapter<MainViewHolder> {

        final List<InnerMovie> servicesDetails = new ArrayList<>();

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_show_services, parent, false);
            return new MainViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
            int pos = holder.getAdapterPosition();
            final InnerMovie innerMovie = servicesDetails.get(pos);

            holder.tvServices.setText(innerMovie.getServicesDetails());

            //TODO here services images setup to glide
            Glide.with(context)
                    .load(innerMovie.getServicesImg())
                    .thumbnail(Glide.with(getActivity()).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgServices);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (innerMovie.getTag()) {
                        case "EasyOrder":
                            intent = new Intent(getActivity(), EasyOrder.class);
                            startActivity(intent);
                            break;
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return servicesDetails.size();
        }

        public void add(InnerMovie innerMovie) {
            servicesDetails.add(innerMovie);
        }
    }

    private class MainViewHolder extends RecyclerView.ViewHolder {

        private final View mView;
        private final ImageView imgServices;
        private final TextView tvServices;

        private MainViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
            imgServices = itemView.findViewById(R.id.imgServices);
            tvServices = itemView.findViewById(R.id.txtServices);
        }
    }

    private class InnerMovie {

        private final String tag;
        private final String servicesImg;
        private final String servicesDetails;

        private InnerMovie(String tag, String servicesImg, String servicesDetails) {
            this.tag = tag;
            this.servicesImg = servicesImg;
            this.servicesDetails = servicesDetails;
        }

        public String getTag() {
            return tag;
        }

        public String getServicesImg() {
            return servicesImg;
        }

        public String getServicesDetails() {
            return servicesDetails;
        }
    }

    private class SlidingImages_Adapter extends PagerAdapter {

        final ArrayList<String> IMAGES;
        final Context context;

        SlidingImages_Adapter(Context context, ArrayList<String> productImage) {
            this.context = context;
            this.IMAGES = productImage;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return IMAGES.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup viewGroup, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.sliding_image_layout, viewGroup, false);

            ImageView imageView = view.findViewById(R.id.image);

            //TODO show restaurant banner setup to glide
            Glide.with(context)
                    .load(IMAGES.get(position))
                    .thumbnail(Glide.with(getActivity()).load(R.drawable.loading))
                    .error(R.drawable.ic_app_transparent)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

            viewGroup.addView(view, 0);
            return view;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        ((BaseActivity) getActivity()).changeTitle(getString(R.string.title_activity_services));

        BaseActivity.bottomNavigationView.getMenu().findItem(R.id.navigation_services).setCheckable(true);

        if (Connectivity.isConnected(getActivity())) {
            SyncData();
            UserTracking UT = new UserTracking(UserTracking.context);
            UT.user_tracking(class_name, getActivity());
        }
    }

    private void SyncData() { //TODO Server Method here
        GateWay gateWay = new GateWay(getActivity());

        JSONObject params = new JSONObject();
        try {
            params.put("contact", gateWay.getContact());
            params.put("email", gateWay.getUserEmail());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlGetCartCount, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    CartCount = Integer.parseInt(response.getString("normal_cart_count"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    ((BaseActivity) getActivity()).updateAddToCartCount(CartCount);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    private void requestFromServerMainCategory() { //TODO Server method here
        if (Connectivity.isConnected(getActivity())) {
            //this for services to notify the recycler view
            services = new ArrayList<>();
            servicesTag = new ArrayList<>();
            servicesDetails = new ArrayList<>();
            productImage = new ArrayList<>();
            //this for services to notify the recycler view
            exclusiveImgPath = new ArrayList<>();
            exclusiveShopCategory = new ArrayList<>();
            servicesDetailsAdapter = new ServicesDetailsAdapter();

            final GateWay gateWay = new GateWay(getActivity());
            gateWay.progressDialogStart();

            JSONObject params = new JSONObject();
            try {
                params.put("city", gateWay.getCity());
                params.put("area", gateWay.getArea());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApplicationUrlAndConstants.urlServicePage, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray imageArray = response.getJSONArray("images");
                        for (int k = 0; k < imageArray.length(); k++) {
                            JSONObject jsonImageObject = (JSONObject) imageArray.get(k);
                            lotMoreThanShopping = jsonImageObject.getString("lotMoreThanShoppingText");
                            services.add(jsonImageObject.getString("services"));
                            servicesTag.add(jsonImageObject.getString("service_tag"));
                            servicesDetails.add(jsonImageObject.getString("service_details"));
                            exclusiveImgPath.add(jsonImageObject.getString("exclusive_image"));
                            exclusiveShopCategory.add(jsonImageObject.getString("exclusive_category"));
                        }

                        if (!response.isNull("images1")) {
                            JSONArray imageArray1 = response.getJSONArray("images1");
                            if (imageArray.length() > 0) {
                                for (int k = 0; k < imageArray1.length(); k++) {
                                    JSONObject jsonImageObject1 = (JSONObject) imageArray1.get(k);
                                    productImage.add(jsonImageObject1.getString("front_search_banner")); //add to arrayList
                                }
                            }
                        }

                        exclusiveImgPath.removeAll(Collections.singleton(""));
                        exclusiveShopCategory.removeAll(Collections.singleton(""));
                        productImage.removeAll(Collections.singleton(""));
                        services.removeAll(Collections.singleton(""));
                        servicesTag.removeAll(Collections.singleton(""));
                        servicesDetails.removeAll(Collections.singleton(""));

                        tvLotMoreThanShopping.setText(lotMoreThanShopping);
                        if (productImage.size() <= 0) {
                            slidingLayout.setVisibility(View.GONE);
                        } else if (productImage.size() > 0 && services.size() > 0) {
                            slidingLayout.setVisibility(View.VISIBLE);
                            AnimatedSlideShow(); //set the banner images as well as lot more that shopping
                        }

                        for (int i = 0; i < services.size(); i++) {
                            InnerMovie innerMovie = new InnerMovie(servicesTag.get(i), services.get(i), servicesDetails.get(i));
                            servicesDetailsAdapter.add(innerMovie);
                        }
                        servicesRecyclerView.setAdapter(servicesDetailsAdapter);
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                    gateWay.progressDialogStop();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    gateWay.progressDialogStop();

                    error.printStackTrace();

                    GateWay gateWay = new GateWay(getActivity());
                    gateWay.ErrorHandlingMethod(error); //TODO ServerError Method here
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    //viewpager
    private void AnimatedSlideShow() {
        try {
            //start----- this is for banner
            SlidingImages_Adapter slidingImages_adapter = new SlidingImages_Adapter(getActivity(), productImage);
            PagerAdapter wrappedAdapter = new InfinitePagerAdapter(slidingImages_adapter);
            viewPager.setAdapter(wrappedAdapter);
            int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15 * 2, getResources().getDisplayMetrics());
            viewPager.setPageMargin(-margin);

            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() { // Auto start of viewpager
                    viewPager.setCurrentItem(currentPage, true);
                    if (currentPage == Integer.MAX_VALUE) {
                        currentPage = 0;
                    } else {
                        ++currentPage;
                    }
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 500, 3000);
            //end----- this is for banner
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}