package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.ShopListActivity;
import app.apneareamein.shopping.model.ShopModel;

public class ShopsCategoryAdapter  extends RecyclerView.Adapter<ShopsCategoryAdapter.ShopCategoryHolder>    {

    ArrayList<ShopModel> shopCategoryList;
    Context mContext;

    public ShopsCategoryAdapter(ArrayList<ShopModel> shopCategoryList, Context mContext) {
        this.shopCategoryList = shopCategoryList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ShopCategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_shop_category,parent,false);
        ShopsCategoryAdapter.ShopCategoryHolder shopCategoryHolder = new ShopsCategoryAdapter.ShopCategoryHolder(view);
        return shopCategoryHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopCategoryHolder holder, final int position) {
        TextView tv_shop_category = holder.tv_shop_category;
        ImageView iv_shop_category = holder.iv_shop_category;
        tv_shop_category.setText(shopCategoryList.get(position).getShopCategoryName());
        Log.d("shopsCategoryList",""+shopCategoryList.get(position).getShopCategoryImage());
        Picasso.with(mContext).load(shopCategoryList.get(position).getShopCategoryImage()).error(R.drawable.picodellogo).into(iv_shop_category);
        holder.iv_shop_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentShopList = new Intent(mContext, ShopListActivity.class);
                intentShopList.putExtra("shopcat",shopCategoryList.get(position).getShopCategoryName());
                mContext.startActivity(intentShopList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return shopCategoryList.size();
    }

    public static class ShopCategoryHolder extends RecyclerView.ViewHolder{
        ImageView iv_shop_category;
        TextView tv_shop_category;
        public ShopCategoryHolder(@NonNull View itemView) {
            super(itemView);
            iv_shop_category = itemView.findViewById(R.id.iv_shop_category);
            tv_shop_category = itemView.findViewById(R.id.tv_shop_category);
        }
    }

}
