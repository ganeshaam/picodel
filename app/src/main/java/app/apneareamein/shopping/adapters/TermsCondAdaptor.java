package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.TermCondModel;

public class TermsCondAdaptor extends RecyclerView.Adapter<TermsCondAdaptor.ViewHolder_Condi>  {

    ArrayList<TermCondModel> arrayList;
    Context mcontext;


    public TermsCondAdaptor(ArrayList<TermCondModel> arrayList, Context mcontext) {
        this.arrayList = arrayList;
        this.mcontext = mcontext;
    }

    @NonNull
    @Override
    public ViewHolder_Condi onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_items_layout,parent,false);
        TermsCondAdaptor.ViewHolder_Condi viewHolder_condi = new TermsCondAdaptor.ViewHolder_Condi(view);
        return viewHolder_condi;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder_Condi holder, int position) {

        holder.tv_title.setText(arrayList.get(position).getTitle());
        holder.radioButton1.setText(arrayList.get(position).getOption1());
        holder.radioButton2.setText(arrayList.get(position).getOption2());

        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //String valuve =
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb ) {
                    Toast.makeText(mcontext, rb.getText(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder_Condi extends RecyclerView.ViewHolder{

        TextView tv_title;
        RadioGroup radioGroup;
        RadioButton radioButton1,radioButton2;

        public ViewHolder_Condi(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);

            radioButton1 = itemView.findViewById(R.id.radioButton1);
            radioButton2 = itemView.findViewById(R.id.radioButton2);
            radioGroup = itemView.findViewById(R.id.radioGroup);

            radioGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*lastSelectionPosition = getAdapterPosition();
                    notifyDataSetChanged();*/
                }
            });
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }
}
