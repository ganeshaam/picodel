package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;



import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.PaymentModeModel;

public class PaymentModeAdaptor extends RecyclerView.Adapter<PaymentModeAdaptor.paymentModeHolder> {

    ArrayList<PaymentModeModel> arrayList;
    Context context;
    private int lastSelectionPosition = -1;
    SharedPreferences sharedPreferences;

    public PaymentModeAdaptor(Context context, ArrayList<PaymentModeModel> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public paymentModeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_payment_mode_items,viewGroup,false);
        PaymentModeAdaptor.paymentModeHolder paymentModeHolder = new PaymentModeAdaptor.paymentModeHolder(view);
        return paymentModeHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final paymentModeHolder paymentModeHolder, final int position) {

        paymentModeHolder.tv_payment_type.setText(arrayList.get(position).getPaymentType() + " " + arrayList.get(position).getPayment_details());
        //paymentModeHolder.rb_selectPaymentMode.setChecked(lastSelectionPosition==position);
            //paymentModeHolder.cb_selectPaymentMode.isChecked();
        if(arrayList.get(position).getPaymentType().equalsIgnoreCase("Cash")){
            paymentModeHolder.cb_selectPaymentMode.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class paymentModeHolder extends RecyclerView.ViewHolder{

        TextView tv_payment_type;
        CardView card_layout;
        CheckBox cb_selectPaymentMode;
        RadioButton rb_selectPaymentMode;

        public paymentModeHolder(@NonNull View itemView) {
            super(itemView);
            tv_payment_type = itemView.findViewById(R.id.tv_payment_type);
            card_layout = itemView.findViewById(R.id.card_layout);
            rb_selectPaymentMode = itemView.findViewById(R.id.rb_selectPaymentMode);
            cb_selectPaymentMode = itemView.findViewById(R.id.cb_selectPaymentMode);

            rb_selectPaymentMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectionPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }
}
