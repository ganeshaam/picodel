package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.SliderModel;

public class SliderShopBannerAdapter  extends RecyclerView.Adapter<SliderShopBannerAdapter.BannerHolder> {

    ArrayList<SliderModel> sliderModelArrayList;
    Context mContext;

    public SliderShopBannerAdapter(ArrayList<SliderModel> arrayList,Context mcontext){
        sliderModelArrayList = arrayList;
        mContext = mcontext;
    }

    @NonNull
    @Override
    public BannerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_banner_slider,parent,false);
        SliderShopBannerAdapter.BannerHolder bannerHolder = new SliderShopBannerAdapter.BannerHolder(view);
        return bannerHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BannerHolder holder, int position) {
        ImageView imageView = holder.rv_img;
        Picasso.with(mContext).load(sliderModelArrayList.get(position).getImgUrl()).error(R.drawable.picodellogo).into(imageView);
    }

    @Override
    public int getItemCount() {
        return sliderModelArrayList.size();
    }

    public class BannerHolder extends RecyclerView.ViewHolder{
        ImageView rv_img;
        public BannerHolder(View itemView){
            super(itemView);
            rv_img = itemView.findViewById(R.id.img_shop_banner);
        }
    }

}