package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.activities.SubProductListActivity;
import app.apneareamein.shopping.model.ProductCodeWiseProduct;
import app.apneareamein.shopping.model.ProductListModel;


public class ProductListAdaptor extends RecyclerView.Adapter<ProductListAdaptor.CarlistHolder>{

    ArrayList<ProductListModel> cartArrayList;
    Context mContext;
    String type;

    public ProductListAdaptor(ArrayList<ProductListModel> cartArrayList, Context mContext) {
        this.cartArrayList = cartArrayList;
        this.mContext = mContext;

    }

    public ProductListAdaptor(ArrayList<ProductListModel> productWiseList, Context mContext,String type) {

        this.cartArrayList = cartArrayList;
        this.mContext = mContext;
        this.type = type;
    }


    @NonNull
    @Override
    public CarlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_search_products,parent,false);
        CarlistHolder carlistHolder = new  CarlistHolder(view);
        return carlistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarlistHolder carlistHolder, int position) {

        TextView points = carlistHolder.tv_points;
        CardView cardView = carlistHolder.cd_view;
        TextView validity = carlistHolder.tv_validity;
        TextView order_id = carlistHolder.tv_order_id;

       // Log.e("points:",""+cartArrayList.get();


        Log.e("new_Adaptorvalue:",""+cartArrayList.get(position).getProduct_name());
       /* points.setText(cartArrayList.get(position).getAmount());
        validity.setText("Expire on:"+cartArrayList.get(position).getValidity());
        String order_id_ch = cartArrayList.get(position).getUser_id();*/

       /* if(order_id_ch!=null || order_id_ch.length()>=2){
            carlistHolder.tv_order_id.setVisibility(View.VISIBLE);
            order_id.setText(cartArrayList.get(position).getOrder_id());//#9E9E9E
        }else {*/

        // }

    }

    @Override
    public int getItemCount() {
        //return cartArrayList.size();
        return cartArrayList.size();
    }

    public static class CarlistHolder extends RecyclerView.ViewHolder {

        CardView cd_view;

        TextView tv_points, tv_validity, tv_order_id;

        public CarlistHolder(@NonNull View itemView) {
            super(itemView);
            tv_points = itemView.findViewById(R.id.tv_points);
            cd_view = itemView.findViewById(R.id.cd_view);
            tv_validity = itemView.findViewById(R.id.tv_validity);
            tv_order_id = itemView.findViewById(R.id.tv_order_id);

        }
      }
    }