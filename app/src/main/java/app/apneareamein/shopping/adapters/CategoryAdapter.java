package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.CategoryModel;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.categoryHolder> {

    ArrayList<CategoryModel> arrayList;
    Context mcontext;

    public CategoryAdapter(ArrayList<CategoryModel> arrayList, Context mcontext) {
        this.arrayList = arrayList;
        this.mcontext = mcontext;
    }

    @NonNull
    @Override
    public categoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_category_items,parent,false);
        CategoryAdapter.categoryHolder viewHolder = new CategoryAdapter.categoryHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull categoryHolder holder, int position) {

        TextView tv_description = holder.tv_c_description;
        RadioButton category = holder.rb_category_type;

        category.setText(arrayList.get(position).getMaincategory());
        tv_description.setText(arrayList.get(position).getDescription());
        if(arrayList.get(position).getMaincategory().equalsIgnoreCase("Fruits and Veggies")){
            category.setTextColor(Color.parseColor("#2E7D32"));

        }else  if(arrayList.get(position).getMaincategory().equalsIgnoreCase("Grocery")){
            category.setTextColor(Color.parseColor("#F57F17"));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class categoryHolder extends RecyclerView.ViewHolder{

        RadioButton rb_category_type;
        TextView tv_c_description;

        public categoryHolder(@NonNull View itemView) {
            super(itemView);

            rb_category_type = itemView.findViewById(R.id.rb_category_type);
            tv_c_description = itemView.findViewById(R.id.tv_c_description);
        }
    }
}
