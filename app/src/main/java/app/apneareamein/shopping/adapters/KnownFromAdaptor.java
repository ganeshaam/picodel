package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.KnownFrom;
import app.apneareamein.shopping.model.ReferPoints;

public class KnownFromAdaptor extends RecyclerView.Adapter<KnownFromAdaptor.CarlistHolder>{

    ArrayList<ReferPoints> cartArrayList;
    Context mContext;
    private int lastSelectionPosition = -1;
    String type;

    public KnownFromAdaptor(ArrayList<ReferPoints> cartArrayList, Context mContext) {
        this.cartArrayList = cartArrayList;
        this.mContext = mContext;

    }

    @NonNull
    @Override
    public CarlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_know_from_items,parent,false);
        CarlistHolder carlistHolder = new  CarlistHolder(view);
        return carlistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CarlistHolder carlistHolder, int position) {


        RadioButton radioButton = carlistHolder.rb_known_from;

        Log.e("masg:",""+cartArrayList.get(position).getAmount());
        radioButton.setText(cartArrayList.get(position).getAmount());

        carlistHolder.rb_known_from.setChecked(lastSelectionPosition==position);


    }

    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }

    public class CarlistHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_not_avail;
        CardView cd_view;
        RadioButton rb_known_from;

        public CarlistHolder(@NonNull View itemView) {
            super(itemView);
            rb_known_from = itemView.findViewById(R.id.rb_known_from);

            rb_known_from.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectionPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });

        }
    }
}

