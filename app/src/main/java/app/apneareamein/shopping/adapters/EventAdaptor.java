package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.zip.Inflater;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.EventModel;

public class EventAdaptor extends RecyclerView.Adapter<EventAdaptor.EventHolder> {

    Context mContext;
    ArrayList<EventModel> modelArrayList;

    public EventAdaptor(Context mContext, ArrayList<EventModel> modelArrayList) {
        this.mContext = mContext;
        this.modelArrayList = modelArrayList;
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_event_items,parent,false);
        EventAdaptor.EventHolder eventHolder = new EventAdaptor.EventHolder(view);
        return eventHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, int position) {

        TextView txtquestion = holder.tv_question;
        RadioButton option1 = holder.rv_option1;
        RadioButton option2 = holder.rv_option2;
        RadioButton option3 = holder.rv_option3;
        RadioButton option4 = holder.rv_option4;

        txtquestion.setText(modelArrayList.get(position).getQuestion());
        option1.setText(modelArrayList.get(position).getOption1());
        option2.setText(modelArrayList.get(position).getOption2());
        option3.setText(modelArrayList.get(position).getOption3());
        option4.setText(modelArrayList.get(position).getOption4());

        //holder.tv_question.setText(modelArrayList.get(position).getQuestion());

        Log.e("question:",""+modelArrayList.get(position).getQuestion());
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }


    public class EventHolder extends RecyclerView.ViewHolder{

        TextView tv_question;
        RadioButton rv_option1,rv_option2,rv_option3,rv_option4;

        public EventHolder(@NonNull View itemView) {
            super(itemView);

            tv_question = (itemView).findViewById(R.id.tv_question);
            rv_option1 = (itemView).findViewById(R.id.rv_option1);
            rv_option2 = (itemView).findViewById(R.id.rv_option2);
            rv_option3 = (itemView).findViewById(R.id.rv_option3);
            rv_option4 = (itemView).findViewById(R.id.rv_option4);

        }
    }


}
