package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.ReferPoints;

public class ReferralPointsAdaptor extends RecyclerView.Adapter<ReferralPointsAdaptor.CarlistHolder>{

    ArrayList<ReferPoints> cartArrayList;
    Context mContext;
    String type;

    public ReferralPointsAdaptor(ArrayList<ReferPoints> cartArrayList, Context mContext) {
        this.cartArrayList = cartArrayList;
        this.mContext = mContext;

    }

    @NonNull
    @Override
    public CarlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_refer_points,parent,false);
        CarlistHolder carlistHolder = new  CarlistHolder(view);
        return carlistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarlistHolder carlistHolder, int position) {

        TextView points = carlistHolder.tv_points;
        CardView cardView = carlistHolder.cd_view;
        TextView validity = carlistHolder.tv_validity;
        TextView order_id = carlistHolder.tv_order_id;

        Log.e("points:",""+cartArrayList.get(position).getAmount());
        points.setText(cartArrayList.get(position).getAmount());
        validity.setText("Expire on:"+cartArrayList.get(position).getValidity());
        String order_id_ch = cartArrayList.get(position).getUser_id();

       /* if(order_id_ch!=null || order_id_ch.length()>=2){
            carlistHolder.tv_order_id.setVisibility(View.VISIBLE);
            order_id.setText(cartArrayList.get(position).getOrder_id());//#9E9E9E
        }else {*/
            carlistHolder.tv_order_id.setVisibility(View.GONE);
            if (cartArrayList.get(position).getBenificial_type().equalsIgnoreCase("benificial")) {
                cardView.setCardBackgroundColor(Color.parseColor("#8ab935"));//benificial
            } else {
                cardView.setCardBackgroundColor(Color.parseColor("#ee1d23"));//benificial
            }
       // }

    }

    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }

    public static class CarlistHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_not_avail;
        CardView cd_view;

        TextView tv_points,tv_validity,tv_order_id;
        public CarlistHolder(@NonNull View itemView) {
            super(itemView);
            tv_points = itemView.findViewById(R.id.tv_points);
            cd_view = itemView.findViewById(R.id.cd_view);
            tv_validity = itemView.findViewById(R.id.tv_validity);
            tv_order_id = itemView.findViewById(R.id.tv_order_id);

        }
    }
}

