package app.apneareamein.shopping.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.apneareamein.shopping.R;
import app.apneareamein.shopping.model.StateCityModel;

public class StateCityAdaptor extends  RecyclerView.Adapter<StateCityAdaptor.stateCityHolder>{

    Context context;
    ArrayList<StateCityModel> arrayList;

    public StateCityAdaptor(Context context, ArrayList<StateCityModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public stateCityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_citylist,parent,false);
        StateCityAdaptor.stateCityHolder cityHolder = new StateCityAdaptor.stateCityHolder(view);
        return cityHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull stateCityHolder holder, int position) {

        TextView tx_cityname = holder.tx_city;

         tx_cityname.setText(arrayList.get(position).getCity()+", "+arrayList.get(position).getState());
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public  static  class  stateCityHolder extends RecyclerView.ViewHolder{

        TextView tx_city;

        public stateCityHolder(@NonNull View itemView) {
            super(itemView);
            tx_city = itemView.findViewById(R.id.tx_city);
        }
    }
}
