package app.apneareamein.shopping.model;

public class InnerMovie {

        private String product_subCat;

        private String product_subCatImage;

    public InnerMovie() {
    }

    public InnerMovie(String product_subCat, String product_subCatImage) {
            this.product_subCat = product_subCat;
            this.product_subCatImage = product_subCatImage;
        }

        public String getProduct_subCat() {
            return product_subCat;
        }

        public String getProduct_subCatImage() {
                return product_subCatImage;
            }
        public void setProduct_subCat(String product_subCat) {
            this.product_subCat = product_subCat;
        }

        public void setProduct_subCatImage(String product_subCatImage) {
            this.product_subCatImage = product_subCatImage;
        }

}
