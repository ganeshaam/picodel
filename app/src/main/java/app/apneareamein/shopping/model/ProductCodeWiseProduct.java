package app.apneareamein.shopping.model;

public class ProductCodeWiseProduct {

    public String code;
    public String shop_id;
    public String shop_name;
    public String shop_category;
    public String product_name;
    public String product_brand;
    public String product_id;
    public String product_image;
    public String product_image1;
    public String product_image2;
    public String product_image3;
    public String product_image4;
    public String product_size;
    public String product_mrp;
    public String product_price;
    public String product_description;
    public String similar_product_status;
    public String select_type;
    public String strAvailable_Qty;
    public String strHindiName;
    public String product_discount;
    public String cart_pstatus;
    public boolean status = false;

    public ProductCodeWiseProduct(String code, String shop_id, String shop_name, String shop_category,
                                  String product_name, String product_brand, String product_id, String product_image,
                                  String product_image1, String product_image2, String product_image3,
                                  String product_image4, String product_size, String product_mrp, String product_price,
                                  String product_discount, String product_description, String similar_product_status, String type,
                                  String available_qty, String hindiname , String cart_pstatus ) {
        this.code = code;
        this.shop_id = shop_id;
        this.shop_name = shop_name;
        this.shop_category = shop_category;
        this.product_name = product_name;
        this.product_brand = product_brand;
        this.product_id = product_id;
        this.product_image = product_image;
        this.product_image1 = product_image1;
        this.product_image2 = product_image2;
        this.product_image3 = product_image3;
        this.product_image4 = product_image4;
        this.product_size = product_size;
        this.product_mrp = product_mrp;
        this.product_price = product_price;
        this.product_discount = product_discount;
        this.product_description = product_description;
        this.similar_product_status = similar_product_status;
        this.select_type = type;
        this.strAvailable_Qty = available_qty;
        this.strHindiName = hindiname;
        this.cart_pstatus = cart_pstatus;
    }

    public String getStrAvailable_Qty() {
        return strAvailable_Qty;
    }

    public String getStrHindiName() {
        return strHindiName;
    }

    public String getCode() {
        return code;
    }

    public String getShop_id() {
        return shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_brand() {
        return product_brand;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getProduct_image() {
        return product_image;
    }

    public String getProduct_size() {
        return product_size;
    }

    public String getProduct_mrp() {
        return product_mrp;
    }

    public String getProduct_price() {
        return product_price;
    }

    public String getProduct_discount() {
        return product_discount;
    }

    public String getSelect_type() {
        return select_type;
    }

    public boolean isStatus(boolean check) {
        return status;
    }

    public String getSimilar_product_status() {
        return similar_product_status;
    }

    public String setProduct_discount(String product_discount) {
        this.product_discount = product_discount;
        return product_discount;
    }

    public boolean setStatus(boolean status) {
        this.status = status;
        return status;
    }

    public String getCart_pstatus() {
        return cart_pstatus;
    }

    public void setCart_pstatus(String cart_pstatus) {
        this.cart_pstatus = cart_pstatus;
    }
}
