package app.apneareamein.shopping.model;

public class CategoryModel {
    String maincategory,product_cat_image,description;

    public String getMaincategory() {
        return maincategory;
    }

    public void setMaincategory(String maincategory) {
        this.maincategory = maincategory;
    }

    public String getProduct_cat_image() {
        return product_cat_image;
    }

    public void setProduct_cat_image(String product_cat_image) {
        this.product_cat_image = product_cat_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
