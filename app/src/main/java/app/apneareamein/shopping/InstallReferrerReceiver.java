package app.apneareamein.shopping;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

public class InstallReferrerReceiver extends BroadcastReceiver {

    public static final String MY_PREFS_NAME = "PICoDEL";


    @Override
    public void onReceive(Context context, Intent intent) {

        String referrerId = intent.getStringExtra("referrer");//referrer
        Log.e("panderianthb:",""+referrerId);

        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("referrerCode", referrerId);
        editor.apply();
        editor.commit();

    }
}
