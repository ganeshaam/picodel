package app.apneareamein.shopping.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {

    private Context _context;
    SharedPreferences prefs;

    public SharedPreferencesUtils(Context _context) {
        this._context = _context;
        try {
            prefs = this._context.getSharedPreferences("application", Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCategory(String title) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("title", title);
        editor.commit();
    }

    public String getCategory() {
        if (prefs.contains("title"))
            return prefs.getString("title", "");
        else
            return "";
    }

    public void setSearchText(String title) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("searchText", title);
        editor.commit();
    }

    public String getSearchText() {
        if (prefs.contains("title"))
            return prefs.getString("searchText", "");
        else
            return "";
    }

    public void ClearText(){
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
    }

    public  void setTermscond(String terms){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("user_terms",terms);
        editor.commit();
    }

    public String getTermscond(String terms) {
        if (prefs.contains("user_terms")) {
            return prefs.getString("user_terms", "NA");
        } else {
            return "NA";
        }


    }
}
