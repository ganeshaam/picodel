package app.apneareamein.shopping.utils;


public interface LoopingPagerAdapter {
    int getRealCount();
}